<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SportsMeta extends Model
{
    //
    protected $table = "sports_meta";
    protected $guarded = [];
    public $timestamps = false;
    protected $fillable = ['id', 'sport_id', 'key', 'field_type', 'display_name', 'details', 'type', 'order'];
}
