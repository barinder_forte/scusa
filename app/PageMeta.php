<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageMeta extends Model
{
    protected $table = 'page_meta';
    protected $guarded = [];
    public $timestamps = false;
    protected $fillable = ['id', 'page_id', 'key', 'display_name', 'value', 'details', 'type', 'order'];
}
