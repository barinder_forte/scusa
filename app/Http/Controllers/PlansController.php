<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use App\Subscription;
use Auth;
use Braintree_Subscription;
class PlansController extends Controller
{
    public function index()
    {
        return view('plans.index')->with(['plans' => Plan::get()]);
    }
    
    public function show(Request $request, Plan $plan)
    {
        if ( $request->user()->subscribedToPlan($plan->braintree_plan, 'main') && !session('error') ) {
            return redirect('/plan/'.$plan->slug)->with('error', 'Unauthorised operation');
        }
    
        return view('plans.show')->with(['plan' => $plan]);
    }
    
     public function paymentList(Request $request = null)
    {
        $data = array();
        // $collection = \Braintree_Subscription::search([
        //     \Braintree_SubscriptionSearch::planId()->is("athlete")
        //     \Braintree_SubscriptionSearch::status()->in([Braintree_Subscription::])
        //     ]);
        
        if($request->status){
        $collection = Braintree_Subscription::search([
            \Braintree_SubscriptionSearch::planId()->is("athlete"),
            \Braintree_SubscriptionSearch::status()->in(
                [$request->status]
            )
            ]);
        // $collection->maximumCount('10');
        # e.g. 33
        
        $data['statusSelected'] = $request->status;

        }else{
         $collection = \Braintree_Subscription::search([
            \Braintree_SubscriptionSearch::planId()->is("athlete")
            ]);
        // $collection->maximumCount('10');
        # e.g. 33
        }
            //   echo "<pre>";  print_r($collection);die;

        $data['collections'] = $collection;
        //   $transaction = \Braintree_Subscription::find('7wtdt2');
        //  echo "<pre>";
        // $token = $transaction->transactions[0]->creditCard['token'];
        // print_r($transaction);die;
        // if($token){
        // $result = \Braintree\CreditCard::update($token, [
        //     'cardholderName' => 'Forte test',
        //     'number' => '4242424242424242',
        //     'expirationMonth' => '11',
        //     'expirationYear' => '2023',
        //     'options' => [
        //         'verifyCard' => true
        //     ]
        // ]);
        // print_r($result);die;
        // }
        return view('payment_history',$data);
    }

     public function paymentListathlete()
    {
        if(Auth::user()->role_id == 2 || Auth::user()->role_id == 3){
            $data = array();
            $data['users'] = \App\User::where('id',Auth::user()->id)->first();
           
              $data['invoices'] =  \DB::table('plans as p')
                ->join('subscriptions as sc', 'sc.braintree_plan', '=', 'p.braintree_plan')
                ->where('sc.user_id',Auth::user()->id)
                ->orderBy('sc.created_at','DESC')
                ->get(); 
            // echo "<pre>";
            // print_r($data['invoices']);die;
            return view('payment_history_athlete',$data);
        }else{
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    
     public function updateCardDetails($creditCardToken){
        $data = array();
        $data['creditCardToken'] = $creditCardToken;
        // print_r($data);die;
        return view('update_card',$data);
    }
 public function updateCardDetailsSave(Request $request){
        // print_r($request->cardtoken);die;
        if($request->cardtoken){
        $result = \Braintree\CreditCard::update($request->cardtoken, [
            // 'cardholderName' => 'Forte test',
            'number' => $request->token['number'],
            'expirationMonth' => $request->token['exp_month'],
            'expirationYear' => $request->token['exp_year'],
            'options' => [
                'verifyCard' => true
            ]
        ]);
        // echo "<pre>";
        // print_r($result->success);die;
        // $user = \Auth::user();
        // $user->card_last_four =  substr($request->token['number'], 12) ;
        // $user->save();
            if($result->success == 1){
                return 'yes';
            }else{
                return 'no';
            }
        }
    }
}