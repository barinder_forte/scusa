<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SportsMeta;
use App\Sport;
use Voyager;
use Illuminate\Support\Facades\Storage;

class sportsController extends Controller
{
    public function store(Request $request)
    {
        if( isset( $request->field ) ){
            // echo "<pre>";
            // echo $request->key;
            $sport_data = SportsMeta::where('id', '=', $request->field )->first();
            $sport_data->display_name = $request->display_name;
            $sport_data->details = $request->details;
            // echo $request->key; die;
            $sport_data->key = str_replace( ' ', '_', strtolower( $sport_data->key ) );
            $sport_data->field_type = $request->field_type;
            // print_r( $sport_data ); die;
            $sport_data->save();
            return redirect('admin/sports/'.$request->sport_id.'/edit')->with([
                'message'    => 'Settings Successfully Updated',
                'alert-type' => 'success',
            ]);
        }else{
            // die('b');
            $lastSetting = SportsMeta::orderBy('order', 'DESC')->first();
            if (is_null($lastSetting)) {
                $order = 0;
            } else {
                $order = intval($lastSetting->order) + 1;
            }
            $request->merge(['order' => $order]);
            $request->merge(['value' => '']);
            $request->merge(['key' => str_replace( ' ', '_', strtolower( $request->key ) )]);
            SportsMeta::create($request->all());
            return redirect('admin/sports/'.$request->sport_id.'/edit')->with([
                'message'    => 'Successfully Created Settings',
                'alert-type' => 'success',
            ]);
        }
    }
    
    public function edit_field(Request $request, $id, $field)
    {
        $gallery_images_array = $request->gallery_images;
        $request->merge(['gallery_images' => '']);
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);
        
        $relationships = $this->getRelationships($dataType);
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        if( $slug == 'pages' ){
            $settings = PageMeta::orderBy('order', 'ASC')->where('page_id', '=', $id)->get();
        }elseif( $slug == 'sports' ){
            $settings = SportsMeta::orderBy('order', 'ASC')->where('sport_id', '=', $id)->get();
        }elseif( $slug == 'sportevents' ){
            $settings = SportEvent::where('sport_id', '=', $id)->get();
        }else{
            $settings = [];
        }
        return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'settings'));
    }
    
    
    
    public function move_up($id){
        $setting = SportsMeta::find($id);
        $swapOrder = $setting->order;
        $previousSetting = SportsMeta::where([ ['order', '<', $swapOrder],['sport_id', '=', $setting->sport_id] ])->orderBy('order', 'DESC')->first();
        $data = [
            'message'    => 'This is already at the top of the list',
            'alert-type' => 'error',
        ];

        if (isset($previousSetting->order)) {
            $setting->order = $previousSetting->order;
            $setting->save();
            $previousSetting->order = $swapOrder;
            $previousSetting->save();

            $data = [
                'message'    => "Moved {$setting->display_name} setting order up",
                'alert-type' => 'success',
            ];
        }
        return back()->with($data);
    }
    
    public function move_down($id)
    {
        $setting = SportsMeta::find($id);
        $swapOrder = $setting->order;

        $previousSetting = SportsMeta::where([ ['order', '>', $swapOrder], ['sport_id', '=', $setting->sport_id] ])->orderBy('order', 'ASC')->first();
        $data = [
            'message'    => 'This is already at the bottom of the list',
            'alert-type' => 'error',
        ];

        if (isset($previousSetting->order)) {
            $setting->order = $previousSetting->order;
            $setting->save();
            $previousSetting->order = $swapOrder;
            $previousSetting->save();

            $data = [
                'message'    => "Moved {$setting->display_name} setting order down",
                'alert-type' => 'success',
            ];
        }

        return back()->with($data);
    }

    public function sportposition(Request $request)
    {
        if( $request->id == '' || !isset($request->id) || $request->id == 'null' || $request->id == 'undefined' ){
            return '';
        }else{
            $sportid = $request->id;    
        }
        $previousSetting = SportsMeta::where('sport_id', '=', $sportid)->where('key','=','position')->first();
        if(isset($previousSetting)){
            $data = (array)json_decode($previousSetting->details);
            if(isset($data['options'])){
                $data = json_encode((array)$data['options']);
                return $data;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    public function get_fields( Request $request ){
        $inputs[ 'fields' ] = SportsMeta::where( 'sport_id', '=', $request->id )->where('field_type', '=', 'field')->orderBy('order', 'asc')->get()->toArray();
        $inputs[ 'stats' ] = SportsMeta::where( 'sport_id', '=', $request->id )->where('field_type', '=', 'stats')->orderBy('order', 'asc')->get()->toArray();
        return $inputs;
    }

    public function delete_column($id, $column)
    {
        // Check permission
        Voyager::canOrFail('browse_settings');
        // echo "<pre>";
        $setting = Sport::find($id);
        if (isset($setting->id)) {
            // print_r($setting->$column); die;
            // If the type is an image... Then delete it
            // if ($setting->type == 'image') {
                if (Storage::disk(config('voyager.storage.disk'))->exists($setting->$column)) {
                    Storage::disk(config('voyager.storage.disk'))->delete($setting->$column);
                }
            // }
            $setting->$column = '';
            $setting->save();
        }

        return back()->with([
            'message'    => "Successfully removed {$column} value",
            'alert-type' => 'success',
        ]);
    }
    
}