<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages;
use Illuminate\Support\Facades\Validator;
use App\Post;
use Session;
use Voyager;
use Mail;
use Illuminate\Support\Facades\Storage;
use App\Mail\contactUs;

class PageController extends Controller
{
    public function __construct(){
        
    }
    
    public function index( $slug = null )
    {
        $page = Pages::where('slug', '=', $slug)->first();
        if( $page ){
            return view('page', compact('page'));
        }else{
            $slug_array = explode('/', $slug);
            $new_slug = end( $slug_array );
            // echo $new_slug; die;
            $page = Pages::where('slug', '=', $new_slug )->first();
            if( $page ){
                return view('page', compact('page'));
            }
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }

    public function contact_show(){
        $userData = array();

        $userData['page'] = Pages::where('slug', '=', 'contact')->first();
            $userData['page_id'] = $userData['page']->id;
        return view('contact',$userData);

    }

    public function contact_submit(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'email' =>'required|email',
           'g-recaptcha-response'=>'required',

        ],['g-recaptcha-response.required' => 'captcha is required.']);

        if ($validator->fails()) {
            return redirect('/contact')
                        ->withErrors($validator)
                        ->withInput();
        }
         $userData = array();

        $userData['page'] = Pages::where('slug', '=', 'contact')->first();
        $userData['page_id'] = $userData['page']->id;
        $admin_email = Voyager::pagemeta('contact_us_email', '20');
        $admin_emails2 = explode(',', $admin_email);
        Mail::to( $admin_emails2[0] )->send(new contactUs($request->all() , $admin_emails2));
        
        Session::flash('message', 'Message Sent Successfully!');  
        return redirect()->back();
    }

    public function deletebanner($page_id){
        
        $pageData= Pages::where('id', '=', $page_id)->first();
        if($pageData){
            $pageData->image = '';
            $pageData->save();
            return 'Deleted' ;
        }else{
            return 'Not Deleted' ;
        }

    }

    public function delete_column($id, $column)
    {
        // Check permission
        Voyager::canOrFail('browse_settings');
        // echo "<pre>";
        $setting = Pages::find($id);
        if (isset($setting->id)) {
            // print_r($setting->$column); die;
            // If the type is an image... Then delete it
            // if ($setting->type == 'image') {
                if (Storage::disk(config('voyager.storage.disk'))->exists($setting->$column)) {
                    Storage::disk(config('voyager.storage.disk'))->delete($setting->$column);
                }
            // }
            $setting->$column = '';
            $setting->save();
        }

        return back()->with([
            'message'    => "Successfully removed {$column} value",
            'alert-type' => 'success',
        ]);
    }



}
