<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
class MenuController extends Controller
{
    public static function display( $menu_name, $view_name ){
        $items = Menu::get()->where('name', '=', $menu_name)->first()->items;
        return view('layouts.'.$view_name, compact('items') );
    }
}
