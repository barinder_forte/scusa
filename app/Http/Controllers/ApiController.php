<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiData;
use Voyager;
use \App\UserInformation;
use Auth;
use App\User;
use App\Post;
use App\SportEvent;
use App\CoachsSchool;
use App\Mail\AccountCreated;
use Mail;
use App\Mail\SubscriptionCancelled;

use Illuminate\Queue\SerializesModels;

class ApiController extends Controller
{
    public function __construct(){
        
   }
     public function schollposition(Request $request){
        $scholllist = ApiData::where('id',$request->id)->first();
        if(strlen($scholllist->zip) == 4){

            $scholllist->zip = '0'.$scholllist->zip;

        }
        if($scholllist->member_naa == 'Yes'){

            $scholllist->member_naa = 'True';

        }else{
          $scholllist->member_naa = 'False';

        }
        return $scholllist;
    }



    public function fetchApiData( )
    {
      
      $saveedApikey = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','school_api_key')->pluck('meta_value')->first();
      if($saveedApikey){

      $result = @file_get_contents("https://api.data.gov/ed/collegescorecard/v1/schools?api_key=".$saveedApikey."&fields=school.name,location.lon,ope8_id,ope6_id,location.lat,id,school.city,school.state,school.instructional_expenditure_per_fte,school.operating,school.zip,school.faculty_salary,school.school_url,school.tuition_revenue_per_fte,school.branches,school.accreditor,school.alias,school.main_campus,school.price_calculator_url,school.ft_faculty_rate,school.locale,school.state_fips,school.carnegie_undergrad,school.women_only,school.religious_affiliation,school.under_investigation,school.region_id,school.carnegie_basic,school.men_only,school.online_only,school.ownership,school.carnegie_size_setting&page=0&per_page=1");
      // // Will dump a beauty json :3
      if($result === FALSE){
          return Redirect()->back()->with(['message'    => "Your Api Key Is Not Valid To Fetch Data",'alert-type' => 'error',]);  
      }
      $resultdata =  json_decode($result, true);

        $resultdataArray = $resultdata['results'];
        $totalResults = $resultdata['metadata']['total'];
        $perPageResults = $resultdata['metadata']['per_page'];
        $totalPage = number_format(($totalResults / 1) , 0);
        $totalPage = str_replace(',', '', $totalPage);
     // print_r($totalPage);
     // die;
    
      for($i = 0;$i <= $totalPage ; $i++){

      $result2 = @file_get_contents("https://api.data.gov/ed/collegescorecard/v1/schools?api_key=".$saveedApikey."&page=".$i."&per_page=1");
      if($result2 === FALSE){
      }else{
     
      $resultdata2 =  json_decode($result2, true);
        $resultdataArray2 = $resultdata2['results'];
         
          foreach($resultdataArray2 as $result1){
                $addData = ApiData::where('school_id',$result1['id'] )->first();
                if(!$addData){
                  $addData = new ApiData(); 
                }
                 $arrayq = array_keys($result1);
                  $array = array_filter($arrayq, 'is_numeric');
                $max = max($array);
                $addDatatuition_in = '';
                $addDatatution_out = '';
              if(isset($max)){
                $addDatatuition_in = $result1[$max]['cost']['tuition']['in_state'];
                $addDatatuition_out = $result1[$max]['cost']['tuition']['out_of_state'];
              }
              // print_r($addDatatution_out);die;
                $slug =  strtolower(str_replace(" ","-",$result1['school']['name']));
                  $schoolslug = ApiData::where('school_name', $result1['school']['name'])->get()->count();
                  if($schoolslug >= 1){
                    $slug = $slug.$schoolslug;
                  }
                $addData->school_name = $result1['school']['name'];
                $addData->school_id = $result1['id'];
                $addData->location_lon = $result1['location']['lon'];
                $addData->location_lat = $result1['location']['lat'];
                $addData->ope8_id = $result1['ope8_id'];
                $addData->ope6_id = $result1['ope6_id'];
                $addData->main_campus = $result1['school']['main_campus'];
                $addData->accreditor = $result1['school']['accreditor'];
                $addData->city = $result1['school']['city'];
                $addData->branches = $result1['school']['branches'];
                $addData->zip = $result1['school']['zip'];
                $addData->school_url = $result1['school']['school_url'];
                $addData->state = $result1['school']['state'];
                $addData->operating = $result1['school']['operating'];
                $addData->instructional_expenditure_per_fte = $result1['school']['instructional_expenditure_per_fte'];
                $addData->faculty_salary = $result1['school']['faculty_salary'];
                $addData->tuition_revenue_per_fte = $result1['school']['tuition_revenue_per_fte'];
                $addData->alias = $result1['school']['alias'];
                $addData->price_calculator_url = $result1['school']['price_calculator_url'];
                $addData->ft_faculty_rate = $result1['school']['ft_faculty_rate'];
                $addData->locale = $result1['school']['locale'];
                $addData->state_fips = $result1['school']['state_fips'];
                $addData->carnegie_undergrad = $result1['school']['carnegie_undergrad'];
                $addData->women_only = $result1['school']['women_only'];
                $addData->religious_affiliation = $result1['school']['religious_affiliation'];
                $addData->under_investigation = $result1['school']['under_investigation'];
                $addData->region_id = $result1['school']['region_id'];
                $addData->carnegie_basic = $result1['school']['carnegie_basic'];
                $addData->men_only = $result1['school']['men_only'];
                $addData->online_only = $result1['school']['online_only'];
                $addData->ownership = $result1['school']['ownership'];
                $addData->carnegie_size_setting = $result1['school']['carnegie_size_setting'];
                $addData->school_slug = $slug;
                $addData->tuition_in_state = $addDatatuition_in;
                $addData->tuition_out_of_state = $addDatatuition_out;
                
                
                $addData->save();       
          }
        }
            // return Redirect()->back()->with(['message'    => 'School Api Data Fetched Successfully','alert-type' => 'success',]); 
        
       }               
           return Redirect()->back()->with(['message'    => 'School Api Data Fetched Successfully','alert-type' => 'success',]); 
         
       }else{
          return Redirect()->back()->with(['message'    => "You don't Have Api Key To Fetch Data",'alert-type' => 'error',]); 
       }
    }
    public function school_search(Request $request){
      if(Auth::User()->role_id != 4){
      $data = array();
      // GET THE DataType based on the slug
      $data['dataType'] = \Voyager::model('DataType')->where('slug', '=', 'schoolslist')->first();

      $data['dataTypeContent'] = ApiData::where('school_id','like',"%".$request->school_search."%")->orWhere('school_name','like',"%".$request->school_search."%")->orWhere('city','like',"%".$request->school_search."%")->orWhere('state','like',"%".$request->school_search."%")->orWhere('zip','like',"%".$request->school_search."%")->orWhere('school_url','like',"%".$request->school_search."%")->orderBy('school_name',"ASC")->paginate();

       $model = false;
         $saveedApikey = \App\UserInformation::where('user_id',Auth::User()->id)->where('meta_key','school_api_key')->pluck('meta_value')->first();
        $data['saveedApikey'] = $saveedApikey;
        $data['isModelTranslatable'] = is_bread_translatable($model);

        if (view()->exists("voyager::schoolslist.browse")) {
            $view = "voyager::schoolslist.browse";
        }
         
          $data['school_search'] = $request->school_search;

      return view( $view , $data);
      }else{
        $post = Post::paginate(4);
            $userData['posts'] = $post;
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
      }

    }
  public function usersearch(Request $request){

    $data = array();

    $data['dataType'] = \Voyager::model('DataType')->where('slug', '=', 'users')->first();
     $users = User::orderBy('created_at','DESC');
    if($request->user_search != ''){
        $users->where('name','like',"%".$request->user_search."%")
        ->orWhere('email','like',"%".$request->user_search."%");
    }
    
    if($request->user_role !=''){
      if($request->user_role == 'both'){
        $users->whereIn('role_id',[2,3]);
      }else{
        $users->where('role_id',$request->user_role);
      }
    }
    if($request->user_approved !=''){
      $users->where('user_approved',$request->user_approved);
    }

    $resultArray = $users->get()->pluck('id')->toArray();

    if($request->paidUser != ''){ 
            $usersInfo8 = User::whereIn('role_id',[2,3])->get();
            foreach ($usersInfo8 as $key => $value) {
                $user = \App\User::find($value->id);
                if( $request->paidUser == 'admin' ){
                    if( ( !$user->subscribed('main') ) && $value->is_paid == 'yes'){
                        $usersInfoArray8['paidByAdmin'][] = $value->id; 
                    }else{
                    }
                }else{
                }
                if( $request->paidUser == 'yes' ){
                    if( $user->subscribed('main') ){
                        $usersInfoArray8['paid'][] = $value->id; 
                    }else{
                    }
                }else{
                }
                if( $request->paidUser == 'no' ){
                    if( ( !$user->subscribed('main') ) && $request->paidUser == $value->is_paid ){
                        $usersInfoArray8['nonpaid'][] = $value->id; 
                    }else{
                    }
                }else{
                }
            }
            if( $request->paidUser == 'yes' ){
                $usersInfoArray9 = $usersInfoArray8['paid']; 
            }else if($request->paidUser == 'admin'){
                $usersInfoArray9 = $usersInfoArray8['paidByAdmin']; 
            }else{
                $usersInfoArray9 = $usersInfoArray8['nonpaid']; 
            }
            $resultArray3 = array_intersect($usersInfoArray9,$resultArray); 
            $users = User::whereIn('id',$resultArray3);
        } 
    if($request->userSports != ''){
      $getSport = UserInformation::where('meta_key','sport')->where('meta_value',$request->userSports)->get()->pluck('user_id')->toArray();
      $users->whereIn('id',$getSport);
      // ->whereIn('role_id',[2,3]);
    }    
    
   

     $data['dataTypeContent'] = $users->paginate(10);
     // print_r(count($users->get() ) );die;
      $model = false;
    $data['isModelTranslatable'] = is_bread_translatable($model);
    if(view()->exists("voyager::users.browse")){
     $view = "voyager::users.browse";
    }
    $data['users_search'] = $request->user_search;

    return view( $view , $data);
  }

  
  public function schoolresultsearchevent(Request $request){

    $schooldata = ApiData::where('school_name','LIKE','%'.$request->schoolname.'%')->orderBy('school_name','ASC')->get();
    $schoollistarray = array();
    if($schooldata){
      foreach($schooldata as $key=>$s){
        $cityState = array();
        $cityState[0] = $s->school_name;
        $cityState[1] = $s->city;
        $city = implode(' - ',array_filter($cityState));
        $cityStateNew = array();
        $cityStateNew[0] = $city;
        $cityStateNew[1] = $s->state;
        $cityStatefinal = implode(', ',array_filter($cityStateNew));
        $schoollistarray[$key]['id'] = $s->id;
        $schoollistarray[$key]['school_name'] = $cityStatefinal;

      }

     return $schoollistarrayjson = json_encode($schoollistarray);

    }else{
      return '';
    }
  }

  public function schoolresultsearcheventschool(Request $request){

    $schooldata = ApiData::where('school_name','LIKE','%'.$request->schoolname.'%')->orderBy('school_name','ASC')->get();
    $schoollistarray = array();
    if($schooldata){
      foreach($schooldata as $key=>$s){
        $cityState = array();
        $cityState[0] = $s->school_name;
        $cityState[1] = $s->city;
        $city = implode(' - ',array_filter($cityState));
        $cityStateNew = array();
        $cityStateNew[0] = $city;
        $cityStateNew[1] = $s->state;
        $cityStatefinal = implode(', ',array_filter($cityStateNew));
        $schoollistarray[$key]['id'] = $s->id;
        $schoollistarray[$key]['school_name'] = $cityStatefinal;
        $schoollistarray[$key]['school_name1'] = $s->school_name;
          $schoollistarray[$key]['city'] = $s->city;
        $schoollistarray[$key]['state'] = $s->state;
      }

     return $schoollistarrayjson = json_encode($schoollistarray);

    }else{
      return '';
    }
  }

  public function schoolresultsearch(Request $request){

    $schooldata = ApiData::where('school_name','LIKE','%'.$request->schoolname.'%')->orderBy('school_name','ASC')->get();
    $schoollistarray = array();
    if($schooldata){
     foreach($schooldata as $key=>$s){
        $cityState = array();
        $cityState[0] = $s->school_name;
        $cityState[1] = $s->city;
        $city = implode(' - ',array_filter($cityState));
        $cityStateNew = array();
        $cityStateNew[0] = $city;
        $cityStateNew[1] = $s->state;
        $cityStatefinal = implode(', ',array_filter($cityStateNew));
        $schoollistarray[$key]['id'] = $s->id;
        $schoollistarray[$key]['school_name'] = $cityStatefinal;
      }

     return $schoollistarrayjson = json_encode($schoollistarray);

    }else{
      return '';
    }
  }
    public function userapprove(Request $request){

      $datauser = explode(',',$request->multiselect);
      // print_r(array_filter($datauser));die;
      if( $request->user_approved == 'Select Action'){
        return Redirect()->back()->with(['message'    => 'Please Select Action.','alert-type' => 'error',]); 
      }
      if(empty(array_filter($datauser)) ){
        return Redirect()->back()->with(['message'    => 'Please Select Users.','alert-type' => 'error',]);
      }
      foreach ($datauser as  $value) {
        if($value != ''){
          if($request->user_approved == 'INACTIVE'){

           $user = User::find($value);
           $user->user_approved = $request->user_approved ;
           $user->save() ;
          }elseif($request->user_approved == 'ACTIVE'){
            $user = User::find($value);
            $user->user_approved = $request->user_approved ;
            if($user->verified == 0 && $user->role_id == 4){
              Mail::to( $user->email )->send(new AccountCreated( $user ));
            }
            $user->verified = '1';
            $user->save() ;

          }elseif($request->user_approved == 'DELETEe'){
          $user = User::find($value);
          self::deleteUserAccount($user->id);
          }
        }
    }
    if($request->user_approved == 'INACTIVE'){

         return Redirect()->back()->with(['message'    => 'User Approved Updated Successfully.','alert-type' => 'success',]); 


      }elseif($request->user_approved == 'ACTIVE'){

          return Redirect()->back()->with(['message'    => 'User Approved Updated Successfully.','alert-type' => 'success',]); 

      }elseif($request->user_approved == 'DELETEe'){

          return Redirect()->back()->with(['message'    => 'Users Deleted Successfully!.','alert-type' => 'success',]); 
      }
      return;
  }
  public function sportevent(Request $request){

    $data = array();
    $data['dataType'] = \Voyager::model('DataType')->where('slug', '=', 'sportevents')->first();
    $data['dataTypeContent'] = SportEvent::where('title','like',"%".$request->event_search."%")->orderBy('created_at','DESC')->paginate();
    $model = false;
    $data['isModelTranslatable'] = is_bread_translatable($model);
    if(view()->exists("voyager::sportevents.browse")){
      $view = "voyager::sportevents.browse";
    }
    $data['event_search'] = $request->event_search;
    return view( $view , $data);
  }
  public function saveApiKay(Request $request){
    if($request->school_api_key){
      $saveApikey = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','school_api_key')->first();
      if(!$saveApikey){
        $saveApikey = new UserInformation();
      }
      $saveApikey->user_id = Auth::User()->id;
      $saveApikey->meta_key = 'school_api_key';
      $saveApikey->meta_value = $request->school_api_key;
      $saveApikey->save();

      return Redirect()->back()->with(['message'    => 'School Api Key Saved Successfully','alert-type' => 'success',]); 
      }else{
        return Redirect()->back()->with(['message'    => 'Please Enter A Api Key','alert-type' => 'error',]); 
    }
  }
  public function schoolresult($slug,Request $request){
     $addData = array();
      $schooldata = ApiData::where('school_slug',$slug)->first();
      $addData['schooldata'] = $schooldata;
      if($schooldata){

      $userlist1 = CoachsSchool::where('school_id',$schooldata->id);
      // if($request->all()){

      // if($request->state){
      //   $userlist1->where('state',$request->state);
      // }
      // if($request->division){
      //   $userlist1->where('division',$request->division);
      // }
      //   $userlist = $userlist1->get()->pluck('user_id')->toArray();
      // }else{
        $userlist = $userlist1->get()->pluck('user_id')->toArray();
      // } 
      $genderKey = 'coach_gender';
      if(Auth::User()->role_id == 4){
        $genderVal = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','coach_gender')->first();
      
      }else{
        $genderVal = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','athelete_gender')->first();
      }
    

    if(Auth::User()->role_id != 1){
        $authSport = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','sport')->first();
        // print_r($authSport);die;
      $authSportUsers = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','sport')
            ->where('ui.meta_value',isset($authSport->meta_value)?$authSport->meta_value:'')
            ->where('u.user_approved','ACTIVE')
            ->whereIn('u.role_id',[4])->get()->pluck('user_id')->toArray();
        $usersInfo7 = UserInformation::select('user_id');
        $usersInfo7->whereIn('user_id',$authSportUsers);
        $usersInfo7->where('meta_key',$genderKey);
        $usersInfo7->where('meta_value' ,isset($genderVal->meta_value)?$genderVal->meta_value:''); 
        $usersInfo7->orWhere('meta_value' ,'Both');
         $usersInfo7->whereIn('user_id',$authSportUsers);
        $usersInfo7->where('meta_key',$genderKey);
            
        $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
        $userlist = array_intersect($usersInfoArray7,$userlist);  
    }
     $user1 = User::whereIn( 'id', $userlist)->where( 'role_id', '=', 4)->where( 'user_status','ACTIVE')->paginate(8);

    $all = User::whereIn( 'id', $userlist)->where( 'role_id', '=', 4)->where( 'user_status', 'ACTIVE')->get();
    $returnArray = array();



                $templatedata = self::getTemplates(31);
           
            $addData['templatedata'] = $templatedata;
           
    $addData['user'] = $user1;
    $addData['all'] = $all;
    $addData['blog_single'] = 'school';

    return view('singleschool',$addData);
    }
    else{
        $userData['page'] = \App\Pages::where('slug', '=', '404-page')->first();
        $userData['page_id'] = $userData['page']->id;
        return view('errors.404',$userData);
    }
  }
public function fetchApiDataCopy( $page_no , $api_key )
    {
      // print_r($page_no);
      // print_r($api_key);die;
      $result2 = @file_get_contents("https://api.data.gov/ed/collegescorecard/v1/schools?api_key=".$api_key."&page=".$page_no."&per_page=1");
      if($result2 === FALSE){
         return 'no';
      }
      $resultdata2  =  json_decode($result2, true);
        $resultdataArray2 = $resultdata2['results'];
         
          foreach($resultdataArray2 as $result1){
                $addData = ApiData::where('school_id',$result1['id'] )->first();
                if(!$addData){
                  $addData = new ApiData(); 
                }
                 $arrayq = array_keys($result1);
                  $array = array_filter($arrayq, 'is_numeric');
                $max = max($array);
                $addDatatuition_in = '';
                $addDatatution_out = '';
              if(isset($max)){
                $addDatatuition_in = $result1[$max]['cost']['tuition']['in_state'];
                $addDatatuition_out = $result1[$max]['cost']['tuition']['out_of_state'];
              }
              // print_r($addDatatution_out);die;
                $slug =  strtolower(str_replace(" ","-",$result1['school']['name']));
                  $schoolslug = ApiData::where('school_name', $result1['school']['name'])->get()->count();
                  if($schoolslug >= 1){
                    $slug = $slug.$schoolslug;
                  }
                $addData->school_name = $result1['school']['name'];
                $addData->school_id = $result1['id'];
                $addData->location_lon = $result1['location']['lon'];
                $addData->location_lat = $result1['location']['lat'];
                $addData->ope8_id = $result1['ope8_id'];
                $addData->ope6_id = $result1['ope6_id'];
                $addData->main_campus = $result1['school']['main_campus'];
                $addData->accreditor = $result1['school']['accreditor'];
                $addData->city = $result1['school']['city'];
                $addData->branches = $result1['school']['branches'];
                $addData->zip = $result1['school']['zip'];
                $addData->school_url = $result1['school']['school_url'];
                $addData->state = $result1['school']['state'];
                $addData->operating = $result1['school']['operating'];
                $addData->instructional_expenditure_per_fte = $result1['school']['instructional_expenditure_per_fte'];
                $addData->faculty_salary = $result1['school']['faculty_salary'];
                $addData->tuition_revenue_per_fte = $result1['school']['tuition_revenue_per_fte'];
                $addData->alias = $result1['school']['alias'];
                $addData->price_calculator_url = $result1['school']['price_calculator_url'];
                $addData->ft_faculty_rate = $result1['school']['ft_faculty_rate'];
                $addData->locale = $result1['school']['locale'];
                $addData->state_fips = $result1['school']['state_fips'];
                $addData->carnegie_undergrad = $result1['school']['carnegie_undergrad'];
                $addData->women_only = $result1['school']['women_only'];
                $addData->religious_affiliation = $result1['school']['religious_affiliation'];
                $addData->under_investigation = $result1['school']['under_investigation'];
                $addData->region_id = $result1['school']['region_id'];
                $addData->carnegie_basic = $result1['school']['carnegie_basic'];
                $addData->men_only = $result1['school']['men_only'];
                $addData->online_only = $result1['school']['online_only'];
                $addData->ownership = $result1['school']['ownership'];
                $addData->carnegie_size_setting = $result1['school']['carnegie_size_setting'];
                $addData->school_slug = $slug;
                $addData->tuition_in_state = $addDatatuition_in;
                $addData->tuition_out_of_state = $addDatatuition_out;
                
                
                $addData->save();       
          }
                     
           return 'yes';
    }

      public function getTemplates($pageID){
        $userID = Auth::User()->id;
        $templates = \App\PageMeta::where([ ['page_id', '=', $pageID] ])->orderBy('order', 'ASC')->get();
        $found = UserInformation::where('user_id', '=', $userID)->where('meta_key', '=', 'emailtemplate')->first();
        //echo "<pre>";
        $data = (array)json_decode($found['meta_value']);
        // print_r($data);
        // die();
        $j = 0;
        $returnArray = array();
        foreach($data as $value ){
            $jd = $j++;
          // print_r($value->Key);
            $returnArray[$jd ]['key'] = $value->Key;
            $returnArray[$jd]['value'] = $value->value;
            $returnArray[$jd ]['message'] = $value->message;
        }
        

        //$templates = array_merge($templates,$found);

        foreach($templates as $key=>$t){
            $returnArray1[$key]['key'] = $t->key;
            $returnArray1[$key]['value'] = $t->display_name;
            $returnArray1[$key]['message'] = $t->value;
        }
        $returnArray11 = array_merge($returnArray1,$returnArray);

        return json_encode($returnArray11);
    }
     public  function deleteUserAccount($userId)
    {  //die('here'); 
            
            $user = User::find($userId);
        if($user && ( $user->role_id == 2 || $user->role_id == 3 ||$user->role_id == 4 ) ){
            if($user->role_id == 2 || $user->role_id == 3){ 
                if($user->subscribed('main')){ 
                  self::cancel($user);
                }
            }       
            \App\User::find($user->id)->delete();
            \App\Notification::where('user_id',$user->id)->orWhere('viewer_user_id',$user->id)->delete();
            \App\LoginHistory::where('user_id',$user->id)->delete();
            if($user->role_id == 2 || $user->role_id == 3){
                \App\Subscription::where('user_id',$user->id)->delete();
                \App\AcademicDetailAthelete::where('user_id',$user->id)->delete();
                // \App\SportEvent::where('author_id',$user->id)->delete();
            }
            // \App\Testimonials::where('author_id',$user->id)->delete();
            // \App\Post::where('author_id',$user->id)->delete();
            // \App\Sport::where('author_id',$user->id)->delete();
            if($user->role_id == 4){
                \App\CoachsSchool::where('user_id',$user->id)->delete();
            }
            $deleteFromEventAttend = \DB::statement("UPDATE sport_event_meta SET `details` = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', `details`, ','),',$user->id,', ',')) WHERE FIND_IN_SET($user->id, `details`) AND `key` = 'coach_register' OR `key` = 'athlete_register';");

            // Auth::logout();
            // \Session::flash('success', 'Your Account is Deleted Permanently!');

            return ;
        }else{
            // \Session::flash('error', 'Invalid User');
            return ;
        }
    }

      public function cancel($user)
    {
        $user->subscription('main')->cancel();
        $user = $user;
       
        Mail::to( $user->email )->send( new SubscriptionCancelled( $user ) );
        return ;
    }
}

?>