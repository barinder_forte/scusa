<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Auth;
use Illuminate\Http\Request;
use App\User;
use App\UserInformation;
use Illuminate\Support\Facades\Validator;
use Hash;
use Session;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userLoggedIn = Auth::User();
        $data['user'] = $userLoggedIn;

        return view('auth.passwords.change_password', $data);
    }
    public function change_password_save(Request $request)
    {
        $user_id = Auth::User()->id;                       
        $obj_user = User::find($user_id);      
            if($request->password != '' ){
                $obj_user->password = Hash::make($request->password); 
            }
        //$obj_user->email = $request->email;
        $obj_user->save();   
        Session::flash('message', 'Updated Successfully!');   
        return redirect()->back();    
    }
   
}
