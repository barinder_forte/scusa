<?php
namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserInformation;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccountCreated;
use App\Mail\SignupThanks;
use App\Mail\AccountCreatedAdminApproval;
use App\Mail\accountSignupNotification;
use App\Mail\SubscriptionCancelled;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Redirect;
use Cookie;
use Voyager;
use Auth;
use Socialite;


class AuthController extends Controller
{
    // Some methods which were generated with the app
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */

    protected $redirectTo = '/';

    public function redirectToProvider($provider)
    {
        // if(  )
        Cookie::forget('signup');
        Cookie::queue('signup', explode('&', $provider)[1], 10);
        // return Cookie::get('signup');
        return Socialite::driver(explode('&', $provider)[0])->redirect();
        // return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $role_id = Cookie::get('signup');
        // return $role_id;
        Cookie::forget('signup');
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider,$role_id);
        if( isset( $authUser['view'] ) && $authUser['view'] != '' ){
            // return redirect('$this->redirectTo')->with('social_info', $authUser['view']);
            Auth::login($authUser, true);
            return redirect('/profile');
        }else if($authUser != 'not found'){
            Auth::login($authUser, true);
            $user = \App\User::where('email',Auth::User()->email)->first();
            if($user->user_status == 'ACTIVE'|| $user->role_id == 1){

                $name = explode(' ', $user->name);
                $currentDate = date('Y-m-d H:i:s');
                $userget = \App\User::find(Auth::User()->id);
                
                $user_match = $name[0].' '.(isset($name[1])?$name[1]:'');
                $slug =  strtolower(str_replace(" ","-",$user_match));
                $userslug = \App\User::where('name', $user_match)->get()->count();
                if( $userslug >= 1 ){
                    $slug = $slug.$userslug;
                }
                $userget->user_slug = $slug;
                $userget->last_login = $currentDate;
                $userget->save();
                /// login history add
                $loginHistory = new \App\LoginHistory();
                $loginHistory->user_id = $userget->id;
                $loginHistory->save();
                // return redirect($this->redirectTo);
                $logged_user = Auth::User();
                if( $logged_user->role_id == 3 ){
                    $parents['firstname'] = $name[0];
                    $parents['lastname'] = (isset($name[1])?$name[1]:'');
                    $parents['email'] = $user->email;
                    $parents_data[0] = $parents;
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'parents')->first();
                    if( $found == '' || $found == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'parents';
                        $data->meta_value = json_encode( $parents_data );
                        $data->save();
                    }
                    $user_sportp = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'sport')->first();
                    if( $user_sportp == '' || $user_sportp == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'sport';
                        $data->meta_value = '2';
                        $data->save();
                    }
                    $user_sportp1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'sport_name')->first();
                    if( $user_sportp1 == '' || $user_sportp1 == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'sport_name';
                        $data->meta_value = 'Soccer';
                        $data->save();
                    }
                }elseif( $logged_user->role_id == 2 ){
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'athelete_firstname')->first();
                    if( $found == '' || $found == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'athelete_firstname';
                        $data->meta_value = $name[0];
                        $data->save();
                    }
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'athelete_lastname')->first();
                    if( $found == '' || $found == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'athelete_lastname';
                        $data->meta_value = (isset($name[1])?$name[1]:'');
                        $data->save();
                    }
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'athelete_email')->first();
                    if( $found == '' || $found == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'athelete_email';
                        $data->meta_value = $user->email;
                        $data->save();
                    }
                    $user_sporta = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'sport')->first();
                    if( ! $user_sporta ){
                        $user_sporta = new UserInformation;
                    }
                    $user_sporta->user_id = $logged_user->id;
                    $user_sporta->meta_key = 'sport';
                    $user_sporta->meta_value = '2';
                    $user_sporta->save();
                    
                    $user_sporta1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'sport_name')->first();
                    if( $user_sporta1 == '' || $user_sporta1 == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'sport_name';
                        $data->meta_value = 'Soccer';
                        $data->save();
                    }
                }else{
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'coach_firstname')->first();
                    if( $found == '' || $found == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'coach_firstname';
                        $data->meta_value = $name[0];
                        $data->save();
                    }
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'coach_lastname')->first();
                    if( $found == '' || $found == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'coach_lastname';
                        $data->meta_value = (isset($name[1])?$name[1]:'');
                        $data->save();
                    }

                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'coach_email')->first();
                    if( $found == '' || $found == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;            
                        $data->meta_key = 'coach_email';
                        $data->meta_value = $user->email;
                        $data->save();
                    }
                    $user_sportc = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'sport')->first();
                    if( $user_sportc == '' || $user_sportc == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'sport';
                        $data->meta_value = '2';
                        $data->save();
                    }
                    $user_sportc1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'sport_name')->first();
                    if( $user_sportc1 == '' || $user_sportc1 == null ){
                        $data = new UserInformation;
                        $data->user_id = $logged_user->id;
                        $data->meta_key = 'sport_name';
                        $data->meta_value = 'Soccer';
                        $data->save();
                    }
                
                }
                // return redirect('/complete-registration')->with('role_id',  $logged_user->role_id);
                return redirect('/profile');
            }else{
                Auth::logout();
                return Redirect('/')->with('error','Your account is pending for admin approval.');
            }
        }else{
            Auth::logout();
            return Redirect('/login')->with('error',"Account doesn't exist, Please sign up");
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider,$role_id)
    {

        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            $authUser['view'] = '';
            $authUser['view'] = 'exist';
            return $authUser;
        }
        $authUser = User::where('email', $user->email)->first();
        if ($authUser) {
            User::where('email', $user->email)->update(['provider' => $provider, 'provider_id' => $user->id]);
            $authUser['view'] = '';
            $authUser['view'] = 'exist';
            return $authUser;
        }
        if($role_id != 'undefined' && $role_id != 'none'){
            $success = User::create([
                'role_id' => $role_id,
                'name'     => $user->name,
                'email'    => $user->email,
                'password' => bcrypt('asghkdashk'),
                'provider' => $provider,
                'provider_id' => $user->id,
                'user_status' => 'ACTIVE',
                'user_approved' => 'INACTIVE',
                
            ]);
            $success2['phone'] = '';
            $success2['sport'] = '';
            $success2['school'] = '';
            if($role_id == 4){
                $admin_emails = Voyager::pagemeta('admin_email', '20');
                $admin_emails2 = explode(',', $admin_emails);
                Mail::to( $admin_emails2[0] )->send(new AccountCreatedAdminApproval( $success, $admin_emails2 , $success2 ));
                Mail::to( $user->email )->send(new AccountCreated( $success ));
            }else{
                // Mail::to( $success->email )->send(new SignupThanks( $success ));
                $data = new UserInformation;
                $data->user_id = $success->id;
                $data->meta_key = 'sport';
                $data->meta_value = '';
                $data->save();
                $success['user_approved'] = 'ACTIVE';
                $success['verified'] = '1';
                $success->save();
            }
            return $success;
        }else{
            return 'not found';
        }
    }

    // User Approval from Admin
    public function approve( $user_id ){
        $current_user = Auth::User();
        $success = User::where('id', $user_id)->first();
        if( $current_user->role_id == 1 && $success->user_approved == 'INACTIVE' ){
            User::where('id', $user_id)->update(['user_approved' => 'ACTIVE']);
        }
        if($success->verified == 0){
            Mail::to( $success->email )->send(new AccountCreated( $success ));
        }
        $success->verified = '1';
        $success->save();
        return redirect('/admin')
                ->with([
                        'message'    => "User Account has been activated succesfully.",
                        'alert-type' => 'success',
                    ]);
    }

    public function getSignOut() {    
        Auth::logout();
        return Redirect('/');
    }
    public function verify($token)
    {  //die('here'); 
        // Auth::logout();
        $user = User::where('email_token',$token)->first();
        if($user){
            if($user->email_token == $token){
                $user->verified = '1';
                $user->email_token = '';
                $user->user_approved = 'ACTIVE';
                $user->save();
                \Session::flash('success', 'Your email is successfully verified!');

                return Redirect('/');
            }else{
                 \Session::flash('error', 'Invalid token');
                return Redirect('/');
            }
        }else{
             \Session::flash('success', 'Your email is already verified!');
            return Redirect('/');
        }
        
    }
    public function deleteUserAccount()
    {  //die('here'); 
   
        $user = Auth::User();
        if($user && ( $user->role_id == 2 || $user->role_id == 3 ||$user->role_id == 4 ) ){
            if($user->role_id == 2 || $user->role_id == 3){ 
                if($user->subscribed('main')){ 
                // $cancel = \App::call('\App\Http\Controllers\SubscriptionsController@cancel');
                    self::cancel($user);
                }
            }
            \App\User::find($user->id)->delete();
            \App\Notification::where('user_id',$user->id)->orWhere('viewer_user_id',$user->id)->delete();
            \App\LoginHistory::where('user_id',$user->id)->delete();
            if($user->role_id == 2 || $user->role_id == 3){
                \App\Subscription::where('user_id',$user->id)->delete();
                \App\AcademicDetailAthelete::where('user_id',$user->id)->delete();
                // \App\SportEvent::where('author_id',$user->id)->delete();
            }
            // \App\Testimonials::where('author_id',$user->id)->delete();
            // \App\Post::where('author_id',$user->id)->delete();
            // \App\Sport::where('author_id',$user->id)->delete();
            if($user->role_id == 4){
                \App\CoachsSchool::where('user_id',$user->id)->delete();
            }
            $deleteFromEventAttend = \DB::statement("UPDATE sport_event_meta SET `details` = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', `details`, ','),',$user->id,', ',')) WHERE FIND_IN_SET($user->id, `details`) AND `key` = 'coach_register' OR `key` = 'athlete_register';");

            Auth::logout();
            \Session::flash('success', 'Your Account is Deleted Permanently!');

            return Redirect('/');
        }else{
            \Session::flash('error', 'Invalid User');
            return Redirect('/');
        }
    }
     public function cancel($user)
    {
        $user->subscription('main')->cancel();
        $user = $user;
       
        Mail::to( $user->email )->send( new SubscriptionCancelled( $user ) );
        return ;
    }
}