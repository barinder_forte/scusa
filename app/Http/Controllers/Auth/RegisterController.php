<?php
namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\User;
use App\UserInformation;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccountCreatedAdminApproval;
use App\Mail\accountSignupNotification;
use App\Mail\VerifyAthleteAccount;
use Response;
use App\Mail\SignupThanks;
use Voyager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\ApiData;
use App\CoachsSchool;
use App\Sport;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    } 

    
    public function create_user(Request $request)
    {
        $rules = array();
        if( $request->role == 2 ){
            $rules["graduation"] = "required";
            $rules["sport"] = "required";
        }
        if( $request->role == 3 ){
            
        }
        if( $request->role == 4 ){
            $rules["sport"] = "required";
            $rules["schoolinput"] = "required";
        }
       $rules["first_name"] = "required|string";
        $rules["last_name"] = "required|string";
        $rules["email"] = "required|string|email|max:255|unique:users";
        $rules["password"] = "required|min:8";
         $rules["checkboxr"] = "required";
        $rules["phone"] = "required|phone";
        $rules["c_code"] = "required";
        $validator = Validator::make($request->all(), $rules,[
            'first_name.required' => 'First Name is required.',
            'c_code.required' => 'Country Code is required.',
            'first_name.string' => 'Number not allowed.',
            'last_name.string' => 'Number not allowed',
            'last_name.required' => 'Last Name is required.',
            'email.required' => ' Email is required.',
            'email.email' => ' Please enter correct email eddress.',
            'email.string' => ' Please enter correct email eddress.',
            'password.required' => 'Password is required.',
            'graduation.required' => 'Graduation year is required.',
            'sport.required' => 'Sport is required.',
            'checkboxr.required' => 'Terms and Conditions field is required.',
            'phone.required' => 'Cell Phone is required.',
           // 'phone.max' => 'Cell Phone is greater than 10.',
            'schoolinput.required' => 'School is required.'
            

        ]);
        // return json_encode($validator->errors());
        // print_r($validator); die;
        if ($validator->fails()) {
            $validate_errors = $validator->errors();
            // print_r($validator->errors());
            // echo $validate_errors;
            if( $request->role == 4 ){
                if( strpos($validate_errors, 'already been') !== false ){
                    return Response::json(array(
                        'activation' => true,
                        'errors' => (object) ['email' => ['Please wait while we are sending you activation email.'] ],
                    ));
                    // $validate_errors->email[0] = "";
                }
            }
            return Response::json(array(
                'error' => true,
                'errors' => $validate_errors
            ));
        }

        if( $request->role == 1 ){
            $role_id = 2;
        }else{
            $role_id = $request->role;
        }
        
        if( $role_id == 2 ){
            $prefix = 'athelete_';
            $postfix = '';
        }elseif( $role_id == 3 ){
            $prefix = 'parent_';
            $postfix = '_0';
            $parents['firstname'] = $request->first_name;
            $parents['lastname'] = $request->last_name;
            $parents['gender'] = $request->parents_gender;
            $parents['email'] = $request->email;
            $parents['phone'] = $request->phone;
            $parents['c_code'] = $request->c_code;
            $parents_data[0] = $parents;
            // $request->parents = $parents;
        }else{
            $prefix = 'coach_';
            $postfix = '';
            $school_i = $request->schoolinput;
            $school_i = explode(' - ',$school_i);
            if(isset($school_i[1])){
                $school_city = explode(', ',$school_i[1])[0];
            }else{
                $school_city = '';
            }
            if(isset($school_i[1])){
                $school_state1 = explode(', ',$school_i[1]);
                if(isset($school_state1[1])){
                    $school_state = $school_state1[1];
                }else{
                    $school_state = '';
                }
            }else{
                $school_state = '';
            }
            $school_i = explode(', ',$school_i[0])[0];
            
            if($school_i){
                $scholllist = ApiData::where('school_name','=',$school_i);
                if($school_city){
                    $scholllist->where('city',$school_city);
                }else{
                    $scholllist->where('city','');
                }
                if($school_city){
                    $scholllist->where('state',$school_state);
                }else{
                    $scholllist->where('state','');
                }
                $scholllist = $scholllist->first();

               // $scholllist = ApiData::whereIn('id',$apidata)->get();
                $schollarray = array();
                if(!$scholllist){

                    $slug =  strtolower(str_replace(" ","-",$school_i));
                            $schoolslug = ApiData::where('school_name', $school_i)->get()->count();
                            if($schoolslug >= 1){
                                $slug = $slug.$schoolslug;
                            }
                    $scholllist = new ApiData(); 
                    $scholllist->school_name = $school_i;
                    $scholllist->school_slug = $slug;
                    $scholllist->save();

                }
            }
            // foreach($scholllist as $key => $scholllistdata){
            if(isset($scholllist)){
                $schollarray['school'] = $scholllist->id;
                $schollarray['school_url'] = $scholllist->school_url;
                $schollarray['school_tution'] = $scholllist->tuition_in_state;
                $schollarray['tution_room_board'] = $scholllist->tuition_revenue_per_fte;
                $schollarray['address1'] = $scholllist->address;
                $schollarray['phone'] = $scholllist->telephone;
                $schollarray['city'] = $scholllist->city;
                $schollarray['state'] = $scholllist->state;
                $schollarray['zip'] = $scholllist->zip;  
                $schollarray['head_coach'] = $request->position;
                $schollarray['coach_gender'] = $request->coach_gender;
                $schollarray['school_size_total'] = $scholllist->school_size_total;
            }
        }
       
        $success =  User::create([
            'name' => $request->first_name." ".$request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'avatar' => 'users/default.png',
            'role_id' => $role_id,
            'user_approved' => 'INACTIVE',
            'email_token' => base64_encode($request->email)
        ]);
        $admin_email = Voyager::pagemeta('admin_email', '20');
        $admin_emails = Voyager::pagemeta('admin_email', '20');
        $admin_emails2 = explode(',', $admin_emails);
        if( $success->role_id == 4 ){
            // $success2 = $success[''] = 
            // Mail::to( $admin_emails2[0] )->send(new AccountCreatedAdminApproval( $success, $admin_emails2 ));
            Mail::to( $success->email )->send(new accountSignupNotification( $success ));
        }else{
               Mail::to( $success->email )->send(new VerifyAthleteAccount( $success ));  
               // Mail::to( $success->email )->send(new SignupThanks( $success ));

             }
        foreach( $request->all() as $key => $value )
        {
            if( $key == 'password' || $key == 'role' || $key == '_token' ){
                continue;
            }
            $data = UserInformation::where('user_id', '=', $success->id)->where('meta_key', '=', $key)->first();
            if( $data == '' ){
                $data = new UserInformation;
                $data->user_id = $success->id;
                if( $key == 'first_name' || $key == 'last_name' || $key == 'email' || $key == 'phone' ){
                    if( $key == 'phone'  ){
                        if( $prefix == 'athelete_' || $prefix == 'parent_' ){
                            $key = str_replace('_', '', $key);
                            $data->meta_key = $prefix.''.$key.''.$postfix;
                        }else{
                            $data->meta_key = $key;
                        }
                    }else{
                        $key = str_replace('_', '', $key);
                        $data->meta_key = $prefix.''.$key.''.$postfix;
                    }
                }else{
                    $data->meta_key = $key;
                }
            }
            $data->meta_value = $value;
            $data->save();
        }

        if( $role_id == 3 ){
            $data = new UserInformation;
            $data->user_id = $success->id;
            $data->meta_key = 'parents';
            $data->meta_value = json_encode( $parents_data );
            $data->save();
        }
        if(isset($schollarray)){
            if( $role_id == 4 && count($schollarray) > 0){
                $data = new CoachsSchool();                
                $data->user_id = $success->id;
                $data->school_id = $schollarray['school']; 
                $data->school_tuition = $schollarray['school_tution']; 
                $data->tuition_room = $schollarray['tution_room_board'];
                $data->state = $schollarray['state'];
                $data->school_url = $schollarray['school_url'];
                $data->phone = $schollarray['phone'];
                $data->sport_id = $request->sport;
                $data->city = $schollarray['city'];
                $data->zip = $schollarray['zip'];
                $data->school_size = $schollarray['school_size_total'];
                $data->head_coach = $schollarray['head_coach'];
                $data->gender = $schollarray['coach_gender'];
                $data->save();
                $data1 = new UserInformation;
                $data1->user_id = $success->id;
                $data1->meta_key = 'school';
                $data1->meta_value = $schollarray['school'];
                $data1->save();
                $data2 = new UserInformation;
                $data2->user_id = $success->id;
                $data2->meta_key = 'coach_www';
                $data2->meta_value = isset($schollarray['school_url'])?$schollarray['school_url']:'';
                $data2->save();
                if(!empty($schollarray['head_coach'])){
                    if($schollarray['head_coach'] == 'True'){
                        $title= "Head Coach";
                    }else{
                        $title= "Assistant Coach";
                    }
                }else{
                    $title= "";
                }
                $datatitle = new UserInformation;
                $datatitle->user_id = $success->id;
                $datatitle->meta_key = 'coach_title';
                $datatitle->meta_value = $title;
                $datatitle->save();
            }
        }
         if( $success->role_id == 4 ){
            $success2 = array(); 
            // $success2 = $success; 
            $success2['school'] = $request->schoolinput; 
            $success2['sport'] = Sport::find($request->sport)->title; 
            $success2['phone'] = $request->phone; 
            //print($success2);die;
            Mail::to( $admin_emails2[0] )->send(new AccountCreatedAdminApproval( $success, $admin_emails2 ,$success2 ));
        }
        Auth::login($success, true);
            $currentDate = date('Y-m-d H:i:s');
            $userget = \App\User::find(Auth::User()->id);
            if($userget->user_status == 'ACTIVE' || $userget->role_id == 1){
                $slug =  strtolower(str_replace(" ","-",$userget->name));
                $userslug = \App\User::where('name', $userget->name)->get()->count();
                if($userslug >=1){
                    $slug = $slug.$userslug;
                }
                $userget->user_slug = $slug;
                $userget->last_login = $currentDate;
                $userget->save();
                return redirect('/complete-registration')->with('success', "Account has been created successfully.");
            }else{
                Auth::logout();
                return Redirect()->back()->with('error','Your account is deactivated. Please Contact admin for more information');
            }
        // return Response::json(array(
        //      'success' => true,
        //      'data'   => "Account has been created successfully.",
            
        // ));
    }
    protected function signup()
    {
        return view('cta_signup');
    } 
   
}
