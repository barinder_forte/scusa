<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\UserInformation;
use App\Sport;
use App\LoginHistory;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }
    
    
protected function authenticated(Request $request, $user)
    {  
        $currentDate = date('Y-m-d H:i:s');
        $userget = \App\User::find($user->id);
        $userget->last_login = $currentDate;
        $userget->save();
        $loginHistory = new LoginHistory();
        $loginHistory->user_id = $user->id;
        $loginHistory->save();
        $status = $user->user_status;
        if( $user->role_id == 1 ){
            if ( $request->ajax() )
                return 'admin';
            // else
                // return Redirect('/admin');
        }elseif($user->role_id == 4 ){
            if($status == 'ACTIVE'){
                // return Redirect('/profile');
            }else{
                Auth::logout();
                return Redirect()->back()->with('error','Your account is deactivated. Please Contact admin for more information');
            }
            
        }else{
            if($status == 'ACTIVE'){
               $data_filled = UserInformation::where( 'user_id', '=', $user->id )->where('meta_key', '=', 'profile_filled')->where('meta_value', '=', 'filled')->first();
                if( $data_filled == '' && $user->role_id == 3 ){
                    // print_r(  );die;
                    return redirect()->intended('/complete-registration#1');;
                }else{

                    // Auth::logout();
                    // return Redirect()->back()->with('error','Your account is deactivated. Please Contact admin for more information');
                    return Redirect('/profile');
                }
            }else{
                    Auth::logout();
                    return Redirect()->back()->with('error','Your account is deactivated. Please Contact admin for more information');
                }
        }
        // return Redirect('/');
    }

}
