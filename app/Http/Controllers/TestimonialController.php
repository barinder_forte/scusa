<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Testimonials;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Storage;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return( 'hello' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('testimonial_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $data = array();
        $data['testimonial'] = Testimonials::find($id);
        $data['blog_single'] = 'testimonials_single';
        
        return view('testimonial_show' , $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_column($id, $column)
    {
        // Check permission
        Voyager::canOrFail('browse_settings');
        // echo "<pre>";
        $setting = Testimonials::find($id);
        if (isset($setting->id)) {
            // print_r($setting->$column); die;
            // If the type is an image... Then delete it
            // if ($setting->type == 'image') {
                if (Storage::disk(config('voyager.storage.disk'))->exists($setting->$column)) {
                    Storage::disk(config('voyager.storage.disk'))->delete($setting->$column);
                }
            // }
            $setting->$column = '';
            $setting->save();
        }

        return back()->with([
            'message'    => "Successfully removed {$column} value",
            'alert-type' => 'success',
        ]);
    }
   
}
