<?php

namespace App\Http\Controllers;
use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubscriptionCreated;
use App\Mail\SubscriptionCancelled;
use App\Mail\ResumeSubscription;
use App\User;

class SubscriptionsController extends Controller
{
    public function index()
    {
        return view('subscriptions.index');
    }
    
    public function cancel(Request $request)
    {
        $request->user()->subscription('main')->cancel();
        $user = $request->user();
        //$userinfo = \App\UserInformation::where('user_id',$request->user()->id)->where('meta_key','subscription_id')->delete();
        Mail::to( $request->user()->email )->send( new SubscriptionCancelled( $user ) );
        return redirect()->back()->with('success', 'You have successfully cancelled your subscription');
    }
    
    public function store(Request $request)
    {
        $plan = Plan::findOrFail($request->plan);
        if (!$request->user()->subscribed('main')) {
           $response = $request->user()->newSubscription('main', $plan->braintree_plan)->create($request->payment_method_nonce);
           
            $user = $request->user();
            Mail::to( $request->user()->email )->send( new SubscriptionCreated( $plan, $user ) );
        } else {
            $request->user()->subscription('main')->swap($plan->braintree_plan);
        }
        
          $invoiceDownId = isset($request->user()->invoicesIncludingPending()[0])?$request->user()->invoicesIncludingPending()[0]->id:'';
        if(($invoiceDownId) != ''){
            $userinfo = \App\UserInformation::where('user_id',$request->user()->id)->where('meta_key','subscription_id')->first();
            if(!$userinfo){
                $userinfo = new \App\UserInformation();
            }
            $userinfo->user_id = $request->user()->id;
            $userinfo->meta_key = 'subscription_id';
            $userinfo->meta_value = $invoiceDownId;
            $userinfo->save();
        }
        
        return redirect('thankyou')->with('success', $plan->braintree_plan.' successfully');
    }
    
    public function resume(Request $request)
    {
        $request->user()->subscription('main')->resume();
        $user = $request->user();
        Mail::to( $request->user()->email )->send( new ResumeSubscription( $user ) );
        return redirect()->back()->with('success', 'You have successfully resumed your subscription');
    }
    
    public function cancelled_hook(Request $request){
        // die('asda');
        if( $request->all() )
            print_r( $request->all() ); 
        return '';
        // die;
    }
    
    public function generate_invoice(Request $request, $invoiceId,$userID){
        $user = User::find($userID);
        return $user->downloadInvoice($invoiceId, [
            'vendor'  => env('APP_NAME', 'Laravel'),
            'product' => 'Premium Subscription',
        ]);
    }

    public function thanks(Request $request){
        if (session('success')):
            return view('thankyou');
        else:
            // return redirect('thankyou')->with('success', ' successfully');
            return redirect('/');
        endif;
    }

}
