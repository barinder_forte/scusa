<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use Auth;
use Session;
use App\User;
use App\UserInformation;
use App\Pages;

class PostsController extends Controller
{
    public function postShow( $slug ){
       $userData = array();
       $post = Post::where('slug',$slug)->first();
       $userData['post'] = $post;
       $userData['blog_single'] = 'blog_single';
       return view('post_single_show', $userData);
    }
     public function blog(  ){
      
       $post = Post::where('id', '>', 0)->latest()->paginate(5);
       $userData['post'] = $post;
       $userData['page'] = Pages::where('slug', '=', 'blog')->first();
       $userData['page_id'] = $userData['page']->id;
       return view('blog',$userData);
    }
   
}