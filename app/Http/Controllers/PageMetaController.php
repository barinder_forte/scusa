<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\PageMeta;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Storage;

class PageMetaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        // Check permission
        // DB::canOrFail('browse_settings');
        if(isset($request->field)){
            $sport_data = PageMeta::where('id', '=', $request->field )->first();
            $sport_data->display_name = $request->display_name;
            $sport_data->details = $request->details;
            $sport_data->key = $request->key;
            $sport_data->type = 'select_dropdown';
            $sport_data->save();
            return redirect('admin/pages/'.$request->page_id.'/edit')->with([
                'message'    => 'Settings Successfully Updated',
                'alert-type' => 'success',
            ]); 
        }
   
        $lastSetting = PageMeta::orderBy('order', 'DESC')->first();

        if (is_null($lastSetting)) {
            $order = 0;
        } else {
            $order = intval($lastSetting->order) + 1;
        }

        $request->merge(['order' => $order]);
        $request->merge(['value' => '']);

        PageMeta::create($request->all());
    
    
        return redirect('admin/pages/'.$request->page_id.'/edit')->with([
            'message'    => 'Successfully Created Settings',
            'alert-type' => 'success',
        ]);
    }

    public function move_up($id)
    {
        $setting = Voyager::model('Setting')->find($id);
        $swapOrder = $setting->order;
        $previousSetting = Voyager::model('Setting')->where('order', '<', $swapOrder)->orderBy('order', 'DESC')->first();
        $data = [
            'message'    => 'This is already at the top of the list',
            'alert-type' => 'error',
        ];

        if (isset($previousSetting->order)) {
            $setting->order = $previousSetting->order;
            $setting->save();
            $previousSetting->order = $swapOrder;
            $previousSetting->save();

            $data = [
                'message'    => "Moved {$setting->display_name} setting order up",
                'alert-type' => 'success',
            ];
        }

        return back()->with($data);
    }

    // public function delete_value($id)
    // {
    //     // Check permission
    //     Voyager::canOrFail('browse_settings');

    //     $setting = Voyager::model('Setting')->find($id);

    //     if (isset($setting->id)) {
    //         // If the type is an image... Then delete it
    //         if ($setting->type == 'image') {
    //             if (Storage::disk(config('voyager.storage.disk'))->exists($setting->value)) {
    //                 Storage::disk(config('voyager.storage.disk'))->delete($setting->value);
    //             }
    //         }
    //         $setting->value = '';
    //         $setting->save();
    //     }

    //     return back()->with([
    //         'message'    => "Successfully removed {$setting->display_name} value",
    //         'alert-type' => 'success',
    //     ]);
    // }

    public function move_down($id)
    {
        $setting = Voyager::model('Setting')->find($id);
        $swapOrder = $setting->order;

        $previousSetting = Voyager::model('Setting')->where('order', '>', $swapOrder)->orderBy('order', 'ASC')->first();
        $data = [
            'message'    => 'This is already at the bottom of the list',
            'alert-type' => 'error',
        ];

        if (isset($previousSetting->order)) {
            $setting->order = $previousSetting->order;
            $setting->save();
            $previousSetting->order = $swapOrder;
            $previousSetting->save();

            $data = [
                'message'    => "Moved {$setting->display_name} setting order down",
                'alert-type' => 'success',
            ];
        }

        return back()->with($data);
    }
    
    public function eventstore(Request $request)
    {
    
        return '';
    
    }
    
    public function page_field_store(Request $request){
        // echo "<pre>";
        // echo $request->key;
        $sport_data = PageMeta::where('id', '=', $request->field )->first();
        $sport_data->display_name = $request->display_name;
        $sport_data->details = $request->details;
        // echo $request->key; die;
        $sport_data->key = str_replace( ' ', '_', strtolower( $sport_data->key ) );
        // print_r( $sport_data ); die;
        $sport_data->save();
        return redirect('admin/pages/'.$request->page_id.'/edit')->with([
            'message'    => 'Settings Successfully Updated',
            'alert-type' => 'success',
        ]);
    }

    public function delete_value($id)
    {
        // Check permission
        Voyager::canOrFail('browse_settings');

        $setting = PageMeta::find($id);

        if (isset($setting->id)) {
            // If the type is an image... Then delete it
            if ($setting->type == 'image') {
                if (Storage::disk(config('voyager.storage.disk'))->exists($setting->value)) {
                    Storage::disk(config('voyager.storage.disk'))->delete($setting->value);
                }
            }
            $setting->value = '';
            $setting->save();
        }

        return back()->with([
            'message'    => "Successfully removed {$setting->display_name} value",
            'alert-type' => 'success',
        ]);
    }
    
}