<?php

namespace App\Http\Controllers;
use Mail;
use Illuminate\Http\Request;
use App\User;
use App\UserInformation;
use App\Sport;
use Response;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Pages;
use App\SportsMeta;
use PDF;
use App\ApiData;
use App\Mail\profileVisitNotification;

use App\AcademicDetailAthelete;
use App\Post;
use App\CoachsSchool;
use App\SportEvent;
use App\Notification;
use Voyager;
use App\PageMeta;
class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function complete_registration(Request $request){
        if(Auth::User()->role_id != 4){
            $logged_user = Auth::User();
            $user = UserInformation::where( 'user_id', '=', $logged_user->id )->get();
            $name = $logged_user->name;
            $name = explode(' ', $name);
            if( $logged_user->role_id == 2 ){
                $userData['firstname'] = array_shift( $name );
                $userData['lastname'] = implode(' ', $name);
            }elseif( $logged_user->role_id == 3 ){
                $userData['parent_firstname'] = array_shift( $name );
                $userData['parent_lastname'] = implode(' ', $name);
            }else{
                $userData['firstname'] = array_shift( $name );
                $userData['lastname'] = implode(' ', $name);
            }
            // $userData['firstname'] = array_shift( $name );
            // $userData['lastname'] = implode(' ', $name);
            foreach( $user as $data )
            {
                $userData[ $data->meta_key ] = $data->meta_value;
            }
            $sports = Sport::select('id','title')->where('status', '=', 'PUBLISHED')->orderBy('title','ASC')->get()->toArray();
            // print_r($sports);die;
            // $userData['sports'] = json_decode($sports);
            $userData['allsports'] = $sports;
            $userData['id'] = $logged_user->id;
            $progressData = self::progress();
            
            $userData['progressData'] = $progressData;
            
            $userData['page'] = Pages::where('slug', '=', 'complete-registration')->first();
            $userData['page_id'] = $userData['page']->id;
        
            return view('signup-step', $userData);
        }else{
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    
    public function save_profile(Request $request){
        // die('a');
        $logged_user = Auth::User();
        $parent_count = $request->parent_count;
        $rules['athelete_firstname'] = 'required|string';
        $error['athelete_firstname.required'] = 'Please Enter First Name';
        $error['athelete_firstname.string'] = 'First Name must be a valid string';
        $rules['athelete_lastname'] = 'required|string';
        $error['athelete_lastname.required'] = 'Please Enter Last Name';
        $error['athelete_lastname.string'] = 'Last Name must be a valid string';
        $rules['sport'] = 'required';
        $error['sport.required'] = 'Please select a sport';
        $rules['graduation'] = 'required';
        $error['graduation.required'] = 'Please select graduation complete year';
        
        for( $i = 0; $i < $parent_count; $i++ )
        {
            $rules[ 'parent_firstname_'.$i ] = 'required|string';
            $error['parent_firstname_'.$i.'.required'] = 'Please Enter First Name';
            $error['parent_firstname_'.$i.'.string'] = 'First Name must be a valid string';
            $rules[ 'parent_lastname_'.$i ] = 'required|string';
            $error['parent_lastname_'.$i.'.required'] = 'Please Enter Last Name';
            $error['parent_lastname_'.$i.'.string'] = 'Last Name must be a valid string';
            $rules[ 'parent_cc_'.$i ] = 'required';
            $error['parent_cc_'.$i.'.required'] = 'Please select a Country Code';
            $rules[ 'parent_phone_'.$i ] = 'required|numeric';
            $error['parent_phone_'.$i.'.required'] = 'Please Enter Phone Number';
            $error['parent_phone_'.$i.'.numeric'] = 'Please Enter a valid Phone Number';
            $rules[ 'parent_email_'.$i ] = 'required|email';
            $error['parent_email_'.$i.'.required'] = 'Email Address is required';
            $error['parent_email_'.$i.'.email'] = 'Please enter a valid Email';
        }
        
        // $validator = Validator::make($request->all(), $rules, $error );

        // if ($validator->fails())
        // {
        //     // return "hello";
        //     // return $validator->errors();
        //     return Response::json(array(
        //         'error' => true,
        //         'errors' => $validator->errors()
        //     ));
        // }
        
        foreach( $request->all() as $key=>$value )
        {
            if( $key != '_token' ){
                $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
                if( $found == '' || $found == null ){
                    $found = new UserInformation;
                    $found->meta_key = $key;
                    $found->user_id = $logged_user->id;
                    $found->save();
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
                }
                $found->meta_value = $value;
                $found->save();
            }
        }
        
        
        if( $logged_user->role_id == 2 || $logged_user->role_id == 1){
            $logged_user->name = $request->athelete_firstname.' '.$request->athelete_lastname;
            // $logged_user->email = 
        }elseif( $logged_user->role_id == 3 ){
            if( isset( $request->parent_firstname_0 ) && isset( $request->parent_lastname_0 ) )
                $logged_user->name = $request->parent_firstname_0.' '.$request->parent_lastname_0;
            if( isset( $request->parent_email_0 ) && $request->parent_email_0 != '' )
                $logged_user->email = $request->parent_email_0;
            $data_filled = \App\UserInformation::where( 'user_id', '=', $logged_user->id )->where('meta_key', '=', 'profile_filled')->where('meta_value', '=', 'filled')->first();
                if( !$data_filled && ( $request->athelete_firstname != '' || $request->athelete_lastname != '' ) ){
                    // die('hdg');
                   $data_filled = new UserInformation();
                   $data_filled->user_id = $request->user()->id;
                   $data_filled->meta_key = 'profile_filled';
                   $data_filled->meta_value = 'filled';
                   $data_filled->save();

                }

        }
        $logged_user->save();
        
        $progressData = self::progress();
        
         return  $progressData;
       
        // return redirect('/complete-registration')->with('success', "Account updated successfully.");
    }
    
    
    public function profile(){
        ini_set('memory_limit','256M');
        $logged_user = Auth::User();
        $progressData = self::progress();
        $user_id = $logged_user->id;
        $user_slug = $logged_user->user_slug;
        $data_filled = \App\UserInformation::where( 'user_id', $user_id )->where('meta_key', 'profile_filled')->where('meta_value', 'filled')->first();
            if( $data_filled == '' && $logged_user->role_id == 3){
                // print_r(  );die;
                return redirect()->intended('/complete-registration#1');;
            }
        $return['progressData'] = $progressData;
        $return['user_id'] = $user_id;
        $return['user_slug'] = $user_slug;
        $return['role_id'] = $logged_user->role_id;
        $return['page'] = Pages::where('slug', '=', 'profile')->first();
        $return['page_id'] = $return['page']->id;
        
        $sports = Sport::where('status', '=', 'PUBLISHED')->get();
        foreach( $sports as $key => $field ){
            $inputs[ 'fields' ] = SportsMeta::where( 'sport_id', '=', $field->id )->where('field_type', '=', 'field')->orderBy('order', 'asc')->get()->toArray();
            $inputs[ 'stats' ] = SportsMeta::where( 'sport_id', '=', $field->id )->where('field_type', '=', 'stats')->orderBy('order', 'asc')->get()->toArray();
            $settings[ $field->id ] = $inputs;
            
           
        } 
        if($logged_user->role_id == '4'){     
        $currentDate = Date('m/d/Y');
        $return['events'] = \DB::table('sport_event_meta as ui')
                   ->join('sport_events as u', 'ui.event_id', '=', 'u.id')
                   ->where('ui.key','coach_register')
                   ->whereRaw("find_in_set(".$user_id.",details)")
                   ->where('u.event_end','>=',$currentDate)
                   ->get();    
        }
                       // print_r($user_id);die;
        // $return['events'] = \DB::table('sport_events as ui')
        //            //->join('sport_events as u', 'ui.event_id', '=', 'u.id')
        //             ->where('ui.author_id',Auth::User()->id)
        //            //->whereRaw("find_in_set(".$user_id.",details)")
        //            //->where('u.event_end','>=',$currentDate)
        //            ->get();     
        // //print_r($return['events']);  
        $return['allsports'] = Sport::select('id','title')->where('status', '=', 'PUBLISHED')->get()->toArray();  
        $return['sport_fields'] = $settings;
        return view( 'profile', $return );
    }
        public function athletic_achievements(Request $request){
        $logged_user = Auth::User();
        $user_id = $logged_user->id;
        // echo "<pre>  ";
       // print_r($request->all());
        $values = array();
        foreach( $request->athletic_achievements as $valuearray ){
            // if(isset($valuearray['athleticname']) && isset($valuearray['newachive'])){
               
            //     if( $valuearray['athleticname'] == 'other' ){
            //         $valuearray['athleticname'] = $valuearray['newachive'];

            //         $achivement_athlete = Voyager::pagemetadetail('achivement_athlete', '16');
            //         $achivement_athlete =json_decode($achivement_athlete);
            //         $achive = (array) $achivement_athlete->options;
            //         $achive[$valuearray['newachive']] = $valuearray['newachive'];
            //         $achive1['default'] = 'hide';
            //         $achive1['options'] = (object) $achive;
            //         $achive2 = (object) $achive1;
            //         $scholllist = PageMeta::where('page_id','=',16)->where('key','=','achivement_athlete')->first();
            //         if($scholllist){
            //             $scholllist->details = json_encode($achive2);
            //         }
            //         $scholllist->save();
            //     }
             // }else if( $valuearray['athleticname'] == 'other' && ! isset($valuearray['newachive'])){
                // $valuearray['athleticname'] = $valuearray['athleticname'];
             // }
            
                // $valuearray['newachive'] = '';
                $values[] = $valuearray;
        }
        $key = 'athletic_achievements';
        $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        if( $found == '' || $found == null ){
            $found = new UserInformation;
            $found->meta_key = $key;
            $found->user_id = $logged_user->id;
            $found->save();
            $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        }
        if( is_array( $values ) ){

            $value = json_encode( $values );

        }else{
            $value = $values;
        }
        
        $found->meta_value = $value;
        $found->save();

        $achivement_athlete = Voyager::pagemetadetail('achivement_athlete', '16');
        $achivement_athlete = json_decode($achivement_athlete);
        if(isset($achivement_athlete->options)){
            $achivement_athlete = $achivement_athlete->options;
        }
        else{
            $achivement_athlete = array();
        }

        $newdata = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
         //$progress['prog'] =$newdata;
        $achivement_athlete = (array) $achivement_athlete;
         sort($achivement_athlete , SORT_STRING);
         // print_r($achivement_athlete);
         // die(); 
         
        $progress['value'] = json_decode($newdata->meta_value);
        $progress['prog'] = self::progress();
        $progress['achive'] = $achivement_athlete;
        return $progress;
    }
    // Update Profile
    public function update_profile( Request $request ){
                // print_r($request->all());die;

        $logged_user = Auth::User();
        $user_id = $logged_user->id;
        $schoollistarray = array();
        if($request->athelete_birth ){
            $birth = explode('/', $request->athelete_birth);
            //print_r($birth);
            $requestth = $birth[2].'/'. $birth[0].'/'. $birth[1];
             $request->request->add(['athelete_birth'=>$requestth]);

        }
        // if( $request->zip ){
        //     $url1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$request->zip."&sensor=false";
        //     $details=file_get_contents($url1);
        //     if($details){
        //     $result = json_decode($details,true);
        //     $lat["latitude"]=$result['results'][0]['geometry']['location']['lat'];
        //     $lat["longitude"]=$result['results'][0]['geometry']['location']['lng'];
        //     $lati = json_encode($lat);

        //     $request->request->add(['lat_long'=>$lati]);
        //  }   
        // }
        $arrayname = array();
        $arraynameemail = array();
        foreach( $request->all() as $key=>$value )
        {
            if( $key != '_token' ){
                if($key == 'coachings' || $key == 'academics'){
                    foreach($value as $keys => $values){
                        if( isset($values['school'])?$values['school']:'' == 'other'){
                            if(isset($values['schoolnameother'])){
                                $scholllist = ApiData::where('school_name','=',$values['schoolnameother'])->first();
                                if(!$scholllist){
                                    $scholllist = new ApiData(); 
                                    $scholllist->school_name = $values['schoolnameother'];
                                    $scholllist->save();
                                    $scholId = $scholllist->id;
                                }else{
                                    $scholId = $scholllist->id;
                                }
                                $value[$keys]['school'] = $scholId;
                                $schoollistarray[$keys]['label'] = $scholllist->school_name;
                                $schoollistarray[$keys]['value'] = $scholllist->id;
                            }
                        }
                    }
                }

                if( $key == 'sportsdata' ){
                    $all_sports = [];
                    foreach ($value as $val) {
                        $all_sports[] = $val['sport'];
                    }
                    $all_sports = array_unique( $all_sports );
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'all_sports')->first();
                    if( $found == '' || $found == null ){
                        $found = new UserInformation;
                        $found->meta_key = 'all_sports';
                        $found->user_id = $logged_user->id;
                        $found->save();
                        $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'all_sports')->first();
                    }
                    $found->meta_value = implode( ',', $all_sports );
                    $found->save();
                }
                $keyuserupdate = array('coach_firstname','athelete_firstname','coach_lastname','athelete_lastname');
                if(in_array($key,$keyuserupdate)){
                   $arrayname[$key] = $value;
                }
                $keyuserupdateemail = array('athelete_email','coach_email');
                if(in_array($key,$keyuserupdateemail)){
                   $arraynameemail = $value;
                }
               // $keyuserupdate2 = array('coach_lastname','athlete_lastname');
               //  if(in_array($key,$keyuserupdate2)){
               //     $arrayname[] = $value;  
               //  }
                $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
                if( $found == '' || $found == null ){
                    $found = new UserInformation;
                    $found->meta_key = $key;
                    $found->user_id = $logged_user->id;
                    $found->save();
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
                }
                if( $key == 'sportsdata' ){
                if( is_array( $value ) ){
                    $value = json_encode( $value );
                    $value = str_replace('null', '""', $value);
                }else{
                    $value = $value;
                }
                }else{
                    if( is_array( $value ) ){
                    $value = json_encode( $value );
                    // $value = str_replace('null', '', $value);
                }else{
                    $value = $value;
                }
                }
                
                $found->meta_value = $value;
                $found->save();
            }

        }
        if(!empty($arrayname)){
           if( ( $logged_user->role_id == 2 || $logged_user->role_id == 4 ) || $logged_user->role_id == 1 ){
            $found1 = User::where('id', '=', $logged_user->id)->first();
                if( $found1 ){
                    $found1->name = implode(' ',$arrayname);
                    // echo $found1->name;
                    $found1->save();
                 }
            }
            if( $request->user()->role_id == 3 ){
                 $data_filled = \App\UserInformation::where( 'user_id', '=', $request->user()->id )->where('meta_key', '=', 'profile_filled')->where('meta_value', '=', 'filled')->first();
                if( !$data_filled ){
                    // die('hdg');
                   $data_filled = new UserInformation();
                   $data_filled->user_id = $request->user()->id;
                   $data_filled->meta_key = 'profile_filled';
                   $data_filled->meta_value = 'filled';
                   $data_filled->save();

                }
            }
        }    
         if(!empty($arraynameemail)){
           if( ( $logged_user->role_id == 2 || $logged_user->role_id == 4 )){
            $foundEMAIL = User::where('id', '=', $logged_user->id)->first();
                if( $foundEMAIL ){
                    $foundEMAIL->email = $arraynameemail;
                    $foundEMAIL->save();
                 }
            }
        }   
     
       if($request->coachings || $request->academics){
            $progress['value'] = $value;
            $progress['prog'] = self::progress();
            $progress['schoollist'] = $schoollistarray;
       }else{
            $progress = self::progress();
       }
        
        return $progress;
    }    
    public function delete_parent(Request $request){
        $logged_user = Auth::User();
        $keyvalue = $request->key;
        $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'parents')->first();
        $new_data= (array) json_decode( $data_1['meta_value'] );
        unset($new_data[$keyvalue]);
        $data_1->meta_value =json_encode($new_data);
        $data_1->save();
    }

     public function usersList(Request $request = null){
        $data = array();
        if(\Auth::User()->role_id == 4 || \Auth::User()->role_id == 1){

                $templatedata = self::getTemplates(30);
           
            $data['templatedata'] = $templatedata;
           
           
            if(Auth::User()->role_id == 1){
               $authusergen = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','athelete_gender')->first();
            }else{
              $authusergen = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','coach_gender')->first();
              $coachSport = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','sport')->pluck('meta_value')->first();
              // print_r($coachSport);die;
            }
           if((isset($authusergen->meta_value)?$authusergen->meta_value:'')  != '' && ( (isset($authusergen->meta_value)?$authusergen->meta_value:'')  != 'Both' && Auth::User()->role_id != 1 ) ){
            
                $coachSportUsers = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','sport')
            ->where('ui.meta_value',$coachSport)
            ->whereIn('u.role_id',[2,3])->get()->pluck('user_id')->toArray();
            $users = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','athelete_gender')
            ->where('ui.meta_value',isset($authusergen->meta_value)?$authusergen->meta_value:'')
            ->whereIn('ui.user_id',$coachSportUsers)
            ->where('u.user_approved','ACTIVE')
            ->where('u.verified','1')
            // ->where('ui.meta_value',$coachSport)
            ->whereIn('u.role_id',[2,3])->select('u.id')->get()->toArray();
            // echo '<pre>';
            // print_r($users);die;
            // $data['users'] = $users;
            $usersAll = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','athelete_gender')
            ->where('u.verified','1')
            ->where('u.user_approved','ACTIVE')
            ->where('ui.meta_value',isset($authusergen->meta_value)?$authusergen->meta_value:'')
            ->whereIn('ui.user_id',$coachSportUsers)
            // ->where('ui.meta_value',$coachSport)
            ->whereIn('u.role_id',[2,3])->get();
       }else if( ( (isset($authusergen->meta_value)?$authusergen->meta_value:'')  == '' ||  (isset($authusergen->meta_value)?$authusergen->meta_value:'')  == 'Both' ) && Auth::User()->role_id != 1  ){
            $users = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','sport')
            ->where('ui.meta_value',$coachSport)
           ->where('u.verified','1')
            ->where('u.user_approved','ACTIVE')
            ->whereIn('u.role_id',[2,3])->select('u.id')->get()->toArray();
            // echo '<pre>';
            // print_r(($users) );die;
            // $data['users'] = $users;
            $usersAll = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','sport')
            ->where('u.user_approved','ACTIVE')
            ->where('u.verified','1')
            ->where('ui.meta_value',$coachSport)
            ->whereIn('u.role_id',[2,3])->distinct('user_id')->get();
            // echo '<pre>';
            // print_r( $data );die;
        } else{
            $users = User::whereIn('role_id',[2,3])
                    ->where('verified','1')
                    ->select('id')->get()->toArray();
            // $data['users'] = $users;
            $usersAll = User::whereIn('role_id',[2,3])->get();
           
        }

            $data['page'] = Pages::where('slug', '=', 'users-list')->first();
            $data['page_id'] = $data['page']->id;
        if(\Auth::User()->role_id == 4){
            $validUsers = array();
            foreach ($usersAll as $key => $value) {
                $date = new \DateTime($value->created_at);
                $now = new \DateTime();
                $current_date = $now->format('Y/m/d 23:59:59'); 
                $now2 = new \DateTime($current_date);
                $diff = $date->diff($now2)->format("%d");
                // echo '<pre>';
                $sub = User::find($value->id);
                if(( $diff < 14 || $sub->is_paid == 'yes') || $sub->subscribed('main') ){
                    $validUsers[] = $value->id;
                }

            }
            // $data['users'] = User::whereIn('id',$validUsers)->paginate(8);
            // $data['usersAll'] = User::whereIn('id',$validUsers)->get();
            $usersAllnew = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$validUsers)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('id')->get();
            $userspage = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$validUsers)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('id')->paginate(8);
            $data['users'] = $userspage;
            $data['usersAll'] = $usersAllnew;
           
            $data['allSports'] = Sport::where('status','PUBLISHED')->where('id',$coachSport)->orderBy('title','ASC')->get();
        }else{
            
            // echo "<pre>";
            // print_r($users);die;
            $usersAllnew = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$users)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('user_id')->get();
            $userspage = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$users)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('user_id')->paginate(8);
            $data['users'] = $userspage;
            $data['usersAll'] = $usersAllnew;
           
            
            $data['allSports'] = Sport::where('status','PUBLISHED')->orderBy('title','ASC')->get();
        }
            // print_r($users);die;
            return view('users_list', $data);
        }else{
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    
    public function school_search(Request $request = null){
        ini_set('max_execution_time', 1000); //1000 seconds = 16:40 minutes
        if(\Auth::User()->role_id != 4){
            $templatedata = self::getTemplates(31);
        }else{
            $templatedata = self::getTemplates(30) ;
            $coachSport = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','sport')->pluck('meta_value')->first();
        }
        if(Auth::User()->role_id != 4){
            $userData = Auth::User();
            $data = array();
            $authGender = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','athelete_gender')->first();

            $sportid = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','sport')->first();
            // print_r('CoachIDs');
            // die;
            if(Auth::User()->role_id != 1){
                if((isset($authGender->meta_value)?$authGender->meta_value:'') != ''){
                    if((isset($sportid->meta_value)?$sportid->meta_value:'') != ''){
                        $CoachsSchools = \DB::table('user_information as ui')
                        ->join('users as us', 'ui.user_id','us.id')
                        ->join('coachschool as u', 'ui.user_id','u.user_id')
                        ->where('ui.meta_key','coach_gender')->where('u.sport_id',$sportid['meta_value'])->where('ui.meta_value' ,isset($authGender->meta_value)?$authGender->meta_value:'')->orWhere('ui.meta_value' ,'Both')->where('ui.meta_key','coach_gender')->where('u.sport_id',$sportid['meta_value'])->where('us.user_approved','ACTIVE')->distinct()->pluck('school_id')->toArray();
                    }else{
                        $CoachsSchools = array();
                    }
                }else{
                    if((isset($sportid->meta_value)?$sportid->meta_value:'') != ''){
                        $CoachsSchools = \DB::table('users as us')
                        ->join('coachschool as u', 'us.id','u.user_id')
                        ->where('us.user_approved','ACTIVE')
                        ->where('u.sport_id',$sportid['meta_value'])->distinct()->pluck('school_id')->toArray(); 
                    }else{
                        // $CoachsSchools = \DB::table('users as us')
                        // ->join('coachschool as u', 'us.id','u.user_id')
                        // ->where('us.user_approved','ACTIVE')
                        // ->distinct()->pluck('school_id')->toArray();
                        $CoachsSchools = array();

                    }
                }
            }else{
                $CoachsSchools = CoachsSchool::distinct()->pluck('school_id')->toArray();
            }
       
        if($request->all()){
            $data['school_name'] = $request->school_name;
            $data['school_city'] = $request->school_city;
            $data['state'] = $request->stateFilter;
            $data['zipcode'] = $request->zipcode;

            $ApiData = ApiData::orderBy('school_name','ASC');
                if($request->school_name != ''){
                    $ApiData->where('school_name','LIKE','%'.$request->school_name.'%');
                }
                if($request->school_city != ''){
                   $ApiData->where('city','LIKE','%'.$request->school_city.'%');
                }
                if($request->stateFilter != ''){
                    $ApiData->where('state','=',$request->stateFilter);
                }
                if($request->zipcode != ''){
                    $ApiData->where('zip','LIKE','%'.$request->zipcode.'%');
                }
                if($request->school_size != ''){
                    $ApiData->where('school_size_total','LIKE','%'.$request->school_size.'%');
                }
                if(Auth::User()->role_id != 1){
                $authSport = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','sport')->first();
                }
                if($request->division){
                    $CoachsSchoolsnew = CoachsSchool::where('division',$request->division);
                    if(isset($authSport->meta_value)){
                        $CoachsSchoolsnew->where('sport_id',$authSport->meta_value);
                    }
                    $CoachsSchoolsnew = $CoachsSchoolsnew->distinct()->pluck('school_id')->toArray();
                    $CoachsSchools = array_intersect($CoachsSchoolsnew, $CoachsSchools);

                }
                if($request->conference){
                    $CoachsSchoolsnew1 = CoachsSchool::where('conference',$request->conference);
                    if(isset($authSport->meta_value)){
                        $CoachsSchoolsnew1->where('sport_id',$authSport->meta_value);
                    }
                    $CoachsSchoolsnew1 = $CoachsSchoolsnew1->distinct()->pluck('school_id')->toArray();
                    $CoachsSchools = array_intersect($CoachsSchoolsnew1, $CoachsSchools);
                     
                }
            if(Auth::User()->role_id != 1){
                $schoolAll = $ApiData->whereIn('id',$CoachsSchools)->pluck('id')->toArray();
                $totalUsA = CoachsSchool::whereIn('school_id',$schoolAll)->distinct()->pluck('user_id')->toArray();
                // $totalUsers = User::select('id')->whereIn('id',$totalUsA)->get()->toArray();

                $total =  $ApiData->whereIn('id',$CoachsSchools)->count();
                $ApiData = $ApiData->orderBy('school_name','ASC')->whereIn('id',$CoachsSchools)->paginate(10);
            }else{
                $totala =  $ApiData->orderBy('school_name','ASC');
                if(isset($CoachsSchoolsnew )){
                    $totala->whereIn('id',$CoachsSchoolsnew );
                }
                if(isset($CoachsSchoolsnew1 )){
                    $totala->whereIn('id',$CoachsSchoolsnew1);
                }
                $total = $totala->count();

                $schoolAll = $totala->pluck('school_id')->toArray();
                $totalUsA = array();

                $ApiData = $ApiData->orderBy('school_name','ASC');
                if(isset($CoachsSchoolsnew )){
                    $ApiData->whereIn('id',$CoachsSchoolsnew );
                }
                if(isset($CoachsSchoolsnew1 )){
                    $ApiData->whereIn('id',$CoachsSchoolsnew1);
                }
                $ApiData = $ApiData->paginate(10);
            }
        }else{
            if(Auth::User()->role_id != 1){
                    $totalUsA = CoachsSchool::whereIn('school_id',$CoachsSchools)->distinct()->pluck('user_id')->toArray();
                    // $totalUsers = User::select('id')->whereIn('id',$totalUsA)->get()->toArray();
                    $total = ApiData::whereIn('id',$CoachsSchools)->get()->count();
                    $ApiData = ApiData::whereIn('id',$CoachsSchools)->orderBy('school_name','ASC')->paginate(10);
            }else{
                $totalUsA = array();
                    // $totalUsers = User::select('id')->whereIn('id',$totalUsA)->get()->toArray();
                $total = ApiData::count();
                $ApiData = ApiData::orderBy('school_name','ASC')->paginate(10);
            }
    
        }
        $dataconferences = CoachsSchool::select('conference')->where('conference','!=',''  );
            if( ( (isset($sportid->meta_value)?$sportid->meta_value:'') != '' ) && Auth::User()->role_id != 1 ){
                $dataconferences->where('sport_id',$sportid->meta_value);
            }
            if( ( (isset($authGender->meta_value)?$authGender->meta_value:'') != '' ) && Auth::User()->role_id != 1 ){
                $dataconferences->where('gender',$authGender->meta_value);
            }
        $dataconferencesnew = $dataconferences->distinct('conference')->orderBy('conference','ASC')->get()->pluck('conference')->toArray();
         $allsizes = ApiData::select('school_size_total')->where('school_size_total','!=',''  )->distinct('school_size_total')->get()->toArray();
         foreach($allsizes as $schoolSize){ 
                $size_array[] = $schoolSize['school_size_total'];
         }
        sort($size_array,  SORT_NUMERIC);
        $data['schoolSizes'] = $size_array;
        sort( $dataconferencesnew , SORT_NATURAL);
        $data['conferences'] = $dataconferencesnew;
        // echo "<pre>"; print_r($dataconferencesnew);die;
        if(Auth::User()->role_id != 1){
            if((isset($authGender->meta_value)?$authGender->meta_value:'') != ''){
                if((isset($sportid->meta_value)?$sportid->meta_value:'') != ''){
                    $CoachIDs = \DB::table('user_information as ui')
                    ->join('users as us', 'ui.user_id','us.id')
                    ->join('coachschool as u', 'ui.user_id','u.user_id')
                    ->where('ui.meta_key','coach_gender')->where('u.sport_id',$sportid['meta_value'])->where('ui.meta_value' ,isset($authGender->meta_value)?$authGender->meta_value:'')->orWhere('ui.meta_value' ,'Both')->where('ui.meta_key','coach_gender')->where('u.sport_id',$sportid['meta_value'])->where('us.user_approved','ACTIVE')->distinct()->pluck('u.user_id')->toArray();
                }else{
                    $CoachIDs = array();
                }
            }else{
                 if((isset($sportid->meta_value)?$sportid->meta_value:'') != ''){
                    $CoachIDs = \DB::table('users as us')
                    ->join('coachschool as u', 'us.id','u.user_id')
                    ->where('us.user_approved','ACTIVE')
                    ->where('u.sport_id',$sportid['meta_value'])->distinct()->pluck('u.user_id')->toArray();
                }else{
                    //  $CoachIDs = \DB::table('users as us')
                    // ->join('coachschool as u', 'us.id','u.user_id')
                    // ->where('us.user_approved','ACTIVE')
                    // ->distinct()->pluck('u.user_id')->toArray();
                    $CoachIDs = array();
                }
            }
        }else{
            $CoachIDs = CoachsSchool::distinct()->pluck('user_id')->toArray();
        }

        $totalUsAOri = array_intersect($CoachIDs, $totalUsA);
        $totalUsers = User::select('id')->whereIn('id',$totalUsAOri)->get()->toArray();
        // print_r(count($totalUsers));die;
        $data['ApiData'] = $ApiData;
        $data['total'] = $total;
        $data['totalUserIds'] = $totalUsers;
        $data['page'] = Pages::where('slug', '=', 'school-search')->first();
        $data['page_id'] = $data['page']->id;
        $data['templatedata'] = $templatedata;
        
        return view('school_search', $data);
        }else{
            $post = Post::paginate(4);
            $userData['posts'] = $post;
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    
    public function profileacadmic( Request $request ){

        $logged_user = Auth::User();
        $user_id = $logged_user->id;
        $delete = AcademicDetailAthelete::where('user_id', $user_id)->delete();

       $schoollistarray = array();
        foreach ($request->academics as $key => $value) {


            $found = new AcademicDetailAthelete();
            
           
            if(isset($value['school_id'])?$value['school_id']:'' != ''){
                $found->school_id = $value['school_id'];
            }else{
                $found->school_id = '';
            }
           
            if(isset($value['qualification'])?$value['qualification']:'' != ''){
            $found->qualification = $value['qualification'];
            }else{
            $found->qualification = '';
            }
            if(isset($value['city'])?$value['city']:'' != ''){
            $found->city = $value['city'];
            }else{
            $found->city = '';
            }
            if(isset($value['state'])?$value['state']:'' != ''){
            $found->state = $value['state'];
            }else{
            $found->state = '';
            }
            if(isset($value['location'])?$value['location']:'' != ''){
            $found->location = $value['location'];
            }else{
            $found->location = '';
            }
            if(isset($value['zip'])?$value['zip']:'' != ''){
            $found->zip = $value['zip'];
            }else{
            $found->zip = '';
            }
          
             $found->user_id = $user_id;

             $found->gpa = $value['gpa'];
             $found->act = $value['act'];
             $found->sats = $value['sats'];
             $found->math = $value['math'];
             $found->act = $value['act'];
             $found->verbal = $value['verbal'];
             $found->save();
        }
     $attendeesArrayFull = \DB::table('academicdetailathelete')->where('user_id',$user_id)->get();

        $progress['value'] = $attendeesArrayFull;
        $progress['prog'] = self::progress();
        $progress['schoollist'] = $schoollistarray;
        return $progress;
    }
    public function deleteacadmic( Request $request ){

        $delete = AcademicDetailAthelete::where('id', $request->key)->delete();
        $progress = self::progress();
        return $progress;
       
    }
    
    
   public function usersListSearch(Request $request){
        $data = array();
            if(\Auth::User()->role_id != 4){
                $templatedata = self::getTemplates(31);
            }else{
                $templatedata = self::getTemplates(30) ;
                $coachSport = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','sport')->pluck('meta_value')->first();
            }
            $data['templatedata'] = $templatedata;
        $data['state'] = $request->stateFilter;
        $data['graduation'] = $request->graduationFilter;
        $data['gender'] = $request->genderFilter;
        $data['height'] = $request->heightFilter;
        $data['weight'] = $request->weightFilter;
        $data['gpa'] = $request->gpa;
        $data['act'] = $request->act;
        $data['position'] = $request->position;
        $data['sportsFilter'] = $request->sportsFilter;
          
        $resultArray = User::whereIn('role_id',[2,3])->pluck('id')->toArray();
        $resultArray2 = User::whereIn('role_id',[2,3])->pluck('id')->toArray();
       
        
        // if($request->athlete_name != ''){
 
        //     $usersInfo1 = User::select('id');
        //     $usersInfo1->where('name' ,'LIKE', '%'.$request->athlete_name.'%');
        //     $usersInfoArray1 = $usersInfo1->pluck('id')->toArray();
        //     $resultArray6 = array_intersect($usersInfoArray1,$resultArray);  
        //     // print_r($resultArray6);die;  
        // }
        if($request->athlete_name != ''){
            $athlete_named = explode(' ', $request->athlete_name);
            $athlete_namef = $athlete_named[0];
            $athlete_namel = isset($athlete_named[1])?$athlete_named[1]:'';

            $usersInfo1 = UserInformation::select('user_id');
            $usersInfo1->where('meta_key','athelete_firstname')->where('meta_value' ,'LIKE', '%'.$athlete_namef.'%')
            ->orWhere('meta_key','athelete_lastname')->where('meta_value' ,'LIKE', '%'.$athlete_namef.'%');
            if($athlete_namel){
               $usersInfo2 = UserInformation::select('user_id');  
            $usersInfo2->orWhere('meta_key','athelete_lastname')->where('meta_value' ,'LIKE', '%'.$athlete_namel.'%');
            $usersInfoArray13 = $usersInfo2->pluck('user_id')->toArray();
            }
            $usersInfoArray14 = $usersInfo1->pluck('user_id')->toArray();
            if(isset($usersInfoArray13)){
                $resultArray5 = array_intersect($usersInfoArray13,$usersInfoArray14); 
            }else{
                $resultArray5 = $usersInfoArray14;
            }
            // $resultArraynew = array_merge($resultArray5,$resultArray6);

            $resultArray = array_intersect($resultArray5,$resultArray);  
            // print_r($resultArraynew);die;
            // print_r($usersInfoArray14);die;
        }
        if($request->sportsFilter != ''){
            $usersInfo1 = UserInformation::select('user_id');
            $usersInfo1->where('meta_key','all_sports')->whereRaw("find_in_set(".$request->sportsFilter.",meta_value)");
            $usersInfo1->orWhere('meta_key','sport')->where('meta_value',$request->sportsFilter);
            $usersInfoArray = $usersInfo1->pluck('user_id')->toArray();
            $resultArray = array_intersect($usersInfoArray,$resultArray);  
        }
        if($request->graduationFilter != ''){
            $usersInfo1 = UserInformation::select('user_id');
            $usersInfo1->where('meta_key','graduation')->where('meta_value' , $request->graduationFilter);
            $usersInfoArray1 = $usersInfo1->pluck('user_id')->toArray();
            $resultArray = array_intersect($usersInfoArray1,$resultArray);  
        }
        if($request->position != ''){
            $usersInfo22 = UserInformation::select('user_id');
            $usersInfo22->where('meta_key','position')->where('meta_value' , $request->position);
            $usersInfoArray22 = $usersInfo22->pluck('user_id')->toArray();
            $resultArray = array_intersect($usersInfoArray22,$resultArray);  
        }
        if($request->genderFilter != ''){
            $usersInfo2 = UserInformation::select('user_id');
            $usersInfo2->where('meta_key','athelete_gender')->where('meta_value' , $request->genderFilter);
            $usersInfoArray2 = $usersInfo2->pluck('user_id')->toArray();
            $resultArray = array_intersect($usersInfoArray2,$resultArray);  
        } 
        // if($request->heightFilter != ''){
        //     $heightFilterVal = explode('-', $request->heightFilter);
        //     $heightFilterVal1 = intval($heightFilterVal[0] - 36);
        //     $heightFilterVal2 = intval($heightFilterVal[1] - 36);
        //     if($heightFilterVal1 > 0 || $heightFilterVal2 > 0){
        //         $usersInfo3 = UserInformation::select('user_id');
        //         $usersInfo3->where('meta_key','athelete_height')->whereBetween('meta_value' ,[ $heightFilterVal1,$heightFilterVal2 ]);
        //         $usersInfoArray3 = $usersInfo3->pluck('user_id')->toArray();
        //         $resultArray = array_intersect($usersInfoArray3,$resultArray); 
        //     }
        // }
        // if($request->weightFilter != ''){
        //     $strg = str_replace('lbs', '', $request->weightFilter);
        //     $weightFilterVal = explode('-', $request->weightFilter);
        //     $weightFilterVal1 = intval($weightFilterVal[0]); ;
        //     $weightFilterVal2 = intval($weightFilterVal[1]);
             
        //     if($weightFilterVal1 > 0 || $weightFilterVal2 > 0){
        //         $usersInfo4 = UserInformation::select('user_id');
        //         $usersInfo4->where('meta_key','athelete_weight')
        //         ->whereBetween('meta_value' ,[ $weightFilterVal1,$weightFilterVal2 ]);
        //         $usersInfoArray4 = $usersInfo4->pluck('user_id')->toArray();
        //         $resultArray = array_intersect($usersInfoArray4,$resultArray); 
        //     }
        // }
        if($request->stateFilter != ''){
            $usersInfo5 = UserInformation::select('user_id');
            $usersInfo5->where('meta_key','state')->where('meta_value' , $request->stateFilter);
            $usersInfoArray5 = $usersInfo5->pluck('user_id')->toArray();
            $resultArray = array_intersect($usersInfoArray5,$resultArray); 
        } //print_r($resultArray);
        if($request->gpa != ''){
            $usersInfo6 = AcademicDetailAthelete::select('user_id')->where('gpa','>=',$request->gpa)->get();
            $usersInfoArray6 = $usersInfo6->pluck('user_id')->toArray();
            $resultArray = array_intersect($usersInfoArray6,$resultArray);
            //print_r($resultArray); 
        }
        if($request->act != ''){
            //echo $request->act;
            $usersInfo7 = AcademicDetailAthelete::select('user_id')->where('act','>=',$request->act)->get();
            $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
            $resultArray = array_intersect($usersInfoArray7,$resultArray); 
           
        }         
        if($request->paiduser != ''){
            $usersInfo8 = User::whereIn('role_id',[2,3])->get();
            foreach ($usersInfo8 as $key => $value) {
                $user = User::find($value->id);
                if( $request->paiduser == 'admin' ){
                    if( ( !$user->subscribed('main') ) && $value->is_paid == 'yes' ){
                        $usersInfoArray8['paidByAdmin'][] = $value->id; 
                    }else{
                    }
                }else{
                }
                if( $request->paiduser == 'yes' ){
                    if( $user->subscribed('main') ){
                        $usersInfoArray8['paid'][] = $value->id; 
                    }else{
                    }
                }else{
                }
                if( $request->paiduser == 'no' ){
                    if( ( ! $value->subscribed('main') ) && $request->paiduser == $value->is_paid ){
                        $usersInfoArray8['nonpaid'][] = $value->id; 
                    }else{
                    }
                }else{
                }
            }
            if( $request->paiduser == 'yes' ){
                $usersInfoArray9 = $usersInfoArray8['paid']; 
            }else if($request->paiduser == 'admin'){
                $usersInfoArray9 = $usersInfoArray8['paidByAdmin']; 
            }else{
                $usersInfoArray9 = $usersInfoArray8['nonpaid']; 
            }
            $resultArray = array_intersect($usersInfoArray9,$resultArray); 
        } 
        if(count($resultArray) > 0){

            if(Auth::User()->role_id == 1){
               $authusergen = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','athelete_gender')->first();
           }else{
              $authusergen = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','coach_gender')->first();
             
              //print_r($coachSport);die;
           }
     if((isset($authusergen->meta_value)?$authusergen->meta_value:'')  != '' && ( (isset($authusergen->meta_value)?$authusergen->meta_value:'')  != 'Both' && Auth::User()->role_id != 1 ) ){
                
            $coachSportUsers = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','sport')
            ->where('ui.meta_value',$coachSport)
            ->whereIn('u.role_id',[2,3])->get()->pluck('user_id')->toArray();
            $users = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','athelete_gender')
            ->where('ui.meta_value',$authusergen->meta_value)
            ->whereIn('u.id',$resultArray)
            ->whereIn('ui.user_id',$coachSportUsers)
            ->whereIn('role_id',[2,3])
            ->where('u.user_approved','ACTIVE')
            ->where('u.verified','1')
            ->select('u.id')->get()->toArray();

            $usersAll = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('meta_key','athelete_gender')
            ->where('meta_value',$authusergen->meta_value)
            ->whereIn('u.id',$resultArray)
            ->whereIn('ui.user_id',$coachSportUsers)
            ->where('u.user_approved','ACTIVE')
            ->where('u.verified','1')
            ->whereIn('u.role_id',[2,3])->get();
           
        }else if( ( (isset($authusergen->meta_value)?$authusergen->meta_value:'')  == '' ||  (isset($authusergen->meta_value)?$authusergen->meta_value:'')  == 'Both' ) && Auth::User()->role_id != 1 ){
              
            $users = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','sport')
            ->where('ui.meta_value',$coachSport)
            ->whereIn('u.id',$resultArray)
            ->whereIn('role_id',[2,3])
            ->where('u.user_approved','ACTIVE')
            ->where('u.verified','1')
            ->select('u.id')->get()->toArray();;
         
           
            $usersAll = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->where('ui.meta_key','sport')
            ->where('ui.meta_value',$coachSport)
            ->whereIn('u.id',$resultArray)
            ->where('u.user_approved','ACTIVE')
            ->where('u.verified','1')
            ->whereIn('u.role_id',[2,3])->get();
           
        }else{
            $users = User::whereIn('role_id',[2,3])->whereIn('id',$resultArray)->select('id')->get()->toArray();;  
            $usersAll =  User::whereIn('role_id',[2,3])->whereIn('id',$resultArray)->get(); 
            
        }
           
        }else{
         $users = User::whereIn('role_id',[10])->select('id')->get()->toArray();; 
         // $data['users'] = $users; 
         $usersAll =  User::whereIn('role_id',[10])->get();

         
        }
        // $data['users'] = $users;
        // $data['usersAll'] = $usersAll;
      
        if(\Auth::User()->role_id == 4){
            $validUsers = array();
            foreach ($usersAll as $key => $value) {
                $date = new \DateTime($value->created_at);
                $now = new \DateTime();
                $current_date = $now->format('Y/m/d 23:59:59'); 
                $now2 = new \DateTime($current_date);
                $diff = $date->diff($now2)->format("%d");
                // echo '<pre>';
                $sub = User::find($value->id);
                if(( $diff < 14 || $sub->is_paid == 'yes') || $sub->subscribed('main')  ){
                    $validUsers[] = $value->id;
                }

            }
            // $data['users'] = User::whereIn('id',$validUsers)->paginate(8);
            // $data['usersAll'] = User::whereIn('id',$validUsers)->get();
            $usersAllnew = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$validUsers)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('user_id')->get();
            $userspage = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$validUsers)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('user_id')->paginate(8);
            $data['users'] = $userspage;
            $data['usersAll'] = $usersAllnew;
            $data['allSports'] = Sport::where('status','PUBLISHED')->where('id',$coachSport)->orderBy('title','ASC')->get();
        }else{
            // $data['users'] = $users;
            // $data['usersAll'] = $usersAll;
           
             $usersAllnew = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$users)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('user_id')->get();
            $userspage = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id','u.id')
            ->whereIn('u.id',$users)
            ->where('meta_key', '=', 'athelete_firstname')
            ->where('meta_value', '!=', '')
            ->whereIn('u.role_id',[2,3])->distinct('user_id')->paginate(8);
            $data['users'] = $userspage;
            $data['usersAll'] = $usersAllnew;
            $data['allSports'] = Sport::where('status','PUBLISHED')->orderBy('title','ASC')->get();
        }

    // print_r($users);die;
    $data['page'] = Pages::where('slug', '=', 'users-list')->first();
    $data['page_id'] = $data['page']->id;
    
    return view('users_list', $data);
    }
    public function profileShow( $slug ){
        $data = array();
        $data_filled = \App\UserInformation::where( 'user_id', Auth::user()->id )->where('meta_key', 'profile_filled')->where('meta_value', 'filled')->first();
            if( $data_filled == '' && Auth::user()->role_id == 3){
                // print_r(  );die;
                return redirect()->intended('/complete-registration#1');;
            }
        $userData = User::where('user_slug',$slug)->first();
        if($userData){
            
            $userInfo = UserInformation::where('user_id',$userData->id)->get();
            if(Auth::User()->id !=  $userData->id && Auth::User()->role_id != 1){
                    $userD = User::where('user_slug',$slug)->where('user_approved','ACTIVE')->first();
                    if(!$userD){
                       return Redirect('/user-not-verified');
                    }

            }
                if($userData->role_id != 4){
                    $templatedata = self::getTemplates(30);
                }else{
                    $templatedata = self::getTemplates(31) ;
                }
            $data['templatedata'] = $templatedata;

            if(  ($userData->role_id != 1 && Auth::User()->role_id != 1 ) && (( $userData->id != Auth::User()->id )  && ( $userData->role_id !=  Auth::User()->role_id  ) ) )  { 
                $currentDate = date('Y-m-d H:i:s');
                $currentDate1hour = date('Y-m-d H:i:s' ,strtotime('-1 hour',strtotime($currentDate)));
                $new = 0;
                $notification = Notification::where('user_id',$userData->id)->where('viewer_user_id',Auth::User()->id)->whereBetween('created_at' , [$currentDate1hour,$currentDate] )->first();
                if(!$notification){
                    $notification = new Notification();
                    $new = 1;
                }
                $notification->user_id = $userData->id ;
                $notification->viewer_user_id = Auth::User()->id ;
                $notification->key = 'profile_viewed' ;
                $notification->updated_at = date('Y-m-d H:i:s') ;
                $notification->save();

                $date = new \DateTime($userData->created_at);
                $now = new \DateTime();
                $current_date = $now->format('Y/m/d 23:59:59'); 
                $now2 = new \DateTime($current_date);
                $diff = $date->diff($now2)->format("%d");

                if(( $new == 1  ) &&  ( (  $userData->subscribed('main')  || $userData->is_paid == 'yes' || $diff < 14 ) && ( ( $userData->role_id != 4 ) && ( \Auth::User()->user_approved == 'ACTIVE' ) ) ) ){
                    $userviewer = Auth::User();
                    Mail::to($userData->email)->send( new profileVisitNotification( $userData,$userviewer , $notification->id)); 
                }
            }

            $data['userData'] = $userData ;
            $data['page'] = Pages::where('slug', '=', 'profile')->first();
            $data['page_id'] = $data['page']->id;
            $userInfoArray = array();
            foreach($userInfo as $info){
               $userInfoArray[$info->meta_key] = $info->meta_value;
            }
             $data['userInfoArray'] = $userInfoArray;
             
            if($userData->role_id == '4'){
                $data['schoolData'] = CoachsSchool::where('user_id',$userData->id)->orderBy('id','DESC')->get();
                $currentDate = Date('m/d/Y');
                $data['events'] = \DB::table('sport_event_meta as ui')
                       ->join('sport_events as u', 'ui.event_id', '=', 'u.id')
                       ->where('ui.key','coach_register')
                       ->whereRaw("find_in_set(".$userData->id.",details)")
                       ->where('u.event_end','>=',$currentDate)
                       ->get();
                       // print_r($data['events']);die;
                return view('coach-profile-show',$data); 
            }else{
                return view('profile-show',$data);
            }
        }else{
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    public function progress(){
        $logged_user = Auth::User();
        if($logged_user->role_id == 4){
            $athlete = array(
            'coach_firstname'=>'5','coach_lastname'=>'5','coach_email'=>'5','sport'=>'5','phone'=>'3','coach_www'=>'2','coach_ncaa'=>'5','coach_ncaa_n_t'=>'5','coach_ncaa_f_f'=>'5','coach_ncaa_p_s'=>'5','coach_ncaa_c_c'=>'5','players'=>'5'
            ,'bio'=>'5','coach_gender'=>'5','coach_title'=>'5','school'=>'5','videogallery'=>'5'
            ,'user_profile_image'=>'5','gallery'=>'5','coachings'=>'10');
        
        }else{   
            $athlete = array(
                'parents'=>'5','sportsdata'=>'10','academics'=>'5','testimonails'=>'5','athelete_acadmic_achivment'=>'5','extra_curricular'=>'5','athletic_achievements'=>'5','coaches'=>'5','athelete_birth'=>'2'
            ,'athelete_email'=>'3','athelete_phone'=>'3','athelete_gender'=>'3','athelete_address'=>'3','athelete_firstname'=>'3','athelete_lastname'=>'3','sport'=>'3','city'=>'3','state'=>'3','zip'=>'3'
            ,'graduation'=>'3','position_name'=>'3','school'=>'2','gallery'=>'3','videogallery'=>'3','user_profile_image'=>'3','athelete_weight'=>'3','athelete_height'=>'3');
        }
        // ,'athelete_facebook'=>'1','athelete_twitter'=>'1'
        //     ,'athelete_instagram'=>'1','athelete_snapchat'=>'1','athelete_skype'=>'1'
        $progress = 0;
        foreach($athlete as $key=> $value){
            $found = UserInformation::where('user_id', $logged_user->id)->where('meta_key', $key)->where('meta_value', '<>','')->where('meta_value', '<>','[]')->where('meta_value', '<>','NA')->first();
            if($key == 'academics'){
                $found = '';
                $found = AcademicDetailAthelete::where('user_id', $logged_user->id)->first();  
            }
            if($key == 'coachings'){
                $found = '';
                $found = CoachsSchool::where('user_id', $logged_user->id)->first();  
            }

            if($found != '' ){
                  $progress = $progress + $value;  
            }
        }
        $userInfoMeta = UserInformation::where('user_id',$logged_user->id)->where('meta_key','progress_data')->first();
        if(!$userInfoMeta){
            $userInfoMeta = new UserInformation();
        }
        $userInfoMeta->user_id = $logged_user->id;
        $userInfoMeta->meta_key = 'progress_data';
        $userInfoMeta->meta_value = $progress;
        $userInfoMeta->save();
        
        return $progress;
    }
    public function progresscomplete(){
        $logged_user = Auth::User();
        if($logged_user->role_id == 3 || $logged_user->role_id == 2 || $logged_user->role_id == 1){
            $athlete = array(
            'parents'=>'50','athelete_firstname'=>'25','athelete_lastname'=>'25');
        }
        $progress = 0;
        foreach($athlete as $key=> $value){
            $found = UserInformation::where('user_id', $logged_user->id)->where('meta_key', $key)->where('meta_value', '<>','')->where('meta_value', '<>','[]')->where('meta_value', '<>','NA')->first();

            if($found != '' ){
                  $progress = $progress + $value;  
            }
        }
        
        return $progress;
    }
    
    public function exportToPdf( $slug ){
        if(!Auth::User()){
            return Redirect('/login');
        }
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        $data = array();
        $user = User::where('user_slug',$slug)->first();

        if($user){    
           
            $data['userData'] = $user;
            $userInfo = UserInformation::where('user_id',$user->id)->get();
           
            $userInfoArray = array();
            foreach($userInfo as $info){
               $userInfoArray[$info->meta_key] = $info->meta_value;
            }
            $data['academics'] = AcademicDetailAthelete::where('user_id',$user->id)->first();
            $data['userInfoArray'] = $userInfoArray;
            // $pdf = \PDF::loadView('profilePDF', $data);
            // return $pdf->stream();
              if($user->role_id == 4){
                $data['schoolData'] = CoachsSchool::where('user_id',$user->id)->orderBy('id','DESC')->get();

                 $pdf = \App::make('dompdf.wrapper');
                 $pdf->setOptions(['isRemoteEnabled'=> true,'defaultFont' => 'rockwell']);
                $pdf->loadView('profilePDFCoach', $data);
                $pdf->setPaper('a4', 'portrait');
                return $pdf->stream();
                return $pdf->download('profilePDFCoach');


                return view('profilePDFCoach',$data);
            }else{
                 $pdf = \App::make('dompdf.wrapper');
                 $pdf->setOptions(['isRemoteEnabled'=> true,'defaultFont' => 'rockwell']);
                 $pdf->setPaper('a4', 'portrait');
                $pdf->loadView('profilePDF', $data);
                
                return $pdf->stream();
                return $pdf->download('profilePDF');

                return view('profilePDF',$data);
            }
        }else{
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    
    public function coachimport(){
         if(!Auth::User()){
            return Redirect('/login');
        }
        $data = array();
        return view('coach_import',$data);
        
    }
    public function coachschool( Request $request ){
            
                $logged_user = Auth::User();
                $user_id = $logged_user->id;
                $schoollistarray = array();
                $delete = CoachsSchool::where('user_id', $user_id)->delete();
                $arrayD = array_reverse($request->coachings);
                
                foreach ($arrayD as $key => $value) {
                    $scholId ='';
                  
                     
                       if(isset($value['school_input'])){
                   // $value['school_input'] = explode(' - ',$value['school_input'])[0];
                   //  $value['school_input'] = explode(', ',$value['school_input'])[0];
                       // $scholllist = ApiData::where('school_name','=',$value['school_input'])->first();
                            $scholllist = ApiData::where('school_name',$value['school_input']);
                            if(isset($value['city'])){
                                $scholllist->where('city',$value['city']);
                            }else{
                                $scholllist->where('city','');
                            }
                             if(isset($value['state'])){
                                $scholllist->where('state',$value['state']);
                            }else{
                                $scholllist->where('state','');
                            }
                            $scholllist = $scholllist->first();
                        if(!$scholllist){
                            $slug =  strtolower(str_replace(" ","-",$value['school_input']));
                                    $schoolslug = ApiData::where('school_name', $value['school_input'])->get()->count();
                                    if($schoolslug >= 1){
                                        $srnd = random_int(11,99);
                                        $slug = $slug."-".$srnd;
                                    }
                            $scholllist = new ApiData();
                            $scholllist->school_name = $value['school_input'];
                            $scholllist->school_slug = $slug;
                            if(isset($value['city'])){
                              $scholllist->city = $value['city'];
                            }
                            if(isset($value['state'])){
                                $scholllist->state = $value['state'];
                             }
                            if(isset($value['school_tuition'])){
                                $scholllist->tuition_in_state = $value['school_tuition'];
                            }
                            if(isset($value['tuition_room'])){
                                $scholllist->tuition_revenue_per_fte = $value['tuition_room'];
                            }
                            if(isset($value['zip'])){
                                $scholllist->zip = $value['zip'];
                            }
                            $scholllist->save();
                            $scholId = $scholllist->id;
                            }else{

                           $scholId = $scholllist->id;
                        }
                            

                    }
                    if(isset($value['id'])){
                         $found = CoachsSchool::where('id', '=', $value['id'])->first();
                     }else{
                         $found = CoachsSchool::where('id', '=', '')->first();
                     }
                     if(!$found){
                          $found = new CoachsSchool();
                     }
                     if($scholId != ''){
                        $found->school_id = $scholId;
                     }else if(isset($value['school_id'])){
                        $found->school_id = $value['school_id'];
                     }else{
                        $found->school_id = '';
                     }

                    $found->user_id = $user_id;
                    //$found->school_id = $value['school_id'];
                    if(isset($value['address1'])){
                         $found->address1 = $value['address1'];
                     }
                     if(isset($value['address2'])){
                         $found->address2 = $value['address2'];
                     }
                     if(isset($value['phone'])){
                          $found->phone = $value['phone'];
                     }
                    
                     if(isset($value['fax'])){
                         $found->fax = $value['fax'];
                     }
                     if(isset($value['div_code'])){
                          $found->div_code = $value['div_code'];
                     }
                     if(isset($value['school_url'])){
                           $found->school_url = $value['school_url'];
                           $dataweb = UserInformation::where('user_id', '=', $user_id)->where('meta_key', '=', 'coach_www')->first();
                            if( $dataweb == '' ){
                                $dataweb = new UserInformation;


                            }
                            $dataweb->user_id = $user_id;
                            $dataweb->meta_key = 'coach_www';
                            $dataweb->meta_value = $value['school_url'];
                            $dataweb->save(); 
                     }
                     if(isset($value['division'])){
                         $found->division = $value['division'];
                     }
                     //if(isset($value['sport_id'])){
                        // $found->sport_id = $value['sport_id'];
                        $found->sport_id = 2;

                    // }
                     if(isset($value['head_coach'])){
                         $found->head_coach = $value['head_coach'];
                     }
                     if(isset($value['rank'])){
                         $found->rank = $value['rank'];
                     }
                      if(isset($value['conference'])){
                         $found->conference = $value['conference'];
                     }
                      if(isset($value['year'])){
                        $found->year = $value['year'];
                     }
                      if(isset($value['students_coached'])){
                        $found->students_coached = $value['students_coached'];
                     }
                      
                    if(isset($value['school_tuition'])){
                         $found->school_tuition = $value['school_tuition'];
                     }
                    if(isset($value['tuition_room'])){
                        $found->tuition_room = $value['tuition_room'];
                     }
                     if(isset($value['school_size'])){
                        $found->school_size = $value['school_size'];
                     }
                     if(isset($value['gender'])){
                        $found->gender = $value['gender'];
                     }
                     if(isset($value['state'])){
                        $found->state = $value['state'];
                     }
                      if(isset($value['city'])){
                        $found->city = $value['city'];
                     }
                    if(isset($value['zip'])){
                        if(strlen($value['zip']) == 4){
                            $found->zip = '0'.$value['zip'];
                        }else{
                            $found->zip = $value['zip'];
                        }
                    }
                   
                     $found->save();
                }

                $attendeesArrayFull = \DB::table('api_fetched_data as u')
                   ->join('coachschool as ui', 'ui.school_id', '=', 'u.id')
                   ->where('ui.user_id',$user_id)
                   ->orderBy('ui.id','DESC')
                   ->get();
                $progress['value'] = $attendeesArrayFull;
                $progress['prog'] = self::progress();
                $progress['schoollist'] = $schoollistarray;
                return $progress;

            }
        public function deleteacoach( Request $request ){

            $delete = CoachsSchool::where('id', $request->key)->delete();
            $progress = self::progress();
            return $progress;
           
        }
        
    public function coachimportsave(Request $request){
      
        $lines = explode("\n", $request->data_chank);
        $data = array();
        $data['data'] = $lines;

        foreach($lines as $row){
            $row = str_getcsv($row);
            // print_r(count($row));die;
            if( count($row) == 25 ){
             if(( isset($row[15])?$row[15]:'' != '' ) && $row[0] != 'SchoolID'){
                
                $newuser = User::where('email',$row[15])->first();
                if(!$newuser){
                    $newuser = new User();
                }
                    $newuser->email = $row[15];
                    $newuser->role_id = 4;    

                    $slug =  strtolower(str_replace(" ","-",$row[4]));
                    $userslug = User::where('name', $row[4])->get()->count();
                    if($userslug >= 1){
                        $rnd = random_int(11,99);
                        $slug = $slug."-".$rnd;
                    }
                    
                    $newuser->user_status = 'ACTIVE'; 
                    $newuser->user_approved = 'ACTIVE'; 
                    $newuser->name = $row[4];
                    $newuser->password = bcrypt('skjhgfdrtyuinbvcycss');
                    $newuser->user_slug = $slug;
                    $newuser->verified = '1';
                    $newuser->save();  
                    $newuser = $newuser->id;

                $school_data = ApiData::where('school_name',$row[6])->where('city',$row[10])->where('state',$row[11])->first();
                //  print_r($school_data);die;
                $zip = explode('-', $row[12]);
                if(!$school_data){
                    $slug =  strtolower(str_replace(" ","-",$row[6]));
                    $schoolslug = ApiData::where('school_name', $row[6])->get()->count();
                    if($schoolslug >= 1){
                        $srnd = random_int(11,99);
                        $slug = $slug."-".$srnd;
                    }
                    $school_data = new ApiData();
                    $school_data['school_name'] = $row[6];
                    $school_data['school_id'] = $row[0];
                    $school_data['address'] = $row[8];
                    $school_data['city'] = $row[10];
                    $school_data['state'] = $row[11];
                    $school_data['zip'] = (isset($zip[0])?$zip[0]:'');
                    $school_data['telephone'] = $row[13];
                    $school_data['school_slug'] = $slug;

                    $school_data->save();
                }
                $sports = Sport::where('title',$row[18])->first();
                if(!$sports){
                    $sports = new Sport();
                    $sports->title = $row[18];
                    $sports->author_id = Auth::User()->id;
                    $sports->save();
                }
            if($school_data){
                $schollarray =  CoachsSchool::where('school_id',$school_data->id)->where('user_id',$newuser)->first();
                if(!$schollarray){
                    $schollarray = new CoachsSchool();
                }
                $schollarray->school_id = $school_data->id;
                $schollarray->user_id = $newuser;
                $schollarray->address1 = $row[8];
                $schollarray->address2 = $row[9];
                $schollarray->city = $row[10];
                $schollarray->state = $row[11];
                $schollarray->zip = (isset($zip[0])?$zip[0]:'');
                $schollarray->phone = $row[13];
                $schollarray->fax = $row[14];
                $schollarray->div_code = $row[16];
                $schollarray->division = $row[17]; 
                $schollarray->sport_id =  $sports->id;  
                $schollarray->head_coach = $row[22]; 
                $schollarray->rank = $row[23];
                $schollarray->conference = $row[24];
                if($row[19] == 'M'){
                    $gender = 'Men';
                }else{
                    $gender = 'Women';
                } 
                $schollarray->gender = $gender;
                $schollarray->save();


                $asscArray = ['coach_firstname'=> $row[2],'coach_lastname'=>$row[3],'coach_title'=> $row[5],'school'=>$school_data->id,'address'=> $row[8],'address_2'=>$row[9],'coach_phone'=>$row[13],'fax'=> $row[14],'coach_email'=>$row[15],'division'=> $row[17],'division_code'=>$row[16],'coach_gender'=>$row[19],'rank'=> $row[23],'is_head_coach'=>$row[22],'is_imported_user' => 'yes', 'sport' => $sports->id];
                   
                foreach($asscArray as $key4=>$r){
                    $userInfo = UserInformation::where('user_id',$newuser)->where('meta_key',$key4)->first();  
                    if(!$userInfo){
                        $userInfo = new UserInformation();
                    }
                    if($key4 == 'coach_gender'){
                        if($r == 'M'){
                       $gender = 'Men';}else{
                            $gender = 'Women'; }
                       $userInfo->meta_value = $gender;
                    }else{
                        $userInfo->meta_value = $r;
                    }
                    $userInfo->user_id = $newuser;
                    $userInfo->meta_key = $key4;
                    
                    $userInfo->save();
                }
                // echo "<pre>";   print_r($asscArray);die;      
            }else{
               // return Redirect()->back()->with(['message'    => 'Coaches Data Not Imported','alert-type' => 'error',]); 
            }
        }
            print_r($data);
        }else{
            $data['status'] = 'invalid';
            return $data;
        }
        }
        //  return Redirect()->back()->with(['message'    => 'Coaches Data Imported Successfully!','alert-type' => 'success',]);
        // }else{
        //      return Redirect()->back()->with(['message'    => 'Please Select A File To Import','alert-type' => 'error',]); 
        // }
    }
 

//  public function coachimportsave(Request $request){
      
//         $lines = explode("\n", $request->data_chank);
//         print_r($lines);
//         foreach($lines as $row){
//             $row = str_getcsv($row);
//             if(( isset($row[0])?$row[0]:'' != '' ) && $row[0] != 'email'){
                
//                 $newuser = User::where('email',$row[0])->first();
//                 if($newuser){
//                 $schollarray =  CoachsSchool::where('user_id',$newuser->id)->first();
               
//                 $gender = 'Men';
//                 $schollarray->gender = $gender;
//                 $schollarray->save();

//                 $asscArray = ['coach_gender'=>'Men'];
//                 foreach($asscArray as $key4=>$r){
//                     $userInfo = UserInformation::where('user_id',$newuser->id)->where('meta_key',$key4)->first();
//                     $gender = 'Men';
//                     $userInfo->meta_value = $gender;
//                     $userInfo->user_id = $newuser->id;
//                     $userInfo->meta_key = $key4;
//                     $userInfo->save();
//                 }
              
//         }
//     }
//     }
// }


  public function getTemplates($pageID){
        $userID = Auth::User()->id;
        $templates = \App\PageMeta::where([ ['page_id', '=', $pageID] ])->orderBy('order', 'ASC')->get();
        $found = UserInformation::where('user_id', '=', $userID)->where('meta_key', '=', 'emailtemplate')->first();
        //echo "<pre>";
        $data = (array)json_decode($found['meta_value']);
        // print_r($data);
        // die();
        $j = 0;
        $returnArray = array();
        foreach($data as $value ){
            $jd = $j++;
          // print_r($value->Key);
            $returnArray[$jd ]['key'] = $value->Key;
            $returnArray[$jd]['value'] = $value->value;
            $returnArray[$jd ]['message'] = $value->message;
        }
        

        //$templates = array_merge($templates,$found);

        foreach($templates as $key=>$t){
            $returnArray1[$key]['key'] = $t->key;
            $returnArray1[$key]['value'] = $t->display_name;
            $returnArray1[$key]['message'] = $t->value;
        }
        $returnArray11 = array_merge($returnArray1,$returnArray);

        return json_encode($returnArray11);
    }
    public function notifications(){
         $userCreatedDate1 = (array) Auth::User()->created_at;
        $current_date = new \DateTime();
        $current_date = $current_date->format('Y-m-d 24:59:59');
        $userCreatedDate = new \DateTime($userCreatedDate1['date']);
        $userCreatedDate = $userCreatedDate->format('Y-m-d 00:00:00');
        $datediff = strtotime($current_date) - strtotime($userCreatedDate);
        $diff1 = floor($datediff / (60 * 60 * 24));
        // print_r($diff1);
        if ( ( Auth::user()->subscribed('main') ||  $diff1 < 15 ) || ( Auth::user()->is_paid == 'yes' || Auth::User()->role_id == 4 ) ){
        $userID = Auth::User()->id;
        $data = array();
        
        if(\Auth::User()->role_id != 4){
            $templatedata = self::getTemplates(31);
        }else{
            $templatedata = self::getTemplates(30) ;
        }
            Notification::where('user_id',$userID)
            ->where('key','profile_viewed')
            ->where('is_seen','0')
            ->update(['is_seen' => '1']);   
          
        
            $notificationstotalv = \DB::table('users as u')
                   ->select('*','u.created_at as userCreate')
                   ->join('notifications as n', 'n.viewer_user_id', '=', 'u.id')
                   ->where('n.user_id',$userID)
                   ->where('n.key','profile_viewed')
                   ->orderBy('n.updated_at','DESC')
                   ->get();
                  
        if(\Auth::User()->role_id == 4){
            $validUsers = array();
            foreach ($notificationstotalv as $key => $value) {
                $date = new \DateTime($value->userCreate);
                $now = new \DateTime();
                $current_date = $now->format('Y/m/d 23:59:59'); 
                $now2 = new \DateTime($current_date);
                $diff = $date->diff($now2)->format("%d");
                // echo '<pre>';
                $sub = User::find($value->viewer_user_id);
                if(( $diff < 14 || $sub->is_paid == 'yes' ) || $sub->subscribed('main') ){
                    $validUsers[] = $value->viewer_user_id;
                }

            }
            $notifications =  Notification::where('user_id',$userID)
            ->where('key','profile_viewed')
            ->whereIn('viewer_user_id',$validUsers)
            ->orderBy('updated_at','DESC')
            ->paginate(10);
            $notificationstotal =  Notification::where('user_id',$userID)
            ->where('key','profile_viewed')
            ->whereIn('viewer_user_id',$validUsers)
            ->orderBy('updated_at','DESC')
            ->count();
        }else{
            $notifications =  Notification::where('user_id',$userID)
            ->where('key','profile_viewed')
            ->orderBy('updated_at','DESC')
            ->paginate(10);
            $notificationstotal =  Notification::where('user_id',$userID)
            ->where('key','profile_viewed')
            ->orderBy('updated_at','DESC')
            ->count();
        }
        
         // echo "<pre>";  
         //           print_r($notifications);die;

        $data['allSports'] = Sport::where('status','PUBLISHED')->orderBy('title','ASC')->get();
        $data['total'] = $notificationstotal;
        $data['templatedata'] = $templatedata;
        $data['notifications'] = $notifications;
        return view('notifications',$data);
        }
        else{
           return back(); 
        }
    } 
    public function notification(Request $request)
    {
        $userID = Auth::User()->id;
        $data = array();
        $notifresult =  Notification::where('user_id',$userID)
        ->where('key','profile_viewed')->get();
        $userid = array();  
        foreach($notifresult as $noti){
            $userid[] = $noti->Viewer->id ;
        }
        $userlist1 = CoachsSchool::whereIn('user_id',$userid);
        $resultArray = User::whereIn('role_id',[2,3])->pluck('id')->toArray();
        if($request->all()){
            if($request->state){
                $userlist1->where('state',$request->state);
            }
            if($request->division){
                $userlist1->where('division',$request->division);
            }
            // if($request->athlete_name != ''){
            //     $usersInfo1 = User::select('id');
            //     $usersInfo1->where('name' ,'LIKE', '%'.$request->athlete_name.'%');
            //     $usersInfoArray1 = $usersInfo1->pluck('id')->toArray();
            //     $resultArray = array_intersect($usersInfoArray1,$resultArray);  
            // }
            if($request->athlete_name != ''){
                $athlete_named = explode(' ', $request->athlete_name);
                $athlete_namef = $athlete_named[0];
                $athlete_namel = isset($athlete_named[1])?$athlete_named[1]:'';

                $usersInfo1 = UserInformation::select('user_id');
                $usersInfo1->where('meta_key','athelete_firstname')->where('meta_value' ,'LIKE', '%'.$athlete_namef.'%')
                ->orWhere('meta_key','athelete_lastname')->where('meta_value' ,'LIKE', '%'.$athlete_namef.'%');
                if($athlete_namel){
                   $usersInfo2 = UserInformation::select('user_id');  
                $usersInfo2->orWhere('meta_key','athelete_lastname')->where('meta_value' ,'LIKE', '%'.$athlete_namel.'%');
                $usersInfoArray13 = $usersInfo2->pluck('user_id')->toArray();
                }
                $usersInfoArray14 = $usersInfo1->pluck('user_id')->toArray();
                if(isset($usersInfoArray13)){
                    $resultArray5 = array_intersect($usersInfoArray13,$usersInfoArray14); 
                }else{
                    $resultArray5 = $usersInfoArray14;
                }
                // $resultArraynew = array_merge($resultArray5,$resultArray6);

                $resultArray = array_intersect($resultArray5,$resultArray);  
                // print_r($resultArraynew);die;
                // print_r($usersInfoArray14);die;
            }
            if($request->sportsFilter != ''){
                $usersInfo1 = UserInformation::select('user_id');
                $usersInfo1->where('meta_key','all_sports')->whereRaw("find_in_set(".$request->sportsFilter.",meta_value)");
                $usersInfo1->orWhere('meta_key','sport')->where('meta_value',$request->sportsFilter);
                $usersInfoArray = $usersInfo1->pluck('user_id')->toArray();
                $resultArray = array_intersect($usersInfoArray,$resultArray);  
            }
            if($request->graduationFilter != ''){
                $usersInfo1 = UserInformation::select('user_id');
                $usersInfo1->where('meta_key','graduation')->where('meta_value' , $request->graduationFilter);
                $usersInfoArray1 = $usersInfo1->pluck('user_id')->toArray();
                $resultArray = array_intersect($usersInfoArray1,$resultArray);  
            }
            if($request->position != ''){
                $usersInfo22 = UserInformation::select('user_id');
                $usersInfo22->where('meta_key','position')->where('meta_value' , $request->position);
                $usersInfoArray22 = $usersInfo22->pluck('user_id')->toArray();
                $resultArray = array_intersect($usersInfoArray22,$resultArray);  
            }
            if($request->genderFilter != ''){
                $usersInfo2 = UserInformation::select('user_id');
                $usersInfo2->where('meta_key','athelete_gender')->where('meta_value' , $request->genderFilter);
                $usersInfoArray2 = $usersInfo2->pluck('user_id')->toArray();
                $resultArray = array_intersect($usersInfoArray2,$resultArray);  
            } 
            // if($request->heightFilter != ''){
            //     $heightFilterVal = explode('-', $request->heightFilter);
            //     $heightFilterVal1 = intval($heightFilterVal[0] - 36);
            //     $heightFilterVal2 = intval($heightFilterVal[1] - 36);
            //     if($heightFilterVal1 > 0 || $heightFilterVal2 > 0){
            //         $usersInfo3 = UserInformation::select('user_id');
            //         $usersInfo3->where('meta_key','athelete_height')->whereBetween('meta_value' ,[ $heightFilterVal1,$heightFilterVal2 ]);
            //         $usersInfoArray3 = $usersInfo3->pluck('user_id')->toArray();
            //         $resultArray = array_intersect($usersInfoArray3,$resultArray); 
            //     }
            // }
            // if($request->weightFilter != ''){
            //     $strg = str_replace('lbs', '', $request->weightFilter);
            //     $weightFilterVal = explode('-', $request->weightFilter);
            //     $weightFilterVal1 = intval($weightFilterVal[0]); ;
            //     $weightFilterVal2 = intval($weightFilterVal[1]);
                 
            //     if($weightFilterVal1 > 0 || $weightFilterVal2 > 0){
            //         $usersInfo4 = UserInformation::select('user_id');
            //         $usersInfo4->where('meta_key','athelete_weight')
            //         ->whereBetween('meta_value' ,[ $weightFilterVal1,$weightFilterVal2 ]);
            //         $usersInfoArray4 = $usersInfo4->pluck('user_id')->toArray();
            //         $resultArray = array_intersect($usersInfoArray4,$resultArray); 
            //     }
            // }
            if($request->stateFilter != ''){
                $usersInfo5 = UserInformation::select('user_id');
                $usersInfo5->where('meta_key','state')->where('meta_value' , $request->stateFilter);
                $usersInfoArray5 = $usersInfo5->pluck('user_id')->toArray();
                $resultArray = array_intersect($usersInfoArray5,$resultArray); 
            } //print_r($resultArray);
            if($request->gpa != ''){
                $usersInfo6 = AcademicDetailAthelete::select('user_id')->where('gpa','>=',$request->gpa)->get();
                $usersInfoArray6 = $usersInfo6->pluck('user_id')->toArray();
                $resultArray = array_intersect($usersInfoArray6,$resultArray);
                //print_r($resultArray); 
            }
            if($request->act != ''){
                //echo $request->act;
                $usersInfo7 = AcademicDetailAthelete::select('user_id')->where('act','>=',$request->act)->get();
                $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                $resultArray = array_intersect($usersInfoArray7,$resultArray); 
               
            } 
        }
           
        $userlist = $userlist1->get()->pluck('user_id')->toArray();
         // print_r($userlist);die;
          $notificationstotalv = \DB::table('users as u')
                   ->select('*','u.created_at as userCreate')
                   ->join('notifications as n', 'n.viewer_user_id', '=', 'u.id')
                   ->where('n.user_id',$userID)
                   ->where('n.key','profile_viewed')
                   ->orderBy('n.updated_at','DESC')
                   ->get();
        if(\Auth::User()->role_id == 4){
            $validUsers = array();
            foreach ($notificationstotalv as $key => $value) {
                $date = new \DateTime($value->userCreate);
                $now = new \DateTime();
                $current_date = $now->format('Y/m/d 23:59:59'); 
                $now2 = new \DateTime($current_date);
                $diff = $date->diff($now2)->format("%d");
                // echo '<pre>';
                $sub = User::find($value->viewer_user_id);
                if(( $diff < 14 || $sub->is_paid == 'yes') || $sub->subscribed('main') ){
                    $validUsers[] = $value->viewer_user_id;
                }

            }
            $notifications =  Notification::where('key','profile_viewed');
            $notifications->WhereIn('viewer_user_id',$resultArray);
            $notifications->WhereIn('viewer_user_id',$validUsers);
            $notifications = $notifications->where('user_id',$userID)
            ->orderBy('updated_at','DESC')
            ->paginate(10);
            $notificationstotal =  Notification::where('key','profile_viewed');
            $notificationstotal->WhereIn('viewer_user_id',$resultArray);
            $notificationstotal->WhereIn('viewer_user_id',$validUsers);
            $notificationstotal = $notificationstotal->where('user_id',$userID)
            ->count();
        }else{
            $notifications =  Notification::where('key','profile_viewed');
            $notifications->whereIn('viewer_user_id',$userlist);
            $notifications = $notifications->where('user_id',$userID)
            ->orderBy('updated_at','DESC')
            ->paginate(10);
            $notificationstotal =  Notification::where('key','profile_viewed');
            $notificationstotal->whereIn('viewer_user_id',$userlist);
            $notificationstotal = $notificationstotal->where('user_id',$userID)
            ->count();
        }
        // print_r($notificationstotal);die;
        if(\Auth::User()->role_id != 4){
            $templatedata = self::getTemplates(31);
        }else{
            $templatedata = self::getTemplates(30) ;
        }   
        $data['allSports'] = Sport::where('status','PUBLISHED')->orderBy('title','ASC')->get();
        $data['total'] = $notificationstotal;
        $data['templatedata'] = $templatedata;
        $data['notifications'] = $notifications;
        return view('notifications',$data);
    } 
    public function notificationSeen(){
        $userID = Auth::User()->id;
        Notification::where('user_id',$userID)
            ->where('key','profile_viewed')
            ->where('is_seen','0')
            ->update(['is_seen' => '1']);   
        return 'True';
    }
    public function notificationProfileShow( $slug ,$noti ){
        $userData = User::where('user_slug',$slug)->first();
        if($userData){
            if( ( ($userData->role_id != 1 && Auth::User()->role_id != 1 ) && $userData->id != Auth::User()->id ) )  {
                $notification2 = Notification::find($noti);
                $notification2->is_read = '1' ;
                $notification2->is_seen = '1' ;
                $notification2->save();
            }
            if($userData->role_id == 4){
               $type = 'coach';
            }else{
                $type = 'athlete';
            }
            return  redirect('/profile/'.$type.'/'.$slug); 
        }else{
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    public function savetemplate(Request $request){
       

    $logged_user = Auth::User();
    
    if($request->eventid != '' && $request->eventid != 'undefined' ){
        $key = 'eventtemplate';
    }else{
        $key = 'emailtemplate';
    }
    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
    if( $found == '' || $found == null ){
        $found = new UserInformation;
        $found->meta_key = $key;
        $found->user_id = $logged_user->id;
        $found->save();
        // $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
    }
    //print_r($data_1);
    $new_data= (array) json_decode( $found['meta_value'] );
    // echo "<pre>";
    // print_r($new_data);
    // die();
    $lenght = count($new_data);
    $new_data[$lenght][ 'Key' ] = $request->templatename;
    $new_data[$lenght][ 'message' ]= $request->message;
    $new_data[$lenght][ 'value' ]= $request->templatename;
    $found->meta_value = json_encode( $new_data );
    $found->save();

    return $new_data[$lenght];


    }
   //Show Profile Page
    public function schoolimport(){
         if(!Auth::User()){
            return Redirect('/login');
        }
        $data = array();
        return view('school_import',$data);
        
    }

    public function schoolimportsave(Request $request){
  
        $lines = explode("\n", $request->data_chank);
        $data = array();
        $data['data'] = $lines;
        foreach($lines as $row){
            $row = str_getcsv($row);
             // print_r($row);

            if( count($row) == 19 ){
                if( $row[0] != 'Institution Name' ){
                $addData = ApiData::where('school_name',$row[0])->where('city',$row[2])->where('state',$row[3])->first();
                    if(!$addData){
                      $addData = new ApiData(); 
                    }
            
                    $slug =  strtolower(str_replace(" ","-",$row[0]));
                      $schoolslug = ApiData::where('school_name', $row[0])->get()->count();
                      if($schoolslug >= 1){
                        $srnd = random_int(11,99);
                        $slug = $slug."-".$srnd;
                      }
                    $addData->school_name = $row[0];
                    $addData->address = $row[1];
                    $addData->city = $row[2];
                    $addData->zip = $row[4];
                    $addData->school_url = $row[6]; 
                    $addData->state = $row[3];
                    $addData->telephone = $row[5];
                    $addData->school_size_total = $row[7];
                    $addData->member_naa = $row[8];
                    $addData->percent_admission = $row[9];
                    $addData->sat_cri_25 = $row[10];
                    $addData->sat_cri_75 = $row[11];
                    $addData->sat_math_25 = $row[12];
                    $addData->sat_math_75 = $row[13];
                    $addData->act_compose_25 = $row[14];
                    $addData->act_compose_75 = $row[15];
                    $addData->tuition_revenue_per_fte = $row[18];
                    $addData->school_slug = $slug;
                    $addData->tuition_in_state = $row[17];
                    $addData->tuition_out_of_state = $row[16];
                    $addData->save(); 
                }
            print_r($data);
        }else{
            $data['status'] = 'invalid';
            return $data;
        }  
        }
    }

    //Delete Notification Single
    public function notificationDelete($notiID){
        $notifications = Notification::find($notiID)->delete();
        \Session::flash('error', 'Notification Deleted Successfully');
       return  redirect()->back(); 
        
    }

    public function exportAthletes( ){
        $getallUsers = User::whereIn('role_id',[2,3])->get();
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=AllAthletes.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $columns = array('athelete_firstname','athelete_lastname','athelete_email','parents','coaches','athelete_address','city','state','zip','athelete_phone','athelete_birth','athelete_height','athelete_weight','athelete_gender','athelete_facebook','athelete_twitter','athelete_instagram','athelete_snapchat','athelete_skype','athelete_whatsapp','graduation','sport','position_name','school','athletic_achievements','athelete_acadmic_achivment','extra_curricular','testimonails','academics','gpa','act','sats','math','verbal','paid');
        $columns2 = array('First Name','Last Name','Email','Parent','Club Coach Info','Address','City','State','Zip','Phone','DOB','Height','Weight','Gender','Facebook','Twitter','Instagram','Snapchat','Skype','Whatsapp','Graduation Year','Sports','Primary Position','School', 'Athletic Achievements','Academic Achievements','Extra Curricular','Reference Letter' , 'Academics' , 'GPA' ,'Sat ACT','Sat Reading','Sat Math','Sat Verbal','paid');
        $callback = function() use ($getallUsers, $columns , $columns2)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns2);
            foreach($getallUsers as $user) {
                foreach( $columns as $data ){
                    $userInfo = UserInformation::where('user_id',$user->id)->where('meta_key',$data)->first();
                // echo "<pre>";
                                     // print_r( $data );

                 if($data == 'paid'){


                $user = User::where('id',$user->id)->first();
                if($user){

                   if(($user->is_paid == 'yes') && (!$user->subscribed('main'))){

                    $userInfoData[$data] = 'Paid by admin';

                   }else if(($user->is_paid == 'no') && ($user->subscribed('main'))){

                    $userInfoData[$data] = 'Paid';

                   }else if(($user->is_paid == 'yes') && ($user->subscribed('main'))){

                    $userInfoData[$data] = 'Paid';


                   }else{

                        $userInfoData[$data] = 'Not Paid';

                   }


                }else{
                    $userInfoData[ $data ] = 'not found';
                }



                 }else if($data == 'sport'){ 

                if(isset($userInfo->meta_value)  && ($userInfo->meta_value != '') ){

                 $sports = Sport::where('id',$userInfo->meta_value)->first();
                    if($sports){
                  

                   $userInfoData[ $data ] = $sports->title;
                    
                    
                    }
                }else{
                 $userInfoData[ $data ] = '';
                }

               // $sports = Sport::where('id',$userInfo->meta_value)->first();


                }else if($data == 'academics'){ 
                    $found = '';
                    $found = AcademicDetailAthelete::where('user_id', $user->id)->first();
                    if($found){
                        $aca = array();
                        $aca[0] = ($found->school_id)?$found->school_id:'';              
                        $aca[1] = ($found->location)?$found->location:'';
                        $aca[2] = ($found->city)?$found->city:'';
                        $aca[3] = ($found->state)?$found->state:'';
                        $aca[4] = ($found->zip)?$found->zip:'';
                        $aca1 = array_filter($aca);
                        $aca2 = implode(', ',$aca1);
                         $userInfoData[ $data ] = $aca2;  
                    }else{
                        $userInfoData[ $data ] = '';
                    }  
                    }else if( $data === 'gpa' ){ 
                            // die('here');
                            $found11 = '';
                            $found11 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found11){
                                 $userInfoData[ $data ] = (($found11->gpa) > 0 )?$found11->gpa:'NA';  
                            }else{
                                $userInfoData[ $data ] = 'NA';
                            }  
                        }else if( $data === 'act' ){ 
                            $found2 = '';
                            $found2 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found2){
                                 $userInfoData[ $data ] = ($found2->act > 0 )?$found2->act:'NA';
                            }else{
                                $userInfoData[ $data ] = 'NA';
                            }  
                        }else if( $data == 'sats' ){ 
                            $found3 = '';
                            $found3 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found3){
                                 $userInfoData[ $data ] = (($found3->sats) > 200 )?$found3->sats:'NA';
                            }else{
                                $userInfoData[ $data ] = 'NA';
                            }  
                        }else if( $data == 'math' ){ 
                            $found4 = '';
                            $found4 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found4){
                                 $userInfoData[ $data ] = ($found4->math > 200 )?$found4->math:'NA';
                            }else{
                                $userInfoData[ $data ] = 'NA';
                            }  
                        }else if( $data == 'verbal' ){ 
                            $found5 = '';
                            $found5 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found5){
                                 $userInfoData[ $data ] = ($found5->verbal > 200 )?$found5->verbal:'NA';
                            }else{
                                $userInfoData[ $data ] = 'NA';
                            }  
                    }else{
                    $ath1 = array();
                    $atc2 = array();
                    $ext2 = array();
                    $test2 = array();
                    if($userInfo){
                        if( $data === 'parents' ){ 
                            if($userInfo->meta_value != '' && $userInfo->meta_value != []){
                                $parents = json_decode($userInfo->meta_value);
                                array_reverse($parents);
                            foreach($parents as $key => $parent){
                            if(isset($parent->firstname) || isset($parent->lastname) || isset($parent->email)){
                                    $userInfoData[ $data ] = (isset($parent->firstname)?$parent->firstname:'') .' '.(isset($parent->lastname)?$parent->lastname:'') .' '.(isset($parent->email) && ( ( isset($parent->email)?$parent->email:'') != '' )?('('.$parent->email.')'):'');
                            }else{
                                $userInfoData[ $data ] = '';
                            }
                            } 
                            }else{
                                $userInfoData[ $data ] = '';
                            } 
                        }else if($data === 'coaches'){
                        $clubcoaches = array();
                        if($userInfo->meta_value != '' && $userInfo->meta_value != []){
                        $clubcoaches = json_decode($userInfo->meta_value);
                        if($clubcoaches){
                            array_reverse($clubcoaches);
                            foreach($clubcoaches as $clubcoach){
                                $coc = array();
                                $coc1 = array();
                                $coc[0] = (isset($clubcoach->type)&& (isset($clubcoach->type) ?$clubcoach->type:"") != "null")?$clubcoach->type:'';
                                             
                                $coc[1] = (isset($clubcoach->clubname)&& (isset($clubcoach->clubname) ?$clubcoach->clubname:"") != "null")?$clubcoach->clubname:'';

                                $coc[2] = (isset($clubcoach->jersey)&& (isset($clubcoach->jersey) ?$clubcoach->jersey:"") != "null")?"Jersey #".$clubcoach->jersey:'';

                                $coc[3] = (isset($clubcoach->coachname)&& (isset($clubcoach->coachname) ?$clubcoach->coachname:"") != "null")?$clubcoach->coachname:'';

                                $coc[4] = (isset($clubcoach->email)&& (isset($clubcoach->email) ?$clubcoach->email:"") != "null")?$clubcoach->email:'';

                                $coc[5] = (isset($clubcoach->phone)&& (isset($clubcoach->phone) ?$clubcoach->phone:"") != "null")?$clubcoach->phone:'';
                                $coc = array_filter($coc);
                                $coc1[] = implode(', ',$coc);
                            }
                        }
                            $userInfoData[ $data ] = implode(' ',$coc1);
                        }else{
                            $userInfoData[ $data ] = '';
                        }
                    }else if($data === 'athletic_achievements'){
                        // $AthleticAchievements = array();
                        if($userInfo->meta_value != '' && $userInfo->meta_value != []){
                        $AthleticAchievements = json_decode($userInfo->meta_value);
                        if($AthleticAchievements){
                            foreach($AthleticAchievements as $athleticAchievement){
                                $ath = array();
                                $ath[0] = (isset($athleticAchievement->athleticname)&& (isset($athleticAchievement->athleticname) ?$athleticAchievement->athleticname:"") != "null")?$athleticAchievement->athleticname:'';
                                    if(isset($athleticAchievement->years) && $athleticAchievement->years != '') {
                                        $arraydata = array(); 
                                        foreach($athleticAchievement->years as $key => $yearsss){
                                            $arraydata[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata) > 0){
                                            $dataat = implode(', ', $arraydata).'.';
                                        }else{
                                            $dataat = "";
                                        }
                                    }else{
                                        $dataat = "";
                                    }                 
                                $ath[1] = $dataat ;
                                $ath = array_filter($ath);
                                $ath1[] = implode(', ',$ath);
                            }
                        }
                            $userInfoData[ $data ] = implode(' ',$ath1);
                        }else{
                            $userInfoData[ $data ] = '';
                        }
                    }else if($data === 'athelete_acadmic_achivment'){
                        // $AcadmicAchievements = array();
                        if($userInfo->meta_value != '' && $userInfo->meta_value != []){
                        $AcadmicAchievements = json_decode($userInfo->meta_value);
                        if($AcadmicAchievements){
                            foreach($AcadmicAchievements as $AcadmicAchievement){
                                $atc = array();
                                $atc[0] = (isset($AcadmicAchievement->achivment))?$AcadmicAchievement->achivment:'';              
                               if(isset($AcadmicAchievement->year) && $AcadmicAchievement->year != '') {
                                        $arraydata1 = array(); 
                                        foreach($AcadmicAchievement->year as $key => $yearsss){
                                            $arraydata1[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata1) > 0){
                                            $dataat1 = implode(', ', $arraydata1).'.';
                                        }else{
                                            $dataat1 = "";
                                        }
                                    }else{
                                        $dataat1 = "";
                                    }                 
                                $atc[1] = $dataat1 ;
                                $atc1 = array_filter($atc);
                                $atc2[] = implode(', ',$atc1);
                            }
                        }
                        $userInfoData[ $data ] = implode(' ',$atc2);
                        }else{
                            $userInfoData[ $data ] = '';
                        }
                    }else if($data === 'extra_curricular'){
                        // $AcadmicAchievements = array();
                        if($userInfo->meta_value != '' && $userInfo->meta_value != []){
                        $extra_curriculars = json_decode($userInfo->meta_value);
                        if(count($extra_curriculars) > 0){
                              // print_r( $extra_curriculars);
                            foreach($extra_curriculars as $extra_curricular){
                                $ext = array();
                                $ext[0] = (isset($extra_curricular->activity))?$extra_curricular->activity:'';              
                                if(isset($extra_curricular->year) && $extra_curricular->year != '') {
                                        $arraydata2 = array(); 
                                        foreach($extra_curricular->year as $key => $yearsss){
                                            $arraydata2[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata2) > 0){
                                            $dataat2 = implode(', ', $arraydata2).'.';
                                        }else{
                                            $dataat2 = "";
                                        }
                                    }else{
                                        $dataat2 = "";
                                    }                 
                                $ext[1] = $dataat2 ;
                                $ext1 = array_filter($ext);
                                $ext2[] = implode(', ',$ext1);
                            }
                        }

                        $userInfoData[ $data ] = implode(' ',$ext2);
                        }else{
                            $userInfoData[ $data ] = '';
                        }
                         // print_r( $userInfoData[ $data ]);
                    }else if($data === 'testimonails'){
                       
                        if($userInfo->meta_value != '' && $userInfo->meta_value != '[]'){
                           
                              // print_r( $userInfo->meta_value);
                        $testimonails = json_decode($userInfo->meta_value);
                        if($testimonails){
                            foreach($testimonails as $testimonail){
                                $test = array();
                                $test[0] = (isset($testimonail->testimonail))?strip_tags($testimonail->testimonail):'';              
                                if(isset($testimonail->year) && $testimonail->year != '')  {
                                        $arraydata3 = array(); 
                                        foreach($testimonail->year as $key => $yearsss){
                                            $arraydata3[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata3) > 0){
                                            $dataat3 = implode(', ', $arraydata3).'.';
                                        }else{
                                            $dataat3 = "";
                                        }
                                    }else{
                                        $dataat3 = "";
                                    }                 
                                $test[1] = $dataat3 ;
                                // $test[2] = isset($testimonail->coach)?$testimonail->coach:'' ;
                                // $test[3] = isset($testimonail->email)?$testimonail->email.'.':'' ;
                                $test1 = array_filter($test);
                                $test2[] = implode(', ',$test1);
                            }
                            $userInfoData[ $data ] = implode(' ',$test2);
                        }
                            
                        }else{
                            $userInfoData[ $data ] = '';
                        }
                    }else if($data == 'athelete_height'){
                        $centimetres = $userInfo->meta_value + 36; 
                        $feet = floor($centimetres/12);
                        $inches = ($centimetres%12);
                        $userInfoData[ $data ] = $feet."'".$inches.'"';
                    }else if($data == 'athelete_weight'){
                        if($userInfo->meta_value != ''){
                            $userInfoData[ $data ] = $userInfo->meta_value.' lbs';
                        }
                    }else if($data == 'athelete_gender'){
                        if($userInfo->meta_value == 'Men'){
                             $userInfoData[ $data ] = "Male";
                        }else if($userInfo->meta_value == 'Women'){
                            $userInfoData[ $data ] = "Female";
                        }else{
                            $userInfoData[ $data ] = "";
                        }
                    }else if($data == 'athelete_phone'){
                       $athPho =  str_replace(array('(',')','-','_',' '),'',$userInfo->meta_value);
                        if($athPho){
                                $userInfoData[ $data ] = "(".substr($athPho,0,3).") ".substr($athPho,3,3)."-".substr($athPho,6);
                        }else{
                            $userInfoData[ $data ] = "";
                        }
                    }else if($data == 'athelete_birth'){
                        $birth = $userInfo->meta_value;
                        if($birth){
                                $userInfoData[ $data ] = date('m-d-Y',strtotime($birth));
                        }else{
                            $userInfoData[ $data ] = "";
                        }
                    }else{
                        $userInfoData[ $data ] = $userInfo->meta_value;
                    }
                }else{
                        $userInfoData[ $data ] = '';
                    }
                }
                }
                // echo "<pre>";
                         // print_r( $userInfoData);
                         // print_r( $user->id);

                // $alldata = implode(' , ',$userInfoData);
                 $properOrderedArray = array_merge(array_flip($columns), $userInfoData);
                                 // print_r( $properOrderedArray);

                fputcsv($file, $properOrderedArray);
            }//die;
            fclose($file);
        };
        return \Response::stream($callback, 200, $headers);
    }
    public function exportParent( ){

        $getallUsers = User::whereIn('role_id',[2,3])->get();
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=AllParents.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        
        $columns = array('parents','athelete_firstname','athelete_lastname','athelete_email','athelete_address','city','state','zip','athelete_phone','athelete_birth','athelete_height','athelete_weight','athelete_gender','athelete_facebook','athelete_twitter','athelete_instagram','athelete_snapchat','athelete_skype','athelete_whatsapp','coaches','graduation','sport','position_name','school','athletic_achievements','athelete_acadmic_achivment','extra_curricular','testimonails','academics','gpa','act','sats','math','verbal','paid');
        $columns1 = array('parents','paid');
        $columns3 = array('gender','firstname' ,'lastname','fullname','email','phone','athelete_firstname','athelete_lastname','athelete_email','athelete_address','city','state','zip','athelete_phone','athelete_birth','athelete_height','athelete_weight','athelete_gender','athelete_facebook','athelete_twitter','athelete_instagram','athelete_snapchat','athelete_skype','athelete_whatsapp','coaches','graduation','sport','position_name','school','athletic_achievements','athelete_acadmic_achivment','extra_curricular','testimonails','academics','gpa','act','sats','math','verbal','paid');
        $columns2 = array('Relationship To Athlete','First Name','Last Name','Full Name','Email','Phone','Athlete First Name','Athlete Last Name','Athlete Email','Address','City','State','Zip','Athlete Phone','DOB','Athlete Height','Athlete Weight','Athlete Gender','Facebook','Twitter','Instagram','Snapchat','Skype','Whatsapp','Club Coach Info','Graduation Year','Sports','Primary Position','School', 'Athletic Achievements','Academic Achievements','Extra Curricular','Reference Letter' , 'Academics', 'GPA' ,'Sat ACT','Sat Reading','Sat Math','Sat Verbal','Paid User');
        $callback = function() use ($getallUsers, $columns , $columns2 , $columns3 , $columns1)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns2);
            $userInfoData2 = array();
            foreach($getallUsers as $user) {
                foreach( $columns1 as $data ){
                    $userInfo = UserInformation::where('user_id',$user->id)->where('meta_key',$data)->first();

                    if( $data == 'parents' && isset($userInfo) ){ 
                         if($userInfo->meta_value != '' && $userInfo->meta_value != []){
                            $parents = json_decode($userInfo->meta_value);
                        if($parents){
                        foreach($parents as $key => $parent){
                                $userInfoData2[ 'gender' ] = (isset($parent->gender)?$parent->gender:'');
                                $userInfoData2[ 'firstname' ] = (isset($parent->firstname)?$parent->firstname:'');
                                $userInfoData2[ 'lastname' ] = (isset($parent->lastname)?$parent->lastname:'');
                                $userInfoData2[ 'fullname' ] = (isset($parent->firstname)?$parent->firstname:'') .' '.(isset($parent->lastname)?$parent->lastname:'');
                                $userInfoData2[ 'email' ] = (isset($parent->email)?$parent->email:'');
                                
                                $daat =  str_replace(array('(',')','-','_',' '),'',isset($parent->phone)?$parent->phone:'');
                                if($daat){
                                $userInfoData2[ 'phone' ] = "(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);
                                }else{
                                    $userInfoData2[ $data3 ] = "";
                                }
                                $userInfoall = \Voyager::UserInformation('all',$user->id );
                               
                    foreach( $columns as $data3 ){

                    if( $data3 != 'parents' && ( isset($userInfoall[$data3]) ||  in_array($data3,array('academics','gpa','act','sats','math','verbal') )  ) ){
                        // print_r($data3);
                         // die('here');

                        $ath1 = array();
                        $atc2 = array();
                        $ext2 = array();
                        $test2 = array();


                         if($data3 == 'academics'){ 
                            $found = '';
                            $found = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found){
                                $aca = array();
                                $aca[0] = ($found->school_id)?$found->school_id:'';              
                                $aca[1] = ($found->location)?$found->location:'';
                                $aca[2] = ($found->city)?$found->city:'';
                                $aca[3] = ($found->state)?$found->state:'';
                                $aca[4] = ($found->zip)?$found->zip:'';
                                $aca1 = array_filter($aca);
                                $aca2 = implode(', ',$aca1);
                                 $userInfoData2[ $data3 ] = $aca2;  
                                    // echo "<pre>";
                                 // print_r( $userInfoData2);die; 
                            }else{
                                $userInfoData2[ $data3 ] = '';
                            }  
                        }else if( $data3 == 'gpa' ){ 
                            
                            $found11 = '';
                            $found11 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found11){
                                 $userInfoData2[ $data3 ] = (($found11->gpa) > 0 )?$found11->gpa:'NA';  
                            }else{
                                $userInfoData2[ $data3 ] = 'NA';
                            }  
                        }else if( $data3 === 'act' ){ 
                            $found2 = '';
                            $found2 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found2){
                                 $userInfoData2[ $data3 ] = ($found2->act > 0 )?$found2->act:'NA';
                            }else{
                                $userInfoData2[ $data3 ] = 'NA';
                            }  
                        }else if( $data3 == 'sats' ){ 
                            $found3 = '';
                            $found3 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found3){
                                 $userInfoData2[ $data3 ] = (($found3->sats) > 200 )?$found3->sats:'NA';
                            }else{
                                $userInfoData2[ $data3 ] = 'NA';
                            }  
                        }else if( $data3 == 'math' ){ 
                            $found4 = '';
                            $found4 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found4){
                                 $userInfoData2[ $data3 ] = ($found4->math > 200 )?$found4->math:'NA';
                            }else{
                                $userInfoData2[ $data3 ] = 'NA';
                            }  
                        }else if( $data3 == 'verbal' ){ 
                            $found5 = '';
                            $found5 = AcademicDetailAthelete::where('user_id', $user->id)->first();
                            if($found5){
                                 $userInfoData2[ $data3 ] = ($found5->verbal > 200 )?$found5->verbal:'NA';
                            }else{
                                $userInfoData2[ $data3 ] = 'NA';
                            }  
                    }else if($data3 === 'coaches'){
                        $clubcoaches = array();
                        if($userInfoall[$data3] != '' && $userInfoall[$data3] != []){
                        $clubcoaches = json_decode($userInfoall[$data3]);
                        if($clubcoaches){
                            array_reverse($clubcoaches);
                            foreach($clubcoaches as $clubcoach){
                                $coc = array();
                                $coc1 = array();
                                $coc[0] = (isset($clubcoach->type)&& (isset($clubcoach->type) ?$clubcoach->type:"") != "null")?$clubcoach->type:'';
                                             
                                $coc[1] = (isset($clubcoach->clubname)&& (isset($clubcoach->clubname) ?$clubcoach->clubname:"") != "null")?$clubcoach->clubname:'';

                                $coc[2] = (isset($clubcoach->jersey)&& (isset($clubcoach->jersey) ?$clubcoach->jersey:"") != "null")?"Jersey #".$clubcoach->jersey:'';

                                $coc[3] = (isset($clubcoach->coachname)&& (isset($clubcoach->coachname) ?$clubcoach->coachname:"") != "null")?$clubcoach->coachname:'';

                                $coc[4] = (isset($clubcoach->email)&& (isset($clubcoach->email) ?$clubcoach->email:"") != "null")?$clubcoach->email:'';

                                $coc[5] = (isset($clubcoach->phone)&& (isset($clubcoach->phone) ?$clubcoach->phone:"") != "null")?$clubcoach->phone:'';
                                $coc = array_filter($coc);
                                $coc1[] = implode(', ',$coc);
                            }
                        }
                            $userInfoData2[ $data3 ] = implode(' ',$coc1);
                        }else{
                            $userInfoData2[ $data3 ] = '';
                        }
                    }else if($data3 === 'athletic_achievements'){
                        // $AthleticAchievements = array();
                        if($userInfoall[$data3] != '' && $userInfoall[$data3] != []){
                        $AthleticAchievements = json_decode($userInfoall[$data3]);
                        if($AthleticAchievements){
                            foreach($AthleticAchievements as $athleticAchievement){
                                $ath = array();
                                $ath[0] = (isset($athleticAchievement->athleticname)&& (isset($athleticAchievement->athleticname) ?$athleticAchievement->athleticname:"") != "null")?$athleticAchievement->athleticname:'';
                                    if(isset($athleticAchievement->years) && $athleticAchievement->years != '') {
                                        $arraydata = array(); 
                                        foreach($athleticAchievement->years as $key => $yearsss){
                                            $arraydata[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata) > 0){
                                            $dataat = implode(', ', $arraydata).'.';
                                        }else{
                                            $dataat = "";
                                        }
                                    }else{
                                        $dataat = "";
                                    }                 
                                $ath[1] = $dataat ;
                                $ath = array_filter($ath);
                                $ath1[] = implode(', ',$ath);
                            }
                        }
                            $userInfoData2[ $data3 ] = implode(' ',$ath1);
                        }else{
                            $userInfoData2[ $data3 ] = '';
                        }
                    }else if($data3 === 'athelete_acadmic_achivment'){
                        // $AcadmicAchievements = array();
                        if($userInfoall[$data3] != '' && $userInfoall[$data3] != []){
                        $AcadmicAchievements = json_decode($userInfoall[$data3]);
                        if($AcadmicAchievements){
                            foreach($AcadmicAchievements as $AcadmicAchievement){
                                $atc = array();
                                $atc[0] = (isset($AcadmicAchievement->achivment))?$AcadmicAchievement->achivment:'';              
                               if(isset($AcadmicAchievement->year) && $AcadmicAchievement->year != '') {
                                        $arraydata1 = array(); 
                                        foreach($AcadmicAchievement->year as $key => $yearsss){
                                            $arraydata1[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata1) > 0){
                                            $dataat1 = implode(', ', $arraydata1).'.';
                                        }else{
                                            $dataat1 = "";
                                        }
                                    }else{
                                        $dataat1 = "";
                                    }                 
                                $atc[1] = $dataat1 ;
                                $atc1 = array_filter($atc);
                                $atc2[] = implode(', ',$atc1);
                            }
                        }
                        $userInfoData2[ $data3 ] = implode(' ',$atc2);
                        }else{
                            $userInfoData2[ $data3 ] = '';
                        }
                    }else if($data3 === 'extra_curricular'){
                        // $AcadmicAchievements = array();
                        if($userInfoall[$data3] != '' && $userInfoall[$data3] != []){
                        $extra_curriculars = json_decode($userInfoall[$data3]);
                        if(count($extra_curriculars) > 0){
                              // print_r( $extra_curriculars);
                            foreach($extra_curriculars as $extra_curricular){
                                $ext = array();
                                $ext[0] = (isset($extra_curricular->activity))?$extra_curricular->activity:'';              
                                if(isset($extra_curricular->year) && $extra_curricular->year != '') {
                                        $arraydata2 = array(); 
                                        foreach($extra_curricular->year as $key => $yearsss){
                                            $arraydata2[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata2) > 0){
                                            $dataat2 = implode(', ', $arraydata2).'.';
                                        }else{
                                            $dataat2 = "";
                                        }
                                    }else{
                                        $dataat2 = "";
                                    }                 
                                $ext[1] = $dataat2 ;
                                $ext1 = array_filter($ext);
                                $ext2[] = implode(', ',$ext1);
                            }
                        }

                        $userInfoData2[ $data3 ] = implode(' ',$ext2);
                        }else{
                            $userInfoData2[ $data3 ] = '';
                        }
                         // print_r( $userInfoData[ $data ]);
                    }else if($data3 == 'sport'){

                         if(isset($userInfoall[$data3])  && ($userInfoall[$data3] != '') ){

                 $sports = Sport::where('id',$userInfoall[$data3])->first();
                    if($sports){
                  
                   $userInfoData2[ $data3 ] = $sports->title;
                    
                    }
                }else{
                 $userInfoData2[ $data3 ] = '';
                }


                    }else if($data3 === 'testimonails'){
                       
                        if($userInfoall[$data3] != '' && $userInfoall[$data3] != '[]'){
                           
                              // print_r( $userInfo->meta_value);
                        $testimonails = json_decode($userInfoall[$data3]);
                        if($testimonails){
                            foreach($testimonails as $testimonail){
                                $test = array();
                                $test[0] = (isset($testimonail->testimonail))?strip_tags($testimonail->testimonail):'';              
                                if(isset($testimonail->year) && $testimonail->year != '')  {
                                        $arraydata3 = array(); 
                                        foreach($testimonail->year as $key => $yearsss){
                                            $arraydata3[$key] = $yearsss->name;
                                        }
                                        if(count($arraydata3) > 0){
                                            $dataat3 = implode(', ', $arraydata3).'.';
                                        }else{
                                            $dataat3 = "";
                                        }
                                    }else{
                                        $dataat3 = "";
                                    }                 
                                $test[1] = $dataat3 ;
                                // $test[2] = isset($testimonail->coach)?$testimonail->coach:'' ;
                                // $test[3] = isset($testimonail->email)?$testimonail->email.'.':'' ;
                                $test1 = array_filter($test);
                                $test2[] = implode(', ',$test1);
                            }
                            $userInfoData2[ $data3 ] = implode(' ',$test2);
                        }
                            
                        }else{
                            $userInfoData2[ $data3 ] = '';
                        }
                    }else if($data3 == 'athelete_height'){
                        $centimetres = $userInfoall[$data3] + 36; 
                        $feet = floor($centimetres/12);
                        $inches = ($centimetres%12);

                        $userInfoData2[ $data3 ] = $feet."'".$inches.'"';;
                    }else if($data3 == 'athelete_phone'){
                       $athPho =  str_replace(array('(',')','-','_',' '),'',$userInfoall[$data3]);
                        if($athPho){
                                $userInfoData2[ $data3 ] = "(".substr($athPho,0,3).") ".substr($athPho,3,3)."-".substr($athPho,6);
                        }else{
                            $userInfoData2[ $data3 ] = "";
                        }
                    }else if($data3 == 'athelete_weight'){
                        if($userInfoall[$data3] != ''){
                             $userInfoData2[ $data3 ] = $userInfoall[$data3].' lbs';
                        }else{
                            $userInfoData2[ $data3 ] = '';
                        }
                    }else if($data3 == 'athelete_gender'){
                        if($userInfoall[$data3] == 'Men'){
                             $userInfoData2[ $data3 ] = "Male";
                        }else if($userInfoall[$data3] == 'Women'){
                            $userInfoData2[ $data3 ] = "Female";
                        }else{
                            $userInfoData2[ $data3 ] = "";
                        }
                    }else if($data3 == 'athelete_birth'){
                        $birth = $userInfoall[$data3];
                        if($birth){
                                $userInfoData2[ $data3 ] = date('m-d-Y',strtotime($birth));
                        }else{
                            $userInfoData2[ $data3 ] = "";
                        }
                    }else{
                         $userInfoData2[ $data3 ] = $userInfoall[$data3];
                    } 
                     
                                       
                    }else{
                       if( $data3 != 'parents' && $data3 != 'paid'){
                        $userInfoData2[ $data3 ] = '';
                       } 
                       else if($data3 == 'paid'){

                                                $user = User::where('id',$user->id)->first();
                            if($user){

                               if(($user->is_paid == 'yes') && (!$user->subscribed('main'))){

                                 $userInfoData2[ $data3 ] = 'Paid by admin';

                               }else if(($user->is_paid == 'no') && ($user->subscribed('main'))){

                                 $userInfoData2[ $data3 ]= 'Paid';

                               }else if(($user->is_paid == 'yes') && ($user->subscribed('main'))){

                                 $userInfoData2[ $data3 ] = 'Paid';


                               }else{

                                     $userInfoData2[ $data3 ] = 'Not Paid';

                               }


                            }else{
                                 $userInfoData2[ $data3 ] = 'not found';
                            }
  

                       }else{
                                                $userInfoData2[ $data3 ] = '';


                       }
                    }
                }
                
                              // echo "<pre>";
                              //   print_r($userInfoData2);
                                $properOrderedArray = array_merge(array_flip($columns3), $userInfoData2);
                                fputcsv($file, $properOrderedArray);
                        }        
                        }else{
                            $userInfoData2[  ] = '';
                        }  
                    }
                    }else if( $data == 'athelete_firstname'){

                    }else if( $data == 'athelete_email'){

                    }else{

                    }
                } 
                 
            }
            fclose($file);
        };
        return \Response::stream($callback, 200, $headers);
    }
     
   
}