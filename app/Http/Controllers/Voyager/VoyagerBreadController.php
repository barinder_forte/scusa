<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\PageMeta;
use App\SportsMeta;
use App\SportEvent;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Cloudder;
use Auth;
use TCG\Voyager\Models\DataType;
use App\SportEventMeta;
use App\User;
use App\UserInformation;
use Mail;
use App\Mail\EventNotification;
use App\Mail\AccountCreated;
use App\Mail\SubscriptionCancelled;

use App\Mail\UserEmailUpdated;

// use Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;
class VoyagerBreadController extends BaseVoyagerBreadController
{
    // use BreadRelationshipParser;
    // Edit Field
    public function edit_field(Request $request, $id, $field)
    {
        $slug = 'sports';
        // echo $slug;
        // print_r( Voyager );
        $dataType = DataType::where('slug', '=', $slug)->first();
            
        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $relationships = $this->getRelationships($dataType);
        // print_r( $relationships );
        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name
        
        // echo "<pre>";
        // print_r( $dataTypeContent );

        // // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        // echo $field;
        $field_data = SportsMeta::where( 'id', '=', $field )->first();
        $settings = SportsMeta::orderBy('order', 'ASC')->where('sport_id', '=', $id)->get();
        // echo "<pre>"; print_r( $field_data ); die;
        return view($view, compact('field_data', 'dataType', 'dataTypeContent', 'isModelTranslatable', 'settings'));
    }
    
    
    public function edit_field_pages(Request $request, $id, $field)
    {
            $slug = 'pages';
            // echo $slug;
            // print_r( Voyager );
            $dataType = DataType::where('slug', '=', $slug)->first();
                
            // Check permission
            Voyager::canOrFail('edit_'.$dataType->name);
    
            $relationships = $this->getRelationships($dataType);
            // print_r( $relationships );
            $dataTypeContent = (strlen($dataType->model_name) != 0)
                ? app($dataType->model_name)->with($relationships)->findOrFail($id)
                : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name
            
            // echo "<pre>";
            // print_r( $dataTypeContent );
    
            // // Check if BREAD is Translatable
            $isModelTranslatable = is_bread_translatable($dataTypeContent);
    
            $view = 'voyager::bread.edit-add';
    
            if (view()->exists("voyager::$slug.edit-add")) {
                $view = "voyager::$slug.edit-add";
            }
            // echo $field;
            $field_data = PageMeta::where( 'id', '=', $field )->first();
            $settings = PageMeta::orderBy('order', 'ASC')->where('page_id', '=', $id)->get();
            // echo "<pre>"; print_r( $field_data ); die;
            return view($view, compact('field_data', 'dataType', 'dataTypeContent', 'isModelTranslatable', 'settings'));
        }
    
    
    // // POST BRE(A)D
    // public function store(Request $request)
    // {
    //     $gallery_images_array = $request->gallery_images;
    //     $zipcode = $request->zipcode;
    //     $request->merge(['gallery_images' => '']);
    //     $slug = $this->getSlug($request);
    //     $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    //     // Check permission
    //     Voyager::canOrFail('add_'.$dataType->name);
    //     //Validate fields with ajax
    //     $val = $this->validateBread($request->all(), $dataType->addRows);
    //     if ($val->fails()) {
    //         return response()->json(['errors' => $val->messages()]);
    //     }
    //     if (!$request->ajax()) {
    //         $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
    //         $id = $data->id;
    //         $return = array();
    //         if( $slug == 'sportevents'){
    //             $zipcode= $request->zipcode;
    //             $url1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
    //             $details=file_get_contents($url1);
    //             $result = json_decode($details,true);
    //             $lat=$result['results'][0]['geometry']['location']['lat'];
    //             $lng=$result['results'][0]['geometry']['location']['lng'];
    //             $newarry = array();
    //             $newarry['latitude'] = $lat;
    //             $newarry['longitude'] = $lng;
    //             $newEventMeta = new SportEventMeta();
    //             $newEventMeta->event_id = $id;
    //             $newEventMeta->key = 'lat_long';
    //             $newEventMeta->details = json_encode($newarry);
    //             $newEventMeta->save();
    //         }     
    //         if(count( $gallery_images_array) > 0  && $slug == 'sportevents'){
    //             $logged_user = Auth::User();
    //             $image = $gallery_images_array;
    //             function randomPassword() {
    //                 $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    //                 $pass = array(); //remember to declare $pass as an array
    //                 $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    //                 for ($i = 0; $i < 10; $i++) {
    //                     $n = rand(0, $alphaLength);
    //                     $pass[] = $alphabet[$n];
    //                 }
    //                 return implode($pass); //turn the array into a string
    //             }
    //             foreach ($image as $key=>$value) {  
    //                 $genratepublic_id = randomPassword();
    //                 $data = Cloudder::upload($value,$genratepublic_id);
    //                 $url = Cloudder::secureShow($genratepublic_id,array('width' => 400, 'height' => 400));
    //                 $return[ $genratepublic_id ] = $url;
    //             }
    //             $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', 'event_gallery')->first();
    //             $key = 'event_gallery';
    //             $found = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
    //             if( $found == '' || $found == null ){
    //                 $found = new SportEventMeta;
    //                 $found->key = $key;
    //                 $found->event_id = $id;
    //                 $found->save();
    //                 $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
    //             }
    //             $found->details = json_encode(  $return );
    //             $found->save();
    //         }
    //         return redirect()->route("voyager.{$dataType->slug}.edit", ['id' =>  $id])
    //         ->with([
    //             'message'    => "Successfully Added New {$dataType->display_name_singular}",
    //             'alert-type' => 'success',
    //         ]);
    //     }
    // }

         // POST BRE(A)D
    public function store(Request $request)
    {
    
        $gallery_images_array = $request->gallery_images;
         $zipcode = $request->zipcode;
       $request->merge(['gallery_images' => '']);
        // $request = (object) $request;
        
        // print_r($request);
        
        
        //          print_r($gallery_images_array) ;
        //   die();
           
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('add_'.$dataType->name);

        //Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        if( $slug == 'sportevents'){
            $request['event_start'] = date("Y-m-d", strtotime($request->event_start));
            $request['event_end'] = date("Y-m-d", strtotime($request->event_end));
            // print_r($request->all());die;
        }
        if (!$request->ajax()) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            
            $id = $data->id;
            
            $return = array();
             if( $slug == 'sportevents'){
                 
                    // $zipcode= $request->zipcode;
                    // $url1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
                    // $details=file_get_contents($url1);
                    // $result = json_decode($details,true);

                    // $newarry = array();
                    // if(isset($result['results'][0]['geometry']['location']['lat']))
                    // {
                    //     $lat=$result['results'][0]['geometry']['location']['lat'];
                    //     $lng=$result['results'][0]['geometry']['location']['lng'];
                        
                    //     $newarry['latitude'] = $lat;
                    //     $newarry['longitude'] = $lng;
                    // }
                    // $newEventMeta = new SportEventMeta();
                    // $newEventMeta->event_id = $id;
                    // $newEventMeta->key = 'lat_long';
                    // $newEventMeta->details = json_encode($newarry);
                    // $newEventMeta->save();

                    $sluge =  strtolower(str_replace(" ","-",$request->title));
                    $newEvent =  SportEvent::find($id);
                    $sportslug = SportEvent::where('title', $newEvent->title)->get()->count();
                    if($sportslug > 1){
                    $sluge = $sluge.$sportslug;
                    }
                    $newEvent->event_slug = $sluge;
                    $newEvent->save();

                    $newEventMetasend1 = SportEventMeta::where('event_id',$id)->where('key','sendemail')->first();
                    if(!$newEventMetasend1){
                  
                        // $finduser = DB::select("select * from user_information where meta_key = 'all_sports' and find_in_set(".$request->sport_id.",meta_value)");
                        $usersInfo1 = UserInformation::select('user_id')->where('meta_key','all_sports')->whereRaw("find_in_set(".$request->sport_id.",meta_value)");
                        $usersInfo1->orWhere('meta_key','sport')->where('meta_value',$request->sport_id);
                        $finduser = $usersInfo1->get();
                        $UsergetMAil = array();
                        foreach($finduser as $findusers){
                           
                            $userlat = UserInformation::where('user_id', '=', $findusers->user_id)->where('meta_key', '=', 'state')->first();
                            


                            if($userlat){
                                $valid = 0;
                                $userGender = UserInformation::where('user_id', '=', $findusers->user_id)->where('meta_key', '=', 'athelete_gender')->first();
                                if($userlat->meta_value == $request->state){
                                    if($userGender){
                                        $gender = ($userGender->meta_value == 'Men')?"Male":"Female";
                                        if($gender == $request->event_gender || $request->event_gender == "Both"){
                                            $valid = 1;
                                        }else{
                                            $valid = 0;
                                        }
                                    }else{
                                        $valid = 1;
                                    }
                                    if($valid == 1){
                                        $userget = User::where('id',$findusers->user_id)->where('user_approved','ACTIVE')->first();
                                    if($userget){
                                        $data3 = array();


                                        $data3['from'] = \Auth::User()->email;
                                        $data3['userName'] = $userget->name;
                                        $data3['templateData'] = self::collectData($userget->id,Auth::User()->id,'event_notification_template',21);
                                        Mail::to($userget->email)->send( new EventNotification( $data3 ));   
                                        $UsergetMAil[] = $userget->id;
                                    }
                                    }  
                                }  
                            }
                        }
                      }
                    $newEventMetasend = SportEventMeta::where('event_id',$id)->where('key','sendemail')->first();
                    if(!$newEventMetasend){
                    $newEventMetasend = new SportEventMeta();  
                    }
                    
                    $newEventMetasend->event_id = $id;
                    $newEventMetasend->key = 'sendemail';
                    $newEventMetasend->details = 'sendemail';
                    $newEventMetasend->save();
                 
                 
             }     
              if(count( $gallery_images_array) > 0  && $slug == 'sportevents'){
                  
                        $logged_user = Auth::User();
                        $image = $gallery_images_array;
                        function randomPassword() {
                            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                            $pass = array(); //remember to declare $pass as an array
                            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                            for ($i = 0; $i < 10; $i++) {
                                $n = rand(0, $alphaLength);
                                $pass[] = $alphabet[$n];
                            }
                             return implode($pass); //turn the array into a string
                        }
                        
                      foreach ($image as $key=>$value) {
                         
                          
                            $genratepublic_id = randomPassword();
                            $data = Cloudder::upload($value,$genratepublic_id);
                            $url = Cloudder::secureShow($genratepublic_id,array('width' => 400, 'height' => 400));
                           
                            $return[ $genratepublic_id ] = $url;
        
                         }
                         


                        $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', 'event_gallery')->first();
                        $key = 'event_gallery';
                        $found = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
                        if( $found == '' || $found == null ){
                        $found = new SportEventMeta;
                        $found->key = $key;
                        $found->event_id = $id;
                        $found->save();
                        $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
                        }
                        
                     
                        
                        $found->details = json_encode(  $return );
                        $found->save();
                          
               }
               if( $slug == 'users'){

                $userget = User::find($id);
                $slugu =  strtolower(str_replace(" ","-",$userget->name));
                $userslug = User::where('name', $userget->name)->get()->count();
                if($userslug > 1){
                $slugu = $slugu.$userslug;
                }
                $userget->user_slug = $slugu;
                if($request->is_paid == 'yes'){
                    $userget->is_paid = $request->is_paid;
                }
                $userget->save();


                if($userget->role_id == 4){
                    $mailName = 'coach_email';
                    $userInfo = UserInformation::where('user_id',$id)->where('meta_key','coach_email')->first();
                }else if($userget->role_id == 3){
                    $mailName = 'parent_email_0';
                    $userInfo = UserInformation::where('user_id',$id)->where('meta_key','parent_email_0')->first();
                }else{
                    $mailName = 'athelete_email';
                    $userInfo = UserInformation::where('user_id',$id)->where('meta_key','athelete_email')->first();
                }
                if(!$userInfo){
                    $userInfo = new UserInformation();
                }
                // print_r($userInfo->meta_value);
                // die();
                if( ( $userInfo->meta_value != '') && ( $userInfo->meta_value != $userget->email ) ){
                    
                    // Mail::to($userInfo->email)->send( new UserEmailUpdated( $userget , $userInfo )); 

                    $userInfo->user_id = $id;
                    $userInfo->meta_key = $mailName;
                    $userInfo->meta_value = $userget->email;
                    $userInfo->save();

                   
                }
                



             }
             if( $slug == 'schoolslist'){

                $userget = \App\ApiData::find($id);
                $slugs =  strtolower(str_replace(" ","-",$userget->name));
                $schoolslug = \App\ApiData::where('school_name', $userget->name)->get()->count();
                if($schoolslug > 1){
                $slugs = $slugs.$schoolslug;
                }
                $userget->school_slug = $slugs;
                $userget->save();

             }
                
            return redirect()
                ->route("voyager.{$dataType->slug}.edit", ['id' =>  $id])
                ->with([
                        'message'    => "Successfully Added New {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]);
        }
    }
    
    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************

    public function edit(Request $request, $id)
    {
       $gallery_images_array = $request->gallery_images;
       $request->merge(['gallery_images' => '']);
       
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            
        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        if( $slug == 'pages' ){
            $settings = PageMeta::orderBy('order', 'ASC')->where('page_id', '=', $id)->get();
        }elseif( $slug == 'sports' ){
            $settings = SportsMeta::orderBy('order', 'ASC')->where('sport_id', '=', $id)->get();
        }elseif( $slug == 'sportevents' ){
            $settings = SportEvent::where('sport_id', '=', $id)->get();
        }else{
            $settings = [];
        }
        return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'settings'));
    }
    

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        
        
        $gallery_images_array = $request->gallery_images;
        
          $zipcode = $request->zipcode;

          $request->merge(['gallery_images' => '']);

        if( !isset( $request->page_meta_content ) ){
            $slug = $this->getSlug($request);
    
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
            // Check permission
            Voyager::canOrFail('edit_'.$dataType->name);
    
            //Validate fields with ajax
            $val = $this->validateBread($request->all(), $dataType->addRows);
    
            if ($val->fails()) {
                return response()->json(['errors' => $val->messages()]);
            }
            if( $slug == 'sportevents'){
                $request['event_start'] = date("Y-m-d", strtotime($request->event_start));
                $request['event_end'] = date("Y-m-d", strtotime($request->event_end));
                // print_r($request->all());die;
            }
            if (!$request->ajax()) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
                $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                 $return = array();
                   if( $slug == 'sportevents'){
                 
                    // $zipcode= $request->zipcode;
                    // $url1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
                    // $details=file_get_contents($url1);
                    // $result = json_decode($details,true);
                    // $newarry = array();
                    // if(isset($result['results'][0]['geometry']['location']['lat']))
                    // {
                    // $lat=$result['results'][0]['geometry']['location']['lat'];
                    // $lng=$result['results'][0]['geometry']['location']['lng'];
                    
                   
                    // $newarry['latitude'] = $lat;
                    // $newarry['longitude'] = $lng;
                    // }
                    $slug1 =  strtolower(str_replace(" ","-",$request->title));
                    $newEvent =  SportEvent::find($id);
                    $sportslug = SportEvent::where('title', $newEvent->title)->get()->count();
                    if($sportslug > 1){
                    $slug1 = $slug1.$sportslug;
                    }
                    $newEventMetasend1 = SportEventMeta::where('event_id',$id)->where('key','sendemail')->first();
                    if(!$newEventMetasend1){
                  
                        // $finduser = DB::select("select * from user_information where meta_key = 'all_sports' and find_in_set(".$request->sport_id.",meta_value)");
                        $usersInfo1 = UserInformation::select('user_id')->where('meta_key','all_sports')->whereRaw("find_in_set(".$request->sport_id.",meta_value)");
                        $usersInfo1->orWhere('meta_key','sport')->where('meta_value',$request->sport_id);
                        $finduser = $usersInfo1->distinct('user_id')->get();
                        $UsergetMAil = array();
                        foreach($finduser as $findusers){
                           
                            $userlat = UserInformation::where('user_id', '=', $findusers->user_id)->where('meta_key', '=', 'state')->first();
                            if($userlat){
                                $valid = 0;
                                $userGender = UserInformation::where('user_id', '=', $findusers->user_id)->where('meta_key', '=', 'athelete_gender')->first();
                                if($userlat->meta_value == $request->state){
                                    if($userGender){
                                        $gender = ($userGender->meta_value == 'Men')?"Male":"Female";
                                        if($gender == $request->event_gender || $request->event_gender == "Both"){
                                            $valid = 1;
                                        }else{
                                            $valid = 0;
                                        }
                                    }else{
                                        $valid = 1;
                                    }
                                    if($valid == 1){
                                        $userget = User::where('id',$findusers->user_id)->where('user_approved','ACTIVE')->first();
                                    if($userget){
                                    
                                    $data3 = array();
                                    $data3['from'] = \Auth::User()->email;
                                    $datatemp = $addevent_notifi = PageMeta::where([ ['page_id', '=', '21'], ['key', '=', 'event_notification_template'] ])->first();
                                    $addevent_notifid1 = str_replace( '{event_id}', $id, $datatemp->value);
                                    $addevent_notifid1 = str_replace( '{event_slug}', $slug1, $addevent_notifid1);
                                    $addevent_notifid1 = str_replace( '{event_name}',$request->title, $addevent_notifid1 );
                                    $addevent_notifid1 = str_replace( '{name}', $userget->name, $addevent_notifid1 );
                                    $addevent_notifid1 = str_replace( '{city}',$request->city, $addevent_notifid1 );
                                    $addevent_notifid1 = str_replace( '{state}', $request->state, $addevent_notifid1 );
                                    $addevent_notifid1 = str_replace( '{zip}', $request->zipcode, $addevent_notifid1 );
                                     $addevent_notifid1 = str_replace( '{dates}', $request->event_start .' to '. $request->event_end, $addevent_notifid1 );
                                         $data3['subject'] =  $datatemp->display_name;
                                   
                                    $data3['templateData'] = $addevent_notifid1;
                                    Mail::to($userget->email)->send( new EventNotification( $data3 ));   
                                    $UsergetMAil[] = $userget->id;
                                    }
                                    }  
                                }  
                            }
                        }
                        // print_r($UsergetMAil);die;
                      }
                  
                    $newEvent->event_slug = $slug1;
                    $newEvent->save();

                    // $newEventMeta = SportEventMeta::where('event_id',$id)->where('key','lat_long')->first();
                    // if(!$newEventMeta){
                    // $newEventMeta = new SportEventMeta();  
                    // }
                    
                    // $newEventMeta->event_id = $id;
                    // $newEventMeta->key = 'lat_long';
                    // $newEventMeta->details = json_encode($newarry);
                    // $newEventMeta->save();

                    $newEventMetasend = SportEventMeta::where('event_id',$id)->where('key','sendemail')->first();
                    if(!$newEventMetasend){
                    $newEventMetasend = new SportEventMeta();  
                    }
                    
                    $newEventMetasend->event_id = $id;
                    $newEventMetasend->key = 'sendemail';
                    $newEventMetasend->details = 'sendemail';
                    $newEventMetasend->save();
                 
                 }
              if(count( $gallery_images_array) > 0  && $slug == 'sportevents'){
                  
                        $logged_user = Auth::User();
                        $image = $gallery_images_array;
                        function randomPassword() {
                            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                            $pass = array(); //remember to declare $pass as an array
                            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                            for ($i = 0; $i < 10; $i++) {
                                $n = rand(0, $alphaLength);
                                $pass[] = $alphabet[$n];
                            }
                             return implode($pass); //turn the array into a string
                        }
                        
                      foreach ($image as $key=>$value) {
                         
                            $genratepublic_id = randomPassword();
                            $data = Cloudder::upload($value,$genratepublic_id);
                            $url = Cloudder::secureShow($genratepublic_id,array('width' => 400, 'height' => 400));
                            $return[ $genratepublic_id ] = $url;
                         }
                         
                        $newdata = array();
                        $key = 'event_gallery';
                        $found = SportEventMeta::where('event_id', '=', $id)->where('key', 'event_gallery')->first();
                        if($found){
                        $newdata =(array)json_decode($found->details);
                        }
                        
                        $datagg = array_merge($newdata,$return);
                        // print_r(json_encode(  $datagg ));
                        // echo "<pre>";
                        // print_r($return);
                        // die;
                        // $found->details = json_encode(  $datagg );
                        // $found->save();
                        
                        //  $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', 'event_gallery')->first();
                        // $key = 'event_gallery';
                        // $found = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
                        if( $found == '' || $found == null ){
                        $found = new SportEventMeta;
                        $found->key = $key;
                        $found->event_id = $id;
                        $found->save();
                        $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
                        }
                        
                        $found->details = json_encode(  $datagg );
                        $found->save();
                          
               }
            if( $slug == 'users'){

                $userget = User::find($id);
                $slug =  strtolower(str_replace(" ","-",$userget->name));
                $userslug = User::where('name', $userget->name)->get()->count();
                if($userslug > 1){
                    $slug = $slug.$userslug;
                }
                $userget->user_slug = $slug;
                if($request->is_paid == 'yes'){
                    $userget->is_paid = $request->is_paid;
                }
                $userget->save();

                 if($userget->role_id == 4){
                    $mailName = 'coach_email';
                    $userInfo = UserInformation::where('user_id',$id)->where('meta_key','coach_email')->first();
                }else if($userget->role_id == 3){
                    $mailName = 'parent_email_0';
                    $userInfo = UserInformation::where('user_id',$id)->where('meta_key','parent_email_0')->first();
                }else{
                    $mailName = 'athelete_email';
                    $userInfo = UserInformation::where('user_id',$id)->where('meta_key','athelete_email')->first();
                }
                if(!$userInfo){
                    $userInfo = new UserInformation();
                }
                // print_r($userInfo->meta_value);
                // die();
                if( ( $userInfo->meta_value != '') && ( $userInfo->meta_value != $userget->email ) ){

                    // Mail::to($userInfo->email)->send( new UserEmailUpdated( $userget , $userInfo )); 

                    $userInfo->user_id = $id;
                    $userInfo->meta_key = $mailName;
                    $userInfo->meta_value = $userget->email;
                    $userInfo->save();
                }

                if(($userget->role_id == 4) && ($request->user_approved == 'ACTIVE')){
                    self::approve($id);
                }

             }
              if( $slug == 'schoolslist'){

                $userget = \App\ApiData::find($id);
                $slug =  strtolower(str_replace(" ","-",$userget->school_name));
                $userslug = \App\ApiData::where('school_name', $userget->school_name)->get()->count();
                if($userslug > 1){
                    $slug = $slug.$userslug;
                }
                $userget->school_slug = $slug;
                $userget->save();
             }
                    
                return redirect()
                ->route("voyager.{$dataType->slug}.edit", ['id' => $id])
                ->with([
                    'message'    => "Successfully Updated {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                    ]);
            }
        }

        else{
            $settings = PageMeta::where('page_id', '=', $request->page_id )->get();
            foreach ($settings as $setting) {
                $content = $this->getContentBasedOnType($request, 'settings', (object) [
                    'type'    => $setting->type,
                    'field'   => $setting->key,
                    'details' => $setting->details,
                ]);
                if ($content === null && isset($setting->value)) {
                    $content = $setting->value;
                }
                $setting->value = $content;
                $setting->save();
            }
            
            return back()->with([
                'message'    => 'Successfully Saved Settings',
                'alert-type' => 'success',
            ]);
        }
    }
  public function approve( $user_id ){
        $current_user = Auth::User();
        $success = User::where('id', $user_id)->first();
        if( $current_user->role_id == 1 && $success->user_approved == 'INACTIVE' ){
            User::where('id', $user_id)->update(['user_approved' => 'ACTIVE']);
        }
        if($success->verified == 0 && $success->role_id == 4){
            Mail::to( $success->email )->send(new AccountCreated( $success ));
        }
        $success->verified = '1';
        $success->save();
        return;
    }
    

    //***************************************
    //                _____
    //               |  __ \
    //               | |  | |
    //               | |  | |
    //               | |__| |
    //               |_____/
    //
    //         Delete an item BREA(D)
    //
    //****************************************

    public function destroy(Request $request, $id)
    {
        
        // return $id; 
        if( !( isset($request->destroy_page_meta) || isset($request->destroy_sport_meta) ) ){
            //echo "nn";
            $slug = $this->getSlug($request);
    
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
            // Check permission
            Voyager::canOrFail('delete_'.$dataType->name);
    
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
    
            // Delete Translations, if present
            if (is_bread_translatable($data)) {
                $data->deleteAttributeTranslations($data->getTranslatableAttributes());
            }
    
            foreach ($dataType->deleteRows as $row) {
                if ($row->type == 'image') {
                    $this->deleteFileIfExists('/uploads/'.$data->{$row->field});
    
                    $options = json_decode($row->details);
    
                    if (isset($options->thumbnails)) {
                        foreach ($options->thumbnails as $thumbnail) {
                            $ext = explode('.', $data->{$row->field});
                            $extension = '.'.$ext[count($ext) - 1];
    
                            $path = str_replace($extension, '', $data->{$row->field});
    
                            $thumb_name = $thumbnail->name;
    
                            $this->deleteFileIfExists('/uploads/'.$path.'-'.$thumb_name.$extension);
                        }
                    }
                }
            }
            if($slug == 'users'){
               $data = self::deleteUserAccount($id);
            }else if($slug == 'schoolslist'){
                $data = self::deleteSchool($id);
            }else{
                $data = $data->destroy($id);
            }
            $data = $data
             ? [
                    'message'    => "Successfully Deleted {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => "Sorry it appears there was a problem deleting this {$dataType->display_name_singular}",
                    'alert-type' => 'error',
                ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }else{
            if( isset($request->destroy_page_meta) ){
                PageMeta::destroy($request->destroy_id);
                return back()->with([
                    'message'    => 'Successfully Deleted Setting',
                    'alert-type' => 'success',
                ]);
            }
            if( isset($request->destroy_sport_meta) ){
                // return $request->destroy_id; die;
                SportsMeta::destroy($request->destroy_id);
                return back()->with([
                    'message'    => 'Successfully Deleted Setting',
                    'alert-type' => 'success',
                ]);
            }
        }
    }
    public  function deleteUserAccount($userId)
    {  //die('here'); 
            
            $user = User::find($userId);
        if($user && ( $user->role_id == 2 || $user->role_id == 3 ||$user->role_id == 4 ) ){
            if($user->role_id == 2 || $user->role_id == 3){ 
                if($user->subscribed('main')){ 
                // $cancel = \App::call('\App\Http\Controllers\SubscriptionsController@cancel');
                    self::cancel($user);
                }
            }       
            \App\User::find($user->id)->delete();
            \App\Notification::where('user_id',$user->id)->orWhere('viewer_user_id',$user->id)->delete();
            \App\LoginHistory::where('user_id',$user->id)->delete();
            if($user->role_id == 2 || $user->role_id == 3){
                \App\Subscription::where('user_id',$user->id)->delete();
                \App\AcademicDetailAthelete::where('user_id',$user->id)->delete();
                // \App\SportEvent::where('author_id',$user->id)->delete();
            }
            // \App\Testimonials::where('author_id',$user->id)->delete();
            // \App\Post::where('author_id',$user->id)->delete();
            // \App\Sport::where('author_id',$user->id)->delete();
            if($user->role_id == 4){
                \App\CoachsSchool::where('user_id',$user->id)->delete();
            }
            $deleteFromEventAttend = \DB::statement("UPDATE sport_event_meta SET `details` = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', `details`, ','),',$user->id,', ',')) WHERE FIND_IN_SET($user->id, `details`) AND `key` = 'coach_register' OR `key` = 'athlete_register';");

            //Auth::logout();
            // \Session::flash('success', 'Your Account is Deleted Permanently!');

            return 1;
        }else{
            // \Session::flash('error', 'Invalid User');
            return 0;
        }
    }
      public function cancel($user)
    {
        $user->subscription('main')->cancel();
        $user = $user;
       
        Mail::to( $user->email )->send( new SubscriptionCancelled( $user ) );
        return ;
    }

    public  function deleteSchool($schoolId)
    {  //die('here');     
        $school = \App\ApiData::find($schoolId);
        if($school){
          \App\CoachsSchool::where('school_id',$school->id)->delete();
          \App\ApiData::find($schoolId)->delete();
          return 1; 
          // Redirect()->back()->with(['message'    => 'School Deleted Permanently!','alert-type' => 'success',]);
        }else{
          return 0;
        }
    }
}
