<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use App\LoginHistory;
use TCG\Voyager\Http\Controllers\VoyagerAuthController as BaseVoyagerBreadController;
class VoyagerAuthController extends BaseVoyagerBreadController
{
    use AuthenticatesUsers;

    public function login()
    {
        return view('voyager::login');
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);
       
        
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);
        
       

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            $user = \App\User::where('email',Auth::User()->email)->first();
            if($user->user_status == 'ACTIVE'|| $user->role_id == 1){
                return $this->sendLoginResponse($request);
            }else{
                \Auth::logout();
                return Redirect()->back()->with('error','Your account is deactivated. Please Contact admin for more information');
            }
        }

       
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /*
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo()
    {
        $currentDate = date('Y-m-d H:i:s');
        $user = \App\User::where('email',Auth::User()->email)->first();
       
            $user->last_login = $currentDate;
            $user->save();
             /// login history add
            $loginHistory = new LoginHistory();
            $loginHistory->user_id = $user->id;
            $loginHistory->save();

            return route('voyager.dashboard');
        
     
    }
}
