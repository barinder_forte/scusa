<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use App\Testimonials;
use App\Mail\AccountCreated;
use Illuminate\Support\Facades\Mail;
use App\Sport;
use App\User;
use App\Post;
use Auth;
use App\Pages;
use Message;
use App\Mail\SubscriptionCancelled;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $user = User::find(3);
        // Mail::to( 'mohitchawla29@yahoo.in' )->send(new AccountCreated( 3 )); die;
        $testimonials = Testimonials::published()->get();
        $sports = Sport::where('status','PUBLISHED')->get();
        $posts = Post::where('status','PUBLISHED')->with('getcat')->get();
        
        $userdata['testimonials'] = json_decode($testimonials);
        $userdata['posts'] = $posts;
        $userdata['sports'] = $sports;
        $userdata['page'] = Pages::where('slug', '=', 'home')->first();
        $userdata['page_id'] = $userdata['page']->id;        // return view('signup-step', $userdata );
        // return view('signup-step', $userdata );
        // Mail::send('email.signup', ['user' => 'forte.test.only@gmail.com'], function ($m) {
        //     $m->from('admin@scusa-live-prnka47.c9users.io.com', 'SCUSA');
        //     $m->to('forte.test.only@gmail.com', 'Forte User')->subject('Your Reminder!');
        // });
        // Mail::to('mohitchawla29@yahoo.in')->queue('This is a text email');
        return view('home', $userdata);
    }
    
    public function orderPost(Request $request)
    {
        // $user = User::find(3);
        // $input = $request->all();
        // $token = $input['stripeToken'];
        // try {
        //     $user->subscription($input['plane'])->create($token,[
        //             'email' => $user->email
        //         ]);
        //     return back()->with('success','Subscription is completed.');
        // } catch (Exception $e) {
        //     return back()->with('success',$e->getMessage());
        // }
    }
    
}
