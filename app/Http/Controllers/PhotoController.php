<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserInformation;
use App\User;
use App\SportEventMeta;
use Auth;
use Cloudder;
class PhotoController extends Controller
{
    
    public function index()
    {
     
                return view('image');

    }

   
    public function store(Request $request)
    {

     
        $logged_user = Auth::User();
        $image = $request->image;
        function randomPassword() {
            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
            $pass = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 10; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
             return implode($pass); //turn the array into a string
        }
        
        $genratepublic_id = randomPassword();
        $data = Cloudder::upload($image,$genratepublic_id);
        $url = 'https://res.cloudinary.com/webforte/image/upload/'.$genratepublic_id .'.png';
        $new = array();
        $new[] =$url; 
        $new[] =$genratepublic_id; 
        
        // / update user table profile image ///
        $logged_user->avatar = $url;
        $logged_user->save();
        
        $value = array('user_profile_image','profile_public_id');
        $i = 0;
        foreach( $value as $key )
        {
            $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
                if( $found == '' || $found == null ){
                    $found = new UserInformation;
                    $found->meta_key = $key;
                    $found->user_id = $logged_user->id;
                    $found->save();
                    $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
                }
            $found->meta_value = $new[$i];
            $found->save();
            $i++;
        }
       // $progress = self::progress();
        $progress = \App::call('App\Http\Controllers\UserController@progress');
        $new['prog']=$progress; 
        return $new;
    
    }

    public function storegallery(Request $request)
    {
        $logged_user = Auth::User();
         $logged_user->id;
         $returnimage = array();
        $data =  $request->all();
        foreach( $data as $image){
            function randomPassword() {
                $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                $pass = array(); //remember to declare $pass as an array
                $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                for ($i = 0; $i < 10; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];
                }
            return implode($pass); //turn the array into a string
         }
            
            $genratepublic_id = randomPassword();
            $data = Cloudder::upload($image,$genratepublic_id);
            $url = Cloudder::secureShow($genratepublic_id,array('width' => 400, 'height' => 400));
            // $url = $url[0];
             $returnimage[ 'url' ] = $url;
             $returnimage[ 'public' ] = $genratepublic_id;
            //$returnimage['key'] =  $genratepublic_id;

        
        }
        
            $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'gallery')->first();
            $key = 'gallery';
            $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        if( $found == '' || $found == null ){
            $found = new UserInformation;
            $found->meta_key = $key;
            $found->user_id = $logged_user->id;
            $found->save();
            $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        }
        
        $new_data= (array) json_decode( $data_1['meta_value'] );
        $lenght = count($new_data);
       
        $new_data[$lenght][ 'url' ] = $url;
        $new_data[$lenght][ 'public' ]= $genratepublic_id;
        $found->meta_value = json_encode( $new_data );
        $found->save();
        // $data = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'gallery')->get()->first();
       
        // $new_data = array_reverse($new_data);
        // $sliced_array = array_slice($new_data, 0, 1);
        // $new_data= json_encode(  $sliced_array );
        $progress = \App::call('App\Http\Controllers\UserController@progress');
        $returnimage['prog']=$progress; 
        return $returnimage;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyimage(Request $request)
    {
        
        $logged_user = Auth::User();
        $public_id = $request->id;
        $result = Cloudder::destroy($public_id);
        $logged_user->avatar = 'users/default.png';
        $logged_user->save();
        $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'user_profile_image')->first();
        $data_1->meta_key = 'user_profile_image';
       
        $data_1->meta_value = '';
        $data_1->save();
        $progress = \App::call('App\Http\Controllers\UserController@progress');
        $returnimage['prog']=$progress; 
        return $returnimage;
        
    }
    
    public function gallerydelete(Request $request)
    {   
        $logged_user = Auth::User();
        $keyvalue = $request->key;
        $index = $request->index;
        $result = Cloudder::destroy($keyvalue);
        $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'gallery')->first();
        $new_data= json_decode( $data_1['meta_value'] );
     
        $key = 'gallery';
        unset($new_data[$index]);
  
        $new_data2 = array_values($new_data);
 
        $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        $found->meta_key = $key;
       
        $found->meta_value = json_encode($new_data2);
        $found->save();
        $progress = \App::call('App\Http\Controllers\UserController@progress');
        $returnimage['prog']=$progress; 
        return $returnimage;
    }
    
    
    public function videostore(Request $request){   
        $logged_user = Auth::User();
        $cloudname = env('CLOUDINARY_CLOUD_NAME','laravel');
        $logged_user->id;
        $data =  $request->all();
        $return = array();
         foreach( $data as $image){
            function randomPassword() {
                    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                    $pass = array(); //remember to declare $pass as an array
                    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                    for ($i = 0; $i < 10; $i++) {
                        $n = rand(0, $alphaLength);
                        $pass[] = $alphabet[$n];
                    }
                return implode($pass); //turn the array into a string
             }
                
                $genratepublic_id = randomPassword();
                $data = Cloudder::upload($image,$genratepublic_id,array("resource_type" => "video"));
                $url = "https://res.cloudinary.com/".$cloudname."/video/upload/".$genratepublic_id.".mp4";
            
            }
            
            
            $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'videogallery')->first();
            
            $key = 'videogallery';
            $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
            if( $found == '' || $found == null ){
                $found = new UserInformation;
                $found->meta_key = $key;
                $found->user_id = $logged_user->id;
                $found->save();
                $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
            }
            $new_data= (array) json_decode( $data_1['meta_value'] );
            $new_data[ $genratepublic_id ] = $url;
            $found->meta_value = json_encode( $new_data );
            $found->save();
            $data = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'videogallery')->get()->first();
            $data = (array)json_decode($data->meta_value);
            $rev = array_reverse($data);
            //  $data = array_splice($rev,0,1);
           
            $ne[]= $rev;
            $progress = \App::call('App\Http\Controllers\UserController@progress');
            $ne['prog']=$progress; 
            return $ne;
                
        }
        
    //     public function videodelete(Request $request){   
    //         $logged_user = Auth::User();
    //         $keyvalue = $request->key;
            
    //         $result = Cloudder::destroy($keyvalue);
            
    //         $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'videogallery')->first();
    //         $new_data= (array) json_decode( $data_1['meta_value'] );
    //         $key = 'videogallery';
    //         unset($new_data[$keyvalue]);
    //         $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
    //         $found->meta_key = $key;
    //         $found->meta_value = json_encode( $new_data );
    //         $found->save();
    //         $progress = \App::call('App\Http\Controllers\UserController@progress');
    //         $new['prog']=$progress; 
    //         return $new;
    // }
    
 
             // public function videodelete(Request $request)
             //    {   
             //        $logged_user = Auth::User();
             //        $keyvalue = $request->key;
             //        $index = $request->index;
             //        $result = Cloudder::destroy($keyvalue);
             //        $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'videogallery')->first();
             //        $new_data= json_decode( $data_1['meta_value'] );
             //        $key = 'videogallery';
             //        unset($new_data[$index]);
             //        $new_data2 = array_values($new_data);
             //        $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
             //        $found->meta_key = $key;
             //        $found->meta_value = json_encode($new_data2);
             //        $found->save();
             //        $progress = \App::call('App\Http\Controllers\UserController@progress');
             //        $returnimage['prog']=$progress; 
             //        return $returnimage;
             //    }
            public function videodelete(Request $request)
            {  
                $logged_user = Auth::User();
                $keyvalue = $request->key;
                $index = $request->index;
                $result = Cloudder::destroy($keyvalue);
                $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'videogallery')->first();
                $new_data= json_decode( $data_1['meta_value'] );
                $key = 'videogallery';
                $i = 0 ;
                foreach ($new_data as $keys ) {

                   if($keyvalue == $keys->public){

                     //   echo $new_data[$i];
                        // print_r($new_data[$i]);
                       unset($new_data[$i]);
                      // print_r($new_data);
                    }

               $i++;
               // unset($new_data[$index]);

               }
               //  print_r($new_data);

               // die();
                //die();
                //unset($new_data[$index]);
                $new_data2 = array_values($new_data);

               $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
                $found->meta_key = $key;

               $found->meta_value = json_encode((array)$new_data2);

               // print_r($found->meta_value);

               // die();

               $found->save();

            $progress = \App::call('App\Http\Controllers\UserController@progress');
            $returnimage['prog']=$progress;
            return $returnimage;
        }
        public function swap(Request $request)
        {   
            
        $logged_user = Auth::User();
        $old = $request->old;
        $new = $request->now;
    
        $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'gallery')->first();
        $new_data= json_decode( $data_1['meta_value'] );
        
        $tem = $new_data[$old];
        $new_data[$old] = $new_data[$new];
        $new_data[$new] = $tem;
        $data_1->meta_key = "gallery";
        $data_1->meta_value = json_encode( $new_data );
        $data_1->save();
        //return $result;
        // unset($new_data[$index]);
  
        // $new_data2 = array_values($new_data);
 
        // $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        // $found->meta_key = $key;
       
        // $found->meta_value = json_encode($new_data2);
        // $found->save();
        // return $result;
        
        
        }
 public function videostore1(Request $request)
    {
        $logged_user = Auth::User();
        $logged_user->id;
        $returnimage = array();
        $data =  $request->all();
        foreach( $data as $image){
            function randomPassword() {
                $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                $pass = array(); //remember to declare $pass as an array
                $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                for ($i = 0; $i < 10; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];
                }
            return implode($pass); //turn the array into a string
         }
            
                $genratepublic_id = randomPassword();
                $videotype = strpos($image,"hudl.com");
                if($videotype  == 0){
                    $videotype = strpos($image,"vimeo");
                    if($videotype  == 0){
                     $image = explode('v=',$image);
                     if( isset($image[1]))  {
                        
                     }else{
                        $image = explode('.be/',$image[0]);
                     }
                     $image ="https://www.youtube.com/embed/".$image[1];
                    }else{
                       $image = explode('com/',$image);
                       $image ="https://player.vimeo.com/video/".$image[1];
                    }
                 }else{
                     $image = explode('/video/',$image);    
                     $image ="https://www.hudl.com/embed/video/".$image[1];
                 }

             $url = $image;
             $returnimage[ 'url' ] = $url;
             $returnimage[ 'public' ] = $genratepublic_id;
           
        }
        
            $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', 'videogallery')->first();
            $key = 'videogallery';
            $found = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        if( $found == '' || $found == null ){
            $found = new UserInformation;
            $found->meta_key = $key;
            $found->user_id = $logged_user->id;
            $found->save();
            $data_1 = UserInformation::where('user_id', '=', $logged_user->id)->where('meta_key', '=', $key)->first();
        }
        
        $new_data= (array) json_decode( $data_1['meta_value'] );
        $lenght = count($new_data);
        $new_data[$lenght][ 'url' ] = $url;
        $new_data[$lenght][ 'public' ]= $genratepublic_id;
        $found->meta_value = json_encode( $new_data );
        $found->save();
        
        $progress = \App::call('App\Http\Controllers\UserController@progress');
        
        $returnimage['prog']=$progress; 
        return $returnimage;
    }
}
