<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\UserInformation;
use App\Sport;
use \Auth;
use App\Subscription;
use App\LoginHistory;

class ChartsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         if(!Auth::User()){
            return Redirect('/login');
        }
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }
    public function athleteCharts()
    {
        $data = array();
        $data['sportsplayed'] = json_encode($this->userSportPlayed());
        $data['ageperYear'] = json_encode($this->userPerYearAge());
        $data['profileComplete'] = json_encode($this->userProfileComplete());
        $data['parentsVsAthlete'] = json_encode($this->parentsVsAthlete());
        $data['athletesForState'] =  json_encode($this->athletesForState()) ;
        $data['coachesForState'] = '';
        $data['activeVsDeactive'] =  json_encode($this->activeVsInactive()) ;
        $data['paidvsnonpaid'] =  json_encode($this->paidVsNonpaid()) ;
        $data['totalemailsent'] =  json_encode($this->totalEmailsSentAthlete()) ;
        $data['usersigninginweekly'] =  json_encode($this->userSingnInAthletes()) ;
        $data['usersingninsingle'] =  json_encode($this->userSingnInSingleAth()) ;
         //return  $data ;
        return view('athlete_chart',$data);
    }
     public function coachCharts()
    {
        $data = array();
        $data['sportsplayed'] = json_encode($this->coachSportPlayed());
        $data['ageperYear'] = '';
        $data['profileComplete'] = json_encode($this->coachProfileComplete()) ;
        $data['parentsVsAthlete'] = '';
        $data['coachesForState'] =  json_encode($this->coachesForState()) ;
        $data['athletesForState'] =  '';
        $data['activeVsDeactive'] =  json_encode($this->activeVsInactiveCoach()) ;
        $data['paidvsnonpaid'] =  '' ;
        $data['totalemailsent'] =  json_encode($this->totalEmailsSentCoach()) ;
        $data['usersigninginweekly'] =  json_encode($this->userSingnInCoaches()) ;
        $data['usersingninsingle'] =  json_encode($this->userSingnInSingleCoach()) ;
       // return  $data ;
        return view('coach_chart',$data);
    }
    public function userProfileComplete()
    {
        $athleteA = array('0'=>'10','11'=>'20','21'=>'30','31'=>'40','41'=>'50','51'=>'60','61'=>'70','71'=>'80','81'=>'90','91'=>'100');
        $j = 0;
        foreach($athleteA as $key=>$a ){

            $progressPrecentageGroupsathletes[$j][0] = $key.'%'. '-' .$a.'%' ;
            $progressPrecentageGroupsathletes[$j][1] = \DB::table('user_information as ui')
                ->join('users as u', 'ui.user_id', '=', 'u.id')
                ->where('ui.meta_key','progress_data')
                ->whereBetween('ui.meta_value', [$key, $a])
                ->where('u.role_id',2)
                ->count();
            $progressPrecentageGroupsparent[$j][0] = $key.'%'. '-' .$a.'%' ;
            $progressPrecentageGroupsparent[$j][1] = \DB::table('user_information as ui')
                ->join('users as u', 'ui.user_id', '=', 'u.id')
                ->where('ui.meta_key','progress_data')
                ->whereBetween('ui.meta_value', [$key, $a])
                ->where('u.role_id',3)
                ->count();
            $j++;
        }
       
        $final_array['athletes'] = $progressPrecentageGroupsathletes;
        $final_array['parents'] = $progressPrecentageGroupsparent;
           
        return $final_array;
    }
    public function userSportPlayed()
    {   // \DB::connection()->enableQueryLog();
        $sports = Sport::select('title','id')->where('status','PUBLISHED')->get();
        $final_array = array();
        $userInfo = array();
        $userInfo2 = array();
        $userInfo3 = array();
        $userInfo4 = array();
        foreach($sports as $key2=>$s ){
            $userInfo = \DB::table('user_information as ui')
                ->join('users as u', 'ui.user_id', '=', 'u.id')
                ->where('meta_key','sport')
                ->where("ui.meta_value",$s->id)
                ->whereIn('u.role_id',[2])
                ->get()->pluck('user_id')->toArray();
            // $userInfo2 = \DB::table('user_information as ui')
            //     ->join('users as u', 'ui.user_id', '=', 'u.id')
            //     ->where('meta_key','all_sports')
            //     ->whereRaw("find_in_set(".$s->id.",meta_value)")
            //     ->whereIn('u.role_id',[2])
            //     ->get()->pluck('user_id')->toArray();
            $userInfo3 = \DB::table('user_information as ui')
                ->join('users as u', 'ui.user_id', '=', 'u.id')
                ->where('meta_key','sport')
                ->where("ui.meta_value",$s->id)
                ->whereIn('u.role_id',[3])
                ->get()->pluck('user_id')->toArray();
             // $userInfo4 = \DB::table('user_information as ui')
             //    ->join('users as u', 'ui.user_id', '=', 'u.id')
             //    ->where('meta_key','all_sports')
             //    ->whereRaw("find_in_set(".$s->id.",meta_value)")
             //    ->whereIn('u.role_id',[3])
             //    ->get()->pluck('user_id')->toArray();
            $final_array['athletes'][$key2][0] = $s->title;
            $final_array['athletes'][$key2][1] = count(array_unique( ($userInfo) ) );
            $final_array['parents'][$key2][0] = $s->title;
            $final_array['parents'][$key2][1] = count(array_unique( ($userInfo3) ) );;
         }
          
       // print_r(\DB::getQueryLog());
        return $final_array;
    }
    public function userPerYearAge()
    {
        $usersperYear = array();
        $age = array('13','14','15','16','17','18');
        $current_date = new \DateTime();
        $current_date = $current_date->format('Y/m/d');
        $j1 = 0; 
        foreach($age as $a){
            $usersAgePer[$j1][0] = $a . " Years Old";
            $usersAgePer[$j1][1] = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id', '=', 'u.id')
            ->where('ui.meta_key','athelete_birth')
            ->where(\DB::raw("TIMESTAMPDIFF(YEAR,`ui`.`meta_value`,'".$current_date."') = ".$a."") , 1)
            ->whereIn('u.role_id',[2,3])
             ->count();
            $j1++;
        }
        return $usersAgePer;
    }
    public function parentsVsAthlete()
    {
        $progressPrecentage[0][0] = 'Parents';
        $progressPrecentage[0][1] = User::where('role_id',3)->count();  
        $progressPrecentage[1][0] = 'Athletes';
        $progressPrecentage[1][1] = User::where('role_id',2)->count();
       
        return $progressPrecentage;
    }
    public function athletesForState()
    {
       $athletesForath = array();
       $athletesForPar = array();

       $states = \DB::table('user_information as ui')
                    ->select('ui.meta_value')
                    ->join('users as u', 'ui.user_id', '=', 'u.id')
                    ->where('ui.meta_key','state')->where('ui.meta_value','!=','NA')->where('ui.meta_value','!=','')
                    ->whereIn('u.role_id',[2,3])
                    ->distinct('ui.meta_value')
                    ->get();
        $athletesFor = array();
        foreach($states as $key=>$u){
            $athletesForath[$key][0] = $u->meta_value;  
            $athletesForath[$key][1] = \DB::table('user_information as ui')
                    ->join('users as u', 'ui.user_id', '=', 'u.id')
                    ->where('ui.meta_key','state')
                    ->where('ui.meta_value','!=','NA')
                    ->where('ui.meta_value','!=','')
                    ->where('ui.meta_value',$u->meta_value)
                    ->whereIn('u.role_id',[2])
                    ->count(); 
            $athletesForPar[$key][0] = $u->meta_value;  
            $athletesForPar[$key][1] = \DB::table('user_information as ui')
                    ->join('users as u', 'ui.user_id', '=', 'u.id')
                    ->where('ui.meta_key','state')
                    ->where('ui.meta_value','!=','NA')
                    ->where('ui.meta_value','!=','')
                    ->where('ui.meta_value',$u->meta_value)
                    ->whereIn('u.role_id',[3])
                    ->count(); 
        }
        $final_array['athletes'] = $athletesForath;
        $final_array['parents'] = $athletesForPar;
        return $final_array;
    }
     public function activeVsInactive()
    {
        $usersArray[0][0] = 'Active';
        $usersArray[0][1] = User::whereIn('role_id',[2,3])->where('user_status','ACTIVE')->count();
        $usersArray[1][0] = 'Deactive';
        $usersArray[1][1] = User::whereIn('role_id',[2,3])->where('user_status','INACTIVE')->count();  
       
        return $usersArray;
    }
     public function paidVsNonpaid()
    {
        $current_date = new \DateTime();
        $current_date = $current_date->format('Y-m-d H:i:s');
        $users = User::whereIn('role_id',[2,3])->select('id')->get();
               $usersArray[0][0] = 'Paid';
        // $usersInfo8 = User::whereIn('role_id',[2,3])->get();
               $usersInfoArray8['paid'] = array();
               $usersInfoArray8['nonpaid'] = array();
            foreach ($users as $key => $value) {
                $user = User::find($value->id);
                if( $user->is_paid == 'yes' ){
                    if( $user->subscribed('main') || $user->is_paid == 'yes' ){
                        $usersInfoArray8['paid'][] = $user->id; 
                    }
                }
                if( $user->is_paid == 'no' ){
                    if( ( ! $user->subscribed('main') ) && $user->is_paid == 'no' ){
                        $usersInfoArray8['nonpaid'][] = $user->id; 
                    }
                }
            }
        // $paidArray1 = \DB::table('subscriptions as ui')
        //     ->join('users as u', 'ui.user_id', '=', 'u.id')
        //     ->where('ui.ends_at','>',$current_date)
        //     ->orWhere('ui.ends_at',null)
        //     ->orderBy('ui.id','DESC')
        //     ->whereIn('u.role_id',[2,3])
        //     ->distinct('user_id')
        //     ->get()
        //     ->pluck('user_id')->toArray();
        // $paidArray2 = User::whereIn('role_id',[2,3])->where('is_paid','yes')->get()->pluck('id')->toArray();
        // $paidArray = array_merge($paidArray1,$paidArray2);

        // $usersArray[0][1] = count( array_intersect( $users , $paidArray ) );
        $usersArray[0][1] = count( $usersInfoArray8['paid'] );

        $usersArray[1][0] = 'Non Paid';
        // $usersArray[1][1] =  count( array_merge(array_diff($users,$paidArray), array_diff($paidArray,$users)) );
        $usersArray[1][1] =  count( $usersInfoArray8['nonpaid'] );
        return $usersArray ;
     
    }
    public function totalEmailsSentAthlete()
    {
        $users = \DB::table('user_information as ui')
                    ->join('users as u', 'ui.user_id', '=', 'u.id')
                    ->whereIn('u.role_id',[2,3])
                    ->where('ui.meta_key','email_counter')
                    ->get();
        $usersArray = array();
        $usersArray1 = array();
        $selectArray = array();
        $total = 0;
        foreach($users as $u){
                $usersArray1[$u->id][0][0] = $u->name;
                $usersArray1[$u->id][0][1] = $u->meta_value;
                $total += $u->meta_value;
                $selectArray[$u->id] = $u->name;
        }
        $usersArray['total_emails'] = $total;
        $usersArray['emails'] = $usersArray1;
        $usersArray['select_data'] = $selectArray;
        return $usersArray;
    }
    public function userSingnInAthletes()
    {
        $usersArrayath = array();
        $usersArrayPar = array();
        $datesArrayPar = array();
        $currentDate = date('Y-m-d 23:59:59');
        $currentDate28 = date('Y-m-d',strtotime('- 1 year',strtotime($currentDate )));
        for($i = $currentDate ; $i > $currentDate28 ; $i = date('Y-m-d 23:59:59',strtotime('- 7 days',strtotime($i ))) ){
            $datesArray[$i] = date('Y-m-d 00:00:00',strtotime('- 6 days',strtotime($i )));
        }
        $datesArray = array_reverse($datesArray);
        $j2 = 0;
        foreach($datesArray as $key=>$d){
        $datesArrayPar[date('M Y',strtotime($d ))] = date('M Y',strtotime($d ));
        if(isset($usersArrayath[date('M Y',strtotime($d ))]) == 1){
            $j2++;
        }else{
            $j2=0;
        }
        $usersArrayath[date('M Y',strtotime($d))][$j2][0] = date('M d-Y',strtotime($d)).' to '.date('M d-Y',strtotime($key ));
        // $usersArrayath[date('M Y',strtotime($d ))][$j2][1] = User::whereBetween('last_login',[$d,$key])->whereIn('role_id',[2])->count();
        $usersArrayath[date('M Y',strtotime($d ))][$j2][1] = \DB::table('login_histories as ui')
            ->join('users as u', 'ui.user_id', '=', 'u.id')
            ->where('u.role_id',2)
            ->whereBetween('ui.created_at',[$d,$key])
            ->distinct('user_id')->count();
        $usersArrayPar[date('M Y',strtotime($d ))][$j2][0] = date('M d-Y',strtotime($d )).' to '.date('M d-Y',strtotime($key ));
        // $usersArrayPar[date('M Y',strtotime($d ))][$j2][1] = User::whereBetween('last_login',[$d,$key])->whereIn('role_id',[3])->count();
        $usersArrayPar[date('M Y',strtotime($d ))][$j2][1] = \DB::table('login_histories as ui')
            ->join('users as u', 'ui.user_id', '=', 'u.id')
            ->where('u.role_id',3)
            ->whereBetween('ui.created_at',[$d,$key])
            ->distinct('user_id')->count();
        }
        $final_array['athletes'] = $usersArrayath;
        $final_array['parents'] = $usersArrayPar;
        $datesArrayPar = array_reverse($datesArrayPar);
        $final_array['datesarray'] = $datesArrayPar;
        return $final_array;
    }
    public function userSingnInSingleAth()
    {
        $users = \DB::table('login_histories as ui')
                    ->select('u.id','u.name')
                    ->join('users as u', 'ui.user_id', '=', 'u.id')
                    ->whereIn('u.role_id',[2,3])
                    ->distinct('ui.user_id')
                    ->get();
        $usersArrayath = array();
        $usersArrayPar = array();
        $userArray = array();
        $userDatesArray = array();
        $currentDate = date('Y-m-d 23:59:59');
        $currentDate28 = date('Y-m-d',strtotime('- 1 year',strtotime($currentDate )));
        for($i = $currentDate ; $i >= $currentDate28 ; $i = date('Y-m-d 23:59:59',strtotime('- 7 days',strtotime($i ))) ){
            $datesArray[$i] = date('Y-m-d 00:00:00',strtotime('- 6 days',strtotime($i )));
        }
        $datesArray = array_reverse($datesArray);
        $j2 = 0;
        foreach($users as $ukey=>$u){
            $userArray[$u->id] = $u->name;
            foreach($datesArray as $key=>$d){$userDatesArray[date('M Y',strtotime($d ))] = date('M Y',strtotime($d ));
                if(isset($usersArrayath[$u->id][date('M Y',strtotime($d ))]) == 1){
                    $j2++;
                }else{
                    $j2=0;
                }
            $usersArrayath[$u->id][date('M Y',strtotime($d ))][$j2][0] = date('M d-Y',strtotime($d)).' to '.date('M d-Y',strtotime($key ));
            $usersArrayath[$u->id][date('M Y',strtotime($d ))][$j2][1] = LoginHistory::whereBetween('created_at',[$d,$key])->where('user_id',$u->id)->count();
           
            }
        }
        $final_array['athletes'] = $usersArrayath;
        $final_array['usersArray'] = $userArray;
         $userDatesArray = array_reverse($userDatesArray);
        $final_array['userDatesArray'] = $userDatesArray;
        return $final_array;
    }
    
    public function coachSportPlayed()
    {
        $sports = Sport::select('title','id')->where('status','PUBLISHED')->get();
        // $j = 0;
        $usersPlayedGroups = array();
        foreach($sports as $key2=>$s ){
            $userInfo = UserInformation::where('meta_key','sport')
            ->where("meta_value",$s->id)->get()->pluck('user_id')->toArray();
            $result = User::whereIn('role_id',[4])->whereIn('id',$userInfo)->count();

            $usersPlayedGroups[$key2][0] = $s->title ;
            $usersPlayedGroups[$key2][1] = $result;
        }
        return $usersPlayedGroups;
    }
    public function coachProfileComplete()
    {
        $progressPrecentage = array();
        $athleteA = array('0'=>'10','11'=>'20','21'=>'30','31'=>'40','41'=>'50','51'=>'60','61'=>'70','71'=>'80','81'=>'90','91'=>'100');
        $j6 = 0;
        foreach($athleteA as $key2=>$a ){
            $userInfo = UserInformation::where('meta_key','progress_data')
                ->whereBetween('meta_value',[$key2 , $a])->get()->pluck('user_id')->toArray();
            $result = User::where('role_id',4)->whereIn('id',$userInfo)->count(); 
            $progressPrecentageGroupscoach['coaches'][$j6][0] = $key2.'%'. '-' .$a.'%';
            $progressPrecentageGroupscoach['coaches'][$j6][1] = $result;
            $j6++;
        }
        return $progressPrecentageGroupscoach;
    }
    public function coachesForState()
    {
        $states = \App\CoachsSchool::select('state')->orderBy('state','ASC')->distinct('state')->get();
        $coachFor = array();
        foreach($states as $key=>$u){
            $coachFor[$key][0] = $u->state;  
            $coachFor[$key][1] =  \DB::table('coachschool as ui')
                ->join('users as u', 'ui.user_id', '=', 'u.id')
                ->whereIn('u.role_id',[4])
                ->where('ui.state',$u->state)
                ->count(); 
        }
        return $coachFor;
    }
    public function activeVsInactiveCoach()
    {
       $usersArraycoach[0][0] = 'Active';
        $usersArraycoach[0][1] = User::whereIn('role_id',[4])->where('user_status','ACTIVE')->count();
        $usersArraycoach[1][0] = 'Deactive';
        $usersArraycoach[1][1] = User::whereIn('role_id',[4])->where('user_status','INACTIVE')->count();
        return $usersArraycoach;
    }
    public function totalEmailsSentCoach()
    {
        $users = \DB::table('user_information as ui')
            ->join('users as u', 'ui.user_id', '=', 'u.id')
            ->where('u.role_id',4)
            ->where('ui.meta_key','email_counter')
            ->get();
        $usersArray = array();
        $usersArray1 = array();
        $selectArray = array();
        $total = 0;
        foreach($users as $u){
            $usersArray1[$u->id][0][0] = $u->name;
            $usersArray1[$u->id][0][1] = $u->meta_value;
            $total += $u->meta_value;
            $selectArray[$u->id] = $u->name;
        }
        $usersArray['total_emails'] = $total;
        $usersArray['emails'] = $usersArray1;
        $usersArray['select_data_coach'] = $selectArray;
        return $usersArray;
    }
    public function userSingnInCoaches()
    {
        $coachsArrayPar = array();
        $datesArrayPar = array();
        $currentDate = date('Y-m-d 23:59:59');
        $currentDate28 = date('Y-m-d',strtotime('- 1 year',strtotime($currentDate )));
        for($i = $currentDate ; $i > $currentDate28 ; $i = date('Y-m-d 23:59:59',strtotime('- 7 days',strtotime($i ))) ){
            $datesArray[$i] = date('Y-m-d 00:00:00',strtotime('- 6 days',strtotime($i )));
        }
        $datesArray = array_reverse($datesArray);
        $j2 = 0;
        foreach($datesArray as $key=>$d){
            $datesArrayPar[date('M Y',strtotime($d ))] = date('M Y',strtotime($d ));
        if(isset($coachsArrayPar[date('M Y',strtotime($d ))]) == 1){
            $j2++;
        }else{
            $j2=0;
        }
        $coachsArrayPar[date('M Y',strtotime($d))][$j2][0] = date('M d-Y',strtotime($d )).' to '.date('M d-Y',strtotime($key ));
        // $coachsArrayPar[date('M Y',strtotime($d ))][$j2][1] = User::whereBetween('last_login',[$d,$key])->count();
        $coachsArrayPar[date('M Y',strtotime($d ))][$j2][1] = \DB::table('login_histories as ui')
            ->join('users as u', 'ui.user_id', '=', 'u.id')
            ->where('u.role_id',4)
            ->whereBetween('ui.created_at',[$d,$key])
            ->distinct('user_id')->count();
        }
        $datesArrayPar = array_reverse($datesArrayPar);
        $final_array['coachs'] = $coachsArrayPar;
        $final_array['datesarray'] = $datesArrayPar;
        return $final_array;
    } 
     public function userSingnInSingleCoach()
    {
        $users = \DB::table('login_histories as ui')
                    ->select('u.id','u.name')
                    ->join('users as u', 'ui.user_id', '=', 'u.id')
                    ->whereIn('u.role_id',[4])
                    ->distinct('ui.user_id')
                    ->get();
        $usersArrayath = array();
        $usersArrayPar = array();
        $userArray = array();
        $userDatesArray = array();
        $currentDate = date('Y-m-d 23:59:59');
        $currentDate28 = date('Y-m-d',strtotime('- 1 year',strtotime($currentDate )));
        for($i = $currentDate ; $i > $currentDate28 ; $i = date('Y-m-d 23:59:59',strtotime('- 7 days',strtotime($i ))) ){
            $datesArray[$i] = date('Y-m-d 00:00:00',strtotime('- 6 days',strtotime($i )));
        }
        $datesArray = array_reverse($datesArray);
        $j2 = 0;
        foreach($users as $ukey=>$u){
            $userArray[$u->id] = $u->name;
            foreach($datesArray as $key=>$d){
                $userDatesArray[date('M Y',strtotime($d ))] = date('M Y',strtotime($d ));
                if(isset($usersArrayath[$u->id][date('M Y',strtotime($d ))]) == 1){
                    $j2++;
                }else{
                    $j2=0;
                }
            $usersArrayath[$u->id][date('M Y',strtotime($d ))][$j2][0] = date('M d-Y',strtotime($d)).' to '.date('M d-Y',strtotime($key ));
            $usersArrayath[$u->id][date('M Y',strtotime($d ))][$j2][1] = LoginHistory::whereBetween('created_at',[$d,$key])->where('user_id',$u->id)->count();
           
            }
        }
        $userDatesArray = array_reverse($userDatesArray);
        $final_array['coaches'] = $usersArrayath;
        $final_array['usersArray'] = $userArray;
        $final_array['userDatesArray'] = $userDatesArray;
        return $final_array;
    }
}
