<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\SportEvent;
use App\SportEventMeta;
use App\Sport;
use Auth;
use Session;
use App\User;
use App\UserInformation;
use Cloudder;
use Mail;
use App\Mail\ContactCoach;
use App\Mail\ContactAthelete;
use Illuminate\Mail\Message;
use Dompdf\Dompdf;
use \PDF;
use App\Mail\EmailSend;
use App\Mail\EventNotification;
use App\Mail\EventVerification;
use DB;
use App\AcademicDetailAthelete;
use Voyager;
use App\PageMeta;
use App\Pages;
use App\Post;
use App\ApiData;
use App\CoachsSchool;

class SportsEventController extends Controller
{
    
    public function addevent(){
           if(!Auth::User()){
               return Redirect('/login');
           }
           if(Auth::User()->role_id == 4 || Auth::User()->role_id == 1){
             
           $data = array();
           if(Auth::User()->role_id != 1){
           $authSport = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','sport')->first();
           $sport = Sport::select('id','title')->where('id',$authSport->meta_value)->get()->toArray();
           $data['sports'] = $sport;
           
           }else{
           $sports = Sport::select('id','title')->where('status', '=', 'PUBLISHED')->get()->toArray();
           
               $data['sports'] = $sports;
         
           // $data['sports'] = \Voyager::Sport('all');
           }
           $tournamentsss = Voyager::pagemetadetail('tournaments', '16');
           $tournamentsss = json_decode($tournamentsss);
           $data['tournamentsss'] = $tournamentsss->options;
           $divisionss = Voyager::pagemetadetail('Division', '16');
           $divisionss = json_decode($divisionss);
           $data['divisionss'] = $divisionss->options;

           $data['scholllist'] = ApiData::select('id','school_name')->orderBy('school_name','ASC')->get();
           // print_r($data['sports']);die;
           $data['page'] = Pages::where('slug', '=', 'addevent')->first();
           $data['page_id'] = $data['page']->id;
           return view('addevent',$data);
           }else{
                $post = Post::paginate(4);
               $userData['posts'] = $post;
               $userData['page'] = Pages::where('slug', '=', '404-page')->first();
               $userData['page_id'] = $userData['page']->id;
               return view('errors.404',$userData);
           }
       }
    public function addevent1(){     
        return view('addeve');
    }
   
    public function show( $slug,Request $request = null){
        
        // if(!Auth::User()){
        //     return Redirect('/login');
        // }
        $data = array();
        if(\Auth::User()->role_id != 4){
           $templatedata = self::getTemplates(18);
        }else{
           $templatedata = self::getTemplates(17) ;
        }
        
        $sportData = SportEvent::where('event_slug',$slug)->first();
   
        $sportsMeta = SportEventMeta::where('event_id', $sportData->id)->where('key', 'event_gallery')->first();
        $sportsRelatedEvents = SportEvent::where('sport_id', $sportData->sport_id)->where('id','<>' , $sportData->id)->where('event_end','>=',date('m/d/Y'))->where('status','PUBLISHED')->orderBy('event_start','ASC')->get();
        
        if(Auth::User()){
            if(Auth::User()->role_id == '4' ) {
                $role1 = 'coach_register';
            }else{
                 $role1 = 'athlete_register';
            }
            $UserRegistered = SportEventMeta::where('event_id', $sportData->id)->where('key', $role1)->first();
            if($UserRegistered){
                $lastArrayu = explode(',',$UserRegistered->details);
                if( in_array(Auth::User()->id ,$lastArrayu)){
                   $UserRegisteredStatus = 'yes';
                }else{
                    $UserRegisteredStatus = 'no';  
                }
            }else{
                $UserRegisteredStatus = 'no'; 
            }
        }else{
            $UserRegisteredStatus = 'no';   
        }
        $role = '';
        $attendeesArray = array();
        $attendeesArrayFull = array();
        if(Auth::User()){
            if(Auth::User()->role_id == '4'){
                $role = 'athlete_register';
            }else if(Auth::User()->role_id == '2' || Auth::User()->role_id == '3') {
                $role = 'coach_register';
            }else{
                $role = 'admin';
            }
             if(Auth::User()->role_id == 4){
                $genderVal = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','coach_gender')->first();
                  $genderKey = 'athelete_gender';
            }else{
                $genderVal = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','athelete_gender')->first();
                  $genderKey = 'coach_gender';
            }
           // print($genderVal);die;

             $AttendeesValues = array();
            if(Auth::User()->role_id != '1'){
            $Attendees = SportEventMeta::where('event_id', $sportData->id)->where('key', $role)->first(); 
            if($Attendees){
                $AttendeesValues = $Attendees->details ;
            }
            }else{
            $attendeesgetA= array();
            $attendeesgetC = array();
            $AttendeesC = SportEventMeta::where('event_id', $sportData->id)->where('key', 'coach_register')->first(); 
            $AttendeesA = SportEventMeta::where('event_id', $sportData->id)->where('key', 'athlete_register')->first();
            if($AttendeesC){
            $attendeesgetC = explode(',',$AttendeesC->details);
            }
            if($AttendeesA){
            $attendeesgetA = explode(',',$AttendeesA->details);
            }
            $comman = array_merge($attendeesgetC , $attendeesgetA);
            $commanvalues = implode(',',$comman);
            // echo '<pre>';print_r($comman);die;
            $AttendeesValues = $commanvalues ;
            }
        if($AttendeesValues){
             $attendeesget = explode(',',$AttendeesValues);
            if($request->all()){
                $resultArray = $attendeesget;
                $data['year_graduating'] = $request->year_graduating;
                $data['position'] = $request->position;
                $data['gpa'] = $request->gpa;
                $data['state'] = $request->state;
                $data['division'] = $request->division;
                if($request->year_graduating != ''){
                    $usersInfo1 = UserInformation::select('user_id');
                    $usersInfo1->where('meta_key','graduation')->where("meta_value",$request->year_graduating);
                    $usersInfoArray = $usersInfo1->pluck('user_id')->toArray();
                    $resultArray = array_intersect($usersInfoArray,$resultArray);  
                }
                if($request->position != ''){
                    $usersInfo2 = UserInformation::select('user_id');
                    $usersInfo2->where('meta_key','position')->where('meta_value' , $request->position);
                    $usersInfoArray2 = $usersInfo2->pluck('user_id')->toArray();
                    $resultArray = array_intersect($usersInfoArray2,$resultArray);  
                }
                if($request->gpa != ''){
                    $usersInfo6 = AcademicDetailAthelete::select('user_id')->where('gpa','>=',$request->gpa)->get();
                    $usersInfoArray6 = $usersInfo6->pluck('user_id')->toArray();
                    $resultArray = array_intersect($usersInfoArray6,$resultArray);
                    //print_r($resultArray); 
                }
                if($request->state != ''){
                    $usersInfo2 = CoachsSchool::select('user_id');
                    $usersInfo2->where('state', $request->state);
                    $usersInfoArray2 = $usersInfo2->pluck('user_id')->toArray();
                    $resultArray = array_intersect($usersInfoArray2,$resultArray);  
                }
                if($request->division != ''){
                    $usersInfo2 = CoachsSchool::select('user_id');
                    $usersInfo2->where('division' , $request->division);
                    $usersInfoArray2 = $usersInfo2->pluck('user_id')->toArray();
                    $resultArray = array_intersect($usersInfoArray2,$resultArray);  
                }
                if(Auth::User()->role_id != 1){
                    if((isset($genderVal->meta_value)?$genderVal->meta_value:'') == 'Both' || (Auth::User()->role_id == 2 || Auth::User()->role_id == 3)){                            
                        $usersInfo7 = UserInformation::select('user_id');
                        if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
                            $usersInfo7->where('meta_key',$genderKey)->where('meta_value',$genderVal->meta_value)->orWhere('meta_value','Both')->where('meta_key',$genderKey);
                        }else{
                            $usersInfo7->where('meta_key',$genderKey);
                        }
                        $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                        $attendeesget = array_intersect($usersInfoArray7,$attendeesget); 
                     
                    }else{
                        $usersInfo7 = UserInformation::select('user_id');
                        $usersInfo7->where('meta_key',$genderKey)->where('meta_value' ,isset($genderVal->meta_value)?$genderVal->meta_value:'');
                        $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                        $attendeesget = array_intersect($usersInfoArray7,$attendeesget);

                    }
                }
                if(count($resultArray) > 0){
                    $attendeesArray = User::whereIn('id',$resultArray)->where('user_approved','ACTIVE')->paginate(5);
                    $attendeesArrayFull = User::whereIn('id',$resultArray)->where('user_approved','ACTIVE')->get();   
                }else{
                    $attendeesArray = array();
                    $attendeesArrayFull = array();
                }
            }else{
                if(Auth::User()->role_id != 1){
                    if((isset($genderVal->meta_value)?$genderVal->meta_value:'') == 'Both' || (Auth::User()->role_id == 2 || Auth::User()->role_id == 3) ){                            
                        $usersInfo7 = UserInformation::select('user_id');
                        if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
                            $usersInfo7->where('meta_key',$genderKey)->where('meta_value',$genderVal->meta_value)->orWhere('meta_value','Both')->where('meta_key',$genderKey);
                        }else{
                            $usersInfo7->where('meta_key',$genderKey);
                        }
                        $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                        $attendeesget = array_intersect($usersInfoArray7,$attendeesget); 
                     // print_r($attendeesget);  die('here');
                    }else{
                         $usersInfo7 = UserInformation::select('user_id');
                        $usersInfo7->where('meta_key',$genderKey)->where('meta_value' ,isset($genderVal->meta_value)?$genderVal->meta_value:'');
                        $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                        $attendeesget = array_intersect($usersInfoArray7,$attendeesget);
                        // print_r($usersInfoArray7);  die('here');
                    }
                }

               $attendeesArray = User::whereIn('id',$attendeesget)->where('user_approved','ACTIVE')->paginate(5);
               $attendeesArrayFull = User::whereIn('id',$attendeesget)->where('user_approved','ACTIVE')->get();  
            }    

        }else{
             $attendeesArray = array();
             $attendeesArrayFull = array();
             
        }
    }
         
        $locations = UserInformation::where('meta_key','state')->select('meta_value')->distinct()->get()->toArray();
      
         return view('event_single_show',compact('sportData','sportsMeta','sportsRelatedEvents','role', 'UserRegisteredStatus' ,'state', 'attendeesArray','attendeesArrayFull','locations','templatedata','data'));
    }
    public function eventlist(){
          $userData = Auth::User();
        $data = array();
        if(\Auth::User()->role_id != 4){
            $templatedata = self::getTemplates(18);
        }else{
            $templatedata = self::getTemplates(17) ;
        }
        $data['templatedata'] = $templatedata;
        if(Auth::User()->role_id != '1' ){
            $arraysur = array();
            if(Auth::User()->role_id != 4){
            $sportid = UserInformation::where('meta_key','all_sports')->where('user_id',Auth::User()->id)->first();
            if($sportid){
            $arraysur = explode(',',$sportid->meta_value);
            }
            }
            $sportid2 = UserInformation::where('meta_key','sport')->where('user_id',Auth::User()->id)->first();
           if($sportid2){
            $arraysur2 = explode(',',$sportid2->meta_value);
            }else{
              $arraysur2 = array();  
            }
            $mergeArray = array_merge($arraysur,$arraysur2 );
            
            $data['allSports'] = Sport::whereIn('id',$mergeArray)->where('status','PUBLISHED')->orderBy('title','ASC')->get();
            // if(! $userData->subscribed('main') && Auth::User()->role_id != 4){
            //     $data['allEvents'] = SportEvent::where('status','PUBLISHED')->where('event_end','>=',date('m/d/Y'))->whereIn('sport_id',$mergeArray)->limit(5)->get();
            // }else{
               $data['allEvents'] = SportEvent::where('status','PUBLISHED')->where('event_end','>=',date('m/d/Y'))->whereIn('sport_id',$mergeArray)->paginate(10);
            // }
            
        }else{
            $data['allSports'] = Sport::where('status','PUBLISHED')->orderBy('title','ASC')->get();
            $data['allEvents'] = SportEvent::where('status','PUBLISHED')->where('event_end','>=',date('m/d/Y'))->paginate(10);
        }
            $data['allStates'] = SportEvent::where('state','!=','')->select('state')->distinct()->get()->toArray();
            $data['page'] = Pages::where('slug', '=', 'event-list')->first();
            $data['page_id'] = $data['page']->id;
         // print_r($data['templatedata']);die;
        return view('event_list',$data);
    }
    
    public function eventRegister($slug){
        
        $sports = Sport::where('status','PUBLISHED')->get();
        
          $Event = SportEvent::where('event_slug',$slug)->first();
         
            if(Auth::User()->role_id == '4') {
                $role = 'coach_register';
            }else{
                $role = 'athlete_register'; 
            }
            $sportsMeta = SportEventMeta::where('event_id', $Event->id)->where('key', $role)->first();
            if($sportsMeta){
                $lastArray = explode(',',$sportsMeta->details);
                if( !in_array(Auth::User()->id ,$lastArray)){
                    array_push($lastArray,Auth::User()->id); 
                    $lastArray = array_filter($lastArray);
                    $sportsMeta->details =  implode(',',$lastArray);
                    $sportsMeta->save();
                    //Session::flash('message', 'You Are Registered Successfiully!'); 
                    return Redirect()->back();
                }else{
                    if (($key1 = array_search(Auth::User()->id, $lastArray)) !== false) {
                        unset($lastArray[$key1]);
                    }
                    $lastArray = array_filter($lastArray);
                    $sportsMeta->details =  implode(',',$lastArray);
                    $sportsMeta->save();
                    //Session::flash('message', 'You Are Registered Successfiully!'); 
                    return Redirect()->back();
                }
            }else if($role != ''){
                
                $sportsMeta = new SportEventMeta(); 
                $sportsMeta->event_id = $Event->id;
                $sportsMeta->key = $role;
                $sportsMeta->details = Auth::User()->id ;
                $sportsMeta->save();
               
                //Session::flash('message', 'You Are Registered Successfully!');  
                return Redirect()->back();
            }else{
                return Redirect()->back();
            }
         
    }
    
    public function store(Request $request){
        
        $imagess = $request->image;
        function randomPassword() {
            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
            $pass = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 10; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
             return implode($pass); //turn the array into a string
        }
        if($imagess != ''){
        $genratepublic_id = randomPassword().'sport';
        $data = Cloudder::upload($imagess,$genratepublic_id);
        $url = Cloudder::secureShow($genratepublic_id,array('width' => 400, 'height' => 400));
        }else{
            $url = '';
        }
        $zipcode= $request->zipcode;
        // $url1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
        // $details=file_get_contents($url1);
        // $result = json_decode($details,true);
        // $lat=$result['results'][0]['geometry']['location']['lat'];
        // $lng=$result['results'][0]['geometry']['location']['lng'];


        $slug =  strtolower(str_replace(" ","-",$request->title));
        $newEvent = new SportEvent();
        $sportslug = SportEvent::where('title', $request->title)->get()->count();
        if($sportslug >= 1){
          $slug = $slug.$sportslug;
        }
        $newEvent->event_slug = $slug;



        $newEvent->title = $request->title;
        $newEvent->author_id = Auth::user()->id;
        $newEvent->description = $request->description;
        $newEvent->event_start = date_format(date_create($request->event_start) , 'm/d/Y');
        $newEvent->event_end = date_format(date_create($request->event_end) , 'm/d/Y');
        $newEvent->place = $request->place;
        $newEvent->city = $request->city;
        $newEvent->state = $request->state;
        $newEvent->zipcode = $request->zipcode;
        $newEvent->division = $request->division;
        $newEvent->address = $request->address;
        $newEvent->event_gender = $request->event_gender;
        $newEvent->event_type = $request->typeevent;
        $newEvent->event_url = $request->event_url;

        // $newEvent->tournaments = $request->tournaments;
        // $newEvent->campus_clinics = $request->campus_clinics;


        if(isset($request->schoolinput)){
              
           $scholllist = ApiData::where('school_name','=',$request->schoolinput)->first();
            if(!$scholllist){
                $slug =  strtolower(str_replace(" ","-",$request->schoolinput));
                        $schoolslug = ApiData::where('school_name',$request->schoolinput)->get()->count();
                        if($schoolslug >= 1){
                            $slug = $slug.$schoolslug;
                        }
                $scholllist = new ApiData();
                $scholllist->school_name = $request->schoolinput;
                $scholllist->school_slug = $slug;
                $scholllist->save();
                $newEvent->school_attending = $scholllist->id;
                }else{

               $newEvent->school_attending = '';
            }
        }
        $newEvent->sport_id = $request->sport_id;
        $newEvent->image = $url;
        $newEvent->gallery_images = '';
        $newEvent->status = 'PENDING';
        $newEvent->save();
        
        $Eid = $newEvent->id;
        // $newarry = array();
        // $newarry['latitude'] = $lat;
        // $newarry['longitude'] = $lng;
        // $newEventMeta = new SportEventMeta();
        // $newEventMeta->event_id = $Eid;
        // $newEventMeta->key = 'lat_long';
        // $newEventMeta->details = json_encode($newarry);
        // $newEventMeta->save();

        if(Auth::User()->role_id == 4){
               $newEventMeta1 = new SportEventMeta();
               $newEventMeta1->event_id = $Eid;
               $newEventMeta1->key = 'coach_register';
               $newEventMeta1->details = Auth::User()->id;
               $newEventMeta1->save();
           }

        $id = $Eid;
        $allImages = $request->file;
        $return = array();
            if(count( $allImages) > 0  ){
                $logged_user = Auth::User();
                $image = $allImages;
                foreach ($image as $value) {
                    $genratepublic_id1 = randomPassword();
                    $data1 = Cloudder::upload($value,$genratepublic_id1);
                    $url1 = Cloudder::secureShow($genratepublic_id,array('width' => 400, 'height' => 400));
                    $return[ $genratepublic_id1 ] = $url1;
                }
                $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', 'event_gallery')->first();
                $key = 'event_gallery';
                $found = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
                if( $found == '' || $found == null ){
                $found = new SportEventMeta;
                $found->key = $key;
                $found->event_id = $id;
                $found->save();
                $data_1 = SportEventMeta::where('event_id', '=', $id)->where('key', '=', $key)->first();
                }
                $found->details = json_encode(  $return );
                $found->save();
            }
            $data4['from'] =  Auth::User()->email;
            $data4['event_id'] =  $id;
            $data4['event_slug'] =  $slug;
            $data4['event_name'] =  $request->title;
            $data4['templateData'] = self::collectData(1,Auth::User()->id,'add_event_notification',21);
            //print_r($data4['templateData']);die;
            $admin_emails = Voyager::pagemeta('admin_email', '20');
            $admin_emails = explode(',', $admin_emails);
            Mail::to($admin_emails[0])->send( new EventVerification( $data4 ,$admin_emails));   
        return '';
    }
    public function eventShow( $userType,$slug  , Request $request = null){
        $data = array();
         if((\Auth::User()->role_id == 4 && $userType != 'coach' ) || (\Auth::User()->role_id != 4 && $userType != 'athlete') || (\Auth::User()->role_id == 1 ) ) {
         if(\Auth::User()->role_id != 4){
          $templatedata = self::getTemplates(18);
        }else{
           $templatedata = self::getTemplates(17) ;
        }
        $sportData = SportEvent::where('event_slug', $slug )->first();
    
        $sportsMeta = SportEventMeta::where('event_id', $sportData->id)->where('key', 'event_gallery')->first();
        $sportsRelatedEvents = SportEvent::where('sport_id', $sportData->sport_id)->where('id','<>' , $sportData->id)->where('event_end','>=',date('m/d/Y'))->where('status','PUBLISHED')->orderBy('event_start','ASC')->get();
        
        if(Auth::User()){
            if(Auth::User()->role_id == '4' ) {
                $role1 = 'coach_register';
            }else{
                $role1 = 'athlete_register';  
            }
            $UserRegistered = SportEventMeta::where('event_id', $sportData->id)->where('key', $role1)->first();
            if($UserRegistered){
             $lastArrayu = explode(',',$UserRegistered->details);
                if( in_array(Auth::User()->id ,$lastArrayu)){
                   $UserRegisteredStatus = 'yes';
                }else{  $UserRegisteredStatus = 'no';  }
             }else{ $UserRegisteredStatus = 'no';  }
        }else{  $UserRegisteredStatus = 'no';   }
        $role = '';
        $attendeesArray = array();
        $attendeesArrayFull = array();
        if(Auth::User()){
             
            if($userType == 'coach'){
            $role = 'coach';
            }else if($userType == 'athlete') {
                $role = 'athlete';
            }else{
                $role = 'admin';
            }
           

             $Attendees = array();
            if(Auth::User()->role_id != '1'){
            $Attendeess = SportEventMeta::where('event_id', $sportData->id)->where('key', $role.'_register')->first(); 
            if($Attendeess){
                $Attendees = $Attendeess->details ;
            }
            }else{
            $attendeesgetA= array();
            $attendeesgetC = array();
            $AttendeesC = SportEventMeta::where('event_id', $sportData->id)->where('key', 'coach_register')->first(); 
            $AttendeesA = SportEventMeta::where('event_id', $sportData->id)->where('key', 'athlete_register')->first();
            if($AttendeesC){
            $attendeesgetC = explode(',',$AttendeesC->details);
            }
            if($AttendeesA){
            $attendeesgetA = explode(',',$AttendeesA->details);
            }
            $comman = array_merge($attendeesgetC , $attendeesgetA);
            $commanvalues = implode(',',$comman);
            // echo '<pre>';print_r($comman);die;
            $Attendees = $commanvalues ;
        }
            if(Auth::User()->role_id == 4){
                $genderVal = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','coach_gender')->first();
                $genderKey = 'athelete_gender';
            }else{
                $genderVal = UserInformation::where('user_id',Auth::User()->id)->where('meta_key','athelete_gender')->first();
                $genderKey = 'coach_gender';
            }
           // print($genderVal);die;
            if($Attendees){
                $attendeesget = explode(',',$Attendees);
                if($request->all()){
                    $resultArray = $attendeesget;
                    $data['year_graduating'] = $request->year_graduating;
                    $data['position'] = $request->position;
                    $data['gpa'] = $request->gpa;
                    $data['state'] = $request->state;
                    $data['division'] = $request->division;
                    if($request->year_graduating != ''){
                        $usersInfo1 = UserInformation::select('user_id');
                        $usersInfo1->where('meta_key','graduation')->where("meta_value",$request->year_graduating);
                        $usersInfoArray = $usersInfo1->pluck('user_id')->toArray();
                        $resultArray = array_intersect($usersInfoArray,$resultArray);  
                    }
                    if($request->position != ''){
                        $usersInfo2 = UserInformation::select('user_id');
                        $usersInfo2->where('meta_key','position')->where('meta_value' , $request->position);
                        $usersInfoArray2 = $usersInfo2->pluck('user_id')->toArray();
                        $resultArray = array_intersect($usersInfoArray2,$resultArray);  
                    }
                   
                    if($request->gpa != ''){
                        $usersInfo6 = AcademicDetailAthelete::select('user_id')->where('gpa','>=',$request->gpa)->get();
                        $usersInfoArray6 = $usersInfo6->pluck('user_id')->toArray();
                        $resultArray = array_intersect($usersInfoArray6,$resultArray);
                        //print_r($resultArray); 
                    }
                    if($request->state != ''){
                        $usersInfo3= CoachsSchool::select('user_id');
                        $usersInfo3->where('state', $request->state);
                        $usersInfoArray3 = $usersInfo3->pluck('user_id')->toArray();
                        $resultArray = array_intersect($usersInfoArray3,$resultArray);  
                    }
                    if($request->division != ''){
                        $usersInfo4 = CoachsSchool::select('user_id');
                        $usersInfo4->where('division' , $request->division);
                        $usersInfoArray4 = $usersInfo4->pluck('user_id')->toArray();
                        $resultArray = array_intersect($usersInfoArray4,$resultArray);  
                    }
                    if(Auth::User()->role_id != 1){
                        if((isset($genderVal->meta_value)?$genderVal->meta_value:'') == 'Both' || (Auth::User()->role_id == 2 || Auth::User()->role_id == 3)){                            
                            $usersInfo7 = UserInformation::select('user_id');
                            if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
                                $usersInfo7->where('meta_key',$genderKey)->where('meta_value',$genderVal->meta_value)->orWhere('meta_value','Both')->where('meta_key',$genderKey);
                            }else{
                                $usersInfo7->where('meta_key',$genderKey);
                            }
                            $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                            $attendeesget = array_intersect($usersInfoArray7,$attendeesget); 
                         // print_r($usersInfoArray7);  die('here');
                        }else{
                             $usersInfo7 = UserInformation::select('user_id');
                            $usersInfo7->where('meta_key',$genderKey)->where('meta_value' ,isset($genderVal->meta_value)?$genderVal->meta_value:'');
                            $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                            $attendeesget = array_intersect($usersInfoArray7,$attendeesget);
                        }     
                    }
                    if(count($resultArray) > 0){
                        $attendeesArray = User::whereIn('id',$resultArray)->where('user_approved','ACTIVE')->paginate(5);
                        $attendeesArrayFull = User::whereIn('id',$resultArray)->where('user_approved','ACTIVE')->get();   
                    }else{
                        $attendeesArray = array();
                        $attendeesArrayFull = array();
                    }
                }else{
                    if(Auth::User()->role_id != 1){
                        if((isset($genderVal->meta_value)?$genderVal->meta_value:'') == 'Both' || (Auth::User()->role_id == 2 || Auth::User()->role_id == 3)){                            
                            $usersInfo7 = UserInformation::select('user_id');
                        if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
                            $usersInfo7->where('meta_key',$genderKey)->where('meta_value',$genderVal->meta_value)->orWhere('meta_value','Both')->where('meta_key',$genderKey);
                        }else{
                            $usersInfo7->where('meta_key',$genderKey);
                        }
                            $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                            $attendeesget = array_intersect($usersInfoArray7,$attendeesget); 
                         // print_r($usersInfoArray7);  die('here');
                        }else{
                             $usersInfo7 = UserInformation::select('user_id');
                            $usersInfo7->where('meta_key',$genderKey)->where('meta_value' ,isset($genderVal->meta_value)?$genderVal->meta_value:'');
                            $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
                            $attendeesget = array_intersect($usersInfoArray7,$attendeesget);
                        }   
                    }

                   $attendeesArray = User::whereIn('id',$attendeesget)->where('user_approved','ACTIVE')->paginate(5);
                   $attendeesArrayFull = User::whereIn('id',$attendeesget)->where('user_approved','ACTIVE')->get();  
                }       
            }else{
                 $attendeesArray = array();
                 $attendeesArrayFull = array();        
            }
        } 
        $locations = UserInformation::where('meta_key','state')->select('meta_value')->distinct()->get()->toArray();
         
         return view('event_new_show',compact('sportData','sportsMeta','sportsRelatedEvents','role', 'state' , 'UserRegisteredStatus' , 'attendeesArray','attendeesArrayFull','locations','templatedata','data'));
        }else{
             $post = Post::paginate(4);
            $userData['posts'] = $post;
            $userData['page'] = Pages::where('slug', '=', '404-page')->first();
            $userData['page_id'] = $userData['page']->id;
            return view('errors.404',$userData);
        }
    }
    public function eventSearch(Request $request){
        $userData = Auth::User();
        $data = array();
        if(\Auth::User()->role_id != 4){
            $templatedata = self::getTemplates(18);
        }else{
            $templatedata = self::getTemplates(17) ;
        }
        $data['templatedata'] = $templatedata;
        if(Auth::User()->role_id != '1'){
                $arraysur = array();
                if(Auth::User()->role_id != 4){
                $sportid = UserInformation::where('meta_key','all_sports')->where('user_id',Auth::User()->id)->first();
                if($sportid){
                $arraysur = explode(',',$sportid->meta_value);
                }
                }
                $sportid2 = UserInformation::where('meta_key','sport')->where('user_id',Auth::User()->id)->first();
                
                $arraysur2 = explode(',',$sportid2->meta_value);
                
                $mergeArray = array_merge($arraysur,$arraysur2 );
           
            $data['allSports'] = Sport::whereIn('id',$mergeArray)->where('status','PUBLISHED')->orderBy('title','ASC')->get();
           
        }else{
            $data['allSports'] = Sport::where('status','PUBLISHED')->orderBy('title','ASC')->get();
            
        }
            $data['allStates'] = SportEvent::where('state','!=','')->select('state')->distinct()->get()->toArray();
            $data['page'] = Pages::where('slug', '=', 'event-list')->first();
            $data['page_id'] = $data['page']->id;

            $sport_id = $request->sport_id;
            $myLocation = $request->location;
            $radius = $request->radius;
            $state = $request->state;
            $date_from = $request->date_from;
            $date_to = $request->date_to;
            $zipcode = $request->location;
            $division = $request->division;
            $school_attending = $request->school_attending;
            $campus_clinics = $request->campus_clinics;
            $tournaments = $request->tournaments;
            
            
            $data['sport_id'] = $sport_id;
            $data['myLocation'] = $myLocation;
            $data['radius'] = $radius;
            $data['state'] = $state;
            $data['date_from'] = $date_from;
            $data['date_to'] = $date_to;
            $data['division'] = $division;
            $data['school_attending'] = $school_attending;
            $data['campus_clinics'] = $campus_clinics;
            $data['tournaments'] = $tournaments;
            $data['search'] = '';
                
            $sports = SportEvent::where('status','PUBLISHED');
            if($sport_id){
                $sports->where('sport_id',$sport_id);
            }
            if($request->event_type){
               $sports->where('event_type',$request->event_type);
           }
             if($state){
                $sports->where('state',$state);
            }
             if($division){
                $sports->where('division',$division);
            }
            if($school_attending){
                $sports->where('school_attending','like','%'.$school_attending.'%');
            }
            // if($campus_clinics){
            //     $sports->where('campus_clinics','like','%'.$campus_clinics.'%');
            // }
            // if($tournaments){
            //     $sports->where('tournaments',$tournaments);
            // }
            if($date_from){
                $sports->where('event_start','>=',date_format(date_create($date_from) , 'm/d/Y'));
            }
            if($date_to){
                $sports->where('event_end','<=',date_format(date_create($date_to) , 'm/d/Y'));
            }
            if($myLocation && $radius){
              
              if($radius == 'only_zip'){
                $sports->where('zipcode',$myLocation);
              }else{
                $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
                $details=file_get_contents($url);
                $result = json_decode($details,true);
                
                $lat=$result['results'][0]['geometry']['location']['lat'];
                
                $lng=$result['results'][0]['geometry']['location']['lng'];
             
                $data1 = SportEventMeta::where('key','lat_long')->get()->toArray();
              $usersNear = array();
              foreach($data1 as $d){
                 $data2 = json_decode($d['details']);
                 $lati2 =  $data2->latitude;
                 $long2 =  $data2->longitude;
                 $event_id = $d['event_id'];
                  
                    $lat1  = $lat;
                    $lon1 = $lng;
                    $lat2 = $lati2;
                    $lon2 = $long2;
                    $theta = $lon1 - $lon2;
                    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                      $dist = acos($dist);
                      $dist = rad2deg($dist);
                      $miles= $dist * 60 * 1.1515;
                      $unit = 'K';
                        $km   = $miles*1.609344;
                     
                  $result =$miles;
                    if($result <= $radius){
                        $usersNear[] = $event_id;
                    }
                }
               
                $sports->whereIn('id',$usersNear);
                }
            }
            
        if(Auth::User()->role_id == 1){
            $data['allEvents'] = $sports->where('event_end','>=',date('m/d/Y'))->paginate(10);
        }else{
            // if(! $userData->subscribed('main') && Auth::User()->role_id != 4){
            //     $data['allEvents'] = $sports->where('event_end','>=',date('m/d/Y'))->whereIn('sport_id',$mergeArray)->limit(5)->get();
            // }else{
                $data['allEvents'] = $sports->where('event_end','>=',date('m/d/Y'))->whereIn('sport_id',$mergeArray)->paginate(10);
            // }
        }
        return view('event_list',$data);
    }
    public function exportToPdf( $slug ){

    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        $data = array();
        if(!Auth::User()){
            return Redirect('/login');
        }
        $user = User::where('user_slug',$slug)->first();
        $data['userData'] = $user;
        $userInfo = UserInformation::where('user_id',$user->id)->get();
       
       $userInfoArray = array();
        foreach($userInfo as $info){
           $userInfoArray[$info->meta_key] = $info->meta_value;
        }
       
        $data['userInfoArray'] = $userInfoArray;
  
        //  $pdf = \PDF::loadView('pdf.invoice', $data);
        //     return $pdf->stream();

        $dompdf = new DOMPDF();
        $dompdf->load_html(view('profilePDF',$data));
        // $dompdf->setPaper('A4', 'landscape');
        
        //     // Render the HTML as PDF
           $dompdf->render();
             $dompdf->stream();
        return view('profilePDF',$data);
    }
    
    public function eventimport(){
        if(!Auth::User()){
           return Redirect('/login');
       }
       if(Auth::User()->role_id == 4 || Auth::User()->role_id == 1){
       $data = array();
       $data['page'] = Pages::where('slug', '=', 'event_import')->first();
       
       $data['page_id'] = $data['page']->id;
       
       return view('event_import',$data);
       }else{
            $post = Post::paginate(4);
           $userData['posts'] = $post;
           $userData['page'] = Pages::where('slug', '=', '404-page')->first();
           $userData['page_id'] = $userData['page']->id;
           return view('errors.404',$userData);
       }
   }
    
    public function importstart(Request $request){
        
        $file  =  $request->file();
        $fileD = fopen($file['file'],"r");
        $column=fgetcsv($fileD);
        while(!feof($fileD)){
         $rowData[]=fgetcsv($fileD);
        }
        foreach($rowData as $row){
            if($row[0] != ''){
                
                $sport = Sport::where('title','=',strtoupper($row[5]))->orWhere('title','=',strtolower($row[5]))->orWhere('title','=',ucfirst($row[5]))->orWhere('title','=',$row[5])->first();
                if(!$sport){
                    $newsport = new Sport();
                    $newsport->title = $row[5];
                    $newsport->author_id = Auth::user()->id;
                    $newsport->status = 'PENDING';
                    $newsport->save();  
                    $sportid = $newsport->id;
                }else{
                    $sportid =  $sport->id;
                }
                
                $newEvent = SportEvent::where('title','=',strtoupper($row[0]))->orWhere('title','=',strtolower($row[0]))->orWhere('title','=',ucfirst($row[0]))->orWhere('title','=',$row[0])->first();
                if(!$newEvent){
                $newEvent = new SportEvent();
                }

                $slug =  strtolower(str_replace(" ","-",$row[0]));
               
                $sportslug = SportEvent::where('title', $row[0])->get()->count();
                if($sportslug >= 1){
                  $slug = $slug.$sportslug;
                }
                $newEvent->event_slug = $slug;
                $newEvent->title = $row[0];
                $newEvent->author_id = Auth::user()->id;
                $newEvent->event_start =date("m/d/Y", strtotime($row[2]));
                $newEvent->event_end = date("m/d/Y", strtotime($row[3]));
                $newEvent->zipcode = $row[1];
                $newEvent->division = $row[4];
                $newEvent->sport_id = $sportid;
                $newEvent->status = 'PUBLISHED';
                $newEvent->description = $row[6];
                $newEvent->place = $row[7];
                $newEvent->city = $row[8];
                $newEvent->state = $row[9];
                $newEvent->event_type = $row[10];
                $newEvent->event_gender = $row[12];
                $newEvent->event_url = $row[13];

                // $newEvent->campus_clinics = $row[11];
               // $newEvent->school_attending = $row[11];
                $newEvent->address = $row[11];
                $newEvent->image = '';
                $newEvent->save();
                $zipcode= $row[1];
                $url1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
                $details=file_get_contents($url1);
                $result = json_decode($details,true);
                if(isset($result['results'][0]['geometry']['location']['lat'])){
                    $lat=$result['results'][0]['geometry']['location']['lat'];
                    $lng=$result['results'][0]['geometry']['location']['lng'];
                }   
                $Eid = $newEvent->id;
                $newarry = array();
                $newarry['latitude'] = $lat;
                $newarry['longitude'] = $lng;
                $newEventMeta = SportEventMeta::where('event_id','=', $Eid)->where('key','=','lat_long')->first();
                if(!$newEventMeta){
                 $newEventMeta = new SportEventMeta();
                }
                $newEventMeta->event_id = $Eid;
                $newEventMeta->key = 'lat_long';
                $newEventMeta->details = json_encode($newarry);
                $newEventMeta->save();
                if(Auth::User()->role_id == 4){
                   $newEventMeta1 = new SportEventMeta();
                   $newEventMeta1->event_id = $newEvent->id;
                   $newEventMeta1->key = 'coach_register';
                   $newEventMeta1->details = Auth::User()->id;
                   $newEventMeta1->save();
                }
            }
         }
         return "success";
    }
    
  public function contactcoaches(Request $request){
        $data = array();

        $userIDs = explode(',',$request->user_ids);
        $total_emails = count($userIDs);
        // Adding email counter
        $userInfo = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','email_counter')->first();
        if(!$userInfo){
            $userInfo = new UserInformation();
            $emailOldCounter = $total_emails; 
        }else{
            $emailOldCounter = $userInfo->meta_value;
            $emailOldCounter = $emailOldCounter + $total_emails;
        }
        $userInfo->user_id =  Auth::User()->id;
        $userInfo->meta_key =  'email_counter';
        $userInfo->meta_value = $emailOldCounter;
        $userInfo->save();
        // Email counter added

        if( Auth::User()->role_id != 4 ){
            $auth_user = Auth::User();
            $data['athelete'] = $auth_user->id;
            $data['replyto'] = $auth_user->email;
            $data['request'] = $request->all();
            $message = $request->message;

            $academics_all = DB::select("select a.`name` as my_name, a.`email` as my_email, b.`school_id` as my_school_name, b.* from users a,academicdetailathelete b where a.`id` = b.`user_id` and a.id = ".$data['athelete']);
            // $academics_all = DB::select("select a.`name` as my_name, a.`email` as my_email, c.`school_name` as my_school_name, b.* from users a,academicdetailathelete b, api_fetched_data c where a.`id` = b.`user_id` and b.`school_id` = c.`id` and a.id = ".$data['athelete']);
            if( is_array($academics_all) && sizeof($academics_all) > 0 ){
                $athelete = $academics_all[0];
            }

            if( $data['request']['eventid'] != '' && $data['request']['eventid'] != 'undefined' ){
                $eventss = DB::select("select a.title as event_title, b.title as sport_title, a.* from sport_events a,sports b where a.`sport_id` = b.`id` and a.id = ".$data['request']['eventid']);
                if( is_array($eventss) && sizeof($eventss) > 0 ){
                    $event = $eventss[0];
                }
                $event_codes = array('{event_title}', '{event_description}', '{event_event_start}', '{event_event_end}', '{event_place}', '{event_city}', '{event_state}', '{event_zipcode}', '{sport_title}', '{event_division}', '{event_tournaments}', '{event_campus_clinics}', '{event_school_attending}');
            }else{
                $event = '';
            }
            if( is_array($academics_all) && sizeof($academics_all) > 0 ){
                $message = $data['request']['message'];
                $message = str_replace('{my_name}', $athelete->my_name, $message);
                $message = str_replace('{my_email}', $athelete->my_email, $message);
                $message = str_replace('{college_name}', $athelete->my_school_name, $message);
            }else{
                $message = str_replace('{my_name}', $auth_user->name, $message);
                $message = str_replace('{my_email}', $auth_user->email, $message);
                $message = str_replace('{college_name}', '', $message);
            }
            // check if email contain the My Academics shortcode
            if (strpos($message, '{my_academics}') !== false && is_array($academics_all) && sizeof($academics_all) > 0 ) {
                $html = '<table style="border-collapse: collapse;"><tr><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">School / College</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Address</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">City</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">State</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Zipcode</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">GPA</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">SATS</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">MATH</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">ACT</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">VERBAL</th></tr>';
                foreach ($academics_all as $key => $value) {
                    $html = $html . '<tr><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->school_id.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->location.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->city.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->state.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->zip.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->gpa.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->sats.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->math.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->act.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->verbal.'</td></tr>';
                }
                $html = $html . '</table>';
                $message = str_replace('{my_academics}', $html, $message);
            }else{
                $message = str_replace('{my_academics}', '', $message);
            }
            // check if email contain the club coach info shortcode
            if (strpos($message, '{my_club_info}') !== false) {
                $coach_club_info = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'coaches'");
                // print_r($coach_club_info);
                if(is_array($coach_club_info) && sizeof($coach_club_info) > 0 ){
                    $coach_club_info = json_decode($coach_club_info[0]->meta_value);
                    if( is_array($coach_club_info) && sizeof($coach_club_info) > 0 ){
                        $html = '<table style="border-collapse: collapse;"><tr><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff;text-align:left; width: 80px;">Club Type</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Club Name</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Jersey Number</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Coach Name</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Coach Email</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Contact</th></tr>';
                        foreach ($coach_club_info as $key => $value) {
                            $value = (array) $value;
                            $value = array_merge( array( "type" => "", "clubname" => "", "jersey" => "", "coachname" => "", "email" => "", "phone" => "" ), $value );
                            $value = (object) $value;
                            $html = $html . '<tr><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 130px;text-align:left;background: #fff;">'.$value->type.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->clubname.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->jersey.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->coachname.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->email.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->phone.'</td></tr>';
                        }
                        $html = $html . '</table>';
                        $message = str_replace('{my_club_info}', $html, $message);
                    }else{
                        $message = str_replace('{my_club_info}', '', $message);
                    }
                }else{
                    $message = str_replace('{my_club_info}', '', $message);
                }
            }

            // Check if shortcode contain atheleteic achievement shortcode
            if (strpos($message, '{athletic_achievements}') !== false) {
                $athelete_achievements = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athletic_achievements'");
                // print_r($athelete_achievements); die;
                if(is_array($athelete_achievements) && sizeof($athelete_achievements) > 0 ){
                    $athelete_achievements = json_decode($athelete_achievements[0]->meta_value);
                    // print_r($athelete_achievements); die;
                    if( is_array($athelete_achievements) && sizeof($athelete_achievements) > 0 ){
                        $html = '<table style="border-collapse: collapse;"><tr><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; text-align:left; width: 80px;">Achievement</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Years</th></tr>';
                        foreach ($athelete_achievements as $key => $value) {
                            $value = (array) $value;
                            // print_r($value); die;
                            $value = array_merge( array( "years" => "", "champ_run" => "", "athleticname" => "","tournamentname" => "" ), $value );
                            // $value = (object) $value;
                            $years = $value['years'];
                            $years_data = [];
                            foreach ($years as $key1 => $value1) {
                                $years_data[] = $value1->name;
                            }
                            $years_data = implode(', ', $years_data);
                            $html = $html . '<tr><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 130px;text-align:left;background: #fff;">'.$value['athleticname'] .'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'. $years_data .'</td></tr>';
                        }
                        $html = $html . '</table>';
                        $message = str_replace('{athletic_achievements}', $html, $message);
                    }else{
                        $message = str_replace('{athletic_achievements}', '', $message);
                    }
                }else{
                    $message = str_replace('{athletic_achievements}', '', $message);
                }
            }
            // Check if shortcode contain academic achievement shortcode
            if (strpos($message, '{academic_achievement}') !== false) {
                $acadmic_achivment = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_acadmic_achivment'");
                if(is_array($acadmic_achivment) && sizeof($acadmic_achivment) > 0 ){
                    $acadmic_achivment = json_decode($acadmic_achivment[0]->meta_value);
                    if( is_array($acadmic_achivment) && sizeof($acadmic_achivment) > 0 ){
                        $html = '<table style="border-collapse: collapse;"><tr><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; text-align:left; width: 80px;">Year</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Achievement</th></tr>';
                        foreach ($acadmic_achivment as $key => $value) {
                            $value = (array) $value;
                            $value = array_merge( array( "year" => "", "achivment" => "" ), $value );
                            $value = (object) $value;
                            $html = $html . '<tr><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 130px;text-align:left;background: #fff;">'.$value->year.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->achivment.'</td></tr>';
                        }
                        $html = $html . '</table>';
                        $message = str_replace('{academic_achievement}', $html, $message);
                    }else{
                        $message = str_replace('{academic_achievement}', '', $message);
                    }
                }else{
                    $message = str_replace('{academic_achievement}', '', $message);
                }
            }
            // Check if shortcode contain Extra Curriculum Activity shortcode
            if (strpos($message, '{extra_curricular_activity}') !== false) {
                $extra_curricular = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'extra_curricular'");
                if(is_array($extra_curricular) && sizeof($extra_curricular) > 0 ){
                    $extra_curricular = json_decode($extra_curricular[0]->meta_value);
                    if( is_array($extra_curricular) && sizeof($extra_curricular) > 0 ){
                        $html = '<table style="border-collapse: collapse;"><tr><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; text-align:left; width: 80px;">Year</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Activity</th></tr>';
                        foreach ($extra_curricular as $key => $value) {
                            $value = (array) $value;
                            $value = array_merge( array( "year" => "", "activity" => "" ), $value );
                            $value = (object) $value;
                            $html = $html . '<tr><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 130px;text-align:left;background: #fff;">'.$value->year.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->activity.'</td></tr>';
                        }
                        $html = $html . '</table>';
                        $message = str_replace('{extra_curricular_activity}', $html, $message);
                    }else{
                        $message = str_replace('{extra_curricular_activity}', '', $message);
                    }
                }else{
                    $message = str_replace('{extra_curricular_activity}', '', $message);
                }
            }
            // Check if shortcode Coaches Awards shortcode
            // if (strpos($message, '{coach_awards}') !== false) {
            //     $coach_awards = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'awards'");
            //     if(is_array($coach_awards) && sizeof($coach_awards) > 0 ){
            //         $coach_awards = json_decode($coach_awards[0]->meta_value);
            //         if( is_array($coach_awards) && sizeof($coach_awards) > 0 ){
            //             $html = '<table style="border-collapse: collapse;"><tr><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; text-align:left; width: 80px;">Award Type</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Award Name</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Award Year</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">School Name</th><th style="background: #3e3e3e; padding: 8px 14px; font-size: 12px; color: #fff; width: 100px;">Coach Name</th></tr>';
            //             foreach ($coach_awards as $key => $value) {
            //                 $value = (array) $value;
            //                 $value = array_merge( array( "type" => "", "name" => "", "year" => "", "school" => "", "coachname" => "" ), $value );
            //                 $value = (object) $value;
            //                 $html = $html . '<tr><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 130px;text-align:left;background: #fff;">'.$value->type.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->name.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->year.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->school.'</td><td style="text-align: center;padding: 9px 8px;font-size: 11px;width: 100px;background: #fff;">'.$value->coachname.'</td></tr>';
            //             }
            //             $html = $html . '</table>';
            //             $message = str_replace('{coach_awards}', $html, $message);
            //         }else{
            //             $message = str_replace('{coach_awards}', '', $message);
            //         }
            //     }else{
            //         $message = str_replace('{coach_awards}', '', $message);
            //     }
            // }
            // print_r($message); die;     
            
            $my_birth = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_birth'");
            if( is_array($my_birth) && sizeof($my_birth) > 0 ){
                $message = str_replace('{my_birth}', $my_birth[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_birth}', '', $message);
            }

            $my_position = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'position_name'");
            if( is_array($my_position) && sizeof($my_position) > 0 ){
                $message = str_replace('{my_position}', $my_position[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_position}', '', $message);
            }

            $class_year = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'graduation'");
            if( is_array($class_year) && sizeof($class_year) > 0 ){
                $message = str_replace('{class_year}', $class_year[0]->meta_value, $message);
            }else{
                $message = str_replace('{class_year}', '', $message);
            }

            $my_phone = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_phone'");
            if( is_array($my_phone) && sizeof($my_phone) > 0 ){
                $message = str_replace('{my_phone}', $my_phone[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_phone}', '', $message);
            }

            $my_facebook = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_facebook'");
            if( is_array($my_facebook) && sizeof($my_facebook) > 0 ){
                $message = str_replace('{my_facebook}', $my_facebook[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_facebook}', '', $message);
            }

            $my_twitter = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_twitter'");
            if( is_array($my_twitter) && sizeof($my_twitter) > 0 ){
                $message = str_replace('{my_twitter}', $my_twitter[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_twitter}', '', $message);
            }

            $my_instagram = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_instagram'");
            if( is_array($my_instagram) && sizeof($my_instagram) > 0 ){
                $message = str_replace('{my_instagram}', $my_instagram[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_instagram}', '', $message);
            }
            
            $my_snapchat = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_snapchat'");
            if( is_array($my_snapchat) && sizeof($my_snapchat) > 0 ){
                $message = str_replace('{my_snapchat}', $my_snapchat[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_snapchat}', '', $message);
            }

            $athelete_whatsapp = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_whatsapp'");
            if( is_array($athelete_whatsapp) && sizeof($athelete_whatsapp) > 0 ){
                $message = str_replace('{my_whatsapp}', $athelete_whatsapp[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_whatsapp}', '', $message);
            }

            $my_skype = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_skype'");
            if( is_array($my_skype) && sizeof($my_skype) > 0 ){
                $message = str_replace('{my_skype}', $my_skype[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_skype}', '', $message);
            }

            // $my_skype = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_skype'");
            // $message = str_replace('{my_skype}', $my_skype[0]->meta_value, $message);

            $my_address = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_address'");
            if( is_array($my_address) && sizeof($my_address) > 0 ){
                $message = str_replace('{my_address}', $my_address[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_address}', '', $message);
            }
            
            $my_height = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_height'");
            if( is_array($my_height) && sizeof($my_height) > 0 ){
                $message = str_replace('{my_height}', $my_height[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_height}', '', $message);
            }
            
            $my_weight = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_weight'");
            if( is_array($my_weight) && sizeof($my_weight) > 0 ){
                $message = str_replace('{my_weight}', $my_weight[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_weight}', '', $message);
            }

            $my_whatsapp = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'athelete_whatsapp'");
            if( is_array($my_whatsapp) && sizeof($my_whatsapp) > 0 ){
                $message = str_replace('{my_whatsapp}', $my_whatsapp[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_whatsapp}', '', $message);
            }

            $my_videogallery = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'videogallery'");
            if(is_array($my_videogallery) && sizeof($my_videogallery) > 0 ){
                $videos = json_decode($my_videogallery[0]->meta_value);
                if( count($videos) > 0 ){
                    $videoAll = array();
                    foreach ($videos as $key => $value) {
                        $videoAll[] = $value->url;
                    }
                    $my_videogallery = implode(',',$videoAll);
                    $message = str_replace('{my_videogallery_urls}', $my_videogallery, $message);
                }else{
                    $message = str_replace('{my_videogallery_urls}', '', $message);
                }
            }else{
                $message = str_replace('{my_videogallery_urls}', '', $message);
            }
            

            $my_sport = DB::select("SELECT `title` from `sports` where id = ( select `meta_value` from `user_information` where `user_id` = ".$data['athelete']." and `meta_key` = 'sport' limit 1 )");
            if(is_array($my_sport) && sizeof($my_sport) > 0 ){
                $message = str_replace('{my_sport}', $my_sport[0]->title, $message);        
            }else{
                $message = str_replace('{my_sport}', '', $message);
            }

            if( $data['request']['eventid'] != '' && $data['request']['eventid'] != 'undefined' && is_array($eventss) && sizeof($eventss) > 0 ){
                foreach ($event_codes as $key => $value) {
                    if (strpos($message, $value) !== false) {
                        $replace = $value;
                        $value = str_replace('{', '', $value);
                        $value = str_replace('}', '', $value);
                        if( isset($event->$value) ){
                            $message = str_replace($replace, $event->$value, $message);
                        }else{
                            $value = explode( '_', $value );
                            array_shift( $value );
                            $value = implode( '_', $value );
                            if( $event->$value ){
                                $message = str_replace($replace, $event->$value, $message);
                            }else{
                                $message = str_replace($replace, '', $message);
                            }
                        }
                    }            
                }
            }

            if(count($userIDs) > 0){
                // print_r($userIDs); die;
                // if(count($userIDs) > 1){
                //     $to = array_shift($userIDs);
                //     if( $to == '' ){
                //         $to = array_shift($userIDs);
                //     }
                //     if(count($userIDs) > 2){
                //         $data['bcc'] = $userIDs;
                //     }else{
                //         $data['bcc'] = [];
                //     }
                // }else{
                //     $to = $userIDs[0];
                //     $data['bcc'] = [];
                // }
                // for ($i =0; $i < 200; $i++) {
                foreach ($userIDs as $key => $value) {
                    $userData = User::select('name')->where('email',$value)->first();
                    $data['message'] = $message;
                    $data['userName'] = $userData->name;
                    Mail::to($value)->send( new ContactAthelete( $data ));
                }
                return  'yes';
            }else{
                return  'no';
            }
        }else{
            $data['coach'] = Auth::User()->id;
            $data['replyto'] = Auth::User()->email;
            $data['request'] = $request->all();
            $message = $request->message;

            if( $data['request']['eventid'] != '' && $data['request']['eventid'] != 'undefined' ){
                $eventss = DB::select("select a.title as event_title, b.title as sport_title, a.* from sport_events a,sports b where a.`sport_id` = b.`id` and a.id = ".$data['request']['eventid']);
                if( is_array($eventss) && sizeof($eventss) ){
                    $event = $eventss[0];
                }
                $event_codes = array('{event_title}', '{event_description}', '{event_event_start}', '{event_event_end}', '{event_place}', '{event_city}', '{event_state}', '{event_zipcode}', '{sport_title}', '{event_division}', '{event_tournaments}', '{event_campus_clinics}', '{event_school_attending}');
            }else{
                $event = '';
            }

            $message = $data['request']['message'];

            $school_check = CoachsSchool::where('user_id', $data['coach'])->get();
            if( sizeof($school_check) > 0 ){
                $coachs_all = DB::select("select a.`name` as my_name, a.`email` as my_email, d.`title` as sport_name, c.`school_name` as my_school_name, b.* from users a,coachschool b, api_fetched_data c, sports d where d.`id` = b.`sport_id` and a.`id` = b.`user_id` and b.`school_id` = c.`id` and a.id = ".$data['coach']);
            }else{
                $coachs_all = DB::select("select a.`name` as my_name, a.`email` as my_email from users a where a.id = ".$data['coach']);
            }
            if( sizeof($coachs_all) > 0 ){
                $coach = $coachs_all[0];
                $message = str_replace('{my_name}', $coach->my_name, $message);
                $message = str_replace('{my_email}', $coach->my_email, $message);
                if( isset($coach->my_school_name) )
                    $message = str_replace('{college_name}', $coach->my_school_name, $message);
            }else{
                $message = str_replace('{my_name}', '', $message);
                $message = str_replace('{my_email}', '', $message);
                if( isset($coach->my_school_name))
                    $message = str_replace('{college_name}', '', $message);
            }
            $my_sport = DB::select("SELECT `title` from `sports` where id = ( select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'sport' limit 1)");
            if(is_array($my_sport) && sizeof($my_sport) > 0 ){
                $message = str_replace('{my_sport}', $my_sport[0]->title, $message);        
            }else{
                $message = str_replace('{my_sport}', '', $message);
            }


            $my_phone = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'phone'");
            if( is_array($my_phone) && count($my_phone) > 0 ){
                $message = str_replace('{my_phone}', $my_phone[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_phone}', '', $message);
            }

            $my_web = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'coach_www'");
            if( is_array($my_web) && count($my_web) > 0 ){
                $message = str_replace('{my_web}', $my_web[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_web}', '', $message);
            }

            $coach_ncaa = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'coach_ncaa'");
            if( is_array($coach_ncaa) && count($coach_ncaa) > 0 ){
                $ncaadata = json_decode($coach_ncaa[0]->meta_value);
                $ncaadata2 = array();
                foreach ($ncaadata as $key => $value) {
                    $ncaadata2[] = $value->name;
                }
                $ncaadata3 = implode(',', $ncaadata2 );
                    // print_r($ncaadata3);die;
                $message = str_replace('{ncaa_championship}', $ncaadata3, $message);
            }else{
                $message = str_replace('{ncaa_championship}', '', $message);
            }

            $coach_ncaa_f_f = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'coach_ncaa_f_f'");
            if( is_array($coach_ncaa_f_f) && count($coach_ncaa_f_f) > 0 ){
                $coach_ncaa_f_fdata = json_decode($coach_ncaa_f_f[0]->meta_value);
                $coach_ncaa_f_fdata2 = array();
                foreach ($coach_ncaa_f_fdata as $key => $value) {
                    $coach_ncaa_f_fdata2[] = $value->name;
                }
                $coach_ncaa_f_fdata3 = implode(',', $coach_ncaa_f_fdata2 );
                    // print_r($ncaadata3);die;
                $message = str_replace('{ncaa_final_four}', $coach_ncaa_f_fdata3, $message);
            }else{
                $message = str_replace('{ncaa_final_four}', '', $message);
            }

            $coach_ncaa_p_s = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'coach_ncaa_p_s'");
            if( is_array($coach_ncaa_p_s) && count($coach_ncaa_p_s) > 0 ){
                $coach_ncaa_p_sdata = json_decode($coach_ncaa_p_s[0]->meta_value);
                $coach_ncaa_p_sdata2 = array();
                foreach ($coach_ncaa_p_sdata as $key => $value) {
                    $coach_ncaa_p_sdata2[] = $value->name;
                }
                $coach_ncaa_p_sdata3 = implode(',', $coach_ncaa_p_sdata2 );
                    // print_r($coach_ncaa_p_sdata3);die;
                $message = str_replace('{ncaa_post_season_appearance}', $coach_ncaa_p_sdata3, $message);
            }else{
                $message = str_replace('{ncaa_post_season_appearance}', '', $message);
            }

            $coach_ncaa_c_c = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'coach_ncaa_c_c'");
            if( is_array($coach_ncaa_c_c) && count($coach_ncaa_c_c) > 0 ){
                $coach_ncaa_c_cdata = json_decode($coach_ncaa_c_c[0]->meta_value);
                $coach_ncaa_c_cdata2 = array();
                foreach ($coach_ncaa_c_cdata as $key => $value) {
                    $coach_ncaa_c_cdata2[] = $value->name;
                }
                $coach_ncaa_c_cdata3 = implode(',', $coach_ncaa_c_cdata2 );
                    // print_r($coach_ncaa_p_sdata3);die;
                $message = str_replace('{conference_championship}', $coach_ncaa_c_cdata3, $message);
            }else{
                $message = str_replace('{conference_championship}', '', $message);
            }

            $coach_ncaa_n_t = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'coach_ncaa_n_t'");
            if( is_array($coach_ncaa_n_t) && count($coach_ncaa_n_t) > 0 ){
                $coach_ncaa_n_tdata = json_decode($coach_ncaa_n_t[0]->meta_value);
                $coach_ncaa_n_tdata2 = array();
                foreach ($coach_ncaa_n_tdata as $key => $value) {
                    $coach_ncaa_n_tdata2[] = $value->name;
                }
                $coach_ncaa_n_tdata3 = implode(',', $coach_ncaa_n_tdata2 );
                    // print_r($coach_ncaa_n_tdata3);die;
                $message = str_replace('{national_tournament}', $coach_ncaa_n_tdata3, $message);
            }else{
                $message = str_replace('{national_tournament}', '', $message);
            }
            
            $my_bio = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'bio'");
            if( is_array($my_bio) && count($my_bio) > 0 ){

                $message = str_replace('{my_bio}', $my_bio[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_bio}', '', $message);
            }

            $coach_title = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'coach_title'");
            if( is_array($coach_title) && count($coach_title) > 0 ){
                $message = str_replace('{my_title}', $coach_title[0]->meta_value, $message);
            }else{
                $message = str_replace('{my_title}', '', $message);
            }

            $my_videogallery = DB::select("select `meta_value` from `user_information` where `user_id` = ".$data['coach']." and `meta_key` = 'videogallery'");
            if(is_array($my_videogallery) && count($my_videogallery) > 1 ){
                $videos = json_decode($my_videogallery[0]->meta_value);
                $my_videogallery = $videos[0]->url;
                $message = str_replace('{my_videogallery_urls}', $my_videogallery, $message);
            }else{
                $message = str_replace('{my_videogallery_urls}', '', $message);
            }
            
            if( sizeof($school_check) > 0 ){
                $school_information = DB::select( "select a.`name` as my_name, a.`email` as my_email, d.`title` as sport_name, c.`school_name` as my_school_name, b.* from users a,coachschool b, api_fetched_data c, sports d where d.`id` = b.`sport_id` and a.`id` = b.`user_id` and b.`school_id` = c.`id` and a.id = ".$data['coach'] );
                if( is_array($school_information) && sizeof($school_information) < 1 ){
                    // when sport id is not available
                    $school_information = DB::select( "select a.`name` as my_name, a.`email` as my_email, c.`school_name` as my_school_name, b.* from users a,coachschool b, api_fetched_data c where a.`id` = b.`user_id` and b.`school_id` = c.`id` and a.id = ".$data['coach'] );
                }
                if( is_array($school_information) && sizeof($school_information) < 1 ){
                    // if school id is not available
                    $school_information = DB::select( "select a.`name` as my_name, a.`email` as my_email, d.`title` as sport_name, b.* from users a,coachschool b, sports d where d.`id` = b.`sport_id` and a.`id` = b.`user_id` and a.id = ".$data['coach'] );
                }
                if( is_array($school_information) && sizeof($school_information) < 1 ){
                    // if school and sport id is not available
                    $school_information = DB::select( "select a.`name` as my_name, a.`email` as my_email b.* from users a,coachschool b where a.`id` = b.`user_id` and a.id = ".$data['coach'] );
                }
                
                    // Check if shortcode Coaches Awards shortcode
                if (strpos($message, '{school_information}') !== false && sizeof($school_information) > 0 ) {
                    $html = '<table style="width: 100%; text-align: left;border: 1px solid #e0e0e0;background: #fff;border-collapse: collapse;"><tr><th style="text-align: center;" colspan="2">School Information</th></tr>';
                    foreach ($school_information as $key => $value) {
                        $value = (array) $value;
                        $value = array_merge( array( "my_school_name" => "", "year" => "", "students_coached" => "", "school_tuition" => "", "tuition_room" => "", "school_size" => "", "phone" => "", "fax" => "", "city" => "", "state" => "", "zip" => "", "division" => "", "div_code" => "", "rank" => "", "sport_name" => "", "conference" => "", "head_coach" => "" ), $value );
                        $value = (object) $value;
                        // print_r($value->school_tuition);
                        if($value->gender == "Men") { $value->gender = "Male";}else if ($value->gender == "Women" ){ $value->gender = "Female";}
                       if($value->head_coach == "True") { $value->head_coach = "Head Coach";}else if ($value->head_coach == "False" ){ $value->head_coach = "Assistant Coach";}
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">School Name</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->my_school_name.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Gender Coached</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->gender.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Year</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->year.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Students Coached</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->students_coached.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">School Tuition</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->school_tuition.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Tuition Room/Board</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->tuition_room.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">School Size</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->school_size.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Address</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->address1.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Address 2</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->address2.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Phone</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->phone.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Fax</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->fax.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">City</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->city.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">State</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->state.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">ZipCode</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->zip.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Division</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->division.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Sport</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->sport_name.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Conference</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->conference.'</td></tr>';
                        $html = $html . '<tr><th style="background: #333;padding: 10px 10px;color: #fff;border-top: 1px solid #ccc;font-size: 12px;">Position</th><td style="background: #fff;padding: 10px 10px;color: #131313;border-top: 1px solid #ccc;font-size: 13px;">'.$value->head_coach.'</td></tr>';
                    }
                    $html = $html . '</table>';
                    // print_r($html); die;
                    $message = str_replace('{school_information}', $html, $message);
                }else{
                    $message = str_replace('{school_information}', '', $message);
                }
            }

            if( $data['request']['eventid'] != '' && $data['request']['eventid'] != 'undefined' && is_array($eventss) && sizeof($eventss) ){
                // echo $message; die;
                foreach ($event_codes as $key => $value) {
                    if (strpos($message, $value) !== false) {
                        $replace = $value;
                        $value = str_replace('{', '', $value);
                        $value = str_replace('}', '', $value);
                        if( isset($event->$value) ){
                            $message = str_replace($replace, $event->$value, $message);
                        }else{
                            $value = explode( '_', $value );
                            array_shift( $value );
                            $value = implode( '_', $value );
                            if( $event->$value ){
                                $message = str_replace($replace, $event->$value, $message);
                            }else{
                                $message = str_replace($replace, '', $message);
                            }
                        }
                    }            
                }
            }
            
            $userIDs = explode(',',$request->user_ids);
            $useremails = array();
            
            if(count($userIDs) > 0){
                // if(count($userIDs) > 1){
                //     $to = array_shift($userIDs);
                //     if( $to == '' ){
                //         $to = array_shift($userIDs);
                //     }
                //     if(count($userIDs) > 2){
                //         $data['bcc'] = $userIDs;
                //     }else{
                //         $data['bcc'] = [];
                //     }
                // }else{
                //     $to = $userIDs[0];
                //     $data['bcc'] = [];
                // }
                foreach ($userIDs as $key => $value) {
                    $userData = User::select('name')->where('email',$value)->first();
                    $data['message'] = $message;
                    $data['userName'] = $userData->name;
                    Mail::to($value)->send( new ContactAthelete( $data ));
                }
                return  'yes';
                // $sport = Sport::find($sports_id);
            }else{
                return  'no';
            }
        }
        return ;        
    }

    public function sendEmail(Request $request){
        $data = array();
        $data['to'] = $request->to_user;
        $data['subject'] = $request->subject;
        $data['message'] = $request->message;
        $data['from'] = Auth::User()->email;
        $data['template'] = $request->template;

        $user = User::where('id', $data['to'])->first(); 
        $userInfo = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','email_counter')->first();

        if(!$userInfo){
            $userInfo = new UserInformation();
            $emailOldCounter = 1; 
        }else{
            $emailOldCounter = $userInfo->meta_value;
            $emailOldCounter++;
        }
        $userInfo->user_id =  Auth::User()->id;
        $userInfo->meta_key =  'email_counter';
        $userInfo->meta_value = $emailOldCounter;
        $userInfo->save();

        $data['userName'] = $user->name;
           
        if(($user->email) != ''){
                    if(Auth::User()->role_id != 4){
                        $pageID = '18';
                    }else{
                        $pageID = '17';   
                    } 
        // $user = $request->user();
        $data['templateData'] = self::collectData($user->id,Auth::User()->id,$request->template,$pageID);
        Mail::to($user->email)->send( new EmailSend( $data ));
        return  'yes';
        
        // $sport = Sport::find($sports_id);
        }else{
            return  'no';
        }
        
        return ;
        
    }
     public function sendEmailToAttendee(Request $request){
        
        $data = array();
        $data['to'] = $request->user_id;
        $data['subject'] = $request->subject;
        $data['message'] = $request->message;
        $data['from'] = Auth::User()->email;
        $data['template'] = $request->template;
        
       
         $user = User::where('id', $data['to'])->first();   
         
         $userInfo = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','email_counter')->first();
         
         if(!$userInfo){
            $userInfo = new UserInformation();
            $emailOldCounter = 1; 
         }else{
             $emailOldCounter = $userInfo->meta_value;
             $emailOldCounter++;
         }
         $userInfo->user_id =  Auth::User()->id;
         $userInfo->meta_key =  'email_counter';
         $userInfo->meta_value = $emailOldCounter;
         $userInfo->save();
         
          $data['userName'] = $user->name;
           
        if(($user->email) != '' ){
                    if(Auth::User()->role_id != 4){
                        $pageID = '18';
                    }else{
                        $pageID = '17';   
                    } 
        // $user = $request->user();
        $data['templateData'] = self::collectData($user->id,Auth::User()->id,$request->template,$pageID);
       
        Mail::to($user->email)->send( new EmailSend( $data ));
        return  'yes';
        
        // $sport = Sport::find($sports_id);
        }else{
            return  'no';
        }
        
        return ;
        
    }
    public function sendEmailToAllAttendees(Request $request){
       
        $data = array();
        $userIDs = explode(',',$request->user_ids);
        $total_emails = count($userIDs);
        $data['subject'] = $request->subject;
        $data['message'] = $request->message;
        $data['from'] = Auth::User()->email;
        $data['template'] = $request->template;
      
        $useremails = array() ;
           
        if((count($userIDs)) > 0){
             
            foreach($userIDs as $ui){
                if($ui != ''){
                    $user2 = User::find($ui);   
                      $userInfo = UserInformation::where('user_id', Auth::User()->id)->where('meta_key','email_counter')->first();     
                    if(!$userInfo){
                        $userInfo = new UserInformation();
                        $emailOldCounter = $total_emails; 
                    }else{
                        $emailOldCounter = $userInfo->meta_value;
                        $emailOldCounter = $emailOldCounter + $total_emails;
                    }
                    $userInfo->user_id =  Auth::User()->id;
                    $userInfo->meta_key =  'email_counter';
                    $userInfo->meta_value = $emailOldCounter;
                    $userInfo->save();
             
                    $data['userName'] = $user2->name;
                    //  print_r($user2->email);
                    if(($user2->email) != ''){
                        if(Auth::User()->role_id != 4){
                            $pageID = '18';
                        }else{
                            $pageID = '17';   
                        } 
                        // $user = $request->user();
                        $data['templateData'] = self::collectData($user2->id,Auth::User()->id,$request->template,$pageID);
                        Mail::to($user2->email)->send( new EmailSend( $data ));
                    }
                //return  'yes';
                }
            }
            return 'yes';
        }
    }
     public function getTemplates($pageID){
        $userID = Auth::User()->id;
        $templates = \App\PageMeta::where([ ['page_id', '=', $pageID] ])->get();
        $found = UserInformation::where('user_id', '=', $userID)->where('meta_key', '=', 'eventtemplate')->first();
        //echo "<pre>";
        $data = (array)json_decode($found['meta_value']);
        // print_r($data);
        // die();
        $j = 0;
        $returnArray = array();
        foreach($data as $value ){
            $jd = $j++;
          // print_r($value->Key);
            $returnArray[$jd ]['key'] = $value->Key;
            $returnArray[$jd]['value'] = $value->value;
            $returnArray[$jd ]['message'] = $value->message;
        }
        

        //$templates = array_merge($templates,$found);

        foreach($templates as $key=>$t){
            $returnArray1[$key]['key'] = $t->key;
            $returnArray1[$key]['value'] = $t->display_name;
            $returnArray1[$key]['message'] = $t->value;
        }

        $returnArray11 = array_merge($returnArray1,$returnArray);

        // echo count($returnArray11);
        // die();
        return json_encode($returnArray11);
    }
    public function collectData($toID,$fromID,$tempId,$pageID){
        $userFrom = User::where('id',$fromID)->first();
        $userTo = User::where('id',$toID)->first();
        
        $addevent_notifi = PageMeta::where([ ['page_id', '=', $pageID], ['key', '=', $tempId] ])->first();
        $addevent_notifid = $addevent_notifi->value;
        $keyArray = ['sport_name','state','city','zip','phone'];
        foreach($keyArray as $key){
            $userFromInfo = UserInformation::where('user_id',$fromID)->where('meta_key',$key)->pluck('meta_value')->first();
            $addevent_notifid = str_replace( '{'.$key.'}', $userFromInfo?$userFromInfo:'',  $addevent_notifid );
         } 
         $addevent_notifid = str_replace( '{name}', $userFrom->name,  $addevent_notifid );
          $addevent_notifid = str_replace( '{email}', $userFrom->email,  $addevent_notifid );
          $addevent_notifi->value = $addevent_notifid;
        return $addevent_notifi;
    }
    public function deletebanner($event_id){
        $eventData= SportEvent::where('id', '=', $event_id)->first();
        if($eventData){
            $eventData->banner_image = '';
            $eventData->save();
            return 'Deleted' ;
        }else{
            return 'Not Deleted' ;
        }

    }
}