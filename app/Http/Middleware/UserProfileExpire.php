<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class UserProfileExpire
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::User()){
            return Redirect('/login');
        }
        if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
           
            $userCreatedDate1 = (array) Auth::User()->created_at;
            $current_date = new \DateTime();
            $current_date = $current_date->format('Y-m-d 24:59:59');
            $userCreatedDate = new \DateTime($userCreatedDate1['date']);
            $userCreatedDate = $userCreatedDate->format('Y-m-d 00:00:00');
            $datediff = strtotime($current_date) - strtotime($userCreatedDate);
            $diff1 = floor($datediff / (60 * 60 * 24));
          
            if($diff1 > 14) {
                if (!$request->user()->subscribed('main') &&  Auth::user()->is_paid != 'yes') {
                    if ($request->ajax() || $request->wantsJson()) {
                        return response('Unauthorized.', 401);
                    }
                     
                    return redirect('/')->with('upgrade_notice','your message here');
                }
                if ($request->user()->verified == 0) {
                    \Session::flash('error', 'Your Email is Not Verified Yet!');
                    return redirect('/');
                }
                $data_filled = \App\UserInformation::where( 'user_id', '=', $request->user()->id )->where('meta_key', '=', 'profile_filled')->where('meta_value', '=', 'filled')->first();
                if( $data_filled == '' && $request->user()->role_id == 3){
                    // print_r(  );die;
                    return redirect()->intended('/complete-registration#1');;
                }else{
                    return $next($request);
                }

            }else{
                if ($request->user()->verified == 0) {
                    \Session::flash('error', 'Your Email is Not Verified Yet!');
                    return redirect('/');
                }
                $data_filled = \App\UserInformation::where( 'user_id', '=', $request->user()->id )->where('meta_key', '=', 'profile_filled')->where('meta_value', '=', 'filled')->first();
                if( $data_filled == '' && $request->user()->role_id == 3 ){
                    // print_r(  );die;
                    return redirect()->intended('/complete-registration#1');;
                }else{
                    return $next($request);
                }
               return $next($request);
            }
        }else{
                return $next($request);
        }

        return $next($request);
    
    }
}
