<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class UserAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::User()){
            return Redirect('/login');
        }
        if(Auth::User()->role_id == '1'){
           return $next($request);
        }else{
          return Redirect()->back();      
        }

        // return $next($request);
    
    }
}
