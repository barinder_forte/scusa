<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AtheleteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->role_id == 1 || Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
            return $next($request);
        }else{
            return view('errors.404');
        }
        return $next($request);
    }
}
