<?php

namespace App\Mail;

use App\PageMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyAthleteAccount extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $success )
    {
        $this->user = $success;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->replyTo('info@sportcontactusa.com');
        $new_account_verify = PageMeta::where([ ['page_id', '=', '14'], ['key', '=', 'verify_email'] ])->first();
          // print_r( $new_account_verify ); die;
        $this->subject = $new_account_verify->display_name;
        // echo "<pre>"; print_r($this->subject); die();
        // $verifyurl = "<a href='{{url()./verify/$this->user->email_token}}' >here</a>";
         $new_account_verify2 = str_replace( '{here}', '<a href="'.url('/').'/verify/'.$this->user->email_token.'">here</a>', $new_account_verify->value );
        return $this->view('email.default')->with(['content' => $new_account_verify2]);
    }
}
