<?php
namespace App\Mail;
use Voyager;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use App\SportEvent;
use App\User;
use Illuminate\Support\Facades\DB;

class ContactCoach extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->replyTo($this->data['replyto']);
        // if( is_array($this->data['bcc']) && count($this->data['bcc']) > 0 ){
        //     $this->bcc($this->data['bcc']);
        // }
        $message = str_replace(null, '', $this->data['message']);
        $message = str_replace('null', '', $message);
        $message = str_replace( '{coach_name}', $this->data['userName'] , $message );
        $message = str_replace( '{athlete_name}', $this->data['userName'] , $message );
        $this->subject($this->data['request']['subject']);
        // echo $this->data['replyto']; die;
        $this->from( "info@sportcontactusa.com","Sport Contact USA" );
        return $this->view('email.default')->with(['content' => $message ]);
    }
}