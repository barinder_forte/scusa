<?php

namespace App\Mail;

use Voyager;
use App\PageMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class SubscriptionCancelled extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user )
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // echo "<pre>";
        $this->replyTo('info@sportcontactusa.com');
        $end_date = $this->user->subscription('main')->ends_at;
        $end_date = date_format($end_date,"M d, Y");
        
        $cancel_subscription = PageMeta::where([ ['page_id', '=', 22], ['key', '=', 'cancel_subscription'] ])->first();
        // $cancel_subscription = Voyager::pagemeta('cancel_subscription', '14');
        $this->subject = $cancel_subscription->display_name;
        $cancel_subscription = str_replace( '{name}', $this->user->name, $cancel_subscription->value );
        $cancel_subscription = str_replace( '{expire}', $end_date, $cancel_subscription );
        // print_r( $cancel_subscription ); die;
        return $this->view('email.default')->with(['content' => $cancel_subscription]);
    }
}