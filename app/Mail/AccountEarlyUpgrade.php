<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PageMeta;

class AccountEarlyUpgrade extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user , $days)
    {
        $this->user = $user;
        $this->days = $days;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {           
        $this->replyTo('info@sportcontactusa.com');
        $upgrade_account = PageMeta::where([ ['page_id', '=', 14], ['key', '=', 'early_upgrade_account'] ])->first();
        // print_r( $upgrade_account ); die;
        $this->subject = $upgrade_account->display_name;
        $upgrade_account = str_replace( '{name}', $this->user->name, $upgrade_account->value );
        $upgrade_account = str_replace( '{days}', $this->days, $upgrade_account );
        // print_r( $upgrade_account ); die;
        return $this->view('email.default')->with(['content' => $upgrade_account]);
    }
}
