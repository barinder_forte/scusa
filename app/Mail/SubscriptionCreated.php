<?php

namespace App\Mail;

use Voyager;
use App\PageMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $plan, $user )
    {
        $this->plan = $plan;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->replyTo('info@sportcontactusa.com');
        $new_subscription_email = PageMeta::where([ ['page_id', '=', 22], ['key', '=', 'new_subscription_email'] ])->first();
        $this->subject = $new_subscription_email->display_name;
        $new_subscription_email = str_replace( '{name}', $this->user->name, $new_subscription_email->value );
        $new_subscription_email = str_replace( '{plan}', $this->plan->name, $new_subscription_email );
        // print_r( $new_account_email ); die;
        return $this->view('email.default')->with(['content' => $new_subscription_email]);
    }
}
