<?php

namespace App\Mail;

use App\PageMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountCreatedAdminApproval extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $success, $admin_emails2 , $success2 )
    {
        $this->user = $success;
        $this->admins = $admin_emails2;
        $this->success2 = $success2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if( is_array($this->admins) && count($this->admins) > 1 ){
            array_shift($this->admins);
            $this->bcc( $this->admins );
        }
        $this->replyTo($this->admins[0]);

        $new_account_verify = PageMeta::where([ ['page_id', '=', '14'], ['key', '=', 'new_account_for_admin_approval'] ])->first();
        $this->subject = $new_account_verify->display_name;
        $new_account_verify = str_replace( '{coach_name}', $this->user->name, $new_account_verify->value ); 
        $new_account_verify = str_replace( '{email}', $this->user->email, $new_account_verify );
        $new_account_verify = str_replace( '{phone}', $this->success2['phone'], $new_account_verify );
        $new_account_verify = str_replace( '{sport}', $this->success2['sport'], $new_account_verify );
         $new_account_verify = str_replace( '{school}', $this->success2['school'], $new_account_verify );
        $activation_link = '<a href="'.url('/admin/approve/'.$this->user->id.'/').'">here</a>';
        $new_account_verify = str_replace( '{here}', $activation_link, $new_account_verify );

        return $this->view('email.default')->with(['content' => $new_account_verify]);
    }
}
