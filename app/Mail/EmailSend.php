<?php

namespace App\Mail;

use Voyager;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class EmailSend extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
        $getTemplate = $this->data['templateData'];
        // $getTemplate = str_replace( '[Name]', $this->data['userName'], $getTemplate );
        // $getTemplate = str_replace( '[Message]', $this->data['message'], $getTemplate );
        $this->subject = $getTemplate->display_name;
        $this->replyTo('info@sportcontactusa.com');
        $this->subject($this->data['subject']);
        return $this->view('email.send-email')->with(['content' => $getTemplate->value ]);
    }
}
