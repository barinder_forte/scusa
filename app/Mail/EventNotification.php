<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PageMeta;

class EventNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $addevent_notifid = $this->data['templateData'];
          
        $this->subject = $this->data['subject'];
         
        $this->replyTo('info@sportcontactusa.com');
         
        return $this->view('email.contact-coach')->with(['content' => $addevent_notifid ]);
    }
}
