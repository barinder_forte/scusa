<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PageMeta;

class AccountUpgrade extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user )
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $this->replyTo('info@sportcontactusa.com');
        $upgrade_account = PageMeta::where([ ['page_id', '=', 14], ['key', '=', 'upgrade_account'] ])->first();
        $this->subject = $upgrade_account->display_name;
        $upgrade_account = str_replace( '{name}', $this->user->name, $upgrade_account->value );
        // print_r( $cancel_subscription ); die;
        return $this->view('email.default')->with(['content' => $upgrade_account]);
    }
}
