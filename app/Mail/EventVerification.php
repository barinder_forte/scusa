<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PageMeta;

class EventVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $admin_emails )
    {
        $this->data = $data;
        $this->admins = $admin_emails;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if( is_array($this->admins) && count($this->admins) > 1 ){
            array_shift($this->admins);
            $this->bcc( $this->admins );
        }

         $addevent_notifid = $this->data['templateData'];
          
        $this->subject = $addevent_notifid->display_name;
        $addevent_notifid = str_replace( '{event_id}', $this->data['event_id'], $addevent_notifid->value);
        $addevent_notifid = str_replace( '{event_slug}', $this->data['event_slug'], $addevent_notifid);
        $addevent_notifid = str_replace( '{event_name}', $this->data['event_name'], $addevent_notifid );
        $this->replyTo('info@sportcontactusa.com');   
        return $this->view('email.contact-coach')->with(['content' => $addevent_notifid ]);
    }
}
