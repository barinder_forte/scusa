<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PageMeta;

class contactUs extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $data , $admin_emails)
    {
        $this->data = $data;
        $this->admins = $admin_emails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->replyTo($this->data['email']);
        if( is_array($this->admins) && count($this->admins) > 1 ){
            array_shift($this->admins);
            $this->bcc( $this->admins );
        }
        $contactUsTemp = PageMeta::where([ ['page_id', '=', '14'], ['key', '=', 'contact_us'] ])->first();
        $contactUsTemp1 = str_replace( '{name}', $this->data['first_name'].' '.$this->data['last_name'], $contactUsTemp->value ); 
         $contactUsTemp1 = str_replace( '{email}', $this->data['email'], $contactUsTemp1 ); 
         $contactUsTemp1 = str_replace( '{message}', $this->data['message'] , $contactUsTemp1 ); 
        return $this->replyTo( $this->data['email'], $this->data['first_name'].' '.$this->data['last_name'] )->view('email.default')->with(['content' => $contactUsTemp1 ]);
    }
}
