<?php

namespace App\Mail;

use App\PageMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class accountSignupNotification extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $success )
    {
        $this->user = $success;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {         
        $this->replyTo('info@sportcontactusa.com');
        $new_account_email = PageMeta::where([ ['page_id', '=', '14'], ['key', '=', 'sign_up_notification_to_user'] ])->first();
        // $new_account_email = Voyager::pagemeta('new_account_email', '14');
        $this->subject = $new_account_email->display_name;
        // echo "<pre>"; print_r($this->subject); die();
        $new_account_email = str_replace( '{name}', $this->user->name, $new_account_email->value );
        // print_r( $new_account_email ); die;
        return $this->view('email.default')->with(['content' => $new_account_email]);
    }
}
