<?php

namespace App\Mail;

use Voyager;
use App\PageMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResumeSubscription extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user )
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // echo "<pre>";
        $this->replyTo('info@sportcontactusa.com');
        $resume_subscription = PageMeta::where([ ['page_id', '=', 22], ['key', '=', 'resume_subscription'] ])->first();
        // $resume_subscription = Voyager::pagemeta('resume_subscription', '14');
        $this->subject = $resume_subscription->display_name;
        $resume_subscription = str_replace( '{name}', $this->user->name, $resume_subscription->value );
        // print_r( $cancel_subscription ); die;
        return $this->view('email.default')->with(['content' => $resume_subscription]);
    }
}
