<?php

namespace App\Mail;

use Voyager;
use App\PageMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProfileComplete extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user )
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // echo "<pre>";
        $this->replyTo('info@sportcontactusa.com');
        $profile_completition_mail = PageMeta::where([ ['page_id', '=', 14], ['key', '=', 'profile_completition_mail'] ])->first();
        $this->subject = $profile_completition_mail->display_name;
        $profile_completition_mail = str_replace( '{name}', $this->user->name, $profile_completition_mail->value );
        // print_r( $cancel_subscription ); die;
        return $this->view('email.default')->with(['content' => $profile_completition_mail]);
    }
}
