<?php

namespace App\Mail;

use Voyager;
use App\PageMeta;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class profileVisitNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user,$viewr,$notificationID )
    {
        $this->user = $user;
        $this->viewr = $viewr;
        $this->notificationID = $notificationID;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->replyTo('info@sportcontactusa.com');
        $profile_completition_mail = PageMeta::where([ ['page_id', '=', 14], ['key', '=', 'profile_visit'] ])->first();
        $this->subject = $profile_completition_mail->display_name;
        if(\Auth::User()->role_id == 4){
            $noti = 'coach';
        }else{
            $noti = 'athlete';
        }
        $profile_completition_mail1 = str_replace( '{name}', $this->user->name, $profile_completition_mail->value );
        $profile_completition_mail2 = str_replace( '{visitername}', $this->viewr->name, $profile_completition_mail1 );

        $profile_completition_mail3 = str_replace( '{here}', '<a href="'.url('/').'/notification/read/'.$this->viewr->user_slug.'/'.$this->notificationID.'">here</a>', $profile_completition_mail2);
        
        // $profile_completition_mail3 = str_replace( '{slug}', $this->viewr->user_slug, $profile_completition_mail2);
         // $profile_completition_mail = str_replace( '{notification_id}',$this->notificationID, $profile_completition_mail3);
        return $this->view('email.default')->with(['content' => $profile_completition_mail3 ]);
    }
}
