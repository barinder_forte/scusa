<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SportEventMeta extends Model
{
    
    protected $table = "sport_event_meta";
    protected $guarded = [];
    public $timestamps = false;
    protected $fillable = ['id', 'event_id', 'key', 'display_name', 'details'];
}
