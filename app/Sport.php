<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Sport extends Model
{
    protected $table = 'sports';
    
    public function sportsMetaData(){
        
       return $this->hasMany('App\SportsMeta','sport_id')->orderBy('order','ASC');
    
    }

}
