<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    
    public function getcat(){
        
       return $this->belongsTo('App\Category','category_id','id');
    
    }
    public function getauthor(){
        
       return $this->belongsTo('App\User','author_id','id');
    
    }
}
