<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoachsSchool extends Model
{
    protected $table = 'coachschool';
    protected $guarded = [];
    public $timestamps = false;
}
