<?php

namespace App\Console\Commands;

use App\User;
use App\UserInformation;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProfileComplete;
use Illuminate\Console\Command;

class CompleteProfileEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:profile-complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send profile completition emails';

    /**
     * The drip e-mail service.
     *
     * @var DripEmailer
     */
    // protected $drip;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->drip = $drip;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = UserInformation::where('meta_key', '=', 'progress_data' )->where('meta_value', '<', 100)->orderby('user_id')->pluck('user_id');
        foreach( $users as $user ){
            $userdata = User::where('id', '=', $user)->first();
            if( $userdata && $userdata->role_id != 1 ){
                // echo $userdata->id." \n ";
                Mail::to( $userdata->email )->send( new ProfileComplete($userdata) );
            }
        }
    }
}
