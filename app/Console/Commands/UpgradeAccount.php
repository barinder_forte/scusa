<?php

namespace App\Console\Commands;

use App\User;
use App\UserInformation;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccountUpgrade;
use App\Mail\AccountEarlyUpgrade;
use Illuminate\Console\Command;

class UpgradeAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:upgrade-account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Upgrade account notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   $userArray = array();
        $today = date('Y-m-d H:i:s');
        $today7 = date('Y-m-d', strtotime("-6 days"));
        $users = User::whereIn('role_id', [ 2,3 ] )->where('created_at','LIKE','%'. $today7 .'%')->get();
        foreach ($users as $user) {
            if( !$user->subscribed('main') &&  $user->is_paid != 'yes'){
                Mail::to( $user->email )->send( new AccountEarlyUpgrade($user , '7') );
                $userArray[] = $user->email;
            }
        }
        $today10 = date('Y-m-d', strtotime("-9 days"));
        $users1 = User::whereIn('role_id', [ 2,3 ] )->where('created_at','LIKE','%'. $today10 .'%')->get();
        foreach ($users1 as $user) {
            if( !$user->subscribed('main') &&  $user->is_paid != 'yes' ){
                Mail::to( $user->email )->send( new AccountEarlyUpgrade($user , '10') );
                $userArray[] = $user->email;
            }
        }
        $today13 = date('Y-m-d', strtotime("-12 days"));
        $users2 = User::whereIn('role_id', [ 2,3 ] )->where('created_at','LIKE','%'. $today13 .'%')->get();
        foreach ($users2 as $user) {
            if( !$user->subscribed('main') &&  $user->is_paid != 'yes'){
                Mail::to( $user->email )->send( new AccountEarlyUpgrade($user , '13') );
                $userArray[] = $user->email;
            }
        }
        $diffArray = ['5','10','15','20','25','30','35','40','45','50','55','60']; 
        foreach($diffArray as $date){
            $Days30 = date('Y-m-d', strtotime("-".$date." days"));
            $users3 = User::whereIn('role_id', [ 2,3 ] )->where('created_at','LIKE','%'. $Days30 .'%')->get();
            foreach ($users3 as $user) {
                if( !$user->subscribed('main') && $user->is_paid != 'yes'){
                    Mail::to( $user->email )->send( new AccountUpgrade($user ) );
                    $userArray[] = $user->email;
                }
            }
        }
        print_r($userArray);
        print_r($today);
    }
}