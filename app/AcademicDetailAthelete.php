<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicDetailAthelete extends Model
{
    protected $table = 'academicdetailathelete';
    public $timestamps = false;
}
