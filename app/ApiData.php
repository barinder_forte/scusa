<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiData extends Model
{
	protected $table = 'api_fetched_data';
	public $timestamps = false;


}