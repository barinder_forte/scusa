<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $guarded = [];
    
    public function Viewer(){
       return $this->belongsTo('App\User','viewer_user_id','id');
    }
}
