// import router from './routes.js';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap-sass');
} catch (e) {}

window.Vue = require('vue');
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);
Vue.config.silent = true;
// import InputTag from 'vue-input-tag';
// import VueRouter from 'vue-router';
// Vue.use(VueRouter);

require('./bootstrap');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('athelete', require('./components/Example.vue'));
Vue.component('athelete', require('./components/signup/Athelete.vue'));
Vue.component('coach', require('./components/signup/coach.vue'));
Vue.component('parent', require('./components/signup/parent.vue'));

Vue.component('addevent', require('./components/add-ons/addevent.vue'));

Vue.component('login', require('./components/login.vue'));
Vue.component('user1', require('./components/profile/athelete/user1.vue'));
Vue.component('user2', require('./components/profile/athelete/user2.vue'));
Vue.component('user3', require('./components/profile/athelete/user3.vue'));
Vue.component('user4', require('./components/profile/athelete/user4.vue'));
Vue.component('academics', require('./components/profile/athelete/academic.vue'));
Vue.component('gallery', require('./components/profile/athelete/gallery.vue'));
Vue.component('profileimage', require('./components/profile/athelete/profileimage.vue'));
Vue.component('videoupload', require('./components/profile/athelete/videoupload.vue'));
Vue.component('step1', require('./components/profile/athelete/complete-step1.vue'));
Vue.component('step2', require('./components/profile/athelete/complete-step2.vue'));
Vue.component('sports', require('./components/profile/athelete/sports.vue'));
Vue.component('textbox', require('./components/inputtypes/text.vue'));
Vue.component('checkbox', require('./components/inputtypes/checkbox.vue'));
Vue.component('selectbox', require('./components/inputtypes/selectbox.vue'));
Vue.component('validate', require('./components/profile/athelete/validate.vue'));
Vue.component('coach1', require('./components/profile/coach/coach1.vue'));
Vue.component('coach2', require('./components/profile/coach/coach2.vue'));
Vue.component('coach3', require('./components/profile/coach/coach3.vue'));
Vue.component('slider', require('./components/inputtypes/slider.vue'));
Vue.component('academic_achiv', require('./components/profile/athelete/achievements/Academic.vue'));
Vue.component('extra_curricular', require('./components/profile/athelete/achievements/extra_curricular.vue'));
Vue.component('awards', require('./components/profile/athelete/achievements/awards.vue'));
Vue.component('testimonails', require('./components/profile/athelete/achievements/testimonails.vue'));
Vue.component('athletic_achievements', require('./components/profile/athelete/achievements/athletic_achievements.vue'));
// Vue.component('progress', require('./components/profile/athelete/achievements/progress.vue'));


const app = new Vue({
    el: '#app',
    data: function(){
    	return {
	        login_open: false,
	        signup_popup: false,
			login_popup: false,
			athelete_show: false,
			parent_show: false,
			coach_show: false,
			before_signup: true,
			signup_popup_message: false,
			step_2: false,
			step_1: true,
			firstname: null,
			showpop_email: false,
			hide_step2:true,
			step_2:false,
			role: 2,
			athelete_class: '',
			coach_class: '',
			parent_class: '',
			overlay:false
    	}
    },
    methods: {
		signup_open: function( form = null ){
			app.signup_popup = true;
			app.athelete_show = true;
			app.athelete_class= 'checked';
			jQuery('html').addClass('popup');
		},
		
		login_form: function(){
			// console.log('hello');
			app.login_popup = true;
			jQuery('html').addClass('popup');
		},
		signup_close: function(){
			app.signup_popup = false;
			app.athelete_show= false;
			app.parent_show= false;
			app.coach_show= false;
			this.athelete_class= '';
			this.parent_class= '';
			this.coach_class= '';
			jQuery('html').removeClass('popup');
		},
		login_close: function(){
			app.login_popup = false;
			jQuery('html').removeClass('popup');
		},
		signup_url: function( show ){
			this.signup_popup = true;
			jQuery('html').addClass('popup');
			console.log( show );
			this.role = show;
			this.athelete_class= '';
			this.parent_class= '';
			this.coach_class= '';

			if( show == '2' ){
				this.athelete_class= 'checked';
				this.athelete_show = true;
				this.parent_show = false;
				this.coach_show = false;
			}
			if( show == '3' ){
				this.parent_class= 'checekd';
				this.athelete_show = false;
				this.parent_show = true;
				this.coach_show = false;
			}
			if( show == '4' ){
				console.log( show );
				this.coach_class= 'checked';
				app.athelete_show = false;
				app.parent_show = false;
				app.coach_show = true;
			}
		},
		goto_step2: function(){
			app.step_2 = true;
			app.step_1 = false;
		},
		goto_step1: function(){
			app.step_2 = false;
			app.step_1 = true;
		}
	}
});