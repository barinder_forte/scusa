// import router from './routes.js';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap-sass');
} catch (e) {}
// to assist with any data manipulation [global usage]
window._ = require('lodash');
window.Vue = require('vue');
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);
Vue.config.silent = true;
// import InputTag from 'vue-input-tag';
// import VueRouter from 'vue-router';
// Vue.use(VueRouter);

require('./bootstrap');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('addevent', require('./components/add-ons/addevent.vue'));
Vue.component('sendemail_attendee', require('./components/add-ons/sendemail_attendee.vue'));
Vue.component('sendemail_allattendees', require('./components/add-ons/sendemail_allattendees.vue'));
Vue.component('profile_email', require('./components/add-ons/profile_email.vue'));
Vue.component('addevent', require('./components/add-ons/import_event.vue'));
Vue.component('email', require('./components/add-ons/event_email.vue'));
Vue.component('coach1', require('./components/profile/coach/coach1.vue'));
Vue.component('coach2', require('./components/profile/coach/coach2.vue'));
Vue.component('coach3', require('./components/profile/coach/coach3.vue'));
Vue.component('user1', require('./components/profile/athelete/user1.vue'));
Vue.component('user2', require('./components/profile/athelete/user2.vue'));
Vue.component('user3', require('./components/profile/athelete/user3.vue'));
Vue.component('user4', require('./components/profile/athelete/user4.vue'));
Vue.component('academics', require('./components/profile/athelete/academic.vue'));
Vue.component('gallery', require('./components/profile/athelete/gallery.vue'));
Vue.component('profileimage', require('./components/profile/athelete/profileimage.vue'));
Vue.component('videoupload', require('./components/profile/athelete/videoupload.vue'));
Vue.component('step1', require('./components/profile/athelete/complete-step1.vue'));
Vue.component('step2', require('./components/profile/athelete/complete-step2.vue'));
Vue.component('sports', require('./components/profile/athelete/sports.vue'));
Vue.component('textbox', require('./components/inputtypes/text.vue'));
Vue.component('radio', require('./components/inputtypes/radio.vue'));
Vue.component('instruction', require('./components/inputtypes/instruction.vue'));
Vue.component('checkbox', require('./components/inputtypes/checkbox.vue'));
Vue.component('download-profile', require('./components/inputtypes/button.vue'));
Vue.component('selectbox', require('./components/inputtypes/selectbox.vue'));
Vue.component('validate', require('./components/profile/athelete/validate.vue'));
Vue.component('slider', require('./components/inputtypes/slider.vue'));
Vue.component('academic_achiv', require('./components/profile/athelete/achievements/Academic.vue'));
Vue.component('extra_curricular', require('./components/profile/athelete/achievements/extra_curricular.vue'));
Vue.component('awards', require('./components/profile/athelete/achievements/awards.vue'));
Vue.component('testimonails', require('./components/profile/athelete/achievements/testimonails.vue'));
Vue.component('athletic_achievements', require('./components/profile/athelete/achievements/athletic_achievements.vue'));
Vue.component('progress-bar', require('./components/add-ons/progress.vue'));
Vue.component('stats', require('./components/profile/athelete/stats.vue'));

const app = new Vue({
    el: '#app',
    data: function(){
    	return {
	        login_open: false,
	        signup_popup: false,
			login_popup: false,
			athelete_show: false,
			parent_show: false,
			coach_show: false,
			before_signup: true,
			signup_popup_message: false,
			step_2: false,
			step_1: true,
			showpop_email:false,
			profile_email:false,
			sendemail_attend:false,
			sendemail_allattendees:false,
			firstname: null,
			hide_step2:true,
			step_2:false,
			role: 2,
			athelete_class: '',
			coach_class: '',
			parent_class: '',
			overlay:false,
			dataform:'',
			athlete_sport_id:'',
			email_event_id:'',
			athlete_sport_id: '',
			athlete_position_id: '',
			sync_user_sport: false,
			sync_sport_user: false,
			coach_school_id:'',
			coach_school_add:'',
			coach_sport_id:'',
			coach_gender_id:'',	
			// coach_sport_name: '',
			sync_coach: false,
    	}
    },
    methods: {
		signup_open: function( form = null ){
			app.signup_popup = true;
			app.athelete_show = true;
			app.athelete_class= 'checked';
			jQuery('html').addClass('popup');
		},
		hidepopemail:function(){
			jQuery('html').removeClass('popup');
			app.showpop_email = false;
			this.showpop_email = false;
		},
		showemail:function(data, event_id){
			jQuery('html').addClass('popup');
			app.showpop_email = true;
			this.showpop_email = true;
			app.dataform = data;
			this.dataform = data;
			this.email_event_id = event_id;
		},
		showemail_profile:function(){
			
			jQuery('html').addClass('popup');
			app.profile_email = true;
			this.profile_email = true;
		},
		sendemailatt:function(){
			
			jQuery('html').addClass('popup');
			app.sendemail_attend = true;
			this.sendemail_attend = true;
		},
    	sendemailattall:function(){
			
			jQuery('html').addClass('popup');
			app.sendemail_allattendees = true;
			this.sendemail_allattendees = true;
		},
		login_form: function(){
			// console.log('hello');
			app.login_popup = true;
			jQuery('html').addClass('popup');
		},
		signup_close: function(){
			app.signup_popup = false;
			app.athelete_show= false;
			app.parent_show= false;
			app.coach_show= false;
			this.athelete_class= '';
			this.parent_class= '';
			this.coach_class= '';
			jQuery('html').removeClass('popup');
		},
		login_close: function(){
			app.login_popup = false;
			jQuery('html').removeClass('popup');
		},
		signup_url: function( show ){
			this.signup_popup = true;
			jQuery('html').addClass('popup');
			console.log( show );
			this.role = show;
			this.athelete_class= '';
			this.parent_class= '';
			this.coach_class= '';

			if( show == '2' ){
				this.athelete_class= 'checked';
				this.athelete_show = true;
				this.parent_show = false;
				this.coach_show = false;
			}
			if( show == '3' ){
				this.parent_class= 'checekd';
				this.athelete_show = false;
				this.parent_show = true;
				this.coach_show = false;
			}
			if( show == '4' ){
				console.log( show );
				this.coach_class= 'checked';
				app.athelete_show = false;
				app.parent_show = false;
				app.coach_show = true;
			}
		},
		goto_step2: function(){
			app.step_2 = true;
			app.step_1 = false;
		},
		goto_step1: function(){
			app.step_2 = false;
			app.step_1 = true;
		}
	}
});