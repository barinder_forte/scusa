import VueRouter from 'vue-router';

let routes = [
    {
        path: '/athelete',
        component: require('./components/signup/Athelete')
    },
    {
        path: '/coach',
        component: require('./components/signup/coach')
    },
    {
        path: '/parent',
        component: require('./components/signup/parent')
    },
    {
        path: '/login',
        component: require('./components/login')
    }
];


export default new VueRouter({
    routes
});