// import router from './routes.js';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap-sass');
} catch (e) {}
// to assist with any data manipulation [global usage]
window._ = require('lodash');
window.Vue = require('vue');
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);
Vue.config.silent = true;
// import InputTag from 'vue-input-tag';
// import VueRouter from 'vue-router';
// Vue.use(VueRouter);

require('./bootstrap');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component( 'athelete', require( './components/signup/Athelete.vue' ) );
Vue.component( 'coach', require('./components/signup/coach.vue') );
Vue.component( 'parent', require('./components/signup/parent.vue') );
Vue.component( 'addevent', require('./components/add-ons/addevent.vue') );
Vue.component( 'addevent1', require('./components/add-ons/addevent1.vue') );
Vue.component( 'registerevent', require('./components/add-ons/registerevent.vue') );
Vue.component( 'login', require('./components/login.vue') );
Vue.component( 'social-login', require('./components/signup/social-login.vue') );

// Vue.component( 'slick', require('./components/add-ons/slider.vue') );
// Vue.component('progress', require('./components/profile/athelete/achievements/progress.vue'));

const app = new Vue({
    el: '#app',
    data: function(){
    	return {
	        login_open: false,
	        signup_popup: false,
			login_popup: false,
			athelete_show: true,
			parent_show: false,
			coach_show: false,
			before_signup: true,
			signup_popup_message: false,
			step_2: false,
			step_1: true,
			firstname: null,
			hide_step2:true,
			step_2:false,
			login_id: 2,
			role: 2,
			athelete_class: 'checked',
			coach_class: '',
			parent_class: '',
			overlay:false,
			profile_switch_tab : true,
    	}
    },
    methods: {
		signup_open: function( form = null ){
			app.signup_popup = true;
			app.athelete_show = true;
			app.athelete_class= 'checked';
			jQuery('html').addClass('popup');
		},
		login_form: function(){
			// console.log('hello');
			app.login_popup = true;
			jQuery('html').addClass('popup');
		},
		login_url: function(id){
			this.login_id = id;
		},
		signup_close: function(){
			app.signup_popup = false;
			app.athelete_show= false;
			app.parent_show= false;
			app.coach_show= false;
			this.profile_switch_tab = true;
			this.athelete_class= '';
			this.parent_class= '';
			this.coach_class= '';
			jQuery('html').removeClass('popup');
		},
		login_close: function(){
			app.login_popup = false;
			jQuery('html').removeClass('popup');
		},
		signup_url2: function( show, hide = '' ){
			if( hide == 'hide' ){
				this.profile_switch_tab = false;
			}
			// this.signup_popup = true;
			// jQuery('html').addClass('popup');
			// console.log( show );
			this.role = show;
			this.athelete_class= '';
			this.parent_class= '';
			this.coach_class= '';

			if( show == '2' ){
				this.athelete_class= 'checked';
				this.athelete_show = true;
				this.parent_show = false;
				this.coach_show = false;
			}
			if( show == '3' ){
				this.parent_class= 'checked';
				this.athelete_show = false;
				this.parent_show = true;
				this.coach_show = false;
			}
			if( show == '4' ){
				// console.log( show );
				this.coach_class= 'checked';
				app.athelete_show = false;
				app.parent_show = false;
				app.coach_show = true;
			}
		},
		goto_step2: function(){
			app.step_2 = true;
			app.step_1 = false;
		},
		goto_step1: function(){
			app.step_2 = false;
			app.step_1 = true;
		}
	}
});