@extends('layouts.signup')

@section('content')
<div class="sign-up-popup" style="display: none" id="show-term">
    <div class="sign-up-content" id="show-content-div">
        <span class="close-term">Close</span>
        <div class="show-inner">
        <p>{!! Voyager::page('body', 53) !!}</p>
    </div>
    </div>
</div> 
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="paypal-form">
          <div class="panel panel-default">
            <div class="panel-heading">UPGRADE CHECKOUT</div>
            <div class="checkout-header">
              <div class="row">
                <div class="col-md-8">
                  <h4>Billed To:</h4>
                  <h2>{{ Auth::user()->name }}</h2>
                  <p>Billed monthly until cancelled</p>
                </div>
                <div class="col-md-4">
                  <h4>TOTAL</h4>
                  <h1>${{ $plan->cost }}</h1>
                </div>
              </div>
            </div>
            <div class="panel-body">
              @if (session('error'))
                @if (Auth::user()->subscription('main')->cancelled())
                    <p>Your subscription ends on {{ Auth::user()->subscription('main')->ends_at->format('dS M Y') }}</p>
                    <form action="{{ url('subscription/resume') }}" method="post">
                     <input type="checkbox" id="terms" onchange="activateButton(this)">  I agree to<a target="_blank" href="/term-and-conditions" class="termpop"> Terms and Conditions.</a>
                        <button type="submit" id="payment-button" disabled="disabled"  class="btn btn-primary btn-flat">Resume subscription</button>
                        {{ csrf_field() }}
                    </form>
                @else
                    <p>When you cancel your subscription, your future payments will also be canceled.</p>
                    <form action="{{ url('subscription/cancel') }}" method="post">
                     <input type="checkbox" id="terms" onchange="activateButton(this)"> I agree to  <a target="_blank" class="termpop" href="/term-and-conditions">Terms and Conditions<sup>*</sup>.</a>
                        <button type="submit" id="payment-button" disabled="disabled"  class="btn btn-primary btn-flat" >Cancel subscription</button>
                        {{ csrf_field() }}
                    </form>
                @endif
              @else
                <p class="methiod">CHOOSE A PAYMENT METHOD</p>
                <form action="{{ url('/subscribe') }}" method="post">
                  <div id="dropin-container"></div>
                  <input type="hidden" name="plan" value="{{ $plan->id }}">
                  {{ csrf_field() }}
                  <hr>
                  <input type="checkbox"  id="terms" onchange="activateButton(this)"> I agree to <a target="_blank" href="/term-and-conditions" class="termpop"> Terms and Conditions<sup>*</sup>.</a>

                  <button id="payment-button" disabled="disabled" class="btn btn-primary btn-flat hidden" type="submit">Submit Order</button>
                </form>
<!--                 <span class="sub">By clicking Submit Order, You agree to out Terms and Agreement.</span>
 -->              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
<script>
      function disableSubmit() {
        document.getElementById("payment-button").disabled = true;
      }
      function activateButton(element) {
        if(element.checked) {
         document.getElementById("payment-button").disabled = false;
        }
        else {
          document.getElementById("payment-button").disabled = true;
        }
      }
      </script>

@section('braintree')
  @if (!session('error'))
      <script src="https://js.braintreegateway.com/js/braintree-2.30.0.min.js"></script>
      
      <script>
          $.ajax({
              url: '{{ url('braintree/token') }}'
          }).done(function (response) {
              braintree.setup(response.data.token, 'dropin', {
                  container: 'dropin-container',
                  onReady: function () {
                      $('#payment-button').removeClass('hidden');
                  }
              });
          });
      </script>
  @endif
@endsection