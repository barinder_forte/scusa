<style>
#myProgress {
  width: 100%;
  background-color: #ddd;
}
#myBar {
  width: 1%;
  background-color: #78d8b5;
}
</style>
<?php if($progressData == '100'){ ?>

<style>
    .completeprofile{
        display: none;
    }

</style>

<?php } ?>
@extends('layouts.signup')

@section('content')


<?php 
if(session()->has('role_id')){
    $userinfo = session()->get('role_id');
}else{
    $userinfo="";
}
?>
<?php 
if(\Auth::User()){
    $role_id = \Auth::User()->role_id;
}else{
    $role_id="";
}
?>
@for( $i = date('Y'); $i <= intval(date('Y'))+7; $i++ )
    <?php $years[] = $i; ?>
@endfor
<div id="steps" class="info-forms">
        <section class="grey-bg heading-top completeprofile">
            <div class="container">
                <div class='progressbar'>
                <div id="myProgress"  class="maindiv">
                     <div class="progressdiv" id="myBar" style="width:{{$progressData}}%"></div>
                     <h2>Your profile is <span class="progressval"><?php echo $progressData;  ?></span>% <br> complete.</h2>
                     <input type="hidden" value="<?php echo $progressData;  ?>" id="progressdata">
                </div>
                </div>
                
            </div>
        </section>
    <step1  :role_id="{{json_encode($role_id)}}" :usermeta="{{ json_encode(Voyager::UserInformation('all', $id )) }}" :allsports="{{ json_encode($allsports) }}" :graduation='{{ json_encode($years) }}'><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></step1>
    
    <form v-show="hide_step2" class="details no-padding">
    <section class="white-bg">
    <div class="container">
    <div class="info-forms">
    <div class="sign-up-form">
    <h3 class="heading-form"><span>2</span>Parent / Guardian Information</h3>
    </div>
    </div>
    </div>
    </section>
    </form>
    <!--<div class="overlay-center" v-cloak v-show="overlay" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div>-->
    <step2 v-show="step_2" :parentdata="{{ json_encode(Voyager::parents($id )) }}" :parentdatabkp="{{ json_encode(Voyager::parents( $id)) }}"></step2>
    <section class="white-bg dashboard-bottom">
        <div class="enter-dashboard">
            <!--<p>Don’t want to set this up now? <a href="/profile">Click to enter your dashboard</a></p>-->
        </div>
    </section>  
</div>
@endsection
<script>
function move(val) {
    if(val == 100){
        $('.completeprofile').hide();
    }else{
        $('.completeprofile').show();
    }
  var progressdata = document.getElementById("progressdata").value; 
  console.log(val);
  if(val != progressdata){
       jQuery('#progressdata').val(val);
      if(val > progressdata){
          $('.progressval').text(val);
          var elem = document.getElementById("myBar");   
          
          console.log(progressdata);
          var width = progressdata;
          var id = setInterval(frame, 50);
          function frame() {
                if (width >= val) {
                  clearInterval(id);
                } else {
                  width++; 
                  elem.style.width = width + '%'; 
                }
          }
      }else{
        
          $('.progressval').text(val);
          var elem = document.getElementById("myBar");   
          
          console.log(progressdata);
          var width = progressdata;
          var id = setInterval(frame, 50);
          function frame() {
                if (width <= val) {
                  clearInterval(id);
                } else {
                  width--; 
                  elem.style.width = width + '%'; 
                }
          }
      }
  }
}
</script>