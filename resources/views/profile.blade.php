<style>
	#myProgress {
	  width: 100%;
	  background-color: #ddd;
	}
	#myBar {
	  width: 1%;
	  background-color: #78d8b5;
	}
	.notification-bar p.account-upgrade a{
		color: #000 !important;
		text-decoration: underline;
	}
</style>
<?php if($progressData == '100'){ ?>
<style>
	.completeprofile{
		display: none;
	}
	.step-forms .right-section .main-info .tab-content .tab-pane .tab-inner .inner-content p.content-achi {
	    text-transform: inherit;
	}
</style>

<?php } ?>
<script >
	if (window.location.hash == "#_=_")
	{
    	window.location.hash = "";
	}
</script>
@extends('layouts.signup')
@section('content')
	<div class="main-container main-inner step-forms">
    	
    	@for( $i = date('Y'); $i <= intval(date('Y'))+7; $i++ )
        	<?php $years[] = $i; ?>
        @endfor
        
       <?php 
       $yearac = array();
        $date = date('Y');
        $start = $date - 5;
        $end= $date + 4;
       for( $j = $start; $j <= $end; $j++ ){
       	
       	$yearac[] = $j;
       	
       }

        $yearAwards = array();
        $dateyearAwards = date('Y');
        $startyearAwards = $dateyearAwards - 30;
        $endyearAwards= $dateyearAwards ;
	       
		$n1 = 0;
		for( $j1 = $startyearAwards; $j1 <= $endyearAwards; $j1++ ){
		$yearAwards[$n1]['name'] = $j1;
		$yearAwards[$n1]['language'] = $j1;
		$n1++;
		}

		$yearachiv = array();
		$n = 0;
		for( $j = $start; $j <= $end; $j++ ){
		$yearachiv[$n]['name'] = $j;
		$yearachiv[$n]['language'] = $j;
		$n++;
		}
       $yearaccadmic = array();
        $startac = $date - 50;
       for( $k = $startac; $k <= $end; $k++ ){
       	
       	$yearaccadmic[] = $k;
       	
       }
        $yearaccadmict = array();
        $startact = $date - 5;
       for( $kt = $startact; $kt <= $date; $kt++ ){
       	
       	$yearaccadmict[] = $kt;
       	
       }
       use App\ApiData;
		$scholllist = ApiData::select('id','school_name')->orderBy('school_name','ASC')->get();
		$schoollistarray = array();
			
		foreach($scholllist as $key=>$s){
			$schools_send[$s->id] = $s->school_name;
			$schoollistarray[$key]['label'] = $s->school_name;
			$schoollistarray[$key]['value'] = $s->id;
		}
		$schoollistarrayjson = json_encode($schoollistarray);
		  $divisionss1 = Voyager::pagemetadetail('Division', '16');
			   $divisionss2 = json_decode($divisionss1);
		if(isset($divisionss2->options)){
			$divisionss = $divisionss2->options;
		}
		else{
			$divisionss = array();
		}
			   
		$achivement_athlete = Voyager::pagemetadetail('achivement_athlete', '16');
		$achivement_athlete = json_decode($achivement_athlete);
		if(isset($achivement_athlete->options)){
			$achivement_athlete = $achivement_athlete->options;
		}
		else{
			$achivement_athlete = array();
		}
		 $achivement_athlete = (array) $achivement_athlete;
         sort($achivement_athlete , SORT_STRING);
       ?>
        
        
		<section class="grey-bg heading-top completeprofile">
			<div class="container">
				<div class="info-forms">
					<h1>Complete your profile</h1>
					<h2>a complete profile will give you better chances for being recruited</h2>
				</div>
				<div class='progressbar'>
				<div id="myProgress"  class="maindiv">
				 	 <div class="progressdiv" id="myBar" style="width:{{$progressData}}%"></div>
				 	 <h2>Your profile is <span class="progressval"><?php echo $progressData;  ?></span>% <br> complete.</h2>
				 	 <input type="hidden" value="<?php echo $progressData;  ?>" id="progressdata">
				</div>
				</div>
				<!--<div class='progressbar'>-->
				<!--	<div class="maindiv" style="width='100%'">-->
				<!--		<div class="progressdiv" style='width:<?php echo $progressData;  ?>%'></div>-->
				<!--		<h2>Your profile is <?php echo $progressData;  ?>% <br> complete.</h2>-->
				<!--	</div>	-->
				<!--</div>-->

			</div>
		</section>
		<section class="grey-bg">
			<div class="">
				<div class="container">
					<div class="info-forms">
						<div class="row">
						<div class="col-lg-8 col-md-12 left-section class-mobile">
								<div class="top-heading">
									<!-- <div class="col-md-8"> -->
										<!--<img src="/images/loader.gif" />-->
										<?php 
											$ath = \Auth::User();
										?>
										@if( $role_id == 4 )
											<coach1 :coach_school_add="coach_school_add" :userauth='{{json_encode($ath)}}' :sync_coach="sync_coach" :school="coach_school_id" :sport="coach_sport_id" :schoolllist='{{ json_encode($schools_send) }}' :genderid="coach_gender_id"  :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}" :sports="{{ json_encode(Voyager::Sport('all')) }}" :graduation='{{ json_encode($years) }}'><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></coach1>
										@else
											<user1 :userauth='{{json_encode($ath)}}' :schoolllist ="{{ json_encode($scholllist) }}"  :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}" :sports="{{ json_encode(Voyager::Sport('all')) }}" :sync_sport="sync_sport_user" :athlete_position_id="athlete_position_id" :athlete_sport_id="athlete_sport_id" :graduation='{{ json_encode($years) }}'><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></user1>
										@endif
									<!-- </div> -->
									<!-- <div class="col-md-4 text-right">
									@if( $role_id == 4 )

										<a  href="/profile/coach/{{$user_slug}}" target="_blank" id='' class="btn orange">View Profile<i class="fa fa-angle-right" aria-hidden="true"></i></a>

									@else

										<a  href="/profile/athlete/{{$user_slug}}" target="_blank" id='' class="btn orange">View Profile <i class="fa fa-angle-right" aria-hidden="true"></i></a>

									@endif
									@if(Auth::User())
										@if((Auth::user()->id == $user_id )&& (Auth::user()->role_id != 4))			
											<a href="/exportToPdf/{{$user_slug}}"  target="_blank" class="btn purple dfgd">Export to PDF<i aria-hidden="true" class="fa fa-angle-right"></i></a> 
										@endif
									@endif
								   
									</div> -->
								</div>
								</div>
							</div>
						<div class="col-lg-4 col-md-12 left-section">
							<div class="upload-photo">
							   <profileimage :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></profileimage>
							</div>
							<div class="left-box video-profile">
								<videoupload :usermeta="{{ json_encode( Voyager::videoupload( $user_id ) ) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></videoupload>
							</div>
							<div class="left-box gallery-profile">
								<gallery :usermeta="{{ json_encode( Voyager::gallery( $user_id ) ) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></gallery>
							</div>
							
						</div>
						<div class="col-lg-8 col-md-12 right-section ">
							<div class="row class-desktop">
								<div class="top-heading">
									<!-- <div class="col-md-8"> -->
										<!--<img src="/images/loader.gif" />-->
										<?php 
											$ath = \Auth::User();
										?>
										@if( $role_id == 4 )
											<coach1 :coach_school_add="coach_school_add" :userauth='{{json_encode($ath)}}' :sync_coach="sync_coach" :school="coach_school_id" :sport="coach_sport_id" :schoolllist='{{ json_encode($schools_send) }}' :genderid="coach_gender_id"  :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}" :sports="{{ json_encode(Voyager::Sport('all')) }}" :graduation='{{ json_encode($years) }}'><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></coach1>
										@else
											<user1 :userauth='{{json_encode($ath)}}' :schoolllist ="{{ json_encode($scholllist) }}"  :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}" :sports="{{ json_encode(Voyager::Sport('all')) }}" :sync_sport="sync_sport_user" :athlete_position_id="athlete_position_id" :athlete_sport_id="athlete_sport_id" :graduation='{{ json_encode($years) }}'><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></user1>
										@endif
									<!-- </div> -->
									<!-- <div class="col-md-4 text-right">
									@if( $role_id == 4 )

										<a  href="/profile/coach/{{$user_slug}}" target="_blank" id='' class="btn orange">View Profile<i class="fa fa-angle-right" aria-hidden="true"></i></a>

									@else

										<a  href="/profile/athlete/{{$user_slug}}" target="_blank" id='' class="btn orange">View Profile <i class="fa fa-angle-right" aria-hidden="true"></i></a>

									@endif
									@if(Auth::User())
										@if((Auth::user()->id == $user_id )&& (Auth::user()->role_id != 4))			
											<a href="/exportToPdf/{{$user_slug}}"  target="_blank" class="btn purple dfgd">Export to PDF<i aria-hidden="true" class="fa fa-angle-right"></i></a> 
										@endif
									@endif
								   
									</div> -->
								</div>
							</div>
							<div class="main-info">
								<!-- Centered Tabs -->
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#contact">Contact Info</a></li>
									@if( $role_id == 4 )
										<li><a data-toggle="tab" href="#school">School Info</a></li>
										<li><a data-toggle="tab" href="#event">Events Attending</a></li>
									@else
										<li><a data-toggle="tab" href="#sports">Sports Stats</a></li>
										<li><a data-toggle="tab" href="#achievements">Achievements</a></li>
										<li><a data-toggle="tab" href="#acadmics">Academics</a></li>
									@endif
								</ul>
								<div class="tab-content"><!-- Tab Conent -->
									@if( $role_id == 4 )
									<div id="contact" class="tab-pane content-coach fade in active">
									@else
									<div id="contact" class="tab-pane fade in active">
									@endif
								    	<div class="tab-inner">
								    	@if( $role_id == 4 )
								    		<coach2 :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}" :yearac='{{ json_encode($yearAwards) }}' ><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></coach2>
								    	@else
										<user2 :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></user2>
										@endif
									</div>
									@if( $role_id != 4 )
									<div class="tab-inner">
										 <!--var_dump(Voyager::parents( $user_id )) -->
									    <user3 :parentdata="{{ json_encode(Voyager::parents( $user_id )) }}" :parentdatabkp="{{ json_encode(Voyager::parents( $user_id )) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></user3>
									</div>
									<div class="tab-inner">
										<!--var_dump(Voyager::coaches( $user_id ))-->
										<user4 :coachesdatabkp="{{ json_encode( Voyager::coaches( $user_id ) ) }}" :coachesdata="{{ json_encode( Voyager::coaches( $user_id ) ) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></user4>
									</div>
									@endif
								</div>
								@if( $role_id == 4 )
								<?php 
								  $attendeesArrayFull = array();
								   $logged_user = Auth::User();
								   $user_id = $logged_user->id;
								  $attendeesArrayFull = \DB::table('api_fetched_data as u')
						          ->join('coachschool as ui', 'ui.school_id', '=', 'u.id')
						           ->where('ui.user_id',$user_id)
						           ->orderBy('ui.id','DESC')
						           ->get();
								  ?>
								<div id="school" class="tab-pane fade">
						    		<coach3 :divisionss="{{ json_encode($divisionss) }}" :allsports="{{ json_encode($allsports) }}" :schoollist='{{ $schoollistarrayjson }}' :sportsdata="{{ json_encode($attendeesArrayFull) }}" ><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></coach3>
						    	</div>
								<div id="event" class="tab-pane fade eventattend">
							    	<div class="tab-inner">
								    	<h3>Events Attending 
								    		<!-- <a class="btn orange pull-right" href="/event/add">Add Event</a> -->
								    		</h3>
								    	<div class="inner-content">
								    	@if(count($events) > 0 )
							    		@foreach($events as $event)	
								    		<div class="container">
												<h1><a target="_blank" href="/event/{{$event->event_slug}}">{{$event->title}}</a></h1>
												<p><strong>Dates:</strong> {{ date_format(date_create( $event->event_start) , 'F d Y')  . ' to ' . date_format(date_create( $event->event_end) , 'F d Y') }}</p>
													<?php  $address[0] = $event->place; $address[1] = $event->city; $address[2] = $event->state; $address[3] = $event->zipcode;
													 $address = array_filter($address);?>
												<p><strong>Address:</strong> {{ implode(', ',$address) }}</p>
													
											</div>
											<div class="clear"></div>
											<hr class='light'>
							    		@endforeach	
								    	@else
										    <div class="noinfo">
												<h3>You’re not currently attending any events. You can <a href="/events">join existing events</a> or <a href="/event/add">add your own here</a> </h3>	
									    	</div>
							    		@endif
						    			</div>
									</div>
								</div>
								@else
								
								<div id="sports" class="tab-pane fade">
						    		<sports :usermeta="{{ json_encode(Voyager::UserInformation('all', $user_id )) }}" :sportfields="{{ json_encode($sport_fields) }}" :sync_sport="sync_user_sport" :athlete_sport_id="athlete_sport_id" :athlete_position_id="athlete_position_id" :primary_position="{{ json_encode(Voyager::UserInformation('position', $user_id )) }}" :sportsdata="{{ json_encode(Voyager::UserInformation('sportsdata', $user_id )) }}" :allsportsnew="{{ json_encode($allsports) }}" :allsports="{{ json_encode(Voyager::Sport('all')) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></sports>
						    	</div>
						    	
						    	
								<div id="achievements" class="tab-pane fade">

								    <div class="tab-inner">
								    
								  <athletic_achievements :achivement_athlete="{{ json_encode($achivement_athlete) }}"  :yearac='{{ json_encode($yearachiv) }}'  :yearacn='{{ json_encode($yearac) }}' :sportsdata="{{ json_encode(Voyager::UserInformation('athletic_achievements', $user_id )) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></athletic_achievements>
								    </div>
								
									<div class="tab-inner">
										<academic_achiv  :yearac='{{ json_encode($yearachiv) }}'  :yearacn='{{ json_encode($yearac) }}' :sportsdata="{{ json_encode(Voyager::UserInformation('athelete_acadmic_achivment', $user_id )) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></academic_achiv>
									</div>

									<div class="tab-inner">
									   <extra_curricular :yearac='{{ json_encode($yearachiv) }}'  :sportsdata="{{ json_encode(Voyager::UserInformation('extra_curricular', $user_id )) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></extra_curricular>
									</div>
<!-- 
									<div class="tab-inner">
									   <awards :yearac='{{ json_encode($yearac) }}' :sportsdata="{{ json_encode(Voyager::UserInformation('awards', $user_id )) }}"><div class="overlay-center" style="text-align: center;"><img src="/images/loader.gif" style="width: 50px;"></div></awards>
									</div> -->
	
									<div class="tab-inner">
									    <testimonails :yearac='{{ json_encode($yearachiv) }}'  :sportsdata="{{ json_encode(Voyager::UserInformation('testimonails', $user_id )) }}"></testimonails>
									</div>
									
								  </div>
									<?php 
								  $attendeesArrayFull = array();
								   $logged_user = Auth::User();
								   $user_id = $logged_user->id;
								  $attendeesArrayFull = \DB::table('academicdetailathelete')->where('user_id',$user_id)->get();

								  ?>
								 <div id="acadmics" class="tab-pane fade">
								  		<academics :schoollist='{{ $schoollistarrayjson }}' :sportsdata="{{ json_encode($attendeesArrayFull) }}"  :sportsdatabkd="{{ json_encode($attendeesArrayFull) }}"></academics>
								  </div>
								 
								 
								  
								  @endif
								  
								</div><!-- Tab Conent -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!--<section class="white-bg">-->
		<!--	<div class="container">-->
		<!--		<div class="info-forms">-->
		<!--			<div class="enter-dashboard">-->
		<!--				<p>Don’t want to set this up now? <a href="#">Click to enter your dashboard</a></p>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</section>-->
	</div>
@endsection

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script>
jQuery(document).ready(function() {


	function checkWidth() {
		var windowWidth = $(window).width();
// alert(windowWidth);
		console.log("sdsadbhgfjhdgfhjgdshfgds"+windowWidth);
	if (windowWidth <= 976) {
	    $(".class-mobile").show()
	    $(".class-desktop").hide()
	}
	if (windowWidth > 976){
	    $(".class-mobile").hide()
	    $(".class-desktop").show()
	}
	 }
	 checkWidth() ;

	$(window).resize(function() {
	checkWidth() ;
	});
});

window.onload = function() {
	move1();
};
function move1() {
	var progressdata = document.getElementById("progressdata").value; 
	var elem = document.getElementById("myBar");   
	console.log(progressdata);
	elem.style.width = progressdata + '%'; 
}
function move(val) {
	if(val == 100){
		$('.completeprofile').hide();
	}else{
		$('.completeprofile').show();
	}
	var progressdata = document.getElementById("progressdata").value; 
	if(val != progressdata){
  	   	jQuery('#progressdata').val(val);
		if(val > progressdata){
			$('.progressval').text(val);
			var elem = document.getElementById("myBar");   
			console.log(progressdata);
			var width = progressdata;
			var id = setInterval(frame, 50);
			function frame() {
			if (width >= val) {
			  	clearInterval(id);
			} else {
			  	width++; 
			  	elem.style.width = width + '%'; 
			}
		}
  	}else{
		$('.progressval').text(val);
			var elem = document.getElementById("myBar");
			console.log(progressdata);
			var width = progressdata;
			var id = setInterval(frame, 50);
			function frame() {
				if (width <= val) {
					clearInterval(id);
				} else {
					width--; 
					elem.style.width = width + '%'; 
				}
			}
	  	}
  	}
}
</script>