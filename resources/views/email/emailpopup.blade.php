@extends('layouts.main')

@section('content')

<div class="log-in-popup">
    <div class="sign-up-content">
        <a href="#" class="close">
            <i class="icono-cross"></i>
        </a>
        <div class="panel-heading">SEND EMAIL</div> 
        <div class="panel-body">
            <div class="container">
                <form role="form" id='email_form' class="form-horizontal">
                    <div class="form-group">
                            <label for="email" class="control-label">TO</label> 
                            <input id="to_email" type="email" name="to_email" class="form-control">
                            <span class="help-block">
                                <strong>
                                </strong>
                            </span>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">SUBJECT LINE</label> 
                        <input id="subject" type="text" name="subject" class="form-control"> 
                        <span class="help-block">
                            <strong>
                            
                            </strong>
                        </span>
                    </div> 
                    <div class="form-group">
                        <label for="" class="control-label">MESSAGE</label> 
                        <textarea id="messge" name="message" class="form-control"> </textarea>
                        <span class="help-block">
                            <strong>
                            
                            </strong>
                        </span>
                    </div> 
                    <div class="form-group">
                        <label for="" class="control-label">SELECT TEMPLATE</label> 
                        <select name='template' id='template' class='form-control'>
                            <option value=''>Select Template</option>
                            <option value=''>Select Template</option>
                            <option value=''>Select Template</option>
                            <option value=''>Select Template</option>
                        </select>
                        <span class="help-block">
                            <strong>
                            
                            </strong>
                        </span>
                    </div> 
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary orange">SEND MAIL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop