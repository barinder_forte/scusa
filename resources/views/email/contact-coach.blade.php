<section>
	<div class="wrapper" style="max-width: 600px; margin: 0 auto;font-family: arial; font-size: 14px;">
		<div class="header" style="text-align: center;border-bottom: 1px solid #d0d0d0; padding: 20px 0;">
	    	<!-- <a href="{{ env('APP_URL') }}"><img src="{{ asset( 'storage/' . Voyager::setting('logo') ) }}"></a> -->
	    </div>
		<div class="main-body" style="background: #f1f1f1; padding: 20px; border: 1px solid #e4e4e4;">
			{!! $content !!}
		</div>
		<div class="footer" style="text-align: center;background: #f65954;color: #fff; padding: 13px 0;">
			<p style="margin: 0;">&copy; {{ date('Y') }} <a href="{{ env('APP_URL') }}">{{ env('APP_NAME') }}</a></p>
		</div>
	</div>
</section>