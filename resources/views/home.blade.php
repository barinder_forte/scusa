@extends('layouts.main')

@section('content')
    <div class="main-banner" style="background-image: url( {{ asset( 'storage/' . Voyager::pagemeta('homepage_banner', '1') ) }} );">
        <div class="container">
            {!! Voyager::pagemeta('top_banner', '1') !!}
            @if(Auth::guest())
                <h3 class="sign-up-text">sign up now</h3>
                <a href="#" class="btn orange" @click.prevent="signup_url(2, 'hide')">I'm a student-athlete</a>
                <a href="#" class="btn black" @click.prevent="signup_url(4, 'hide')">I'm a coach / club director</a>
            @endif
        </div>
    </div>
    
    <div class="main-container">        
        
        <section class="main-slider">
            
            @foreach($sports as $sport)
                @if($sport->image != 'image/sport.png')
                     <div class="slides"> 
                        @if($sport->sport_url != 'null' && $sport->sport_url != '')
                            <a href="{{$sport->sport_url}}">
                        @endif
                        <div class='slide-links'>
                            <img src="{{($sport->image != 'image/sport.png')?asset('storage/'.$sport->image) : 'images/sport.png' }}" />
                            <h2>{{$sport->title}}</h2>
                        </div >
                        @if($sport->sport_url != 'null' && $sport->sport_url != '')
                            </a>
                        @endif
                    </div>
                @endif

            @endforeach
            
        </section>
        @if(Voyager::pagemeta('section_2', '1'))
        <section class="online-recruting" style="background-image: url( {{ asset('storage/'. Voyager::pagemeta('section_2_banner', '1') ) }} )">
            <div class="container">
                {!! Voyager::pagemeta('section_2', '1') !!}
                @if (Auth::guest())
                    <a href="#" class="btn orange" @click.prevent="signup_open">Sign up now</a>
                @endif
            </div>
        </section>
        @endif
        <section class="sport-contact">
            <h2>{!!  Voyager::pagemeta('section_3_title', '1') !!}</h2>
            <div class="container">
                <div class="col-md-6">
                    {!!  Voyager::pagemeta('section_3', '1') !!}
                    @if (Auth::guest())
                        <a href="#" class="btn orange" @click.prevent="signup_open">Sign up now</a>
                    @endif
                </div>
                <div class="col-md-6 right-img">
                    <img class="img-responsive" src="{{ asset('storage/'. Voyager::pagemeta('section_3_right_image', '1') ) }}">
                </div>
            </div>
        </section>

        <section class="not-sure">
            <div class="container">
                <div class="col-md-6 pull-right">
                    {!!  Voyager::pagemeta('section_4', '1') !!}
                     @if (Auth::guest())
                    <a href="#" class="btn orange font-second" @click.prevent="signup_open">Sign up now</a>
                      @endif
                </div>
                <div class="col-md-6">
                    <div class=""><img class="img-responsive" src="{{ asset('storage/'. Voyager::pagemeta('section_4_left_image', '1') ) }}"></div>
                </div>
            </div>
        </section>

        <section class="event-section">
            <div class="container">
                <div class="col-md-6">
                    {!! Voyager::pagemeta('section_5', '1') !!}
                     @if (Auth::guest())
                    <a href="#" class="btn orange font-third" @click.prevent="signup_open">Sign up now</a>
                      @endif
                </div>
                <div class="col-md-6 right-img">
                    <img class="img-responsive" src="{{ asset('storage/'. Voyager::pagemeta('section_5_right_image', '1') ) }}">
                </div>
            </div>
        </section>

        <section class="testimonial-seciton">
            <h1>{{ Voyager::pagemeta('testimonials_heading', '1') }}</h1>
            <div class="testimoniol-content">
                @if( $testimonials ) @foreach( $testimonials as $testimonial )
                    <div class="content">
                       <!--  <img src="@if( $testimonial->image ){{ asset( 'storage/'.Voyager::setting('logo') ) }}@else {{env('APP_URL').'/storage/LMPpFvQ9dhs1YR2bAfTPJ4Mhx2zqtLBqjAiNejX6.jpeg'}} @endif" @if( isset($testimonial->image2x) ) data-rjs="{{ asset( 'storage/'.$testimonial->image2x ) }}" @endif > -->
                        <a href="/testimonialshow/{{$testimonial->id}}"><h2>{{ $testimonial->title }}</h2></a>
                        <p>{{ substr(strip_tags($testimonial->body), 0, 200) . ' ...'}}</p>
                        <a href="/testimonialshow/{{$testimonial->id}}" class="btn orange">Read full review</a>
                    </div>
                @endforeach @endif
            </div>
        </section>

        <section class="founder-section">
            <div class="container">
                <div class="col-md-6">
                    {!! Voyager::pagemeta('section_6_left_description', '1') !!}
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="{{ asset('storage/'. Voyager::pagemeta('section_6_right_image', '1') ) }}">
                </div>
            </div>
        </section>

        <div class="back-top <?php if(! \Auth::User()){ echo 'back-top-not-loged-in not-log-in'; } ?>">
            <div class="container">
                <a href="#" class="back-top">Back to top</a>
            </div>
        </div>
        @if (Auth::guest())
        <section class="sign-up-section">
            <h1>Sign up now</h1>
            <a href="#" class="btn orange" @click.prevent="signup_url(2, 'hide')">I'm a student-athlete</a>
            <a href="#" class="btn black" @click.prevent="signup_url(4, 'hide')">I'm a coach / club director</a>
        </section>
        @endif
        @if( count($posts) > 0 )
            <h1><center>View Our Latest Sport Recruiting News</center></h1>
            <section class="main-bottom-slider">
                @foreach($posts as $post)
                <div class="slides">
                   <a class="" href="/blog/{{$post->slug}}"> 
                       <!--  @if($post->image)
                            <img src="{{asset('storage/'.$post->image)}}">
                        @else
                            <img src='https://via.placeholder.com/357x521'>
                        @endif -->
                        <div class="content">
                            <!--<p><strong>{{$post->title}}</strong></p>-->
                            <h2>{{$post->title}}</h2>
                            <!--<P class="cat">{{isset($post->getcat)?$post->getcat->name :''}}</P>-->
                            <p>{{substr(strip_tags($post->body), 0, 75) . ' ...'}} </p>
                            <a class="event-link" href="/blog/{{$post->slug}}">Click here</a>
                        </div>
                    </a>
                </div>
                @endforeach
            </section>
        @endif
    </div>
    @if (session('upgrade_notice'))
    <?php $plans = \App\Plan::get(); ?>
    <div class="upgrade-popup">
        <div class="inner-popup">
            <h1>It's Time to Upgrade</h1>
            @if(  count($plans) > 0 )
            @foreach( $plans as $key=>$plan )
            @if(  $key == 0 )
                <h3>For only ${{ number_format($plan->cost, 0) }}/Month</h3>
            @endif
            @endforeach
            @else
                <h3>For only $7/Month</h3>
            @endif
            <p>This feature is not available for your current plan. Please upgrade to access this feature.</p>
            <?php 
            $plan = App\Plan::find(1);     ?>
            <a href="{{ url('/plan/'.$plan->slug) }}" class="btn orange">Upgrade Now</a>
        </div>
    </div>
    @endif
@endsection
