@extends('layouts.signup')
@section('content')
<div id="steps" class="info-forms">
    <section class="white-bg">
        <div class="container">
            <div class="info-forms">
                <div class="sign-up-form">
                    <div class="row">
                      <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-default credit-card-box">
                            <div class="panel-heading display-table" >
                                <div class="row display-tr" >
                                    <h3 class="panel-title display-td" >Payment Details Form</h3>
                                    <div class="display-td" >                            
                                        <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                    </div>
                                </div>                    
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                  {!! Form::open(['url' => route('order-post'), 'data-parsley-validate', 'id' => 'payment-form']) !!}
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block">
                                      <button type="button" class="close" data-dismiss="alert">×</button> 
                                            <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    <div class="form-group" id="product-group">
                                        {!! Form::label('plane', 'Select Plan:') !!}
                                        {!! Form::select('plane', ['google' => 'Google ($10)', 'game' => 'Game ($20)', 'movie' => 'Movie ($15)'], 'Book', [
                                            'class'                       => 'form-control',
                                            'required'                    => 'required',
                                            'data-parsley-class-handler'  => '#product-group'
                                            ]) !!}
                                    </div>
                                    <div class="form-group" id="cc-group">
                                        {!! Form::label(null, 'Credit card number:') !!}
                                        {!! Form::text(null, null, [
                                            'class'                         => 'form-control',
                                            'required'                      => 'required',
                                            'data-stripe'                   => 'number',
                                            'data-parsley-type'             => 'number',
                                            'maxlength'                     => '16',
                                            'data-parsley-trigger'          => 'change focusout',
                                            'data-parsley-class-handler'    => '#cc-group'
                                            ]) !!}
                                    </div>
                                    <div class="form-group" id="ccv-group">
                                        {!! Form::label(null, 'CVC (3 or 4 digit number):') !!}
                                        {!! Form::text(null, null, [
                                            'class'                         => 'form-control',
                                            'required'                      => 'required',
                                            'data-stripe'                   => 'cvc',
                                            'data-parsley-type'             => 'number',
                                            'data-parsley-trigger'          => 'change focusout',
                                            'maxlength'                     => '4',
                                            'data-parsley-class-handler'    => '#ccv-group'
                                            ]) !!}
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group" id="exp-m-group">
                                            {!! Form::label(null, 'Ex. Month') !!}
                                            {!! Form::selectMonth(null, null, [
                                                'class'                 => 'form-control',
                                                'required'              => 'required',
                                                'data-stripe'           => 'exp-month'
                                            ], '%m') !!}
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="exp-y-group">
                                            {!! Form::label(null, 'Ex. Year') !!}
                                            {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, [
                                                'class'             => 'form-control',
                                                'required'          => 'required',
                                                'data-stripe'       => 'exp-year'
                                                ]) !!}
                                        </div>
                                      </div>
                                    </div>
                                      <div class="form-group">
                                          {!! Form::submit('Place order!', ['class' => 'btn btn-lg btn-block btn-primary btn-order', 'id' => 'submitBtn', 'style' => 'margin-bottom: 10px;']) !!}
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                            <span class="payment-errors" style="color: red;margin-top:10px;"></span>
                                        </div>
                                      </div>
                                  {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
@endsection
