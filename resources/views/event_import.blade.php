@extends('layouts.main')

@section('content')

<div class="main-container main-inner step-forms">
   		<div class="importevent">
   		<a download href="{{ asset('storage/'.Voyager::setting('demo_event_csv') )}}" class="downloadbutton">Download Demo CSV</a>
   		</div>
        <addevent></addevent>
  
</div>
	
@stop


