@extends('layouts.signup')
@section('content')
<link href="{{ asset('css/simplelightbox.min.css') }}" rel='stylesheet' type='text/css'>
<link href="{{ asset('css/demo.css') }}" rel='stylesheet' type='text/css'>
<div class="main-container main-inner step-forms coach_profile">
	<!-- @if($userData["id"] == \Auth::User()->id )
		<section class="grey-bg heading-top">
			<div class="container">
				<div class="info-forms">
					<h1>{{Voyager::pagemeta('page_heading', '13')}}</h1>
					<h2>{{Voyager::pagemeta('page_subheading', '13')}}</h2>
				</div>
			</div>
		</section>
	@endif -->
	
		<section class="grey-bg">
				<div class="container">
					<div class="info-forms">
						<div class="col-lg-4 col-md-12 left-section">
							<div class="upload-photo">
								  <img class='img-responsive' src="<?php if($userData->avatar == 'users/default.png' || $userData->avatar == ''){ echo env('APP_URL').'/storage/EFuG6j0fKnCeYPkABRMIvbzHdUi2rLC4xB24f4lB.png'; } else{ $userimage = explode('/upload/',$userData->avatar)[1];
            						  echo "https://res.cloudinary.com/webforte/image/upload/h_400/".$userimage;
								  } ?>" >

							</div>
							<div class="left-box">
								<h1>video highlights </h1>
								<div class="uploaded-images video-profile">
									<?php  $galleryVideos = json_decode(isset($userInfoArray['videogallery'])? ($userInfoArray['videogallery']):'');  ?>
							        @if($galleryVideos)
							        <?php $vide = 0; ?>
									@foreach($galleryVideos as $video)
									<div id="popupvideo{{$vide}}" class="popupbutton"><a class="click enchor-video"></a>
									<div class="col-md-12 padding-10 ">
                                    	<iframe src='{{$video->url}}' width="100%" height="200"></iframe>
									</div>
									</div>
									<?php $vide++; ?>
									@endforeach
									@else
									<div class="noinfo">
										<h3>No Information Available</h3>	
									</div>
									@endif
								</div>
							</div>
							<div class="left-box gallery-profile">
								<h1>photos </h1>
								<div class="uploaded-images">
									<ul class='gallery list-group'>
									<?php  $galleryImages = json_decode(isset($userInfoArray['gallery'])?($userInfoArray['gallery']):'');?>
							        @if($galleryImages)
									@foreach($galleryImages as $image)
									<li class="list-group-items">
									<a href='https://res.cloudinary.com/webforte/image/upload/{{$image->public}}.png'><img class="img-responsive" src='https://res.cloudinary.com/webforte/image/upload/{{$image->public}}.jpg'></a>
									</li>
									@endforeach
									@else
									<div class="noinfo">
										<h3>No Information Available</h3>	
									</div>
									@endif
									</ul>
								</div>
							</div>
							
						</div>
						<div class="col-lg-8 col-md-12 right-section">
							<div class="row">
								<div class="top-heading">
									<div class="col-md-8">
										<h1>{{(isset($userInfoArray['coach_firstname'])?($userInfoArray['coach_firstname']):'') ." ". (isset($userInfoArray['coach_lastname'])?($userInfoArray['coach_lastname']):'')}}</h1>
										<?php if(isset($userInfoArray['school'])){
								    			 $scholld = \App\ApiData::where('id',$userInfoArray['school'])->first();
								    			 } ?>
										<h2>{{isset($scholld)?($scholld->school_name):''}}</h2>
									</div>
									<div class="col-md-4 text-right">
									@if(Auth::User())
									@if(Auth::user()->id != $userData["id"])	
										<?php 
                                    $userIdsArray = array();
                                    $userIdsArray1[0]['userid'] = str_replace("'", '',(isset($userInfoArray['coach_email'])?($userInfoArray['coach_email']):'')) ;
                                    $userIdsArray1[0]['username'] = str_replace("'", '',(isset($userInfoArray['coach_firstname'])?($userInfoArray['coach_firstname']):'') ." ". (isset($userInfoArray['coach_lastname'])?($userInfoArray['coach_lastname']):'')) ;
                                    $userIdsArray = json_encode($userIdsArray1);
                                    ?>
                                    <a @click.prevent='showemail( <?php echo $userIdsArray; ?>)'  class="btn orange contact_me">
                                    Contact Me</a>

                                    <!--     <a @click.prevent="showemail_profile" href="#"
id='show_login' name='{{$userData["id"]}}' class="btn orange contact_me">Contact Me <i class="fa fa-
angle-right" aria-hidden="true"></i></a> -->                                     @endif
@if(Auth::user()->id == $userData["id"])                                                     <a
href="/exportToPdf/{{$userData['user_slug']}}" class="btn purple dfgd">Export to PDF<i aria-
hidden="true" class="fa fa-angle-right"></i></a>                                      @endif
@if(Auth::user()->id == $userData["id"])                                                     <a
href="/profile/"  class="btn orange backtoprofile">Back to Profile<i aria-hidden="true" class="fa
fa-angle-right"></i></a>                                      @endif
@endif                                     </div>                                     <div class
="col-md-12">                                         <ul class="top-listings">
@if(isset($userInfoArray['coach_title']))                                             <li>
<p> Title:
<span>{{isset($userInfoArray['coach_title'])?($userInfoArray['coach_title']):''}}</span></p>
</li>                                             @endif
@if(isset($userInfoArray['sport']))                                             <li>
<?php if(isset($userInfoArray['sport'])){ $sportname =
\App\Sport::where('id',$userInfoArray['sport'])->pluck('title')->first(); } ?>
<p>Sport: <span>{{isset($sportname)?($sportname):''}}</span></p>
</li>                                             @endif
@if(isset($userInfoArray['coach_gender']))                                             <li>
<p>Gender Coached: <span>@if($userInfoArray['coach_gender'] == 'Men') {{'Male'}}
@elseif($userInfoArray['coach_gender'] == 'Women') {{'Female'}} @else Male/Female @endif</span></p>
</li>                                             @endif
</ul>                                     </div>                                 </div>
</div>                             <div class="main-info">                                 <!--
Centered Tabs -->                                 <ul class="nav nav-tabs">
<li class="active"><a data-toggle="tab" href="#contact">Contact Info</a></li>
<li><a data-toggle="tab" href="#school">School Info</a></li><li><a data-toggle="tab"
href="#event">Events Attending</a></li>                                 </ul>
<div class="tab-content"><!-- Tab Conent -->                                     <div id="contact"
class="tab-pane fade in active">                                         <div class="tab-inner">
<h3>Coach information</h3>                                             <div class="inner-content">
@if(Auth::user()->id == $userData['id'])                                             <h4>Contact
Information (ONLY VISIBLE BY YOU)</h4>
@if(isset($userInfoArray['coach_email'])) <?php $a = 0 ; ?>
<p><span><a href="mailto:{{$userInfoArray['coach_email']}}"><img src="/images/icon/Send.png" /></a>
{{isset($userInfoArray['coach_email'])?($userInfoArray['coach_email']):''}}</span></p>
@endif                                             @if(isset($userInfoArray['phone']))<?php $a = 0 ;
?>                                             <p><span><a href='tel: <?php $daat =
str_replace(array('(',')','-','_',' '),'',$userInfoArray['phone']);
echo   $formatted = "(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);

											    			?>'><i class="fa fa-mobile" aria-hidden="true" ></i> </a>
								    	    <?php 

											    			if(isset($userInfoArray['phone'])){


											    			 $daat =  str_replace(array('(',')','-','_',' '),'',$userInfoArray['phone']);
											    			 if(isset($userInfoArray['c_code']) && $userInfoArray['c_code']!= ''){
											    			 	$code = '+'.$userInfoArray['c_code'];

											    			 }else{
											    			 	$code = '';

											    			 }


											   echo $formatted =$code.' (
                                   '.substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);
											    			}



											    			



											    		?></span></p>	
								    	    @endif
								    		@if(isset($userInfoArray['coach_www']))<?php $a = 0 ; ?>
								    		<p><span ><a target="_blank" href="http://{{($userInfoArray['coach_www'])}}"><i class="fa fa-globe"  aria-hidden="true"></i> {{isset($userInfoArray['coach_www'])?($userInfoArray['coach_www']):''}}</a></span>
								    		</p>	
								    		@endif
								    		<!--<p><label>State:</label> <span>{{isset($userInfoArray['state'])?($userInfoArray['state']):''}}</span></p>-->
								    		<!--<p><label>Zip:</label> <span>{{isset($userInfoArray['zip'])?($userInfoArray['zip']):''}}</span></p>-->
								    		<p>&nbsp;</p>
								    		@endif
								    		@if(isset($userInfoArray['bio']))
								    		<?php  $bio = strip_tags($userInfoArray['bio']); ?>
								    		<h4 class="coach_bio">Coach Bio</h4>
              								@if($bio != '')
								    		<?php $a = 0 ; ?>
								    		<span class="lc">{!! $userInfoArray['bio'] !!}</span>
								    		@else
								    		<p>Not Filled in</p>
								    		@endif
								    		@endif
								    		<form>
							    				<?php $ncaas =json_decode( isset($userInfoArray['coach_ncaa'])?($userInfoArray['coach_ncaa']):'' );?>
							    				@if(count($ncaas) > 0 && isset($ncaas[0]->name) )<?php $a = 0 ; ?>
									    			<label>National Championship</label>
									    			<div class="vue-input-tag-wrapper read-only">
									    				@foreach($ncaas as $ncaa)
									    					<span class="input-tag"><span>{{$ncaa->name}}</span> </span>
									    				@endforeach	
									    			</div>
							    				@endif
							    				<?php $ncaaffs =json_decode( isset($userInfoArray['coach_ncaa_f_f'])?($userInfoArray['coach_ncaa_f_f']):'' );?>
							    				@if(count($ncaaffs) > 0 && isset($ncaaffs[0]->name) )
							    				<?php $a = 0 ; ?>
												    <label>Final Four</label>
									    			<div class="vue-input-tag-wrapper read-only">
									    				@foreach($ncaaffs as $ncaaff)
									    					<span class="input-tag"><span>{{$ncaaff->name}}</span> <!----></span>
									    				@endforeach	
							    					</div>
							    				@endif
							    				<?php $ncaants =json_decode( isset($userInfoArray['coach_ncaa_n_t'])?($userInfoArray['coach_ncaa_n_t']):'' );?>
							    				@if(count($ncaants) > 0 && isset($ncaants[0]->name) )
							    				<?php $a = 0 ; ?>
												    <label>National Tournament</label>
									    			<div class="vue-input-tag-wrapper read-only">
									    				@foreach($ncaants as $ncaant)
									    					<span class="input-tag"><span>{{$ncaant->name}}</span> <!----></span>
									    				@endforeach	
							    					</div>
							    				@endif
							    				<?php $ncaapss =json_decode( isset($userInfoArray['coach_ncaa_p_s'])?($userInfoArray['coach_ncaa_p_s']):'' );?>
							    				@if(count($ncaapss) > 0 && isset($ncaapss[0]->name) )
							    				<?php $a = 0 ; ?>	
												    <label>Post Season Appearance</label>
									    			<div class="vue-input-tag-wrapper read-only">
									    				@foreach($ncaapss as $ncaaps)
									    					<span class="input-tag"><span>{{$ncaaps->name}}</span> <!----></span>
									    				@endforeach	
									    			</div>
								    			@endif
								    				<?php $ncaaccs =json_decode( isset($userInfoArray['coach_ncaa_c_c'])?($userInfoArray['coach_ncaa_c_c']):'' );?>
							    				@if(count($ncaaccs) > 0 && isset($ncaaccs[0]->name) )
							    				<?php $a = 0 ; ?>
													    <label>Confrence Championship</label>
										    			<div class="vue-input-tag-wrapper read-only">
										    				@foreach($ncaaccs as $ncaacc)
										    					<span class="input-tag"><span>{{$ncaacc->name}}</span> <!----></span>
										    				@endforeach	
										    			</div>
									    		@endif
										    	@if(isset($userInfoArray['players']))	
										    	<?php  $players = strip_tags($userInfoArray['players']); ?>
									    		<label>Players Drafted Professionally</label>
	              								@if($players != '')
									    		<?php $a = 0 ; ?>
									    		<span class="lc">{!! ($userInfoArray['players']) !!}</span>
									    		@else
									    		<p>Not Filled in</p>
									    		@endif
									    		@endif

								    		</form>
								    		
								    		@if(!isset($a))
								    			<div class="noinfo">
													<h3>No Information Available</h3>	
									    		</div>
								    		@endif
									    	</div>
										</div>
								  	</div>
								  	<div id="school" class="tab-pane fade">
								    	<div class="tab-inner">
									    	<h3>School Information</h3>
									  		<div class="inner-content">
									  		<form>
									  		<?php $getSchool = 0 ; ?>
								    		@if( count($schoolData) > 0 )
								    		@foreach($schoolData as $coaching)
								    			<?php
								    			if(isset($coaching->school_id)){
								    			 $scholllist = \App\ApiData::where('id',$coaching->school_id)->first();
								    			 } 
								    			if(isset($coaching->sport_id) && (isset($coaching->sport_id)?$coaching->sport_id:'') !== 'null' ){
								    			 $sportData = \App\Sport::where('id',$coaching->sport_id)->first();
								    			} 
								    			if(isset($coaching->division)?$coaching->division:'' != "" && isset($coaching->division)?$coaching->division:'' != "null"){
									    			$divisionss = \Voyager::pagemetadetail('Division', '16');
										    		$divisionss = json_decode($divisionss);
												   	$divisionss = $divisionss->options;
												   	foreach($divisionss as $key => $val){
												   		if($coaching->division == $val){
												   			$divisionOri = $key;
												   		}
												   	}
											    }
								    			 ?>
											   	@if(isset($scholllist->school_name))
											   	<?php $getSchool = 1 ; ?>
											   	@endif
								    			<h4>{{isset($scholllist->school_name)?$scholllist->school_name:''}}</h4>
								    			<div class="school-info">
										  		
										  			@if(isset($coaching->gender) && ((isset($coaching->gender)?$coaching->gender:'') != 'null' ) )
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Gender Coached:</label>
										  				</div>
										  				<div class="col-md-4">
										  					@if($coaching->gender == 'Men') {{'Male'}} @elseif($coaching->gender == 'Women') {{'Female'}} @else {{'Male/Female'}} @endif
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->year)?$coaching->year:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Year:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->year)?$coaching->year:''}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->school_tuition)?$coaching->school_tuition:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>School Tuition:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->school_tuition)?'$'.($coaching->school_tuition):''}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->tuition_room)?$coaching->tuition_room:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  				 	<label>Room/Board:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->tuition_room)?'$'.($coaching->tuition_room):''}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->school_size)?$coaching->school_size:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>School Size:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->school_size)?$coaching->school_size:''}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->school_url)?$coaching->school_url:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Website Name:</label>
										  				</div>
										  				<div class="col-md-4">
										  			    <a target="_blank" href="http://{{isset($coaching->school_url)?$coaching->school_url:'#'}}">{{isset($coaching->school_url)?$coaching->school_url:''}}</a>
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->address1)?$coaching->address1:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Address:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->address1)?$coaching->address1:''}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->address2)?$coaching->address2:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Address 2:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->address2)?$coaching->address2:''}}
										  				</div>
										  			</div>
										  			@endif
										  		
										  			@if(isset($coaching->city)?$coaching->city:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>City:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->city)?$coaching->city:''}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->state)?$coaching->state:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>State:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->state)?$coaching->state:''}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->zip)?$coaching->zip:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Zip Code:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{isset($coaching->zip)?$coaching->zip:''}}
										  				</div>
										  			</div>
										  			@endif
										  				@if(isset($coaching->phone)?$coaching->phone:'' != "")



										  				<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Phone:</label>
										  				</div>
										  				<div class="col-md-4">
<?php 

											    			if(isset($coaching->phone)){


											    			 $daat =  str_replace(array('(',')','-','_',' '),'',$coaching->phone);
											    			   echo   $formatted = "(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);

											    			}



											    			



											    		?>										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->fax)?$coaching->fax:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Fax:</label>
										  				</div>
										  				<div class="col-md-4">
										  					<?php 
											    			if(isset($coaching->fax)){
											    			 $daat =  str_replace(array('(',')','-','_',' '),'',$coaching->fax);
											    			   echo   $formatted = "(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);

											    			}
											    		?>
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($divisionOri))
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Division:</label>
										  				</div>
										  				<div class="col-md-4">
										  				{{$divisionOri}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($sportData->title)?$sportData->title:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Sport :</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{$sportData->title}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->conference)?$coaching->conference:'' != "")
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Conference:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{$coaching->conference}}
										  				</div>
										  			</div>
										  			@endif
										  			@if(isset($coaching->head_coach) && (isset($coaching->head_coach)?$coaching->head_coach:'') != 'null' )
										  			<?php $getSchool = 1 ; ?>
										  			<div class="row">
										  				<div class="col-md-4">
										  					<label>Position:</label>
										  				</div>
										  				<div class="col-md-4">
										  					{{($coaching->head_coach == 'True' )? 'Head Coach' : 'Assistant Coach'}}
										  				</div>
										  			</div>
										  			@endif
									  			</div>
									  		<!-- <div class="clear"></div>
											<hr class='light'> -->
									  		@endforeach	
								    		@endif
								    		<?php // print_r($getSchool); die; ?>
								    		@if( $getSchool == 0 )
								    		 	<div class="noinfo">
													<h3>No Information Available</h3>	
									    		</div> 
									    	@endif
								    		</form>
								  			</div>
										</div>
							 		</div>
							 		<div id="event" class="tab-pane fade eventattend">
								    	<div class="tab-inner">
									    	<h3>Events Attending</h3>
									    	<div class="inner-content">
									    	@if(count($events) > 0 )
								    		@foreach($events as $event)	
									    		<div class="container">
													<h1><a target="_blank" href="/event/{{$event->event_slug}}">{{$event->title}}</a></h1>
													<p><strong>Dates:</strong> {{ date_format(date_create( $event->event_start) , 'F d Y')  . ' to ' . date_format(date_create( $event->event_end) , 'F d Y') }}</p>
													<?php  $address[0] = $event->place; $address[1] = $event->city; $address[2] = $event->state; $address[3] = $event->zipcode;
													 $address = array_filter($address);?>
													<p><strong>Address:</strong> {{ implode(', ',$address) }}</p>
													
												</div>
												<div class="clear"></div>
												<hr class='light'>
								    		@endforeach	
									    	@else
											    <div class="noinfo">
													<h3>No Information Available</h3>	
										    	</div>
								    		@endif
									    	</div>
										</div>
								  	</div>
								</div>
							</div>
						</div>
					</section>
<div class="modal fade videowatch in" id="myModal" role="dialog" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close cloSEPOP" data-dismiss="modal">×</button>
				</div>
				<div class="modal-body">
				<div class="uploaded-images">
									<?php  $galleryVideos = json_decode(isset($userInfoArray['videogallery'])? ($userInfoArray['videogallery']):'');  ?>
							        @if($galleryVideos)
							        <?php $vide3 = 0; ?>
									@foreach($galleryVideos as $video)

									<?php if(isset($video->url)){ ?>
										<p style="display: none" id="videohide" class="popupvideo{{$vide3}} videohide"><iframe src="{{$video->url}}" frameborder="0" width="100%" height="500"></iframe></p>
									<?php } ?>
									<!-- <a href="#" id="video{{$vide}}" class="popupbutton">click</a>
									<div class="col-md-12 padding-10 ">
                                    	<iframe src='{{$video->url}}' width="100%" height="200"></iframe>
									</div> -->


									<?php $vide3++; ?>
									@endforeach
									@else
									<div class="noinfo">
										<h3>No Information Available</h3>	
									</div>
									@endif
								</div>
				
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{  asset('js/simple-lightbox.js') }}"></script>
<script>
	jQuery(document).ready(function($){
		 $gallery = $('.gallery .list-group-items a').simpleLightbox();
	});

</script>
<script type="text/javascript">
		jQuery( document ).ready(function($) {
			$( ".popupbutton" ).on('click',function(e) {
					e.preventDefault();

					console.log($(this).attr('id'));

					show = $(this).attr('id');

					$('.'+show).show();

					$('.videowatch').show();

					$('body').addClass("bodyadd");

				});
			$( ".cloSEPOP" ).on('click',function() {

					console.log(show);

					$('.videohide').hide();
					$('.videowatch').hide();
					$('uploaded-images iframe').hide();

					$('.'+show+' iframe').attr('src', $('.'+show+' iframe').attr('src'));

					$('body').removeClass("bodyadd");

				});

			jQuery('#et_mobile_nav_menu .mobile_nav').meanmenu({
				meanMenuContainer: '#main-header',
				meanScreenWidth: "980"
			});

		});

	</script>
 
@stop

