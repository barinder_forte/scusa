<?php 
use App\Sport;
$sports = Sport::where('status',"=",'PUBLISHED')->orderBy('title','ASC')->get()->toArray();
use App\ApiData;
$scholllist = ApiData::select('id','school_name')->orderBy('school_name','ASC')->get();
// $schoollistarray = array();
    
// foreach($scholllist as $key=>$s){
    
//  $schoollistarray[$key]['label'] = $s->school_name;
//  $schoollistarray[$key]['value'] = $s->id;
// }
// print_r($scholllist);die;
$schoollistarrayjson = json_encode($scholllist);

?>

<div v-cloak v-show="signup_popup" class="sign-up-popup">
    <div v-show="before_signup" class="sign-up-content">
        <a @click.prevent="signup_close" href="#" class="close"><i class="icono-cross"></i></a>
        <h1>Create an account</h1>
        <div class="radio-section" v-show="profile_switch_tab">
            <a @click="signup_url(2)"  href="#" :class="athelete_class"><input type="radio" checked name="role" value="2" v-model="this.role">i&#39;m an athlete</a>
            <a @click="signup_url(4)" href="#" :class="coach_class"><input type="radio" name="role" value="4" v-model="this.role">i&#39;m a coach</a>
            <a @click="signup_url(3)" href="#" :class="parent_class"><input type="radio" name="role" value="3" v-model="this.role">i&#39;m a parent</a> 
        </div>
        @for( $i = date('Y'); $i <= intval(date('Y'))+7; $i++ )
        
            <?php $years[] = $i; ?>
        @endfor
        <?php //$years = [ date('Y'), intval(date('Y')) + 8 ]; ?>
        
        <div v-show="athelete_show">
            <athelete :graduation = "{{json_encode($years)}}" :allsports="{{ json_encode($sports) }}"></athelete>   
        </div>
        <div v-show="coach_show">
            <coach  @if(isset($sports)) :allsports='{{ json_encode($sports) }}' @endif   @if(isset($schoollistarrayjson)) :schoolllist='{{ $schoollistarrayjson }}' @endif  ></coach>
        </div>
        <div v-show="parent_show">
            <parent></parent>
        </div>  
    </div>

    <div v-cloak v-show="signup_popup_message" class="sign-up-message"><br /></div>
    
</div>
<div class="sign-up-popup" style="display: none" id="show-term">
    <div class="sign-up-content" id="show-content-div">
        <span class="close-term">Close</span>
        <div class="show-inner">
        <p>{!! Voyager::page('body', 53) !!}</p>
    </div>
    </div>
</div> 