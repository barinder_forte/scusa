@extends('voyager::master')

@section('page_title','All '.$dataType->display_name_plural)

@section('page_header')
    <div class="col-md-6 col-sm-6">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
    </div>
    <div class="col-md-6 col-sm-6 text-right">
<!--         <a  id="fetch_btn" class="btn btn-warning fetch-btn">Fetch Data With Api </a> 
 -->    </div>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
        
            <div class="col-md-12">
                <div class="right-box school-lists">
                    <div class="panel panel-bordered">
                        <div class="panel-body table-responsive">
                            <div class="col-md-6">
                                <form class=""  action='/admin/schoolslists' method="GET" >
                                 {{csrf_field()}}
                                    <div class="row">
                                        <label>Search</label>

                                        <div class="col-md-6">
                                            <input type='text' value='{{ isset($school_search)?$school_search :"" }}' name="school_search" id="school_search" placeholder="Search for Schools" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <input type='submit'  class="btn orange" value="Submit" >
                                            <a href="/admin/schoolslist" class="btn btn-warning">Reset</a> 
                                        </div>
                                    </div>
                                </form> 
                            </div>
                           <div class="col-md-6 text-right">
                                   <a href="/admin/school/import" id="school_import" class="btn btn-warning fetch-btn">Import School CSV</a> 
                            </div>
                      <!--   <form class=""  action='/admin/save/apikey' method="POST" >
                                 {{csrf_field()}}
                                    <div class="row"> -->
                                   <!--      <div class="col-md-4">
                                        </div> -->
                                       <!--  <label>Api Key</label>
                                        <div class="col-md-8">
                                            <input type='text' value='{{ isset($saveedApikey)?$saveedApikey :"" }}' name="school_api_key" id="school_api_key" placeholder="Enter Api Key Here" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <input type='submit'  class="btn btn-sm btn-info pull-left" value="Save Api Key" >
                                        </div>
                                    </div>
                                </form> 
                            </div> -->
                            <table id="dataTable" class="row table table-hover">
                                <thead>
                                    <tr>
                                        @foreach($dataType->browseRows as $rows)
                                        <th>{{ $rows->display_name }}</th>
                                        @endforeach
                                        <th class="actions">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      
                                    @foreach($dataTypeContent as $data)
                                    <tr>
                                        @foreach($dataType->browseRows as $row)
                                            <td>
                                                <?php $options = json_decode($row->details); ?>
                                                @if($row->type == 'image')
                                                    <img src="@if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($options, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)
                                                            @if($item->{$row->field . '_page_slug'})
                                                            <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                                            @else
                                                            {{ $item->{$row->field} }}
                                                            @endif
                                                        @endforeach

                                                        {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                                                    @elseif(property_exists($options, 'options'))
                                                        @foreach($data->{$row->field} as $item)
                                                         {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                        @endforeach
                                                    @endif

                                                @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                                                    @if($data->{$row->field . '_page_slug'})
                                                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                                                    @else
                                                        {!! $options->options->{$data->{$row->field}} !!}
                                                    @endif

                                                @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                                                    <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                                                @elseif($row->type == 'date')
                                                {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                                                @elseif($row->type == 'checkbox')
                                                    @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                                                        @if($data->{$row->field})
                                                        <span class="label label-info">{{ $options->on }}</span>
                                                        @else
                                                        <span class="label label-primary">{{ $options->off }}</span>
                                                        @endif
                                                    @else
                                                    {{ $data->{$row->field} }}
                                                    @endif
                                                    @elseif($row->type == 'text')
                                                    @if($row->field == 'school_url')
                                          <?php 
                                           $url = str_replace('Https://', '', ($data->{$row->field}) ); 
                                           $url2 = str_replace('https://', '', $url ); 
                                              $url3 = str_replace('Http://', '', $url2) ;
                                              $url4 = str_replace('http://', '', $url3) ;?>
                                        <div class="readmore" ><a href="
                                          {{ 'http://'.$url4 }}" target='_blank' title="School Site Url" >{{ $url4 }}</a></div>
                                                            @else
                                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                                <div class="readmore">{{ strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                            @endif
                                                 @elseif($row->type == 'text_area')
                                                
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                            </td>
                                        @endforeach
                                        <td class="no-sort no-click" id="bread-actions">
                                            @if($data->role_id != 1)
                                            @if (Voyager::can('delete_'.$dataType->name))
                                                <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}" id="delete-{{ $data->id }}">
                                                    <i class="voyager-trash"></i>
                                                </a>
                                            @endif
                                            @endif
                                             @if (Voyager::can('edit_'.$dataType->name))
                                                <a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->id) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm"></span>
                                                </a>
                                            @endif
                                            @if (Voyager::can('read_'.$dataType->name))
                                                <a href="/college/{{$data->school_slug}}" title="View" class="btn btn-sm btn-warning pull-right">
                                                    <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm"></span>
                                                </a>
                                            @endif
                                             
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                       
                            <div class="pull-right">
                              {!! $dataTypeContent->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="voyager-trash"></i> Are you sure you want to delete
                            this {{ strtolower($dataType->display_name_singular) }}?</h4>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                            {{ method_field("DELETE") }}
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger pull-right delete-confirm"
                                     value="Yes, delete this {{ strtolower($dataType->display_name_singular) }}">
                        </form>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <?php  $totalPage1 = 0 ;
        $saveedApikey = \App\UserInformation::where('user_id',Auth::User()->id)->where('meta_key','school_api_key')->pluck('meta_value')->first();
      if($saveedApikey){

      $result = @file_get_contents("https://api.data.gov/ed/collegescorecard/v1/schools?api_key=".$saveedApikey."&fields=school.name&page=0&per_page=1");
      // // Will dump a beauty json :3
      if($result === FALSE){
          $totalPage1 = 0 ;
      }else{
      $resultdata =  json_decode($result, true);

        $resultdataArray = $resultdata['results'];
        $totalResults = $resultdata['metadata']['total'];
        $perPageResults = $resultdata['metadata']['per_page'];
        $totalPage1 = number_format(($totalResults / 1) , 0);
        $totalPage1 = str_replace(',', '', $totalPage1);
   
    }
    }
    ?>
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
<script > 
 $(document).ready(function () {
        $('#fetch_btn').click(function(e){
            e.preventDefault();
           //alert("hello");
            var totalPage = <?php echo $totalPage1; ?> ;
            if(totalPage > 0){

                $('#voyager-loader').show();
                setTimeout(function(){  

                var apiKey =  $('#school_api_key').val() ;
                for(i = 0;i <= totalPage ; i++){
                $.ajax({
                    url: "/admin/fetch/ApiDataCopy/"+i+'/'+apiKey,
                    method: 'GET',
                    async: false,
                   
                }).done(function(response) {
                    console.log(response);
                    
                });
                }
                }, 3000);
             //   $('#voyager-loader').hide();
                toastr.success('Schools Data Fetched Successfully!');
            }else{
                $('#voyager-loader').hide();
                toastr.error('Faild!'); 
            }
            
        });
    });
    </script>
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    @if($isModelTranslatable)
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({
                    "order": [],
                    @if(config('dashboard.data_tables.responsive')), responsive: true ,@endif
                    "bInfo": false,
                    "paging" : false,
                    "searching": false,
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
            @endif
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });

   
    </script>
@stop 
