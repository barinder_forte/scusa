@extends('voyager::master')

@section('page_title','All '.$dataType->display_name_plural)

@section('page_header')
    <h1 class="page-title addnewuser">
        <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        @if (Voyager::can('add_'.$dataType->name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success">
                <i class="voyager-plus"></i> Add New
            </a>
        @endif
    </h1>
    @include('voyager::multilingual.language-selector')
@stop
<?php //die('yes here'); ?>
@section('content')
    <div class="page-content container-fluid form-css">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body table-responsive">
                            <form class=""  action='/admin/user' method="GET" >
                                 {{csrf_field()}}
                                    <div class="row">
                                       <div class="col-md-2">
                                            <input type='text' value='{{ isset($users_search)?$users_search :"" }}' name="user_search" id="school_search" placeholder="Search By Name Or Email" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                            <select name='user_role' class="js-example-basic-single">
                                                <option value='' >Select Role</option>
                                               <!--  <option <?php// if((isset($_GET['user_role'])?$_GET['user_role']:'') == 1) echo "selected";?> value='1'>{{'Adminstrator'}}</option> -->
                                                <option <?php if((isset($_GET['user_role'])?$_GET['user_role']:'') == 2) echo "selected";?> value='2'>Athlete</option>
                                                <option <?php if((isset($_GET['user_role'])?$_GET['user_role']:'') == 3) echo "selected";?> value='3'>Parents</option>  <option <?php if((isset($_GET['user_role'])?$_GET['user_role']:'') == 'both') echo "selected";?> value='both'>Athlete&Parents</option>
                                                <option <?php if((isset($_GET['user_role'])?$_GET['user_role']:'') == 4) echo "selected";?> value='4'>Coach</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <select name='user_approved' class="js-example-basic-single">
                                                <option value='' >Select Status</option>
                                                <option <?php if((isset($_GET['user_approved'])?$_GET['user_approved']:'') == 'ACTIVE') echo "selected";?> value='ACTIVE'>ACTIVE</option>
                                                <option <?php if((isset($_GET['user_approved'])?$_GET['user_approved']:'') == 'INACTIVE') echo "selected";?> value='INACTIVE'>INACTIVE</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <select name='paidUser' class="js-example-basic-single">
                                                <option value="" >Select Paid/Non Paid Users</option>
                                                <option <?php if((isset($_GET['paidUser'])?$_GET['paidUser']:'') == 'yes') echo "selected";?> value='yes'>Paid</option>
                                                <option <?php if((isset($_GET['paidUser'])?$_GET['paidUser']:'') == 'admin') echo "selected";?> value='admin'>Paid By Admin</option>
                                                 <option <?php if((isset($_GET['paidUser'])?$_GET['paidUser']:'') == 'no') echo "selected";?> value='no'>Non Paid</option>
                                            </select>
                                        </div>
                                        <?php $sports = Voyager::sport('all') ;
                                        // print_r($sports);die;
                                        ?>
                                         <div class="col-md-2">
                                            <select name='userSports' class="js-example-basic-single">
                                                <option value='' >Select Sports</option>
                                                @foreach($sports as $key=> $sport)
                                                <option <?php if((isset($_GET['userSports'])?$_GET['userSports']:'') == $key) echo "selected";?> value='{{$key}}'>{{$sport}}</option>
                                               @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type='submit'  class="btn orange" value="Submit" >
                                            <a href="/admin/users" class="btn btn-warning">Reset</a> 
                                        </div> 
                                        
                                    </div>
                                </form> 
                                <form id="target"  action='/admin/userapprove' method="GET" >
                                {{csrf_field()}}
                                    <div class="row">
                                    <div class="col-md-3"> 
                                    <input type="hidden" name="multiselect" class="multiselectval">
                                        <select name='user_approved' class="sel js-example-basic-single">
                                            <option>Select Action</option>
                                            <option value='ACTIVE'>ACTIVE</option>
                                            <option value='INACTIVE'>INACTIVE</option>
                                            <option value='DELETEe'>DELETE</option>
                                        </select>
                                      </div>
                                     <div class="col-md-1">     
                                    <input type='submit'  class="btn orange submitbutton" value="Submit" >
                                    </div>
                                    <div class="col-md-2 pull-right">
                                        <a href="/admin/export/athletes" class="btn btn-warning">Export Athletes CSV</a> 
                                    </div>
                                    <div class="col-md-2 pull-right">
                                        <a href="/admin/export/parents" class="btn btn-warning">Export Parent CSV</a> 
                                    </div> 
                                    
                                    </div>
                                </form> 
                        <table id="dataTable" class="row table table-hover">
                            <thead>
                                <tr>
                                    <th>


                                       <input type="checkbox" class="multiselect">


                                    </th>
                                
                                    @foreach($dataType->browseRows as $rows)
                                    <th>{{ $rows->display_name }}</th>
                                    @endforeach
                                    <th class="">Sport</th>
                                    <th class="actions">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataTypeContent as $data)
                                <tr>
                                    <td>
                                    @if($data->role_id != 1)
                                    <input type="checkbox" class="checkedtrue" value="{{ $data->id }}" name="checkboxapp[]">
                                    @endif
                                    </td>    

                                    @foreach($dataType->browseRows as $row)
                                        <td>
                                            <?php $options = json_decode($row->details); ?>
                                            @if($row->type == 'image')

                                              <!--  <a href='/profile/{{$data->id}}' title='Show Profile'> <img src="@if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px"></a>
                                             -->
                                             <img title='Show Profile' style="width:80px; height: 80px;" class='img-responsive' src="<?php if($data->{$row->field} == 'users/default.png' || $data->{$row->field} == ''){ echo env('APP_URL').'/storage/EFuG6j0fKnCeYPkABRMIvbzHdUi2rLC4xB24f4lB.png'; } else{ 
                                                if(isset( explode('/upload/',($data->{$row->field}))[1] )){
                                                $userimage = explode('/upload/',($data->{$row->field}))[1];
                                                    echo "https://res.cloudinary.com/webforte/image/upload/w_50,h_50/".$userimage;
                                                      }
                                                    else{
                                                       echo env('APP_URL').'/storage/'.($data->{$row->field}) ; 
                                                    }} ?>" >

                                            @elseif($row->type == 'select_multiple')
                                                @if(property_exists($options, 'relationship'))

                                                    @foreach($data->{$row->field} as $item)
                                                        @if($item->{$row->field . '_page_slug'})
                                                        <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                                        @else
                                                        {{ $item->{$row->field} }}
                                                        @endif
                                                    @endforeach

                                                    {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                                                @elseif(property_exists($options, 'options'))
                                                    @foreach($data->{$row->field} as $item)
                                                     {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                    @endforeach
                                                @endif

                                            @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                                                @if($data->{$row->field . '_page_slug'})
                                                    <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                                                @else
                                                    {!! $options->options->{$data->{$row->field}} !!}
                                                @endif


                                            @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                                                <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                                            @elseif($row->type == 'date')
                                            {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                                            @elseif($row->type == 'checkbox')
                                                @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                                                    @if($data->{$row->field})
                                                    <span class="label label-info">{{ $options->on }}</span>
                                                    @else
                                                    <span class="label label-primary">{{ $options->off }}</span>
                                                    @endif
                                                @else
                                                {{ $data->{$row->field} }}
                                                @endif
                                             @elseif($row->display_name == 'name')

                                                <!--@include('voyager::multilingual.input-hidden-bread-browse')-->

                                                 @if($data->role_id == 4)
                                                    <a href='/profile/coach/{{$data->user_slug}}' title='Show Profile'> 
                                                 @else     
                                                    <a href='/profile/athlete/{{$data->user_slug}}' title='Show Profile'> 
                                                 @endif
                                                    <span>{{ $data->{$row->field} }}</span> </a>
                                            @elseif($row->display_name == 'role_id')
                                            <?php $role = TCG\Voyager\Models\Role::where('id',($data->{$row->field}))->first(); ?>
                                             {{ isset($role->display_name)?$role->display_name:''  }}
                                            @elseif($row->type == 'text')
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <div class="readmore">{{ strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                            @elseif($row->type == 'text_area')
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <div class="readmore">{{ strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                            @elseif($row->type == 'rich_text_box')
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <div class="readmore">{{ strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                            @else
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <span>{{ $data->{$row->field} }}</span>
                                            @endif
                                        </td>
                                    @endforeach
                                    <td>
                        <?php
                        $sportid = \App\UserInformation::where('user_id',$data->id)->where('meta_key','sport')->first();
                        if(isset($sportid) && ( isset($sportid)?$sportid->meta_value:'') != 'null'){
                        $sportall = \App\Sport::find($sportid->meta_value);
                        }
                        // print($sportall);
                        ?>
                                        <p>{{isset($sportall)?$sportall->title:''}}</p>
                                    </td>  
                                    <td class="no-sort no-click" id="bread-actions">
                                        @if($data->role_id != 1)
                                        @if (Voyager::can('delete_'.$dataType->name))
                                            <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}" id="delete-{{ $data->id }}">
                                                <i class="voyager-trash"></i>
                                            </a>
                                        @endif
                                        @endif
                                        @if (Voyager::can('edit_'.$dataType->name))
                                            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->id) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i>
                                            </a>
                                        @endif
                                        @if($data->role_id == 4)
                                            @if (Voyager::can('read_'.$dataType->name))
                                                <a href="/profile/coach/{{$data->user_slug}}" target="_blank" title="View" class="btn btn-sm btn-warning pull-right">
                                                    <i class="voyager-eye"></i> 
                                                </a>
                                            @endif
                                        @else
                                             @if (Voyager::can('read_'.$dataType->name))
                                                <a href="/profile/athlete/{{$data->user_slug}}" target="_blank" title="View" class="btn btn-sm btn-warning pull-right">
                                                    <i class="voyager-eye"></i> 
                                                </a>
                                            @endif
                                        @endif
                                        <!--<form role="form" id='resetPasswordForm'  method="POST" action="#" class="form-horizontal">-->
                                           
                                        <!--    <div class="form-group">-->
                                        <!--    <input id="token" name='_token' type="hidden" name="email" value='{{csrf_token()}}'  class="form-control">-->
                                        <!--    <input id="email" type="hidden" name="email" value='{{$data->email}}'  class="form-control">-->
                                        <!--    </div>-->
                                        <!--    <div class="form-group">-->
                                        <!--    <button type='submit' data="{{$data->email}}" id='resetPasswordButton-{{$data->id}}'  title="Reset Password" class="resetPasswordButton btn btn-sm btn-warning pull-right ">-->
                                        <!--     <i class="voyager-lock"></i>   -->
                                        <!--     </button>-->
                                        <!--</div>-->
                                        <!--</form>-->
                                        
                                        
                                    </td>
                                </tr>
                                @endforeach
                                <?php //die; ?>
                            </tbody>
                        </table>
                        
                             <div class="pull-right">
                              {!! $dataTypeContent->appends(Request::except('users'))->render() !!}
                           
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> Are you sure you want to delete
                        this {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                                 value="Yes, delete this {{ strtolower($dataType->display_name_singular) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    @if($isModelTranslatable)
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
    
        $(document).ready(function () {
            $("body").delegate('.multiselect','click', function(e){

            var status=$('.multiselect')[0].checked;
            if(status == true){
                $('.checkedtrue').prop('checked',true);
            }else{
                $('.checkedtrue').prop('checked',false);
            }  
            })

            $("body").delegate('.submitbutton','click', function(e){
            e.preventDefault();
            // var con = '';
            // var selectval = '';
                var checkboxes = document.getElementsByName('checkboxapp[]');
                var user_status = $('.sel option:selected').val();
                console.log(user_status);
                var vals = "";
                    for (var i=0, n=checkboxes.length;i<n;i++){
                        if (checkboxes[i].checked){
                        this.messagecheck=false;
                        vals += checkboxes[i].value+",";
                    }
                  }
               $('.multiselectval').val(vals);

               // console.log(vals);

               // if(vals == ' ' || user_status == 'Select Status'){
               //  console.log("please Select User And Status.");
               // }else{
                $( "#target" ).submit();
              // }

               // if(vals != ','){
               //   con = '2';
               // }else{
               //  alert("Please Select User.");
               // }
               // if(user_status != ''){
               //      selectval = '3';

               // }else{
               //      alert("Please Select User Status.");

               // }


              // $('.deletedata').val(vals);
            })





            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({
                    "order": [],
                    @if(config('dashboard.data_tables.responsive')), responsive: true ,@endif
                    "bInfo": false,
                    "paging" : false,
                    "searching": false,
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
            @endif
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
         

            $('#delete_modal').modal('show');
        });
       
    $('.resetPasswordButton').on('click',function(e){
        e.preventDefault();
         var token = $('#token').val();
         var email = $(this).attr('data');
        
        $.ajax({
            url:'/password/email',
            type:'post',
            data: '_token='+token+'&email= '+email ,
            success:function(response){
              console.log(response);
              alert('Reset Password Email Sent Successfully!');
            }
        });
    })
    </script>
@stop
