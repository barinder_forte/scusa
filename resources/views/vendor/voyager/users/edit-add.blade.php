@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Add New' }}@endif {{ $dataType->display_name_singular }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-edit-add" role="form"
                          action="@if(isset($dataTypeContent->id)){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($dataTypeContent->id))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name"
                                    placeholder="Name" id="name"
                                    value="@if(isset($dataTypeContent->name)){{ old('name', $dataTypeContent->name) }}@else{{old('name')}}@endif">
                            </div>

                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="text" class="form-control" name="email"
                                       placeholder="Email" id="email"
                                       value="@if(isset($dataTypeContent->email)){{ old('email', $dataTypeContent->email) }}@else{{old('email')}}@endif">
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                @if(isset($dataTypeContent->password))
                                    <br>
                                    <small>Leave empty to keep the same</small>
                                @endif
                                <input type="text" class="form-control" name="password"
                                       placeholder="Password" id="password"
                                       value="">
                                       
                             <input type="button" class="button" value="Generate Password" id="Generate" tabindex="2">
                             <input type="button" class="button" value="Empty Password" id="empty" tabindex="2">

                            </div>

                           <!--  <div class="form-group">
                                <label for="password">Avatar</label>
                                <div style="display: none" class="update">   
                                    <img id="blah" width="200px" height="100px" src="#" alt="your image" />
                                </div>
                                @if(isset($dataTypeContent->avatar))
                                <img class="imageupload" src="{{ Voyager::image( $dataTypeContent->avatar ) }}"
                               
                                         style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                @endif
                                <input type="file" name="avatar" id="imageupload">
                            </div> -->

                            <div class="form-group">
                                <label for="role">User Role</label>
                                <select name="role_id" id="role" class="form-control">
                                    <?php $roles = TCG\Voyager\Models\Role::all(); ?>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}" @if(isset($dataTypeContent) && $dataTypeContent->role_id == $role->id) selected @endif>{{$role->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                             <div class="form-group">
                                <label for="role">User Status</label>
                                <select name="user_status" id="user_status" class="form-control">
                                     <option value="ACTIVE" @if(isset($dataTypeContent) && $dataTypeContent->user_status == 'ACTIVE') selected @endif>ACTIVE</option>
                                     <option value="INACTIVE" @if(isset($dataTypeContent) && $dataTypeContent->user_status == 'INACTIVE') selected @endif>INACTIVE</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="role">User Approved</label>
                                <select name="user_approved" id="user_approved" class="form-control">
                                     <option value="ACTIVE" @if(isset($dataTypeContent) && $dataTypeContent->user_approved == 'ACTIVE') selected @endif>ACTIVE</option>
                                     <option value="INACTIVE" @if(isset($dataTypeContent) && $dataTypeContent->user_approved == 'INACTIVE') selected @endif>INACTIVE</option>
                                </select>
                            </div>
                          
                        <?php $user = APP\User::where('id',$dataTypeContent->id)->first();
                             
                                if(! $user->subscribed('main')){ 
                                $da = "";
                             }else{
                              $da = "style=display:none";
                              ?>
                            <div class="form-group">
                                <label for="role">Paid User Marked</label>
                                <select class="form-control" disabled="disabled">
                                    <option value="Yes">Yes</option>
                                </select>
                            </div>

                            <?php } 
                            ?>
                            <div class="form-group" <?php echo $da; ?>>
                                <label for="role">Paid User Marked</label>
                                <select name="is_paid" id="is_paid" class="form-control">
                                    <option value="" >Please select</option>
                                    <option value="yes" @if(isset($dataTypeContent) && $dataTypeContent->is_paid == 'yes') selected @endif><p>Yes</p></option>
                                    <option value="no" @if(isset($dataTypeContent) && $dataTypeContent->is_paid == 'no') selected @endif>No</option>
                                </select>
                            </div>
                           
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            
            $('#empty').hide();
            $('.toggleswitch').bootstrapToggle();
            jQuery('#empty').on('click',function(){
            jQuery('#password').val('');
              $('#empty').hide();
            })
            jQuery('#Generate').on('click',function(){
                
           
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            var length = 25;
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            jQuery('#password').val(pass);
                        $('#empty').show();

            })
            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

        $("#imageupload").change(function(){
            readURL(this);
            jQuery(".imageupload").hide();
            jQuery(".update").show();

        });
     });
    </script>
    <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
@stop
