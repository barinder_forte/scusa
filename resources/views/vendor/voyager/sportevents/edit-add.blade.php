@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@if(isset($dataTypeContent->id))
    @section('page_title','Edit '.$dataType->display_name_singular)
@else
    @section('page_title','Add '.$dataType->display_name_singular)
@endif

@section('head')
    <script type="text/javascript" src="{{ voyager_asset('lib/js/jsonarea/jsonarea.min.js') }}"></script>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

<?php
use App\ApiData;
$school_attending = ApiData::select('id','school_name')->orderBy('school_name','ASC')->get();
?>
@section('content')
    <div class="page-content container-fluid">
    <p><strong>View Link : </strong><a class="url-link" href="/event/{{$dataTypeContent->event_slug}}"><?php  echo url('/'); ?>/event/{{$dataTypeContent->event_slug}}</a></p>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Add New' }}@endif {{ $dataType->display_name_singular }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(isset($dataTypeContent->id)){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($dataTypeContent->id))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- If we are editing -->
                            @if(isset($dataTypeContent->id))
                                <?php $dataTypeRows = $dataType->editRows; ?>
                                <?php $event_id = $dataTypeContent->id; ?>
                            @else
                                <?php $dataTypeRows = $dataType->addRows;
                                $event_id = '';
                                ?>
                            @endif
    
                            @foreach($dataTypeRows as $row)
                                <div class="form-group @if($row->type == 'hidden') hidden @endif">
                                <label for="name">{{ $row->display_name }}</label>
                                 @if($row->display_name == 'Sport Name')
                                  
                                  <?php $sports = Voyager::Sport('all'); 
                                  ?>
                                  <select aria-hidden="true" tabindex="-1" class="form-control select2"  name="sport_id">
                                      <option>Select Sport</option>
                                     
                                      @foreach($sports as $key=>$s)
                                   <?php    //  print_r($s); die;?>
                                      <option <?php if($dataTypeContent->sport_id == $key) { echo 'selected';}  ?> value='{{$key}}' >{{$s}}</option>
                                      
                                      @endforeach
                                      
                                    </select>
                                @elseif($row->display_name == 'State')
                                    <?php $state = $dataTypeContent->state;  ?>
                                    <select name="state" id="state" class="form-control select2">
                                        <option value=''>Select State</option>
                                        <option value="AL" <?PHP if((isset($state)?$state:'')=="AL") echo "selected";?>>Alabama</option>
                                        <option value="AK" <?PHP if((isset($state)?$state:'')=="AK") echo "selected";?>>Alaska</option>
                                        <option value="AZ" <?PHP if((isset($state)?$state:'')=="AZ") echo "selected";?>>Arizona</option>
                                        <option value="AR" <?PHP if((isset($state)?$state:'')=="AR") echo "selected";?>>Arkansas</option>
                                        <option value="CA" <?PHP if((isset($state)?$state:'')=="CA") echo "selected";?>>California</option>
                                        <option value="CO" <?PHP if((isset($state)?$state:'')=="CO") echo "selected";?>>Colorado</option>
                                        <option value="CT" <?PHP if((isset($state)?$state:'')=="CT") echo "selected";?>>Connecticut</option>
                                        <option value="DE" <?PHP if((isset($state)?$state:'')=="DE") echo "selected";?>>Delaware</option>
                                        <option value="DC" <?PHP if((isset($state)?$state:'')=="DC") echo "selected";?>>District of Columbia</option>
                                        <option value="FL" <?PHP if((isset($state)?$state:'')=="FL") echo "selected";?>>Florida</option>
                                        <option value="GA" <?PHP if((isset($state)?$state:'')=="GA") echo "selected";?>>Georgia</option>
                                        <option value="HI" <?PHP if((isset($state)?$state:'')=="HI") echo "selected";?>>Hawaii</option>
                                        <option value="ID" <?PHP if((isset($state)?$state:'')=="ID") echo "selected";?>>Idaho</option>
                                        <option value="IL" <?PHP if((isset($state)?$state:'')=="IL") echo "selected";?>>Illinois</option>
                                        <option value="IN" <?PHP if((isset($state)?$state:'')=="IN") echo "selected";?>>Indiana</option>
                                        <option value="IA" <?PHP if((isset($state)?$state:'')=="IA") echo "selected";?>>Iowa</option>
                                        <option value="KS" <?PHP if((isset($state)?$state:'')=="KS") echo "selected";?>>Kansas</option>
                                        <option value="KY" <?PHP if((isset($state)?$state:'')=="KY") echo "selected";?>>Kentucky</option>
                                        <option value="LA" <?PHP if((isset($state)?$state:'')=="LA") echo "selected";?>>Louisiana</option>
                                        <option value="ME" <?PHP if((isset($state)?$state:'')=="ME") echo "selected";?>>Maine</option>
                                        <option value="MD" <?PHP if((isset($state)?$state:'')=="MD") echo "selected";?>>Maryland</option>
                                        <option value="MA" <?PHP if((isset($state)?$state:'')=="MA") echo "selected";?>>Massachusetts</option>
                                        <option value="MI" <?PHP if((isset($state)?$state:'')=="MI") echo "selected";?>>Michigan</option>
                                        <option value="MN" <?PHP if((isset($state)?$state:'')=="MN") echo "selected";?>>Minnesota</option>
                                        <option value="MS" <?PHP if((isset($state)?$state:'')=="MS") echo "selected";?>>Mississippi</option>
                                        <option value="MO" <?PHP if((isset($state)?$state:'')=="MO") echo "selected";?>>Missouri</option>
                                        <option value="MT" <?PHP if((isset($state)?$state:'')=="MT") echo "selected";?>>Montana</option>
                                        <option value="NE" <?PHP if((isset($state)?$state:'')=="NE") echo "selected";?>>Nebraska</option>
                                        <option value="NV" <?PHP if((isset($state)?$state:'')=="NV") echo "selected";?>>Nevada</option>
                                        <option value="NH" <?PHP if((isset($state)?$state:'')=="NH") echo "selected";?>>New Hampshire</option>
                                        <option value="NJ" <?PHP if((isset($state)?$state:'')=="NJ") echo "selected";?>>New Jersey</option>
                                        <option value="NM" <?PHP if((isset($state)?$state:'')=="NM") echo "selected";?>>New Mexico</option>
                                        <option value="NY" <?PHP if((isset($state)?$state:'')=="NY") echo "selected";?>>New York</option>
                                        <option value="NC" <?PHP if((isset($state)?$state:'')=="NC") echo "selected";?>>North Carolina</option>
                                        <option value="ND" <?PHP if((isset($state)?$state:'')=="ND") echo "selected";?>>North Dakota</option>
                                        <option value="OH" <?PHP if((isset($state)?$state:'')=="OH") echo "selected";?>>Ohio</option>
                                        <option value="OK" <?PHP if((isset($state)?$state:'')=="OK") echo "selected";?>>Oklahoma</option>
                                        <option value="OR" <?PHP if((isset($state)?$state:'')=="OR") echo "selected";?>>Oregon</option>
                                        <option value="PA" <?PHP if((isset($state)?$state:'')=="PA") echo "selected";?>>Pennsylvania</option>
                                        <option value="RI" <?PHP if((isset($state)?$state:'')=="RI") echo "selected";?>>Rhode Island</option>
                                        <option value="SC" <?PHP if((isset($state)?$state:'')=="SC") echo "selected";?>>South Carolina</option>
                                        <option value="SD" <?PHP if((isset($state)?$state:'')=="SD") echo "selected";?>>South Dakota</option>
                                        <option value="TN" <?PHP if((isset($state)?$state:'')=="TN") echo "selected";?>>Tennessee</option>
                                        <option value="TX" <?PHP if((isset($state)?$state:'')=="TX") echo "selected";?>>Texas</option>
                                        <option value="UT" <?PHP if((isset($state)?$state:'')=="UT") echo "selected";?>>Utah</option>
                                        <option value="VT" <?PHP if((isset($state)?$state:'')=="VT") echo "selected";?>>Vermont</option>
                                        <option value="VA" <?PHP if((isset($state)?$state:'')=="VA") echo "selected";?>>Virginia</option>
                                        <option value="WA" <?PHP if((isset($state)?$state:'')=="WA") echo "selected";?>>Washington</option>
                                        <option value="WV" <?PHP if((isset($state)?$state:'')=="WV") echo "selected";?>>West Virginia</option>
                                        <option value="WI" <?PHP if((isset($state)?$state:'')=="WI") echo "selected";?>>Wisconsin</option>
                                        <option value="WY" <?PHP if((isset($state)?$state:'')=="WY") echo "selected";?>>Wyoming</option>
                                    </select>
                                @elseif($row->display_name == 'Division')
                                
                                <?php $divisionss = Voyager::pagemetadetail('Division', '16');
                
                                       $divisionss = json_decode($divisionss);
                                       $divisionss = $divisionss->options;
                                      
                                        ?>
                                    <select name="division" aria-hidden="true" tabindex="-1" class="form-control select2">
                                         <option  value="">Select Division</option>
                                         <?php foreach($divisionss as $key => $div){ ?>
                                           <option <?php if($dataTypeContent->division == $div) { echo 'selected';}  ?> value='{{$div}}' >{{$key}}</option>
                                         <?php } ?>
                                    </select>
                                @elseif($row->display_name == 'Event Start')
                                
                               <?php  $eventstart = ($dataTypeContent->event_start != '1970-01-01' && $dataTypeContent->event_start)?date("m/d/Y", strtotime($dataTypeContent->event_start)):date("m/d/Y"); ?>

                                
                                <input type="text" class="form-control" name="event_start" placeholder="Event Start" value="{{$eventstart}}" id="dp1500982155819">
                                  
                                @elseif($row->display_name == 'Event End')
                                
                               <?php  $event_end = ($dataTypeContent->event_end != '1970-01-01' && $dataTypeContent->event_end)?date("m/d/Y", strtotime($dataTypeContent->event_end)):date("m/d/Y"); ?>

                                
                                <input type="text" class="form-control" name="event_end" placeholder="Event End" value="{{$event_end}}">  
                                  
                                @elseif($row->display_name == 'Tournaments') 
                                 <?php $tournamentsss = Voyager::pagemetadetail('tournaments', '16');
                
                                       $tournamentsss = json_decode($tournamentsss);
                                       $tournamentsss = $tournamentsss->options;
                                      
                                        ?>
                                    <select name="tournaments" aria-hidden="true" tabindex="-1" class="form-control select2">
                                         <option  value="">Select Tournaments</option>
                                        
                                         <?php foreach($tournamentsss as $key => $divs){ ?>
                                         
                                           <option <?php if($dataTypeContent->tournaments == $divs) { echo 'selected';}  ?> value='{{$divs}}' >{{$key}}</option>
                                           
                                         <?php } ?>
                                    </select>
                                
                                @elseif($row->display_name == 'School Attending') 
                                    <select name="school_attending" aria-hidden="true" tabindex="-1" class="form-control select2">
                                         <option  value="">Select School</option>
                                        
                                         <?php foreach($school_attending as $school_attendings){ ?>
                                         
                                           <option <?php if($dataTypeContent->school_attending == $school_attendings->id) { echo 'selected';}  ?> value='{{$school_attendings->id}}' >{{$school_attendings->school_name}}</option>
                                           
                                         <?php } ?>
                                    </select>



                                @elseif($row->display_name == 'Event Gallery Images')
                                   
                                   <label for="files"> </label>
                                    <input id="files" type="file" name="gallery_images[]" multiple/>
                                    @if(isset($dataTypeContent->id))
                                    <output id="result" />
                                   <?php $GalleryImagesData = App\SportEventMeta::where('event_id',$dataTypeContent->id )->where('key','event_gallery')->first(); ?>
                                    @if(isset($GalleryImagesData))
                                    <?php
                                    $ImagesJson =($GalleryImagesData->details);
                                    $ImagesJsonD = json_decode($ImagesJson);
                                   ?>
                                    @foreach($ImagesJsonD as $key=>$data)
                                        <div class='thumbnail {{$key}}'>
                                            <a class="close" id="{{$key}}" class="delete"  href="#">×</a>
                                            <img class='' height='200px' width='200px' src='{{$data}}' title=''/>
                                        </div>
                                    @endforeach
                                    @endif
                                    
                                    @else 
                                     <output id="result" />
                                  
                                   @endif
                                   
                                @elseif($row->display_name == 'Event Image' && $dataTypeContent->id)
                                    @if (stripos($dataTypeContent->image, "sportevents/") !== false)
                                        <div class='thumbnail '>
                                           <img class='' style='width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;' src='{{asset("storage/".$dataTypeContent->image)}}' title=''/>
                                        </div>
                                    @else 
                                    <div class='thumbnail '>
                                        <img class='' style='width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;' src='{{$dataTypeContent->image}}' title=''/>
                                    </div>
                                    @endif
                                    <input id="file" type="file" name="image" />
                                @else
                                @if($row->display_name == 'Banner Image')
                                @if($dataTypeContent->banner_image)
                                <div class="img_settings_container imagedelete">
                                    <a href="#" id="{{$dataTypeContent->id}}" name="image" class="voyager-x voyager_banner_delete"></a>
                                    <img src="{{ Storage::disk(config('voyager.storage.disk'))->url( $dataTypeContent->banner_image)}}" style="width:200px; height:auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                </div>
                                @endif
                                <input type="file" name="banner_image">
                                @else
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                @endif
                                @endif
                                </div>
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>
    
                </div>
            </div>
        </div>
    </div>
    
    
    
    @if(isset($dataTypeContent->id))
    <!-- ACF Fields -->
    @if( $dataType->display_name_singular == 'Page' || $dataType->display_name_singular == 'Sport' )
    <div class="page-content container-fluid">
        <form action="{{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}" method="POST" enctype="multipart/form-data">
            {{ method_field("PUT") }}
            {{ csrf_field() }}
            <input type="hidden" value="{{$dataTypeContent->id}}" name="page_id" required="required">
            <input type="hidden" name="page_meta_content" value="{{ $dataType->slug }}" />
            
            <div class="panel">
                @if($settings) @foreach($settings as $setting)
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $setting->display_name }}  <code>Voyager::pagemeta('{{ $setting->key }}', '{{$dataTypeContent->id}}')</code>
                        </h3>
                        <div class="panel-actions">
                            <a href="/admin/sportsmeta/{{$setting->id}}/move_up">
                                <i class="sort-icons voyager-sort-asc"></i>
                            </a>
                            <a href="/admin/sportsmeta/{{$setting->id}}/move_down">
                                <i class="sort-icons voyager-sort-desc"></i>
                            </a>
                            <i class="voyager-trash"
                               data-id="{{ $setting->id }}"
                               data-display-key="{{ $setting->key }}"
                               data-display-name="{{ $setting->display_name }}"></i>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        @if ($setting->type == "text")
                            {{ $setting->type }}
                        @elseif($setting->type == "text_area")
                            {{ $setting->type }}
                        @elseif($setting->type == "image")
                            {{ $setting->type }}
                        @elseif($setting->type == "select_dropdown")
                            {{ $setting->type }} {{ $setting->details }}
                        @elseif($setting->type == "radio_btn")
                            {{ $setting->type }} {{ $setting->details }}
                        @elseif($setting->type == "checkbox")
                            {{ $setting->type }} {{ $setting->details }}
                        @elseif($setting->type == "slider")
                            {{ "Range Selector" }} {{ $setting->details }}
                        @endif

                    </div>
                    
                    @if(!$loop->last)
                        <hr>
                    @endif
                @endforeach @endif
            </div>
            
            @if( $dataType->display_name_singular != 'Sport' )
                <button type="submit" class="btn btn-primary pull-right">Save Settings</button>
            @endif
        </form>
    
        <div style="clear:both"></div>

        <div class="panel" style="margin-top:10px;">
            <div class="panel-heading new-setting">
                <hr>
                <h3 class="panel-title"><i class="voyager-plus"></i> New Setting</h3>
            </div>
            <div class="panel-body">
                @if( $dataType->display_name_singular == 'Page' ):
                <form action="{{ route('pagemeta.store') }}" method="POST">
                @elseif( $dataType->display_name_singular == 'Sport' )
                <form action="{{ route('sportsmeta.store') }}" method="POST">
                @endif
                    {{ csrf_field() }}
                    <input type="hidden" name="page_meta_content" value="{{ $dataType->slug }}" />
                    <input type="hidden" value="{{$dataTypeContent->id}}" name="@if($dataType->display_name_singular == 'Page'){{'page_id'}}@elseif($dataType->display_name_singular == 'Sport'){{ 'sport_id' }}@endif" required="required">
                    <div class="col-md-4">
                        <label for="display_name">Name</label>
                        <input type="text" class="form-control" name="display_name" placeholder="Setting name ex: Admin Title" required="required">
                    </div>
                    <div class="col-md-4">
                        <label for="key">Key</label>
                        <input type="text" class="form-control" name="key" placeholder="Setting key ex: admin_title" required="required">
                    </div>
                    <div class="col-md-4">
                        <label for="asdf">Type</label>
                        <select name="type" class="form-control" required="required">
                            <option value="">Choose type</option>
                            <option value="text">Text Box</option>
                            <option value="slider">Number Slider</option>
                            <option value="text_area">Text Area</option>
                            <!--<option value="rich_text_box">Rich Textbox</option>-->
                            <!--<option value="code_editor">Code Editor</option>-->
                            <option value="checkbox">Check Box</option>
                            <option value="radio_btn">Radio Button</option>
                            <option value="select_dropdown">Select Dropdown</option>
                            <option value="multi_dropdown">Multi-Select Dropdown</option>
                            <!--<option value="file">File</option>-->
                            <option value="image">Image</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <a id="toggle_options"><i class="voyager-double-down"></i> OPTIONS</a>
                        <div class="new-settings-options">
                            <label for="options">Options
                                <small>(optional, only applies to certain types like dropdown box or radio button)
                                </small>
                            </label>
                            <div id="options_editor" class="form-control min_height_200" data-language="json"></div>
                            <textarea id="options_textarea" name="details" class="hidden"></textarea>
                            <div id="valid_options" class="alert-success alert" style="display:none">Valid Json</div>
                            <div id="invalid_options" class="alert-danger alert" style="display:none">Invalid Json</div>
                        </div>
                    </div>
                    <script>
                        jQuery('document').ready(function ($) {
                            $('#toggle_options').click(function () {
                                $('.new-settings-options').toggle();
                                if ($('#toggle_options .voyager-double-down').length) {
                                    $('#toggle_options .voyager-double-down').removeClass('voyager-double-down').addClass('voyager-double-up');
                                } else {
                                    $('#toggle_options .voyager-double-up').removeClass('voyager-double-up').addClass('voyager-double-down');
                                }
                            });
                        });
                    </script>
                    <div style="clear:both"></div>
                    <button type="submit" class="btn btn-primary pull-right new-setting-btn">
                        <i class="voyager-plus"></i> Add New Setting
                    </button>
                    <div style="clear:both"></div>
                    <div>
                        <code>Note: </code>
                        <p>For Text Box { "instruction" : "", "inputs" : [ { "name" : "hours", "unit" : "minutes" }, { 
"name" : "minutes", "unit" : "seconds" }, ..... number of boxes ] } </p>
                        <p>For checkbox, Radio Button, Select Box, Multi-select enter object with value and label like: { "instruction" : "Any Instruction", "units" : "eg: Seconds", "options" : { "value" : "label", "value2" : "label2" } }</p>
                        <p>For Textarea, Image leave options empty. { "instructions" : "lorem ipsum" }</p>
                        <p>For Number Slider: {"instruction":"Select the range","units":"","type":"single|multi","input":{"min":"0","max":"10"}}  For type use either single or multi</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- -->
    @endif
    @endif
    
    </div>
    
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Are You Sure</h4>
                </div>

                <div class="modal-body">
                    <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Are you sure you want to delete the <span id="delete_setting_title"></span> Setting?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.destroy', $dataTypeContent->id) }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="hidden" name="destroy_sport_meta" value="destroy" />
                        <input type="hidden" id="destroy_id" name="destroy_id" value="" />
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Yes, Delete This Setting">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
    <script>
        jQuery('document').ready(function ($) {
            $('.voyager-trash').click(function () {
                var display = $(this).data('display-name') + '/' + $(this).data('display-key');
                $('#destroy_id').val( $(this).attr('data-id') );
                $('#delete_setting_title').text(display);
                $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(this).data('id'));
                $('#delete_modal').modal('show');
            });

            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataTypeContent->getTable() }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
        
        
        
        
    </script>
    @if($isModelTranslatable)
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
    <script src="{{ voyager_asset('lib/js/ace/ace.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_ace_editor.js') }}"></script>
    <script src="{{ voyager_asset('js/slugify.js') }}"></script>

    <script>
    window.onload = function(){
        
    //Check File API support
    if(window.File && window.FileList && window.FileReader)
    {
        var filesInput = document.getElementById("files");
        
        filesInput.addEventListener("change", function(event){
            
            var files = event.target.files; //FileList object
            var output = document.getElementById("result");
            
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener("load",function(event){
                    
                    var picFile = event.target;
                    
                    var div = document.createElement("div");
                    
                    div.innerHTML = "<img class='thumbnail' height='200px' width='200px' src='" + picFile.result + "'" +
                            "title='" + picFile.name + "'/>";
                    
                    output.insertBefore(div,null);            
                
                });
                
                 //Read the image
                picReader.readAsDataURL(file);
            }                               
           
        });
    }
    else
    {
        console.log("Your browser does not support File API");
    }
}
        
         $('.close').click(function (evt) {
            evt.preventDefault();
            var link = $(this).attr('id');
            $('.'+link).remove();
            
            
            var eventid = '<?php echo $event_id ?>';
            $.ajax({
                type: "post",
                url: '/eventdelete',
                data: {
                    href: link,
                    eventid : eventid,
                    
                }
            }).done(function (data) {
               
            });
        });

        $("input[name='event_start']").datepicker({
            // dateFormat: 'mm/dd/yy',
            changeMonth: true, 
            changeYear: true, 
            yearRange: "-10:+05"
    
        });
        $("input[name='event_end']").datepicker({
            // dateFormat: 'yy-mm-dd',
            changeMonth: true, 
            changeYear: true, 
            yearRange: "-10:+05"
        });
        var options_editor = ace.edit('options_editor');
        options_editor.getSession().setMode("ace/mode/json");

        var options_textarea = document.getElementById('options_textarea');
        options_editor.getSession().on('change', function() {
            console.log(options_editor.getValue());
            options_textarea.value = options_editor.getValue();
        });
    </script>
     <script>
        jQuery('document').ready(function ($) {
            $('.voyager_banner_delete').click(function (e) {
                e.preventDefault();
                var valueid = $(this).attr('id');
                $(".imagedelete").empty();
                $.ajax({
                    type: 'GET',
                    url: '/admin/delete/eventbanner/'+valueid,
                    //data: 'id=' +valueid,
                    success: function(data) {
                        console.log(data);
                    },
                    error: function (error) {
                         console.log(error);
                    }
                });   
            });
        });

    </script>
@stop
