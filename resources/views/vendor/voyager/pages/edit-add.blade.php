@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@if(isset($dataTypeContent->id))
    @section('page_title','Edit '.$dataType->display_name_singular)
@else
    @section('page_title','Add '.$dataType->display_name_singular)
@endif

@section('head')
    <script type="text/javascript" src="{{ voyager_asset('lib/js/jsonarea/jsonarea.min.js') }}"></script>
@stop
@if((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 14)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>{{ 'General Email Templates' }}
        </h1>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 31)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>Email To Coach ( School Search && Coach Profile )
        </h1>
        <p style="padding: 0px 20px;">Use these shortcodes <em>
        {coach_name} {college_name}   {my_name} {my_email} {my_position} {class_year} {athlete_school} {club_name} {club_jersey_number} {gpa} {my_phone} {my_facebook} {my_twitter} {my_instagram} {my_snapchat} {my_skype} {my_whatsapp} {my_videogallery_urls} {my_sport} {my_address} {my_height} {my_weight} {my_birth} {my_academics} {my_club_info} {athletic_achievements} {academic_achievement} {extra_curricular_activity} </em></p>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 17)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>{{ 'Athlete Email Templates' }}
        </h1>
        <p style="padding: 0px 20px;">Use these shortcodes <em>{athlete_name}  {athlete_school}   {event_title} {event_description} {event_event_start} {event_event_end} {event_place} {event_city} {event_state} {event_zipcode} {sport_title} {event_division} {event_tournaments} {event_campus_clinics} {event_school_attending}<br/>
<!--         {coach_name}<br/> -->
        {my_name} {my_email} {college_name} {my_sport} {my_phone} {my_web} {ncaa_championship} {national_tournament} {ncaa_final_four} {ncaa_post_season_appearance} {conference_championship} {my_bio} {my_title} {my_videogallery_urls}  {school_information} {gender_coached} </em></p>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 20)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>{{ 'Email Settings' }}
        </h1>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 30)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>Email To Athlete ( Users List Page && Athlete Profile )
        </h1>
        <p style="padding: 0px 20px;">Use these shortcodes <em>
        {athlete_name} {athlete_school}   {my_name} {my_email} {college_name} {my_sport} {my_phone} {my_web} {ncaa_championship} {national_tournament} {ncaa_final_four} {ncaa_post_season_appearance} {conference_championship} {my_bio} {my_title} {my_videogallery_urls}  {school_information}  {gender_coached}</em></p>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 18)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>{{ 'Caoch Email Templates' }}
        </h1>
         <p style="padding: 0px 20px;">Use these shortcodes <em>{coach_name} {college_name}      {event_title} {event_description} {event_event_start} {event_event_end} {event_place} {event_city} {event_state} {event_zipcode} {sport_title} {event_division} {event_tournaments} {event_campus_clinics} {event_school_attending}<br/>
<!--         {coach_name}<br/> -->
        {my_name} {my_email} {my_position} {athlete_school} {class_year} {my_phone} {club_name} {club_jersey_number} {gpa} {my_facebook} {my_twitter} {my_instagram} {my_snapchat} {my_skype} {my_whatsapp} {my_videogallery_urls} {my_sport} {my_address} {my_height} {my_weight} {my_birth} {my_academics} {my_club_info} {athletic_achievements} {academic_achievement} {extra_curricular_activity}</em></p>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 21)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>{{ 'Event Notofications' }}
        </h1>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 22)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>{{ 'Subscription Notifications' }}
        </h1>
    @stop
@elseif((isset($dataTypeContent->id)?$dataTypeContent->id:'') != 8)
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
        </h1>
        @include('voyager::multilingual.language-selector')
    @stop
@else
    @section('page_header')
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>{{ 'Chart Titles' }}
        </h1>
    @stop
@endif

@section('content')

@if( !( (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 8 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 17 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 21 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 22 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 20 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 18 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 30 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 31 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 14  && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 16) )
<style type="text/css">
    ol.breadcrumb{
        display: none;
    }
</style>
@endif

 @if( (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 8 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 17
  && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 21 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 22 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 20 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 30 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 31 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 18 && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 14  && (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 16)
    <div class="page-content container-fluid">
     <p><strong>View Link : </strong><a class="url-link" href="/{{$dataTypeContent->slug}}"><?php  echo url('/'); ?>/{{$dataTypeContent->slug}}</a></p>
        <div class="row">
            <div class="col-md-12">
               
                <div class="panel panel-bordered">
                
                    <div class="panel-heading">
                        <h3 class="panel-title">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Add New' }}@endif {{ $dataType->display_name_singular }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
               
                    <form role="form"
                            class="form-edit-add"
                            action="@if(isset($dataTypeContent->id)){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($dataTypeContent->id))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(isset($dataTypeContent->id))
                                <?php $dataTypeRows = $dataType->editRows; ?>
                            @else
                                <?php $dataTypeRows = $dataType->addRows; ?>
                            @endif
                             <div class="col-md-8">
                            @foreach($dataTypeRows as $row)
                            @if($row->order == 2 || $row->order == 3 || $row->order == 4 || $row->order == 5 || $row->order == 6 || $row->order == 7 || $row->order == 8 || $row->order == 9 || $row->order == 10 )



                             @if( ($dataTypeContent->id == 1 && $row->order == 3) ||  ($dataTypeContent->id == 1 && $row->order == 4) || ($dataTypeContent->id == 1 && $row->order == 6) || ($dataTypeContent->id == 1 && $row->order == 7) || ($dataTypeContent->id == 1 && $row->order == 8) || ($dataTypeContent->id == 1 && $row->order == 9) || ($dataTypeContent->id == 1 && $row->order == 10) || ($dataTypeContent->id == 1 && $row->order == 5))



                             @else
                            
                                <div class="form-group @if($row->type == 'hidden') hidden @endif">
                                    @if($row->order == 5)
                                    <h2 class="heading">Banner Section</h2>
                                    @endif
                                     @if($row->order == 8)
                                    <h2 class="heading">CTA Section</h2>
                                    @endif
                                <label for="name">{{ $row->display_name }}</label>
                                @include('voyager::multilingual.input-hidden-bread-edit-add')
                                @if( $row->type == 'image' )
                                    <a href="{{ route('page.delete_column', [$dataTypeContent->id, $row->field]) }}" class="voyager-x"></a>
                                @endif
                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                    {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                @endforeach
                                </div>

                             @endif


                            @endif
                            @endforeach
                            </div>
                            <div class="col-md-4">
                            @foreach($dataTypeRows as $row)
                             @if($row->order >10 )

                              @if( ($dataTypeContent->id == 1 && $row->order == 14))



                                @else
                                <div class="form-group @if($row->type == 'hidden') hidden @endif">
                                <label for="name">{{ $row->display_name }}</label>
                                
                                @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)

                                    
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                </div>

                            @endif 
                            @endif   
                            @endforeach

 </div>                     
                       
                                 <div class="button submit"><button type="submit" class="btn btn-primary save">Save</button></div>
                        

                        </div><!-- panel-body -->
                         @if( $dataTypeContent->id != 1)
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                        @endif
                    </form>
                      
                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>
                  
                </div>
            </div>
        </div>
    </div>
    @endif
    @if( (isset($dataTypeContent->id)?$dataTypeContent->id:'') != 16)

    @if(isset($dataTypeContent->id))
    <!-- ACF Fields -->
    @if( $dataType->display_name_singular == 'Page' || $dataType->display_name_singular == 'Sport' )
    <div class="page-content container-fluid">
        <form action="{{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}" method="POST" enctype="multipart/form-data">
            {{ method_field("PUT") }}
            {{ csrf_field() }}
            <input type="hidden" value="{{$dataTypeContent->id}}" name="page_id" required="required">
            <input type="hidden" name="page_meta_content" value="{{ $dataType->slug }}" />
           
            <div class="panel">
                @if((isset($dataTypeContent->id)?$dataTypeContent->id:'') != 8)
                @if($settings) @foreach($settings as $setting)

                   @if((isset($dataTypeContent->id)?$dataTypeContent->id:'') == 14)
                        <h3 class="heading-template">{{ Voyager::setting($setting->key) }}</h3>
                    @endif
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $setting->display_name }}<code>Voyager::pagemeta('{{ $setting->key }}', '{{$dataTypeContent->id}}')</code>
                        </h3>
                        <div class="panel-actions">
                            <a href="/admin/{{$dataType->slug}}/{{$dataTypeContent->id}}/edit/{{$setting->id}}">
                                <i class="voyager-edit"></i>
                            </a>
                            <i class="voyager-trash"
                               data-id="{{ $setting->id }}"
                               data-display-key="{{ $setting->key }}"
                               data-display-name="{{ $setting->display_name }}"></i>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        @if ($setting->type == "text")
                            <input type="text" class="form-control" name="{{ $setting->key }}" value="{{ $setting->value }}">
                        @elseif($setting->type == "text_area")
                            <textarea class="form-control" name="{{ $setting->key }}">@if(isset($setting->value)){{ $setting->value }}@endif</textarea>
                        @elseif($setting->type == "rich_text_box")
                            <textarea class="form-control richTextBox" name="{{ $setting->key }}">@if(isset($setting->value)){{ $setting->value }}@endif</textarea>
                        @elseif($setting->type == "code_editor")
                            <?php $options = json_decode($setting->details); ?>
                            <div id="{{ $setting->key }}" data-theme="{{ @$options->theme }}" data-language="{{ @$options->language }}" class="ace_editor min_height_400" name="{{ $setting->key }}">@if(isset($setting->value)){{ $setting->value }}@endif</div>
                            <textarea name="{{ $setting->key }}" id="{{ $setting->key }}_textarea" class="hidden">@if(isset($setting->value)){{ $setting->value }}@endif</textarea>
                        @elseif($setting->type == "image" || $setting->type == "file")
                            @if(isset( $setting->value ) && !empty( $setting->value ) && Storage::disk(config('voyager.storage.disk'))->exists($setting->value))
                                <div class="img_settings_container">
                                    <a href="{{ route('pagemeta.delete_value', $setting->id) }}" class="voyager-x"></a>
                                    <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($setting->value) }}" style="width:200px; height:auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                </div>
                                <div class="clearfix"></div>
                            @elseif($setting->type == "file" && isset( $setting->value ))
                                <div class="fileType">{{ $setting->value }}</div>
                            @endif
                            <input type="file" name="{{ $setting->key }}">
                        @elseif($setting->type == "select_dropdown")
                            <?php $options = json_decode($setting->details); ?>
                            <?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
                            <select class="form-control" name="{{ $setting->key }}">
                                <?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                                @if(isset($options->options))
                                    @foreach($options->options as $index => $option)
                                        <option value="{{ $index }}" @if($default == $index && $selected_value === NULL){{ 'selected="selected"' }}@endif @if($selected_value == $index){{ 'selected="selected"' }}@endif>{{ $option }}</option>
                                    @endforeach
                                @endif
                            </select>

                        @elseif($setting->type == "radio_btn")
                            <?php $options = json_decode($setting->details); ?>
                            <?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
                            <?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                            <ul class="radio">
                                @if(isset($options->options))
                                    @foreach($options->options as $index => $option)
                                        <li>
                                            <input type="radio" id="option-{{ $index }}" name="{{ $setting->key }}"
                                                   value="{{ $index }}" @if($default == $index && $selected_value === NULL){{ 'checked' }}@endif @if($selected_value == $index){{ 'checked' }}@endif>
                                            <label for="option-{{ $index }}">{{ $option }}</label>
                                            <div class="check"></div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        @elseif($setting->type == "checkbox")
                            <?php $options = json_decode($setting->details); ?>
                            <?php $checked = (isset($setting->value) && $setting->value == 1) ? true : false; ?>
                            @if (isset($options->on) && isset($options->off))
                                <input type="checkbox" name="{{ $setting->key }}" class="toggleswitch" @if($checked) checked @endif data-on="{{ $options->on }}" data-off="{{ $options->off }}">
                            @else
                                <input type="checkbox" name="{{ $setting->key }}" @if($checked) checked @endif class="toggleswitch">
                            @endif
                        @endif

                    </div>
                    
                    @if(!$loop->last)
                        <hr>
                    @endif
                @endforeach @endif
                @else
                 @if($settings) @foreach($settings as $key=>$setting)
                   
                    <div class="panel-heading">
                        @if($key == 0)
                        <h2> Athlete Titles </h2>
                        @elseif($key == 10 )
                        <h2> Coaches Titles </h2>
                        @endif
                        <h3 class="panel-title">
                            {{ $setting->display_name }}<code>Voyager::pagemeta('{{ $setting->key }}', '{{$dataTypeContent->id}}')</code>
                        </h3>
                        <div class="panel-actions">
                            <i class="voyager-trash"
                               data-id="{{ $setting->id }}"
                               data-display-key="{{ $setting->key }}"
                               data-display-name="{{ $setting->display_name }}"></i>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        @if ($setting->type == "text")
                            <input type="text" class="form-control" name="{{ $setting->key }}" value="{{ $setting->value }}">
                        @elseif($setting->type == "text_area")
                            <textarea class="form-control" name="{{ $setting->key }}">@if(isset($setting->value)){{ $setting->value }}@endif</textarea>
                        @elseif($setting->type == "rich_text_box")
                            <textarea class="form-control richTextBox" name="{{ $setting->key }}">@if(isset($setting->value)){{ $setting->value }}@endif</textarea>
                        @elseif($setting->type == "code_editor")
                            <?php $options = json_decode($setting->details); ?>
                            <div id="{{ $setting->key }}" data-theme="{{ @$options->theme }}" data-language="{{ @$options->language }}" class="ace_editor min_height_400" name="{{ $setting->key }}">@if(isset($setting->value)){{ $setting->value }}@endif</div>
                            <textarea name="{{ $setting->key }}" id="{{ $setting->key }}_textarea" class="hidden">@if(isset($setting->value)){{ $setting->value }}@endif</textarea>
                        @elseif($setting->type == "image" || $setting->type == "file")
                            @if(isset( $setting->value ) && !empty( $setting->value ) && Storage::disk(config('voyager.storage.disk'))->exists($setting->value))
                                <div class="img_settings_container">
                                    <a href="{{ route('voyager.settings.delete_value', $setting->id) }}" class="voyager-x"></a>
                                    <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($setting->value) }}" style="width:200px; height:auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                </div>
                                <div class="clearfix"></div>
                            @elseif($setting->type == "file" && isset( $setting->value ))
                                <div class="fileType">{{ $setting->value }}</div>
                            @endif
                            <input type="file" name="{{ $setting->key }}">
                        @elseif($setting->type == "select_dropdown")
                            <?php $options = json_decode($setting->details); ?>
                            <?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
                            <select class="form-control" name="{{ $setting->key }}">
                                <?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                                @if(isset($options->options))
                                    @foreach($options->options as $index => $option)
                                        <option value="{{ $index }}" @if($default == $index && $selected_value === NULL){{ 'selected="selected"' }}@endif @if($selected_value == $index){{ 'selected="selected"' }}@endif>{{ $option }}</option>
                                    @endforeach
                                @endif
                            </select>

                        @elseif($setting->type == "radio_btn")
                            <?php $options = json_decode($setting->details); ?>
                            <?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
                            <?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                            <ul class="radio">
                                @if(isset($options->options))
                                    @foreach($options->options as $index => $option)
                                        <li>
                                            <input type="radio" id="option-{{ $index }}" name="{{ $setting->key }}"
                                                   value="{{ $index }}" @if($default == $index && $selected_value === NULL){{ 'checked' }}@endif @if($selected_value == $index){{ 'checked' }}@endif>
                                            <label for="option-{{ $index }}">{{ $option }}</label>
                                            <div class="check"></div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        @elseif($setting->type == "checkbox")
                            <?php $options = json_decode($setting->details); ?>
                            <?php $checked = (isset($setting->value) && $setting->value == 1) ? true : false; ?>
                            @if (isset($options->on) && isset($options->off))
                                <input type="checkbox" name="{{ $setting->key }}" class="toggleswitch" @if($checked) checked @endif data-on="{{ $options->on }}" data-off="{{ $options->off }}">
                            @else
                                <input type="checkbox" name="{{ $setting->key }}" @if($checked) checked @endif class="toggleswitch">
                            @endif
                        @endif

                    </div>
                    
                    @if(!$loop->last)
                        <hr>
                    @endif
                @endforeach @endif
                @endif
            </div>
           
            @if( $dataType->display_name_singular != 'Sport' )
                <button type="submit" class="btn btn-primary pull-right">Save Settings</button>
            @endif
        </form>
    
        <div style="clear:both"></div>
        @if( isset( $field_data )  )
        <div class="popup-verlay"></div>
        @endif
        <div class="panel @if( isset( $field_data ) ) popup @endif" style="margin-top:10px;">
            <div class="panel-heading new-setting">
                <hr>
                <h3 class="panel-title"><i class="voyager-plus"></i> New Setting</h3>
            </div>
            <div class="panel-body">
                @if( isset( $field_data )  )
                    <form action="{{ url('admin/page_field_store') }}" method="POST">
                    <input type="hidden" value="{{ $field_data->id }}" name="field"/>
                @else
                    <?php $field_data = array(); $field_data = (object) $field_data; ?>
                    <form action="{{ route('pagemeta.store') }}" method="POST">
                @endif
                    {{ csrf_field() }}
                    <input type="hidden" name="page_meta_content" value="{{ $dataType->slug }}" />
                    <input type="hidden" value="{{$dataTypeContent->id}}" name="page_id" required="required">
                    <div class="col-md-4">
                        <label for="display_name">Name</label>
                        <input type="text" value="@if( isset( $field_data->display_name ) ){{$field_data->display_name}}@endif" class="form-control" name="display_name" placeholder="Setting name ex: Admin Title" required="required">
                    </div>
                    <div class="col-md-4">
                        <label for="key">Key</label>
                        <input type="text" value="@if( isset( $field_data->key ) ) {{ $field_data->key }} @endif" class="form-control" name="key" placeholder="Setting key ex: admin_title" required="required" @if( isset( $field_data->key ) ) {{ 'disabled' }} @endif>
                    </div>
                    <div class="col-md-4">
                        <label for="asdf">Type</label>
                        <select name="type" @if( isset( $field_data->type ) ) {{ 'disabled' }} @endif class="form-control" required="required">
                            <option value="" @if( isset( $field_data->type ) && $field_data->type == '' ) {{ 'selected' }} @endif>Choose type</option>
                            <option value="text" @if( isset( $field_data->type ) && $field_data->type == 'text' ) {{ 'selected' }} @endif>Text Box</option>
                            <option value="text_area" @if( isset( $field_data->type ) && $field_data->type == 'text_area' ) {{ 'selected' }} @endif>Text Area</option>
                            <option value="rich_text_box" @if( isset( $field_data->type ) && $field_data->type == 'rich_text_box' ) {{ 'selected' }} @endif>Rich Textbox</option>
                            <option value="code_editor" @if( isset( $field_data->type ) && $field_data->type == 'code_editor' ) {{ 'selected' }} @endif>Code Editor</option>
                            <option value="checkbox" @if( isset( $field_data->type ) && $field_data->type == 'checkbox' ) {{ 'selected' }} @endif>Check Box</option>
                            <option value="radio_btn" @if( isset( $field_data->type ) && $field_data->type == 'radio_btn' ) {{ 'selected' }} @endif>Radio Button</option>
                            <option value="select_dropdown" @if( isset( $field_data->type ) && $field_data->type == 'select_dropdown' ) {{ 'selected' }} @endif>Select Dropdown</option>
                            <option value="file" @if( isset( $field_data->type ) && $field_data->type == 'file' ) {{ 'selected' }} @endif>File</option>
                            <option value="image" @if( isset( $field_data->type ) && $field_data->type == 'image' ) {{ 'selected' }} @endif>Image</option>
                        </select>
                    </div>
                    <div style="clear:both"></div>
                    @if( !isset( $field_data->type ) )
                    <div class="col-md-12">
                        <a id="toggle_options"><i class="voyager-double-down"></i> OPTIONS</a>
                        <div class="new-settings-options">
                            <label for="options">Options
                                <small>(optional, only applies to certain types like dropdown box or radio button)
                                </small>
                            </label>
                            <div id="options_editor" class="form-control min_height_200" data-language="json"></div>
                            <textarea id="options_textarea" name="details" class="hidden"></textarea>
                            <div id="valid_options" class="alert-success alert" style="display:none">Valid Json</div>
                            <div id="invalid_options" class="alert-danger alert" style="display:none">Invalid Json</div>
                        </div>
                    </div>
                    @endif
                    <script>
                        jQuery('document').ready(function ($) {
                            $('#toggle_options').click(function () {
                                $('.new-settings-options').toggle();
                                if ($('#toggle_options .voyager-double-down').length) {
                                    $('#toggle_options .voyager-double-down').removeClass('voyager-double-down').addClass('voyager-double-up');
                                } else {
                                    $('#toggle_options .voyager-double-up').removeClass('voyager-double-up').addClass('voyager-double-down');
                                }
                            });
                        });
                    </script>
                    <div style="clear:both"></div>
                    <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right new-setting-btn">
                        @if( isset( $field_data ) )
                            <i class="voyager-plus"></i> Save Setting
                        @else
                            <i class="voyager-plus"></i> Add New Setting
                        @endif
                    </button>
                    </div>
                    <div style="clear:both"></div>
                </form>
            </div>
        </div>
    </div>
    <!-- -->
    @endif
    @endif
    </div>
    
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Are You Sure</h4>
                </div>

                <div class="modal-body">
                    <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Are you sure you want to delete the <span id="delete_setting_title"></span> Setting?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.destroy', $dataTypeContent->id) }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="hidden" name="destroy_page_meta" value="destroy" />
                        <input type="hidden" id="destroy_id" name="destroy_id" value="" />
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Yes, Delete This Setting">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
    <script>
        jQuery('document').ready(function ($) {
            $('.voyager_banner_delete').click(function (e) {
                e.preventDefault();
                var valueid = $(this).attr('id');
                $(".imagedelete").empty();
                $.ajax({
                type: 'GET',
                url: '/admin/delete/banner/'+valueid,
                //data: 'id=' +valueid,
                success: function(data) {
                    console.log(data);
                },
                error: function (error) {
                     console.log(error);
                }
            });   
            });
            $('.voyager-trash').click(function () {
                var display = $(this).data('display-name') + '/' + $(this).data('display-key');
                $('#destroy_id').val( $(this).attr('data-id') );
                $('#delete_setting_title').text(display);
                $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(this).data('id'));
                $('#delete_modal').modal('show');
            });

            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataTypeContent->getTable() }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    @if($isModelTranslatable)
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
    <script src="{{ voyager_asset('lib/js/ace/ace.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_ace_editor.js') }}"></script>
    <script src="{{ voyager_asset('js/slugify.js') }}"></script>
    <script>
        var options_editor = ace.edit('options_editor');
        options_editor.getSession().setMode("ace/mode/json");

        var options_textarea = document.getElementById('options_textarea');
        options_editor.getSession().on('change', function() {
            console.log(options_editor.getValue());
            options_textarea.value = options_editor.getValue();
        });
    </script>
@stop
@else
   
    @if(isset($dataTypeContent->id))
    <!-- ACF Fields -->
    @if( $dataType->display_name_singular == 'Page' || $dataType->display_name_singular == 'Sport' )
    <div class="page-content container-fluid">
        <form action="{{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}" method="POST" enctype="multipart/form-data">
            {{ method_field("PUT") }}
            {{ csrf_field() }}
            <input type="hidden" value="{{$dataTypeContent->id}}" name="page_id" required="required">
            <input type="hidden" name="page_meta_content" value="{{ $dataType->slug }}" />
            <div class="panel">
                @if($settings) @foreach($settings as $setting)
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $setting->display_name }}  <code>Voyager::pagemeta('{{ $setting->key }}', '{{$dataTypeContent->id}}')</code>     {{ $setting->field_type }}
                        </h3>
                        <div class="panel-actions">
                            <a href="/admin/{{$dataType->slug}}/{{$dataTypeContent->id}}/edit/{{$setting->id}}">
                                <i class="voyager-edit"></i>
                            </a>
                            <i class="voyager-trash"
                               data-id="{{ $setting->id }}"
                               data-display-key="{{ $setting->key }}"
                               data-display-name="{{ $setting->display_name }}"></i>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        @if ($setting->type == "text")
                            {{ $setting->type }}
                        @elseif($setting->type == "text_area")
                            {{ $setting->type }}
                        @elseif($setting->type == "instruction")
                            {{ $setting->type }}
                        @elseif($setting->type == "image")
                            {{ $setting->type }}
                        @elseif($setting->type == "select_dropdown")
                            {{ $setting->type }} {{ $setting->details }}
                        @elseif($setting->type == "radio_btn")
                            {{ $setting->type }} {{ $setting->details }}
                        @elseif($setting->type == "checkbox")
                            {{ $setting->type }} {{ $setting->details }}
                        @elseif($setting->type == "slider")
                            {{ "Range Selector" }} {{ $setting->details }}
                        @endif

                    </div>
                    
                    @if(!$loop->last)
                        <hr>
                    @endif
                @endforeach @endif
            </div>
            @if( $dataType->display_name_singular != 'Sport' )
                <button type="submit" class="btn btn-primary pull-right">Save Settings</button>
            @endif
        </form>
    
        <div style="clear:both"></div>
        @if( isset( $field_data )  )
        <div class="popup-verlay"></div>
        @endif
        <div class="panel @if( isset( $field_data ) ) popup @endif" style="margin-top:10px;">
            <div class="panel-heading new-setting">
                <hr>
                <h3 class="panel-title"><i class="voyager-plus"></i> New Setting</h3>
            </div>
            <div class="panel-body">
                @if( isset( $field_data )  )
                    <form action="{{ route('pagemeta.store') }}" method="POST">
                    <input type="hidden" value="{{ $field_data->id }}" name="field"/>
                @else
                    <?php $field_data = array(); $field_data = (object) $field_data; ?>
                    <form action="{{ route('pagemeta.store') }}" method="POST">
                @endif
                    {{ csrf_field() }}
                    <input type="hidden" name="page_meta_content" value="{{ $dataType->slug }}" />
                    <input type="hidden" value="{{$dataTypeContent->id}}" name="page_id" required="required">
                   
                    <div class="col-md-4">
                        <label for="display_name">Name</label>
                        <input type="text" value="@if( isset( $field_data->display_name ) ) {{ $field_data->display_name }} @endif" class="form-control" name="display_name" placeholder="Setting name ex: Admin Title" required="required">
                    </div>
                    <div class="col-md-4">
                        <label for="key">Key</label>
                         <input type="hidden" value="@if( isset( $field_data->key ) ) {{ $field_data->key }} @endif" class="form-control" name="key" placeholder="Setting key ex: admin_title" required="required" >
                        <input type="text" value="@if( isset( $field_data->key ) ) {{ $field_data->key }} @endif" class="form-control" name="key" placeholder="Setting key ex: admin_title" required="required" @if( isset( $field_data->key ) ) {{ 'disabled' }} @endif>
                    </div>
                    <div class="col-md-4">
                        <label for="asdf">Type</label>
                        <select name="type" @if( isset( $field_data->type ) ) {{ 'disabled' }} @endif class="form-control" required="required">
                            <option value="" @if( isset( $field_data->type ) && $field_data->type == '' ) {{ 'selected' }} @endif>Choose type</option>
                            <option value="text" @if( isset( $field_data->type ) && $field_data->type == 'text' ) {{ 'selected' }} @endif>Text Box</option>
                            <option value="instruction" @if( isset( $field_data->type ) && $field_data->type == 'instruction' ) {{ 'selected' }} @endif>Simple Instruction( No Input )</option>
                            <option value="slider" @if( isset( $field_data->type ) && $field_data->type == 'slider' ) {{ 'selected' }} @endif>Number Slider</option>
                            <option value="text_area" @if( isset( $field_data->type ) && $field_data->type == 'text_area' ) {{ 'selected' }} @endif>Text Area</option>
                            <!--<option value="rich_text_box">Rich Textbox</option>-->
                            <!--<option value="code_editor">Code Editor</option>-->
                            <option value="checkbox" @if( isset( $field_data->type ) && $field_data->type == 'checkbox' ) {{ 'selected' }} @endif>Check Box</option>
                            <option value="radio_btn" @if( isset( $field_data->type ) && $field_data->type == 'radio_btn' ) {{ 'selected' }} @endif>Radio Button</option>
                            <option value="select_dropdown" @if( isset( $field_data->type ) && $field_data->type == 'select_dropdown' ) {{ 'selected' }} @endif>Select Dropdown</option>
                            <option value="multi_dropdown" @if( isset( $field_data->type ) && $field_data->type == 'multi_dropdown' ) {{ 'selected' }} @endif>Multi-Select Dropdown</option>
                            <!--<option value="file">File</option>-->
                            <option value="image">Image</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <a id="toggle_options"><i class="voyager-double-down"></i> OPTIONS</a>
                        <div class="new-settings-options">
                            <label for="options">Options
                                <small>(optional, only applies to certain types like dropdown box or radio button)
                                </small>
                            </label>
                            <div id="options_editor" class="form-control min_height_200" data-language="json"></div>
                            <textarea id="options_textarea" name="details" class="hidden">@if( isset( $field_data->details ) ) {{ $field_data->details }} @endif</textarea>
                            <div id="valid_options" class="alert-success alert" style="display:none">Valid Json</div>
                            <div id="invalid_options" class="alert-danger alert" style="display:none">Invalid Json</div>
                        </div>
                    </div>
                    <script>
                        jQuery('document').ready(function ($) {
                            $('#toggle_options').click(function () {
                                $('.new-settings-options').toggle();
                                if ($('#toggle_options .voyager-double-down').length) {
                                    $('#toggle_options .voyager-double-down').removeClass('voyager-double-down').addClass('voyager-double-up');
                                } else {
                                    $('#toggle_options .voyager-double-up').removeClass('voyager-double-up').addClass('voyager-double-down');
                                }
                            });
                        });
                    </script>
                    <div style="clear:both"></div>
                    <button type="submit" class="btn btn-primary pull-right new-setting-btn">
                        @if( isset( $field_data ) )
                            <i class="voyager-plus"></i> Save Setting
                        @else
                            <i class="voyager-plus"></i> Add New Setting
                        @endif
                    </button>
                    <div style="clear:both"></div>
                
                </form>
            </div>
        </div>
    </div>
    <!-- -->
    @endif
    @endif
    </div>
    
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Are You Sure</h4>
                </div>

                <div class="modal-body">
                    <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Are you sure you want to delete the <span id="delete_setting_title"></span> Setting?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.destroy', $dataTypeContent->id) }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="hidden" name="destroy_page_meta" value="destroy" />
                        <input type="hidden" id="destroy_id" name="destroy_id" value="" />
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Yes, Delete This Setting">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
    <script>
        jQuery('document').ready(function ($) {
            $('.voyager-trash').click(function () {
                var display = $(this).data('display-name') + '/' + $(this).data('display-key');
                $('#destroy_id').val( $(this).attr('data-id') );
                $('#delete_setting_title').text(display);
                $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(this).data('id'));
                $('#delete_modal').modal('show');
            });

            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataTypeContent->getTable() }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    @if($isModelTranslatable)
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
    <script src="{{ voyager_asset('lib/js/ace/ace.js') }}"></script>
    <script src="{{ voyager_asset('js/voyager_ace_editor.js') }}"></script>
    <script src="{{ voyager_asset('js/slugify.js') }}"></script>
    <script>
        var options_editor = ace.edit('options_editor');
        options_editor.getSession().setMode("ace/mode/json");
        <?php if( isset( $field_data->details ) ): ?>
            options_editor.setValue(JSON.stringify( <?php echo $field_data->details; ?> ));
        <?php endif; ?>
        var options_textarea = document.getElementById('options_textarea');
        options_editor.getSession().on('change', function() {
            console.log(options_editor.getValue());
            options_textarea.value = options_editor.getValue();
        });
    </script>
@stop


@endif