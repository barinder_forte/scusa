<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
        a.button, button {
            background: linear-gradient(to right, #fd3542, #fd7647) !important;
            color: #fff !important;
            border: none !important;
            padding: 10px 30px;
            box-shadow: 0 0 !important;
            border-radius: 20px !important;
        }
        .wrapper{
            background: transparent;
        }
        .header{
            background: #fff;
        }
        .footer {
            background: #fd3542;
            width: 600px !important;
        }
        .footer p, .footer a{
            color: #fff !important;
        }
        p{
            word-break: break-all;
        }
    </style>

    <div class="wrapper" style="max-width: 600px; margin: 0 auto;font-family: arial; font-size: 14px;">
        {{ $header or '' }}
        
        <!-- Email Body -->
       <div class="main-body" style="background: #f1f1f1; padding: 20px; border: 1px solid #e4e4e4;">
            {{ Illuminate\Mail\Markdown::parse($slot) }}
            {{ $subcopy or '' }}
        </div>
        
        {{ $footer or '' }}
    </div>
    
</body>
</html>
