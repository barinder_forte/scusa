<div class="footer" style="text-align: center;background: #f65954;color: #fff; padding: 13px 0;">
	<p style="margin: 0;">&copy; {{ date('Y') }} <a href="{{ env('APP_URL') }}">{{ env('APP_NAME') }}</a></p>
</div>