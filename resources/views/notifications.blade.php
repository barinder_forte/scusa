@extends('layouts.signup')
@section('content')
<?php

  function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }

?>
	<div class="main-container main-inner">
		<section class="grey-bg">
			<div class="container">
				@if($total > 0 || isset($_GET['_token']) )
				<div class="col-md-8 padding-right">
				@else
				<div class="col-md-12 padding-right">
				@endif
				@if($total > 0 || isset($_GET['_token']) )
				<div class="atheletes-heading">
					<div class="row">
						<div class="col-md-8">
							<h2> All Notifications ({{$total}})
							</h2>
						</div>
						@if( $total > 0  )
						<div class="col-md-4 text-right">
								<?php 
									$userIdsArray1 = array();
									$emails = array();
									foreach($notifications as $key=>$noti){
										$userInfo = \Voyager::UserInformation('all', $noti->viewer_user_id );
										if($noti->Viewer->role_id != 4){
											$userName = (isset($userInfo['athelete_firstname'])?$userInfo['athelete_firstname']:'').' '.str_replace("'", '',isset($userInfo['athelete_lastname'])?$userInfo['athelete_lastname']:'');
											$useremail = str_replace("'", '',isset($userInfo['athelete_email'])?$userInfo['athelete_email']:'');
										}else{
											$userName = (isset($userInfo['coach_firstname'])?$userInfo['coach_firstname']:'').' '.str_replace("'", '',isset($userInfo['coach_lastname'])?$userInfo['coach_lastname']:'');
											$useremail = str_replace("'", '',isset($userInfo['coach_email'])?$userInfo['coach_email']:'');
										}
										if(!in_array($useremail,$emails)){
											$userIdsArray1[$key]['userid'] = $useremail ;
											$userIdsArray1[$key]['username'] = $userName;
											$emails[$key] = $useremail ;
										}
									}
									$userIdsArray = json_encode($userIdsArray1);
									?>
								
								<a @click.prevent='showemail( <?php echo $userIdsArray; ?>)'  class="btn <?php if( ( (Auth::user()->role_id == 2 || Auth::user()->role_id == 3) && ((!Auth::user()->subscribed('main')) && Auth::user()->is_paid != 'yes') ) || ( Auth::user()->user_approved == 'INACTIVE' && Auth::user()->role_id == 4) ){ echo 'light addblurrClass'; }else{ echo 'orange'; } ?> emailAllEvent">email all</a>	
						</div>
						@endif
					</div>
					</div>
					@endif
					<div class="events-section notification-main">
					@if($total > 0 || isset($_GET['_token']) )
						@if($total > 0 )
						@foreach($notifications as $noti)
						<?php if($noti->is_read == 1) { $class = 'notification_read'; }
						else{ $class = 'notification_unread';} ?>
						<div class="events-content {{$class}}">
							<div class="events-data">
								<h3>
								<?php if($noti->Viewer->role_id == 4){
		                			$type = 'coach';
		                		}else{
		                			$type = 'athlete';
		                		}
                				$userInfo2 = \Voyager::UserInformation('all', $noti->viewer_user_id );
								if($type == 'athlete'){
									$userName = (isset($userInfo2['athelete_firstname'])?$userInfo2['athelete_firstname']:'').' '.str_replace("'", '',isset($userInfo2['athelete_lastname'])?$userInfo2['athelete_lastname']:'');
									$useremail = str_replace("'", '',isset($userInfo2['athelete_email'])?$userInfo2['athelete_email']:'');
								}else{
									$userName = (isset($userInfo2['coach_firstname'])?$userInfo2['coach_firstname']:'').' '.str_replace("'", '',isset($userInfo2['coach_lastname'])?$userInfo2['coach_lastname']:'');
									$useremail = str_replace("'", '',isset($userInfo2['coach_email'])?$userInfo2['coach_email']:'');
								}
								?>

									<a href="/notification/read/{{$noti->Viewer->user_slug}}/{{$noti->id}}">{{$userName}} </a>viewed your profile on 
									{{date('F',strtotime($noti->created_at))}} {{addOrdinalNumberSuffix(date('d',strtotime($noti->created_at))).","}} 
									{{date('Y',strtotime($noti->created_at))}}

								</h3>
							</div>
							<div class="events-data">
								<p>
								<?php  	
									$userIdsArf = '';
									$userIdsAr = array(); 
											$userIdsAr[0]['userid'] = $useremail;
												$userIdsAr[0]['username'] = $userName;
											$userIdsArf = json_encode($userIdsAr);
										?>	
									
								<a href="/notification/read/{{$noti->Viewer->user_slug}}/{{$noti->id}}">See Profile</a>
									<a  @click.prevent='showemail( <?php echo $userIdsArf; ?>)' href="#" id='show_login' class="send_email_event" >Send Email</a>	
									<a class="deletenotificationclass" href="/notification/delete/{{$noti->id}}" id="{{$noti->id}}"><i title="Delete Notification" class="fa fa-trash "></i></a>
								</p>
							</div>
						</div>
						@endforeach
						{!! $notifications->appends(Request::except('page'))->render() !!}
						@else
						<div class="atheletes-heading noresultfound noschool">
							<div class="row">
								<div class="col-md-8">
									<h2>No Result Found</h2>
								</div> 
							<div class="col-md-4 text-right">	
							</div>
							</div>
						</div>
						@endif
						
					@else

					@if( \Auth::user()->role_id == 4 )

						<div class="atheletes-heading nonotification noresultfound no-school-found"><div class="row"><div class="col-md-8 no resultfoundnoti">
						<h4>No one has viewed your profile just yet. Follow these steps to become viewed!</h4>
						<ul><li>- Fill out your profile fully</li>
						<li>- Join <a href="/events">Events</a> you will be taking part of</li>
						<li>- View <a href="/athletes">Athlete</a> you are interested in</li>
						<li>- Reach out to Athlete and send an email</li>
						</ul>
						</div> <div class="col-md-4 text-right"></div></div>
						</div>

					@else

						<div class="atheletes-heading nonotification noresultfound no-school-found"><div class="row"><div class="col-md-8 no resultfoundnoti">
						<h4>No one has viewed your profile just yet. Follow these steps to become viewed!</h4>
						<ul><li>- Fill out your profile fully</li>
						<li>- Join <a href="/events">Events</a> you will be taking part of</li>
						<li>- View <a href="/colleges">colleges</a> you are interested in</li>
						<li>- Reach out to coaches and send an email</li>
						</ul>
						</div> <div class="col-md-4 text-right"></div></div>
						</div>
					@endif
					@endif
				</div>
			</div>

				
			@if($total > 0 || isset($_GET['_token']))	
			@if( Auth::user() )
					@if( \Auth::user()->role_id == 4 )
			<div class="col-md-4 col-sm-12 right-content">
					<div class="right-box">
						<h1>filter by</h1>
						<form class="filter-form" action='/notification' method='GET'>
							{{csrf_field() }}
							<div class="row">
                                <div class="col-lg-12 col-xs-12 ">
                                    <label>Athlete Name</label>
									<input class="js-example-basic-single athlete_name" type="text" name='athlete_name' id='athlete_name' value="{{isset( $_GET['athlete_name'] )?$_GET['athlete_name']:''}}">
                                </div>
                            </div>
							<!-- <label>Sports Playing</label>
							<select id='sportsFilter' name='sportsFilter'  class="js-example-basic-single sportsFilter">
								<option value=''>Select Sport</option>
								@foreach($allSports as $s)
								<option <?PHP //if((isset($_GET['sportsFilter'])?$_GET['sportsFilter']:'')== $s->id) echo "selected";?> value='{{$s->id}}'>{{$s->title}}</option>
								@endforeach
							</select> -->
							<label>Year Graduating</label>
							<select id='graduationFilter' name='graduationFilter'  class="js-example-basic-single graduationFilter">
								<option value=''>Select Year</option>
								<option <?PHP if((isset($_GET['graduationFilter'])?$_GET['graduationFilter']:'')== '2017') echo "selected";?> value='2017'>2017</option>
								<option <?PHP if((isset($_GET['graduationFilter'])?$_GET['graduationFilter']:'') == "2018") echo "selected";?> value='2018'>2018</option>
								<option <?PHP if((isset($_GET['graduationFilter'])?$_GET['graduationFilter']:'') == "2019") echo "selected";?> value='2019'>2019</option>
								<option <?PHP if((isset($_GET['graduationFilter'])?$_GET['graduationFilter']:'') == "2020") echo "selected";?> value='2020'>2020</option>
								<option <?PHP if((isset($_GET['graduationFilter'])?$_GET['graduationFilter']:'') == "2021") echo "selected";?> value='2021'>2021</option>
							</select>

							<label>Position Played</label>
							<select name="position" id="playedpos" class="js-example-basic-single">
								<option value="">Select Position</option>
								<option value="other">other</option>
							</select>

							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<label>GPA</label>
									<select name='gpa' id='gpa'  class="js-example-basic-single">
										<option  value=''>Select GPA</option>
										<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="1") echo "selected";?> value='1'> >= 1</option>
										<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="2") echo "selected";?> value='2'> >= 2</option>
										<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="3") echo "selected";?> value='3'> >= 3</option>
										<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="4") echo "selected";?> value='4'> >= 4</option>
										<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="5") echo "selected";?> value='5'>5</option>
									</select>
								</div>

								<div class="col-lg-6 col-xs-12">
									<label>ACT</label>
									<select name='act' id='act' class="js-example-basic-single">
										<option  value=''>Select ACT</option>
										@for($i = 1; $i <= 36 ; $i++)
										@if($i <= 35 )
											<option <?php if((isset($act)?$act:'')== $i) echo "selected";?> value='{{$i}}'> >={{$i}} </option>
											@else
											<option <?php if((isset($act)?$act:'')== $i) echo "selected";?> value='{{$i}}'> {{$i}} </option>
										@endif
										@endfor
									</select>
								</div>
							</div>

							<label style="display: none">Gender</label>
							<select style="display: none" id='genderFilter' name='genderFilter'  class="js-example-basic-single genderFilter">
								<option  value=''>Select Gender</option>
								<option <?PHP if((isset($_GET['genderFilter'])?$_GET['genderFilter']:'')=="Men") echo "selected";?> value='Men'>Male</option>
								<option <?PHP if((isset($_GET['genderFilter'])?$_GET['genderFilter']:'')=="Women") echo "selected";?> value='Women'>Female</option>
							</select>

							<label>State</label>
								<select  id='stateFilter' name='stateFilter'  class="js-example-basic-single stateFilter" >
									<option value=''>Select State</option>
									<option value="AL" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="AL") echo "selected";?>>Alabama</option>
									<option value="AK" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="AK") echo "selected";?>>Alaska</option>
									<option value="AZ" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="AZ") echo "selected";?>>Arizona</option>
									<option value="AR" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="AR") echo "selected";?>>Arkansas</option>
									<option value="CA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="CA") echo "selected";?>>California</option>
									<option value="CO" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="CO") echo "selected";?>>Colorado</option>
									<option value="CT" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="CT") echo "selected";?>>Connecticut</option>
									<option value="DE" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="DE") echo "selected";?>>Delaware</option>
									<option value="DC" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="DC") echo "selected";?>>District of Columbia</option>
									<option value="FL" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="FL") echo "selected";?>>Florida</option>
									<option value="GA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="GA") echo "selected";?>>Georgia</option>
									<option value="HI" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="HI") echo "selected";?>>Hawaii</option>
									<option value="ID" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="ID") echo "selected";?>>Idaho</option>
									<option value="IL" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="IL") echo "selected";?>>Illinois</option>
									<option value="IN" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="IN") echo "selected";?>>Indiana</option>
									<option value="IA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="IA") echo "selected";?>>Iowa</option>
									<option value="KS" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="KS") echo "selected";?>>Kansas</option>
									<option value="KY" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="KY") echo "selected";?>>Kentucky</option>
									<option value="LA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="LA") echo "selected";?>>Louisiana</option>
									<option value="ME" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="ME") echo "selected";?>>Maine</option>
									<option value="MD" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="MD") echo "selected";?>>Maryland</option>
									<option value="MA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="MA") echo "selected";?>>Massachusetts</option>
									<option value="MI" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="MI") echo "selected";?>>Michigan</option>
									<option value="MN" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="MN") echo "selected";?>>Minnesota</option>
									<option value="MS" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="MS") echo "selected";?>>Mississippi</option>
									<option value="MO" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="MO") echo "selected";?>>Missouri</option>
									<option value="MT" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="MT") echo "selected";?>>Montana</option>
									<option value="NE" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="NE") echo "selected";?>>Nebraska</option>
									<option value="NV" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="NV") echo "selected";?>>Nevada</option>
									<option value="NH" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="NH") echo "selected";?>>New Hampshire</option>
									<option value="NJ" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="NJ") echo "selected";?>>New Jersey</option>
									<option value="NM" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="NM") echo "selected";?>>New Mexico</option>
									<option value="NY" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="NY") echo "selected";?>>New York</option>
									<option value="NC" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="NC") echo "selected";?>>North Carolina</option>
									<option value="ND" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="ND") echo "selected";?>>North Dakota</option>
									<option value="OH" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="OH") echo "selected";?>>Ohio</option>
									<option value="OK" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="OK") echo "selected";?>>Oklahoma</option>
									<option value="OR" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="OR") echo "selected";?>>Oregon</option>
									<option value="PA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="PA") echo "selected";?>>Pennsylvania</option>
									<option value="RI" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="RI") echo "selected";?>>Rhode Island</option>
									<option value="SC" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="SC") echo "selected";?>>South Carolina</option>
									<option value="SD" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="SD") echo "selected";?>>South Dakota</option>
									<option value="TN" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="TN") echo "selected";?>>Tennessee</option>
									<option value="TX" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="TX") echo "selected";?>>Texas</option>
									<option value="UT" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="UT") echo "selected";?>>Utah</option>
									<option value="VT" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="VT") echo "selected";?>>Vermont</option>
									<option value="VA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="VA") echo "selected";?>>Virginia</option>
									<option value="WA" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="WA") echo "selected";?>>Washington</option>
									<option value="WV" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="WV") echo "selected";?>>West Virginia</option>
									<option value="WI" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="WI") echo "selected";?>>Wisconsin</option>
									<option value="WY" <?PHP if((isset($_GET['stateFilter'])?$_GET['stateFilter']:'')=="WY") echo "selected";?>>Wyoming</option>
								</select>


							<!-- <div class="row">
                                <div class="col-lg-6 col-xs-12">
                                    <label>height</label>

								<input type="hidden" name="heightFilter" value="<?php // echo isset( $_GET['heightFilter'] ) && $_GET['heightFilter'] != '' ? str_replace('lbs','',$_GET['heightFilter']) : '36-36'; ?>"
								 id="amount2" readonly style="border:0; color:#f6931f; font-weight:bold;">


								<p id="result"><span class="min"></span> - <span class="max"></span></p>
								<div id="slider"></div>


                                </div>

                                <div class="col-lg-6 col-xs-12 lbsplace">
                                    <label>Weight</label>
                                    
									<input type="text" value="<?php // echo isset( $_GET['weightFilter'] ) && $_GET['weightFilter'] != '' ? str_replace('lbs','',$_GET['weightFilter']) : '0-0'; ?>" name="weightFilter" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">

									<div id="slider-range"></div>
                                </div>
                            </div> -->
							<div class="clearfix">&nbsp;</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<input type="submit" class='btn btn-info' name="submit" value='Submit'>
								</div>
								<div class="col-lg-6 col-xs-12">
									<a  href='/notifications/' class='btn btn-info orange' >Clear</a>
								</div>
							</div>
						</form>
						<!-- @if($total > 0 )

						<a @click.prevent='showemail( <?php //echo $userIdsArray; ?>)'  class="emailAllEvent">
						<button class="btn orange">	
						Email These Users</a>

						@endif -->

					</div>
				</div>
				@else

					<div class="col-md-4 col-sm-12 right-content">
					<div class="right-box">
						<h1>filter by</h1>
						<form class="filter-form"  action='/notification' method="GET">
                          {{csrf_field()}}
                            <label>State</label>
                            
                                <select  <?php if(isset($_GET['state'])){ echo 'autofocus'; } ?>   id='state' name='state'  class="js-example-basic-single state" >
                                    <option value=''>Select State</option>
                                    <option value="AL" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AL") echo "selected";?>>Alabama</option>
                                    <option value="AK" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AK") echo "selected";?>>Alaska</option>
                                    <option value="AZ" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AZ") echo "selected";?>>Arizona</option>
                                    <option value="AR" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AR") echo "selected";?>>Arkansas</option>
                                    <option value="CA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="CA") echo "selected";?>>California</option>
                                    <option value="CO" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="CO") echo "selected";?>>Colorado</option>
                                    <option value="CT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="CT") echo "selected";?>>Connecticut</option>
                                    <option value="DE" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="DE") echo "selected";?>>Delaware</option>
                                    <option value="DC" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="DC") echo "selected";?>>District of Columbia</option>
                                    <option value="FL" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="FL") echo "selected";?>>Florida</option>
                                    <option value="GA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="GA") echo "selected";?>>Georgia</option>
                                    <option value="HI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="HI") echo "selected";?>>Hawaii</option>
                                    <option value="ID" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="ID") echo "selected";?>>Idaho</option>
                                    <option value="IL" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="IL") echo "selected";?>>Illinois</option>
                                    <option value="IN" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="IN") echo "selected";?>>Indiana</option>
                                    <option value="IA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="IA") echo "selected";?>>Iowa</option>
                                    <option value="KS" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="KS") echo "selected";?>>Kansas</option>
                                    <option value="KY" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="KY") echo "selected";?>>Kentucky</option>
                                    <option value="LA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="LA") echo "selected";?>>Louisiana</option>
                                    <option value="ME" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="ME") echo "selected";?>>Maine</option>
                                    <option value="MD" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MD") echo "selected";?>>Maryland</option>
                                    <option value="MA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MA") echo "selected";?>>Massachusetts</option>
                                    <option value="MI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MI") echo "selected";?>>Michigan</option>
                                    <option value="MN" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MN") echo "selected";?>>Minnesota</option>
                                    <option value="MS" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MS") echo "selected";?>>Mississippi</option>
                                    <option value="MO" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MO") echo "selected";?>>Missouri</option>
                                    <option value="MT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MT") echo "selected";?>>Montana</option>
                                    <option value="NE" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NE") echo "selected";?>>Nebraska</option>
                                    <option value="NV" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NV") echo "selected";?>>Nevada</option>
                                    <option value="NH" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NH") echo "selected";?>>New Hampshire</option>
                                    <option value="NJ" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NJ") echo "selected";?>>New Jersey</option>
                                    <option value="NM" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NM") echo "selected";?>>New Mexico</option>
                                    <option value="NY" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NY") echo "selected";?>>New York</option>
                                    <option value="NC" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NC") echo "selected";?>>North Carolina</option>
                                    <option value="ND" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="ND") echo "selected";?>>North Dakota</option>
                                    <option value="OH" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="OH") echo "selected";?>>Ohio</option>
                                    <option value="OK" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="OK") echo "selected";?>>Oklahoma</option>
                                    <option value="OR" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="OR") echo "selected";?>>Oregon</option>
                                    <option value="PA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="PA") echo "selected";?>>Pennsylvania</option>
                                    <option value="RI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="RI") echo "selected";?>>Rhode Island</option>
                                    <option value="SC" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="SC") echo "selected";?>>South Carolina</option>
                                    <option value="SD" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="SD") echo "selected";?>>South Dakota</option>
                                    <option value="TN" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="TN") echo "selected";?>>Tennessee</option>
                                    <option value="TX" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="TX") echo "selected";?>>Texas</option>
                                    <option value="UT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="UT") echo "selected";?>>Utah</option>
                                    <option value="VT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="VT") echo "selected";?>>Vermont</option>
                                    <option value="VA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="VA") echo "selected";?>>Virginia</option>
                                    <option value="WA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WA") echo "selected";?>>Washington</option>
                                    <option value="WV" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WV") echo "selected";?>>West Virginia</option>
                                    <option value="WI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WI") echo "selected";?>>Wisconsin</option>
                                    <option value="WY" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WY") echo "selected";?>>Wyoming</option>
                                </select>
                            <label>Division</label>
                            <?php $divisionss = \Voyager::pagemetadetail('Division', '16');
				    		$divisionss = json_decode($divisionss);
						   	$divisionss = $divisionss->options;
						    if(isset($_GET['division'])){ 
                            	 $division = $_GET['division'];
                            }
						  		?>
							<select class="js-example-basic-single" name="division" >
								 <option  value="">Select Division</option>
								 <?php foreach($divisionss as $key => $div){ ?>
								 	<option <?php if((isset($division)?$division:'') == $div){ echo 'selected';} ?> value="<?php echo $div; ?>"><?php echo $key; ?></option>
								 <?php } ?>
							</select>
	                       	<div class="col-md-6">  
	                            <input type="submit" class='btn btn-info' name="submit" value='Submit'>
	                       	</div>
	                       	<div class="col-md-6">  
	                            <a class='btn orange' href="/notifications" >Clear</a>
	                        </div>
	                    </form>
					</div>
				</div>
				@endif
				@endif
					@endif
			</div>
		</section>
	</div>
	@if( Auth::user() )
					@if( \Auth::user()->role_id == 4 )
	<?php if(isset($_GET['position'])){

$postion = $_GET['position'];
}else{

$postion = '';

}
$found = \App\UserInformation::where('user_id', '=', \Auth::user()->id)->where('meta_key', '=', 'sport')->first();
if($found->meta_value){
$positionsport = $found->meta_value; 
}else{
$positionsport = ''; 
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script >
$(document).ready(function() {
		 
		
		$( window ).on( "load", function() {
						console.log($( "#sportsFilter option:selected" ).val());
				var vald = '<?php echo $postion; ?>';
				$.ajax({
					type: 'GET',
					url: '/spouser/position',
					data: 'id=<?php echo $positionsport; ?>',
					success: function(data) {
						var parsed = JSON.parse(data);
						var arr = [];
						$('#playedpos').html('');
						$('#playedpos').append('<option value="">Select Position</option>');

						for(var x in parsed){
							$('#playedpos').append('<option value="' + x + '">' + parsed[x] + '</option>');
						}

						$('#playedpos').append('<option value="other">other</option>');
						$('#playedpos option[value="'+vald+'"]').attr('selected','selected');
					},
	                error: function (error) {
	                    $('#playedpos').html('');
						$('#playedpos').append('<option value="">Select Position</option>');
	                }
				});
			
		});

		$('#sportsFilter').on('change', function() {
			$.ajax({
				type: 'GET',
				url: '/spouser/position',
				data: 'id=' +this.value,
				success: function(data) {
					var parsed = JSON.parse(data);
					var arr = [];
					$('#playedpos').html('');
					$('#playedpos').append('<option value="">Select Position</option>');
					for(var x in parsed){
						$('#playedpos').append('<option value="' + x + '">' + parsed[x] + '</option>');
					}
					$('#playedpos').append('<option value="other">other</option>');
				},
                error: function (error) {
                   $('#playedpos').html('');
				   $('#playedpos').append('<option value="">Select Position</option>');
                }
			});
		});

 
});

    </script>
    @endif
    @endif
<script >
$(document).ready(function() {
    jQuery('.deletenotificationclass').on( 'click', function(e) {
        e.preventDefault();
        var link  = $(this).attr('href');
        var txt;
        var r = confirm("Are you sure, You want to delete this Notification");
        if (r == true) {
            window.location.replace(link);
        } else {
        }
    });
});
</script>
@stop

