<style>
h3.payment-heading {
    text-align: left !important;
    font-size: 28px;
    margin: 0 0 20px 0;
}
 .card-pay-btn-div {
    text-align: right !important;
}
.paidbyadmin h3{
    text-align: center;
    font-size: 28px;
    margin: 0 0 20px 0;
}
.payment-history div.heading {
    background: #000;
    float: left;
    width: 100%;
}
.payment-history div {
    width: 100%;
    display: block;
}
.payment-history div.heading div {
    width: 11%;
    float: left;
    color: #fff;
    font-size: 12px;
    text-align: center;
    padding: 11px 0;
}
.payment-history.new .no_result_found_payment {
    padding: 70px 0 30px 0;
    font-size: 26px;
}
.payment-history.new {
   margin: 0 0 50px 0;
   background: #e2e2e2 ;
   text-align: center;
   float: left;
   width: 100%;
}
.payment-history.new .payment-content div {
   width: 11%;
   float: left;
   font-size: 12px;
   text-align: center;
   padding: 11px 0;
}
.payment-history.new .payment-content div a {
   background: #000;
   color: #fff;
   padding: 4px 14px;
}


</style>

@extends('layouts.signup')
@section('content')
<?php $user = \App\User::find(\Auth::user()->id);

 if( $user->braintree_id != '' ){
	 $userInvoiceS = $user->invoicesIncludingPending();
 }

?>
<div class='clearfix'>&nbsp;</div>
<div class='clearfix'>&nbsp;</div>
<div class="container">
    <div class="page-content container-fluids">
        <div class='row'>
            <div class='col-md-6'>
                <h3 class="payment-heading">
                   Payment History
                </h3>
            </div>
            @if(isset($userInvoiceS) && count($userInvoiceS) > 0)
            <?php //echo "<pre>"; print_r($userInvoiceS[0]);die; 
            $tokenC = $userInvoiceS[0]->creditCard['token']; ?>
                <div class='col-md-6 card-pay-btn-div'>
                    <a class="btn orange card-pay-btn" href="/payment/updateCardDetails/{{$tokenC}}">
                       Update Payment To A New Credit Card
                    </a>
                </div>
            @endif
        </div> 
        <div class="payment-history new">
            <div class="heading">
                <div>User Name</div>
                <div>Braintree ID</div>
                <div>Athlete Plan</div>
                <div>Card Brand</div>
                <div>Card Last Four Digits</div>
                <div>Created At</div>
                <div>Ends At</div>
                <div>Cost</div>
                <div>Actions</div>
            </div>
            
            @if( $user->braintree_id != '' )
            <?php $userInvoiceS = $user->invoicesIncludingPending();
            ?>
            @if(count($userInvoiceS) > 0)
            @foreach ($userInvoiceS as $invoice)
            <div class="payment-content">
                <?php //echo "<pre>";
            // print_r( $invoice->creditCard['token']);die;
                    $invoiceDownId = $invoice->id;
                    $tokenC = $invoice->creditCard['token'];
                ?>
                    <div>{{ $invoice->customerDetails->firstName .' '. $invoice->customerDetails->lastName}}</div>
                    <div>{{ $invoice->subscriptionId }}</div>
                    <div>{{ $invoice->planId }}</div>
                    <div>{{ $invoice->creditCard['cardType'] }}</div>
                    <div>{{ $invoice->creditCard['last4'] }}</div>
                    <div>{{date_format($invoice->createdAt , 'd M Y')  }}</div>
                    <div>@if(isset($invoice->subscription['billingPeriodEndDate'])){{date_format($invoice->subscription['billingPeriodEndDate'] , 'd M Y')  }}@endif</div>
                    <div>{{'$'. $invoice->amount }}</div>
                    <div><a href="/user/invoice/{{isset($invoiceDownId)? $invoiceDownId:''}}/{{$user->id}}">Download</a>
                       <!--  <a href="/payment/updateCardDetails/{{$tokenC}}">Edit</a> -->
                    </div>
            </div>
            
            @endforeach
            @else
            <div>
                <div>
                    <div class="no_result_found_payment">
                    <h4> No Result Found</h4>
                    </div>
                </div>
            </div>
            @endif
            @else
            <div>
                <div>
                    <div class="no_result_found_payment">
                    <h4> No Result Found</h4>
                    </div>
                </div>
            </div>
            @endif
        </div> 
        @if(\Auth::user()->is_paid == 'yes')
        <div class="paidbyadmin"  >
            <h3>Payment Done From Admin</h3>
        </div>
        @endif
    </div>
</div>

@stop

