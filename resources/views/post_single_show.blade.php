@extends('layouts.signup')

@section('content')
@if(Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
 	<div class="main-container main-inner">
		<section class="white-bg">
			<div class="container">
				<!-- <div class='row'> -->
					@if($post->image == '')
						<div class="col-md-12 col-sm-12 left-content padding-right">
					@else
						<div class="col-md-12 col-sm-12 left-content padding-right">
					@endif
						<h2 class="main-title">{{$post->title}}</h2>
						<p>{!!$post->body!!}</p>
					</div>
				<!-- 	@if($post->image != '')
					<div class="col-md-4 col-sm-12 right-content">
						<div class="right-box">
							
							<div class="uploaded-images">
								<div class="col-md-12 padding-10 ">
									<img height='200px' width='200px' src="{{asset('storage/'.$post->image)}}" title='' class="img-responsive" >
								</div>
							</div>
						</div>
					</div>
					@endif -->
				<!-- </d/iv> -->
				<!-- <div class='row'> -->
					<div class="col-md-4">
						<a class="back_home" href="/blog">
						<i aria-hidden="true" class="fa fa-arrow-left"></i>
						Back to Blog</a>
			    	</div>
			    <!-- </div> -->
			</div>
		</section>
		@if(Voyager::setting('cta_show_post') == 'show')
			@if(Voyager::setting('cta_image_post') )
				<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::setting('cta_image_post') ) }} )">
						<div class="container">
							{!! Voyager::setting('cta_post_content') !!}
						</div>
					</div>
				</div>	
	 		@endif
	 	@endif
@stop

