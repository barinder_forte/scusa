@extends('layouts.signup')
@section('content')
<div id="steps" class="info-forms thankyouclass">
    <section class="white-bg">
        <div class="container">
            <div class="info-forms">
                <div class="sign-up-form thankyouclass-form">
                    <div class="row">
                       <!--  <h2>Thank you <strong>{{Auth::user()->name}}</strong> for subscribing to {{ session('success') }}</h2>
                        <div class="col-md-12 message-thanks">
                            <p>Your subscription has been started.</p> -->
                            <h2>Thank you for subscribing to Sport Contact USA. You now have full access to the website, so get started by reaching out to coaches and joining events today!</h2>
                        </div>
                        <a class="btn profile-btn" href="/profile">Go to profile</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection