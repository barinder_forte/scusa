@extends('layouts.main')
@section('content')
<?php 
$posts = App\Testimonials::paginate(4);         
?>
<div class="main-container main-inner">
	<section class="grey-bg">
		<div class="container">
			<div class="related-events">
				<div class="col-lg-12">
					<h1>You Can view Testimonials</h1>
					<div class="row">
					<div class="events-main">
						@foreach($posts as $post)
							<div class="col-md-3">
								<div class="events-box">
									<h2><a href="/testimonialshow/{{$post->id}}">{{$post->title}}</a></h2>
									<p>{{substr(strip_tags($post->body), 0, 75) . ' ...'}} </p>
									<a class="read-more" href="/testimonialshow/{{ $post->id }}">Read More</a> 
								</div>
							</div>
						@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection