@extends('layouts.ctasignup')
@section('content')
<?php 
use App\Sport;
$sports = Sport::where('status',"=",'PUBLISHED')->orderBy('title','ASC')->get()->toArray();
use App\ApiData;
$scholllist = ApiData::select('id','school_name')->orderBy('school_name','ASC')->get();

$schoollistarrayjson = json_encode($scholllist);

?>
<div class="log-in-popup site-login">
    <div class="sign-up-content">
        <div class="panel-heading">Create an account</div>
            <div class="panel-body">
                <div class="container signup-new">
                    <div class="radio-section" v-show="profile_switch_tab">
                        <a @click="signup_url2(2)"  href="#" :class="athelete_class"><input type="radio"  name="role" value="2" v-model="this.role">i&#39;m an athlete</a>
                        <a @click="signup_url2(4)" href="#" :class="coach_class"><input type="radio" name="role" value="4" v-model="this.role">i&#39;m a coach</a>
                        <a @click="signup_url2(3)" href="#" :class="parent_class"><input type="radio" name="role" value="3" v-model="this.role">i&#39;m a parent</a> 
                    </div>
           
                    @for( $i = date('Y'); $i <= intval(date('Y'))+7; $i++ )
                        <?php $years[] = $i; ?>
                    @endfor
                    <?php //$years = [ date('Y'), intval(date('Y')) + 8 ]; ?>
                    <div >
                        <athelete v-show="athelete_show" :graduation = "{{json_encode($years)}}" :allsports="{{ json_encode($sports) }}"></athelete>   
                    </div>
                    <div v-show="coach_show">
                        <coach  @if(isset($sports)) :allsports='{{ json_encode($sports) }}' @endif   @if(isset($schoollistarrayjson)) :schoolllist='{{ $schoollistarrayjson }}' @endif  ></coach>
                    </div>
                    <div v-show="parent_show">
                        <parent></parent>
                    </div> 
                    <div v-cloak v-show="signup_popup_message" class="sign-up-message sign-up-messagenew sign-up-without-popup"><br /></div> 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sign-up-popup" style="display: none" id="show-term">
    <div class="sign-up-content" id="show-content-div">
        <span class="close-term">Close</span>
        <div class="show-inner">
        <p>{!! Voyager::page('body', 53) !!}</p>
    </div>
    </div>
</div> 
@endsection
