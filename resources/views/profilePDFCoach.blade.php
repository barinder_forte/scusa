<!DOCTYPE html>
<html>
<head>
  <title>Coach PDF</title>
</head>
  <body style="A_CSS_ATTRIBUTE:all;padding-top: 160px">
  <div class="fix-header" style="A_CSS_ATTRIBUTE:all;position: fixed;top: 10px; left: 10px;max-width: 1140px;
    margin: 0 auto;
    width: 85%;
    right: 0;
    z-index: 999;">
    <div class="client-profile" style="A_CSS_ATTRIBUTE:all;text-align: center;
    margin: 0px 0 20px 0;">
        <h2 style="A_CSS_ATTRIBUTE:all;margin: 0 0 0 30px;font-size: 12px;font-weight: bolder;">Sport Contact USA <br> Coach Profile</h2>
      </div>
        <?php if(isset($userInfoArray['sport']) && (isset($userInfoArray['sport'])?$userInfoArray['sport']:'') !== 'null' ){
              $sportData = \App\Sport::where('id',$userInfoArray['sport'])->first();
            }?> 
      <div class="maini-heading" style="A_CSS_ATTRIBUTE:all;background-image: url('http://res.cloudinary.com/webforte/image/upload/h_100,w_670/circlen_gnbvmg.png');background-size: 100% 100%; background-repeat: no-repeat;
    text-align: center;
    color: #fff;
    padding: 0px 30px 20px 30px;
    margin: 0;
    height: 100%;
    ">
        <h1 style="A_CSS_ATTRIBUTE:all;margin: 0; text-transform: capitalize;">{{(isset($userInfoArray['coach_firstname'])?($userInfoArray['coach_firstname']):'') ." ". (isset($userInfoArray['coach_lastname'])?($userInfoArray['coach_lastname']):'')}}</h1>
          <h2 style="A_CSS_ATTRIBUTE:all;margin: 0;text-transform: capitalize;font-size: 14px;">{{isset($sportData->title)?($sportData->title):""}} </h2>
          <h2 style="A_CSS_ATTRIBUTE:all;margin: 0;text-transform: capitalize;font-size: 14px;">{{isset($userInfoArray['coach_title'])?($userInfoArray['coach_title']):""}} </h2>
      </div>
  </div>
  <div class="pdf-wrapper" style="A_CSS_ATTRIBUTE:all;max-width: 1140px;margin: 0;width: 94%;display: block;">
    <div class="inner-pdf" style="A_CSS_ATTRIBUTE:all;position: relative;width: 100%;margin: 0;z-index: 99;">
      
      <div class="left-section" style="A_CSS_ATTRIBUTE:all;width: 35%;float: left;padding-right: 0%;">
            <div class="important-heading" style="A_CSS_ATTRIBUTE:all;color: #fff;width: 100%;margin: 0 0 120px 0;position: relative;height: 0;">
              <h3 style="">Important Information</h3>
            </div>
           <div class="img">
          <img class='img-responsive' style="max-width: 200px;" src="<?php if($userData->avatar == 'users/default.png' || $userData->avatar == ''){ echo 'https://res.cloudinary.com/webforte/image/upload/h_170,w_180/default_qsdhgx.jpg'; } else{ 
            $userimage = explode('/upload/',$userData->avatar)[1];
            $userimage2 = str_replace('png', 'jpg', $userimage);
            echo "https://res.cloudinary.com/webforte/image/upload/h_170,w_180/".$userimage2; } ?>" > 
        </div>
        <h3 class="" style="text-transform: uppercase;
                    margin: 0 0 10px 0;
                    font-size: 14px;" >Coach information</h3>
                    <?php $check = 0 ; ?>
                    
                    <?php $ncaas =json_decode( isset($userInfoArray['coach_ncaa'])?($userInfoArray['coach_ncaa']):'' );?>
                      @if(count($ncaas) > 0 && isset($ncaas[0]->name) )
                           <p><label>National Championship:</label> <br />
                                @foreach($ncaas as $ncaa)
                                  <?php $check = 1 ; ?>
                                 <?php $ncaafs[] = $ncaa->name; ?>
                              @endforeach
                           {{implode(", ",$ncaafs)}}
                           </p> 
                           @endif
                          
                           <?php $ncaaffs =json_decode( isset($userInfoArray['coach_ncaa_f_f'])?($userInfoArray['coach_ncaa_f_f']):'' );?>
                        @if(count($ncaaffs) > 0 && isset($ncaaffs[0]->name) )
                            <p><label>Final Four:</label> <br />
                                @foreach($ncaaffs as $ncaaff)
                                <?php $check = 1 ; ?>
                                <?php $ncaaffss[] = $ncaaff->name; ?>
                              @endforeach
                           {{implode(", ",$ncaaffss)}}
                           </p> 
                        @endif  
                        <?php $ncaants =json_decode( isset($userInfoArray['coach_ncaa_n_t'])?($userInfoArray['coach_ncaa_n_t']):'' );?>
                    @if(count($ncaants) > 0 && isset($ncaants[0]->name) )
                          <p><label>National Tournament:</label><br />
                                @foreach($ncaants as $ncaant)
                                <?php $check = 1 ; ?>
                                 <?php $ncaantsd[] = $ncaant->name; ?>
                              @endforeach
                           {{implode(", ",$ncaantsd)}}
                           </p> 
                    @endif
                    <?php $ncaapss =json_decode( isset($userInfoArray['coach_ncaa_p_s'])?($userInfoArray['coach_ncaa_p_s']):'' );?>
                    @if(count($ncaapss) > 0 && isset($ncaapss[0]->name) )
                           <p><label> Post Season Appearance:</label> <br />
                                @foreach($ncaapss as $ncaaps)
                                <?php $check = 1 ; ?>
                                 <?php $ncaaccss[] = $ncaaps->name; ?>
                              @endforeach
                           {{implode(", ",$ncaaccss)}}
                           </p>
                    @endif
                    <?php $ncaaccs =json_decode( isset($userInfoArray['coach_ncaa_c_c'])?($userInfoArray['coach_ncaa_c_c']):'' );?>
                    @if(count($ncaaccs) > 0 && isset($ncaaccs[0]->name) )
                        <p><label>Confrence Championship:</label> <br />
                              @foreach($ncaaccs as $ncaacc)
                              <?php $check = 1 ; ?>
                                 <?php $ncaacs[] = $ncaacc->name; ?>
                              @endforeach
                           {{implode(", ",$ncaacs)}}
                        </p>

                    @endif
                     @if($check == 0)
                      <p class="no-info">No Information Available</p>
                    @endif 
                      <?php  $galleryVideos = json_decode(isset($userInfoArray['videogallery'])? ($userInfoArray['videogallery']):'');  ?>
                     @if(count($galleryVideos) > 0)
                        <h2 style="font-size:14px;
        text-transform: uppercase;">Video Highlights</h2>
                  @foreach($galleryVideos as $key => $video)
                  
                  <p>
                    <a class="btn video-btn" href="{{$video->url}}" target="_blank">
                      {{$video->url}}
                    </a>
                  </p> 
                  @endforeach
                 
                  
                              
                  @endif
      </div>
      <div class="right-section" style="A_CSS_ATTRIBUTE:all;padding-top: 17px; float: right;width: 55%;">
       
        <div class="info-section" style="top: 30px;">
           <?php if(isset($userInfoArray['school'])){
                     $scholld = \App\ApiData::where('id',$userInfoArray['school'])->first();
                 } ?>
          <h3 class="school-name">{{isset($scholld->school_name)?$scholld->school_name:""}}</h3>
          <h3><a class="email" href="mailto:{{isset($userInfoArray['coach_email'])?($userInfoArray['coach_email']):''}}">{{(isset($userInfoArray['coach_email'])?($userInfoArray['coach_email']):'')}}</a></h3>
          <h3 style=""><a href="#">


<?php 

                                if(isset($userInfoArray['phone'])){


                                 $daat =  str_replace(array('(',')','-','_',' '),'',$userInfoArray['phone']);
                                  if(isset($userInfoArray['c_code']) && $userInfoArray['c_code']!= ''){
                                  $code = '+'.$userInfoArray['c_code'];

                                 }else{
                                  $code = '';

                                 }

                                   echo $formatted =$code. ' (
                                   '.substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);

                                }



                                



                              ?>


        </a></h3>
        </div>
         
        <ul>
          <h2>{{"SCHOOL INFORMATION"}}</h2>
          @if( count($schoolData) > 0 )
          <?php $goingthere = 0;?>
                  @foreach($schoolData as $key=>$coaching)
                     <!-- @if(( isset($coaching->school_id) && (isset($coaching->school_id)?$coaching->school_id:'') !== 'null' )|| isset($coaching->year) || 
                     isset($coaching->students_coached) || isset($coaching->school_tuition) || 
                     isset($coaching->school_size) || isset($coaching->address1) || isset($coaching->address2) || isset($coaching->city) || isset($coaching->state) || isset($coaching->zip) || isset($coaching->conference)   || (isset($coaching->head_coach) && (isset($coaching->head_coach)?$coaching->head_coach:'') != 'null' )  ||( isset($coaching->division) && (isset($coaching->division)?$coaching->division:'') !== 'null' )  || isset($coaching->tuition_room) || (isset($coaching->gender) && (isset($coaching->gender)?$coaching->gender:'') != 'null' ) || (isset($coaching->sport_id) && (isset($coaching->sport_id)?$coaching->sport_id:'') !== 'null' )  ) -->
                        
                        <?php //echo $key; echo "<pre>"; print_r($schoolData);die();
                        if(isset($coaching->school_id)){
                           $scholllist = \App\ApiData::where('id',$coaching->school_id)->first();
                        } 
                        if(isset($coaching->sport_id) && (isset($coaching->sport_id)?$coaching->sport_id:'') !== 'null' ){
                           $sportData = \App\Sport::where('id',$coaching->sport_id)->first();
                        } 
                        if(isset($coaching->division)?$coaching->division:'' != "" && isset($coaching->division)?$coaching->division:'' != "null"){
                           $divisionss = \Voyager::pagemetadetail('Division', '16');
                           $divisionss = json_decode($divisionss);
                           $divisionss = $divisionss->options;
                           foreach($divisionss as $key => $val){
                              if($coaching->division == $val){
                                 $divisionOri = $key;
                              }
                           }
                        }
                        ?>
                        @if(isset($scholllist->school_name))
                          <?php $goingthere = 1;?>
                          <h4>{{$scholllist->school_name}}</h4>
                        @endif 
                              @if(isset($coaching->gender) && ((isset($coaching->gender)?$coaching->gender:'') != 'null' ) )
                              <?php $goingthere = 1;?>
                                 <p><label>Gender Coached: </label> @if($coaching->gender == 'Men'){{'Male'}}@elseif($coaching->gender == 'Women'){{'Female'}} @else {{'Male/Female'}} @endif</p>
                              @endif
                              @if(isset($coaching->year)?$coaching->year:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>Year: </label> {{isset($coaching->year)?$coaching->year:''}}</p>
                              @endif
                              
                              @if(isset($coaching->school_tuition)?$coaching->school_tuition:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>School Tuition: </label>{{isset($coaching->school_tuition)?'$'.($coaching->school_tuition):''}}</p>
                              @endif
                              @if(isset($coaching->tuition_room)?$coaching->tuition_room:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>Tuition Room/Board: </label>{{isset($coaching->tuition_room)?'$'.($coaching->tuition_room):''}}   </p>
                              @endif
                              @if(isset($coaching->school_size)?$coaching->school_size:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>School Size: </label>{{isset($coaching->school_size)?$coaching->school_size:''}}</p>
                              @endif
                              @if(isset($coaching->school_url)?$coaching->school_url:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>Website Name: </label>
                                 <a target="_blank" href="http://{{($coaching->school_url)}}">{{isset($coaching->school_url)?$coaching->school_url:'#'}}</a></p>
                              @endif
                              @if(isset($coaching->address1)?$coaching->address1:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>Address: </label>{{isset($coaching->address1)?$coaching->address1:''}}</p>
                              @endif
                              @if(isset($coaching->address2)?$coaching->address2:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>Address 2: </label>{{isset($coaching->address2)?$coaching->address2:''}}</p>
                              @endif
                             
                              @if(isset($coaching->city)?$coaching->city:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>City: </label>{{isset($coaching->city)?$coaching->city:''}}</p>
                              @endif
                              @if(isset($coaching->state)?$coaching->state:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>State: </label>{{isset($coaching->state)?$coaching->state:''}}</p>
                              @endif
                              @if(isset($coaching->zip)?$coaching->zip:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>Zip Code: </label>{{isset($coaching->zip)?$coaching->zip:''}}</p>
                              @endif
                               @if(isset($coaching->phone)?$coaching->phone:'' != "")
                               <?php $goingthere = 1;?>
                                 <p><label>Phone: </label><?php
                                if(isset($coaching->phone)){ $daat =  str_replace(array('(',')','-','_',' '),'',$coaching->phone);
                                   echo   $formatted = "(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);

                                }?></p>
                              @endif
                              @if(isset($coaching->fax)?$coaching->fax:'' != "")
                              <?php $goingthere = 1;?>
                                 <p><label>Fax: </label><?php
                                if(isset($coaching->fax)){ $daat =  str_replace(array('(',')','-','_',' '),'',$coaching->fax);
                                   echo   $formatted = "(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);

                                }?></p>
                              @endif
                              @if(isset($divisionOri))
                              <?php $goingthere = 1;?>
                                    <p><label>Division: </label>{{$divisionOri}}</p>
                              @endif
                              @if(isset($sportData->title)?$sportData->title:'' != "")
                              <?php $goingthere = 1;?>
                                    <p><label>Sport: </label>{{$sportData->title}}</p>
                              @endif
                              @if(isset($coaching->conference)?$coaching->conference:'' != "")
                              <?php $goingthere = 1;?>
                                    <p><label>Conference: </label>{{$coaching->conference}}</p>       
                              @endif
                              @if(isset($coaching->head_coach) && (isset($coaching->head_coach)?$coaching->head_coach:'') != 'null' )
                              <?php $goingthere = 1;?>
                                    <p><label>Position: </label>{{($coaching->head_coach == 'True' )? 'Head Coach' : 'Assistant Coach'}}</p>
                                
                              @endif
                  
                  @endif
                @endforeach
              @endif 
              @if($goingthere == 0)
                <p class="no-info">No Information Available</p>
              @endif
            </ul>
              @if(isset($userInfoArray['bio']))
              <?php $bio = strip_tags($userInfoArray['bio']); ?>
              @if($bio != '')
              <p> <h2>Coach Bio</h2>
              {!! ($userInfoArray['bio']) !!}</p>
              @endif
              @endif

              @if(isset($userInfoArray['players']))
              <?php $players = strip_tags($userInfoArray['players']); ?>
              @if($players != '')
               <p><h2>Players Drafted Professionally</h2>
                {!! ($userInfoArray['players']) !!}</p> 
             @endif  
             @endif  
            <ul>

        
       
        </ul>
          <!-- <li>All Conference, All State, Team Captain, 2016</li>
          <li>Led Team In Assists And Points, 2016</li>
          <li>State Cup Semi-Finalists, 2016</li>
          <li>Team MVP and State All Tournament Team, 2015</li>
          <li>ODP State Team And National Runners Up, 2015</li>
          <li>Led Team In Goals And Assists, 2015</li>
          <li>All Conference And All State 2nd Team, 2015</li>
          <li>All Conference And Rookie Of The Year, 2014</li>
          <li>Volunteered In Haiti, 2016</li>
          <li>Lil’ Kickers Soccer Coach For The Youth, 2016</li>
          <li>Spanish Club Member, Unified Club Member, Character</li>
          <li>Counts Member And DECA Participant </li>
        </ul> -->

   <!--      <div class="text-center">
          <h4>CLUB COACH</h4>
          <h4>Sher Yang</h4>
          <h4><a class="email" href="#">yangsher@hotmail.com</a></h4>
          <h4><a href="#">763-442-1401</a></h4>
          <h4><br /></h4>
          <h4>HIGH SCHOOL COACH</h4>
          <h4>Derek Engler</h4>
          <h4><a class="email" href="#">dlengler3@gmail.com</a></h4>
          <h4><a href="#">612-889-8149</a></h4>
        </div> -->

      </div>
    </div>
  </div>

  <style>

   .fix-header {
      max-width: 1140px;
      margin: 0 auto;
      width: 94%;
      right: 0;
      z-index: 999;
  }
 /* .maini-heading {
      background-color: #000;
      text-align: center;
      color: #fff;
      padding: 20px 30px 25px 30px;
      border-radius: 10px 10px;
      margin: 0;
  }*/
  .maini-heading h1{
    text-transform: capitalize;
    font-size: 28px;
  }
  .right-section .info-section h3.school-name{
      text-transform: capitalize;
      font-size: 12px;
    }
  .important-heading h3 {
    margin: 0;
    font-size: 11px;
    color: #fff;
    position: relative;
    top: 40px;
    left: 20px;
  }
  .important-heading:before {
      height: 0;
      border-color: transparent transparent transparent #000;
      border-style: solid;
      border-width: 50px 0px 50px 260px;
      width: 0;
      content: "";
      position: absolute;
  }
  body{
    position: relative;
  }
  /*.fix-header:before {
   content: "";
      position: fixed !Important;
      width: 30px;
      height: 1500px;
      position: absolute;
      background: #cc6633;
      left: 25%;
      top: 0;
      z-index: -9;
    }*/
  body:before {
      content: "";
      width: 30px;
      height: 1500px;
      position: absolute;
      background: #cc6633;
      left: 30%;
      top: 0;
      z-index: -9;
  }
  .left-section .img {
      margin: 0 0 20px 0;
  }
  .left-section h2 {
      margin: 0 0 7px 0;
      font-size: 10px;
  }
  .right-section {
      float: left;
      width: 63%;
      padding-left: 8%;
  }
  .right-section .info-section{
    margin-bottom: 10px;
  }
  .right-section .info-section h3 {
        margin: 0 0 10px 0;
        font-size: 12px;
    }
    .right-section .info-section h3 a {
        color: #000;
        text-decoration: none;
    }
    .right-section .info-section h3 a.email {
        color: #c63;
    }
    .right-section ul {
        margin: 0 0 10px 0;
        padding: 0;
    }
    .right-section .text-center {
        /* text-align: center; */
        padding: 0px;
    }
    .right-section ul li {
        font-size: 12px;
        margin: 0 0 9px 0;
        text-transform: capitalize;
        padding-left: 10px;
    }
    .right-section ul li a {
        text-transform: none;
    }
    .right-section h2 {
        text-transform: uppercase;
        margin: 0 0 10px 0;
        font-size: 14px;
    }
    .right-section .text-center h4 {
        margin: 0;
        font-size: 12px;
        font-weight: bold;
        text-transform: capitalize;
    }
    .right-section .text-center h4 a {
        color: #000;
        text-decoration: none;
        text-transform: none;
    }
    .right-section .text-center h4 a.email {
        color: #c63;
    }
    .right-section .profile-link {
        text-align: right;
        margin: 0 0 10px 0;
    }
    .right-section .profile-link a {
        background: #000;
        color: #fff;
        text-decoration: none;
        padding: 7px 10px;
        border-radius: 20px;
        font-size: 10px;
    }
    .right-section h4 {
        text-transform: uppercase;
        margin: 0 0 7px 0;
        font-size: 12px;
    }
    .right-section p {
        text-transform: capitalize;
        font-size: 11px;
        margin: 0 0 9px 0 !important;
    }
    .left-section p {
        margin: 0 5px 5px 0;
        font-size: 12px;
        font-weight: bold;
    }
    a.btn.video-btn{
      /*background: #cc6633;*/
      color: #cc6633;
      text-decoration: underline;
      padding: 0;
      display: inline-block;
      width: 185px;
      word-break: break-all;
      word-wrap: break-word;
    }
    
  </style>


</body>
</html>