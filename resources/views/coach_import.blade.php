@extends('voyager::master')

<style type="text/css">
    ol.breadcrumb{
        display: none;
    }
</style>
<style>
    body{font-size:8pt; color:#333}
    #wrap{width:500px; margin:5px auto}
    #responce{height:200px; overflow-y:scroll; border:1px solid #ddd;}
</style>
@section('content')

<div class='clearfix'>&nbsp;</div>
<div class='clearfix'>&nbsp;</div>

<div class="container">
<!-- <div class="showmessage" style="display:none">
    <a href="#" class="close">Close</a>
    <div class="importmessage"><p>File Import Successfully.</p></div>
</div> -->
    <div class="page-content coach-import-main container-fluids">
       <a download href="{{ asset('storage/'.Voyager::setting('demo_event_coach') )}}" class="coach_import">Download Demo CSV</a>
        <form  method="GET" class="form" id="importCoachForm" accept="text/csv" enctype="multipart/form-data" >
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" class="">
            <div class='row'>
                <div class='col-md-12'>
                    <h3 >
                        Import Coaches CSV File
                    </h3>
                </div>
            </div> 
            <!--  <div class='row'>
                <div class='col-md-12'>
                   <ul id="responce">
                
                    </ul> --><!-- // response
                </div>
            </div>  -->
            <div class='row'>
                <div class='col-md-6'>
                    <div class='form-group'>
                        <input type="file" name='file' id="file" class="form-control" >
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <input type="submit"  class="btn orange" id="btnUpload" value="Submit">
                    </div>
                </div>
            </div>    
        </form>
    </div>
</div>
<script>
            var reader_offset = 0;      //current reader offset position
            var buffer_size = 1024;     //
            var pending_content = '';
            
            /**
            * Read a chunk of data from current offset.
            */
            function readAndSendChunk()
            {
                var reader = new FileReader();
                var file = $('#file')[0].files[0];
                reader.onloadend = function(evt){
                    
                    // show succes message
                    if(evt.loaded == 0){

                        jQuery('#voyager-loader').hide();
                        // jQuery('.showmessage').show();
                        toastr.success('File Imported Successfully!');
                        $('#file').val('').clone(true);

                        // alert('File Imported Successfully!'); 
                    }
                    //check for end of file
                    if(evt.loaded == 0) return;
                    
                    //increse offset value
                    reader_offset += evt.loaded;
                    
                    //check for only complete line
                    var last_line_end = evt.target.result.lastIndexOf('\n');
                    var content = pending_content + evt.target.result.substring(0, last_line_end);
                    
                    pending_content = evt.target.result.substring(last_line_end+1, evt.target.result.length);
                    
                    //upload data
                    send(content);
                };
                var blob = file.slice(reader_offset, reader_offset + buffer_size);
                reader.readAsText(blob);
            }
            
            /**
            * Send data to server using AJAX
            */
            function send(data_chank)
            {


                $.ajax({
                    url: "/admin/coach/import/save",
                    method: 'POST',
                   
                    data:{ 'data_chank' : data_chank,
                        "_token": $('#token').val()
                    }
                }).done(function(response) {
                
                    //show responce message received from server
                    // $( '#responce' ).append( '<li>' + response + '</li>');
                    
                    //try for next chank
                    if(response.status == 'invalid'){
                        toastr.error('Please Select Correct CSV File!');
                         $('#file').val('').clone(true);
                        //val = '';
                        return false;
                    }else{
                        readAndSendChunk();
                        jQuery('#voyager-loader').show();
                    }


                });
            }
            
            /**
            * On page load
            */
             $(function(){
                $("#file").change(function () {
                    var fileExtension = ['csv'];
                    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                       
                         val = 1;
                    }else{
                        val = 2;
                    }
                 });

                $('#btnUpload').click(function(e){
                    e.preventDefault();
                     if(typeof(val) != 'undefined' && val == 2 ){
                        reader_offset = 0;
                        pending_content = '';
                        readAndSendChunk();
                        val = '';
                    }else if(typeof(val) != 'undefined' && val == 1){
                        toastr.error('Only formats are allowed : .csv');
                        // alert("Only formats are allowed : csv");
                        // $('#file').val(''); 
                        $('#file').val('').clone(true);
                        val = '';
                        return false;

                    }else{
                        toastr.error('Please Select A File');
                        val = '';
                        return false;
                    }
                });
            });
    //         jQuery( document ).ready(function($) {
    //     jQuery('.close').click( function ($){
    //         jQuery('.showmessage').hide();
           
    //     });      
    // });
        </script>
@stop

