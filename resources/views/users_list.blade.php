@extends('layouts.signup')

@section('content')
	<?php $userData = \Auth::User(); ?>
	<div class="main-container main-inner ">
		<section class="grey-bg">
			<div class="container">
				<div class="col-md-8 padding-right <?php if( $userData->user_approved == 'INACTIVE' && $userData->role_id == 4){ echo 'addblur-userlist-athlete'; } ?>">
						<?php 	if($usersAll){
										$userIdsArray1 = array();
							        	foreach($usersAll as $key=>$attendee){
											//$userIdsArray1[] = $attendee->id ;
											// if( $attendee->role_id == 3 ){
												$userInfo = \Voyager::UserInformation('all', $attendee->id );
												$userIdsArray1[$key]['userid'] = str_replace("'", '',isset($userInfo['athelete_email'])?$userInfo['athelete_email']:'');
											
												$userIdsArray1[$key]['username'] = str_replace("'", '',isset($userInfo['athelete_firstname'])?$userInfo['athelete_firstname']:'').' '.str_replace("'", '',isset($userInfo['athelete_lastname'])?$userInfo['athelete_lastname']:'');
											// }else{
											// 	$userIdsArray1[$key]['userid'] = str_replace("'", '',$attendee->email);
											// 	$userIdsArray1[$key]['username'] = str_replace("'", '',$attendee->name);
											// }
										}
										$userIdsArray = json_encode($userIdsArray1);
					// <!--$userIdsArray1[$key]['userid'] = $attendee->id ;-->
					//  <!--$userIdsArray1[$key]['username'] = $attendee->name ;-->
					//  <!--$userIdsArray = json_encode($userIdsArray1);-->
									}else{
										$userIdsArray = '' ;	
									}
									?>
					@if($users && count($users) != 0)
					<div class="atheletes-heading">
						<div class="row">
							<div class="col-md-8">
								<h2>Search Results ({{ $usersAll?count($usersAll):'0' }})</h2>
							</div>
							<div class="col-md-4 text-right">
							<a @click.prevent='showemail( <?php echo $userIdsArray; ?>)'  class="btn <?php if( $userData->user_approved == 'INACTIVE' && $userData->role_id == 4){ echo 'light addblurrClass'; }else{echo 'orange'; } ?> emailAllEvent">email all</a>				</div>
						</div>
					</div>
					@else
					<div class="atheletes-heading noresultfound">
						<div class='events-data'>
						<h4>Users Not Found</h4>
						</div>
					</div>

					@endif
					<div class="events-section atheletes-main">
						@if($users)
						@foreach($users as $user)
						<?php $userSchool = '' ; $userSchool = App\UserInformation::where('user_id',$user->id);
							$userSchool = $userSchool->where('meta_key','school')->first();
								$school = isset($userSchool->meta_value)?$userSchool->meta_value:'';
							// $userPosition = App\UserInformation::where('user_id',$user->id)->where('meta_key','position')->first();
		
		                    $arrayformjson = $user->id;
							?>
						<div class="events-content">
							<div class="events-data athlete-img">
								<img height='50px' width='50px' src="<?php if($user->avatar == 'users/default.png' || $user->avatar == ''){ echo asset('storage/users/default.png'); } else{ 
									$userimage = explode('/upload/',$user->avatar)[1];
									echo "https://res.cloudinary.com/webforte/image/upload/w_50,h_50/".$userimage; 
								} ?>">
							</div>
							<div class="events-data">
								<h3>
									<?php //if( $user->role_id == 3 ){
												$userInfo2 = \Voyager::UserInformation('all', $user->id );
											// if(isset($userInfo2['athelete_firstname']) || isset($userInfo2['athelete_lastname'])){
												$userName = (isset($userInfo2['athelete_firstname'])?$userInfo2['athelete_firstname']:'').' '.str_replace("'", '',isset($userInfo2['athelete_lastname'])?$userInfo2['athelete_lastname']:'');
											// }else{
											// 	$userName = str_replace("'", '',$user->name) ;
											// } ?>
									{{$userName}}
									<!-- {{$user->id}} -->
									<span>{{isset($school)?$school:''}}  
									</span>
								</h3>
							</div>
							<div class="events-data">
								<p>
									<?php  	
									$userIdsArf = '';
									$userIdsAr = array(); 
											// if( $user->role_id == 3 ){
												// $userInfo = \Voyager::UserInformation('all', $user->id );
												$userIdsAr[0]['userid'] = str_replace("'", '',isset($userInfo2['athelete_email'])?$userInfo2['athelete_email']:'');
												// if(isset($userInfo2['athelete_firstname']) || isset($userInfo2['athelete_lastname'])){
												
												$userIdsAr[0]['username'] = $userName;
											// }else{
											// 	// $userIdsAr[0]['userid'] = $user->email ;
											// $userIdsAr[0]['username'] = str_replace("'", '',$user->name) ;
											// }
											
											$userIdsArf = json_encode($userIdsAr);
										?>	

									<a href="/profile/athlete/{{$user->user_slug}}">View Profile</a>
									<a @click.prevent='showemail( <?php echo $userIdsArf; ?>)' href="#" id='show_login' class="send_email_event" >Send Email</a>
								</p>
							</div>
						</div>
						@endforeach
						@endif
				</div>
			
						{!! $users->appends(Request::except('page'))->render() !!}
				</div>

				<div class='col-md-4 col-sm-12 right-content '>
					<div class='right-box '>
						<h1>filter by</h1>
						<form class="filter-form athlete-list-form" action='/athletes/search' method='GET'>
							{{csrf_field() }}
							<div class="row">
                                <div class="col-lg-12 col-xs-12 ">
                                    <label>Athlete Name</label>
									<input class="js-example-basic-single athlete_name" type="text" name='athlete_name' id='athlete_name' value="{{isset( $_GET['athlete_name'] )?$_GET['athlete_name']:''}}">

									
                                </div>
                            </div>
                             <?php if(\Auth::User()->role_id == 1){ ?>
								<label>Paid/Non-Paid Athletes</label>
								<select id='graduationFilter' name='paiduser'  class="js-example-basic-single graduationFilter">
									<option value=''>Select Paid/Non-Paid</option>
									<option <?PHP if((isset($_GET['paiduser'])?$_GET['paiduser']:'')=="yes") echo "selected";?> value='yes'>Paid</option>
									<option <?php if((isset($_GET['paiduser'])?$_GET['paiduser']:'') == 'admin') echo "selected";?> value='admin'>Paid By Admin</option>
									<option <?PHP if((isset($_GET['paiduser'])?$_GET['paiduser']:'')=="no") echo "selected";?> value='no'>Non Paid</option>

								</select>
							<?php } ?>
							<!-- <label>Sports Playing</label>
							<select id='sportsFilter' name='sportsFilter'  class="js-example-basic-single sportsFilter">
								<option value=''>Select Sport</option>
								@foreach($allSports as $s)
								<option <?PHP// if((isset($sportsFilter)?$sportsFilter:'')== $s->id) echo "selected";?> value='{{$s->id}}'>{{$s->title}}</option>
								@endforeach
							</select> -->
							<label>Year Graduating</label>
							<select id='graduationFilter' name='graduationFilter'  class="js-example-basic-single graduationFilter">
								<option value=''>Select Year</option>
								<option <?PHP if((isset($graduation)?$graduation:'')=="2017") echo "selected";?> value='2017'>2017</option>
								<option <?PHP if((isset($graduation)?$graduation:'')=="2018") echo "selected";?> value='2018'>2018</option>
								<option <?PHP if((isset($graduation)?$graduation:'')=="2019") echo "selected";?> value='2019'>2019</option>
								<option <?PHP if((isset($graduation)?$graduation:'')=="2020") echo "selected";?> value='2020'>2020</option>
								<option <?PHP if((isset($graduation)?$graduation:'')=="2021") echo "selected";?> value='2021'>2021</option>
							</select>

							<label>Position Played</label>
							<select name="position" id="playedpos" class="js-example-basic-single">
								<option value="">Select Position</option>
								<option value="other">other</option>
							</select>

							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<label>GPA</label>
									<select name='gpa' id='gpa'  class="js-example-basic-single">
										<option  value=''>Select GPA</option>
										<option <?PHP if((isset($gpa)?$gpa:'')=="1") echo "selected";?> value='1'> >= 1</option>
										<option <?PHP if((isset($gpa)?$gpa:'')=="2") echo "selected";?> value='2'> >= 2</option>
										<option <?PHP if((isset($gpa)?$gpa:'')=="3") echo "selected";?> value='3'> >= 3</option>
										<option <?PHP if((isset($gpa)?$gpa:'')=="4") echo "selected";?> value='4'> >= 4</option>
										<option <?PHP if((isset($gpa)?$gpa:'')=="5") echo "selected";?> value='5'>5</option>
									</select>
								</div>

								<div class="col-lg-6 col-xs-12">
									<label>ACT</label>
									<select name='act' id='act' class="js-example-basic-single">
										<option  value=''>Select ACT</option>
										@for($i = 1; $i <= 36 ; $i++)
											@if($i <= 35 )
											<option <?php if((isset($act)?$act:'')== $i) echo "selected";?> value='{{$i}}'> >={{$i}} </option>
											@else
											<option <?php if((isset($act)?$act:'')== $i) echo "selected";?> value='{{$i}}'> {{$i}} </option>
											@endif
										@endfor
									</select>
								</div>
							</div>

							<label style="display: none">Gender</label>
							<select style="display: none" id='genderFilter' name='genderFilter'  class="js-example-basic-single genderFilter">
								<option  value=''>Select Gender</option>
								<option <?PHP if((isset($gender)?$gender:'')=="Men") echo "selected";?> value='Men'>Male</option>
								<option <?PHP if((isset($gender)?$gender:'')=="Women") echo "selected";?> value='Women'>Female</option>
							</select>

							<label>State</label>
								<select  id='stateFilter' name='stateFilter'  class="js-example-basic-single stateFilter" >
									<option value=''>Select State</option>
									<option value="AL" <?PHP if((isset($state)?$state:'')=="AL") echo "selected";?>>Alabama</option>
									<option value="AK" <?PHP if((isset($state)?$state:'')=="AK") echo "selected";?>>Alaska</option>
									<option value="AZ" <?PHP if((isset($state)?$state:'')=="AZ") echo "selected";?>>Arizona</option>
									<option value="AR" <?PHP if((isset($state)?$state:'')=="AR") echo "selected";?>>Arkansas</option>
									<option value="CA" <?PHP if((isset($state)?$state:'')=="CA") echo "selected";?>>California</option>
									<option value="CO" <?PHP if((isset($state)?$state:'')=="CO") echo "selected";?>>Colorado</option>
									<option value="CT" <?PHP if((isset($state)?$state:'')=="CT") echo "selected";?>>Connecticut</option>
									<option value="DE" <?PHP if((isset($state)?$state:'')=="DE") echo "selected";?>>Delaware</option>
									<option value="DC" <?PHP if((isset($state)?$state:'')=="DC") echo "selected";?>>District of Columbia</option>
									<option value="FL" <?PHP if((isset($state)?$state:'')=="FL") echo "selected";?>>Florida</option>
									<option value="GA" <?PHP if((isset($state)?$state:'')=="GA") echo "selected";?>>Georgia</option>
									<option value="HI" <?PHP if((isset($state)?$state:'')=="HI") echo "selected";?>>Hawaii</option>
									<option value="ID" <?PHP if((isset($state)?$state:'')=="ID") echo "selected";?>>Idaho</option>
									<option value="IL" <?PHP if((isset($state)?$state:'')=="IL") echo "selected";?>>Illinois</option>
									<option value="IN" <?PHP if((isset($state)?$state:'')=="IN") echo "selected";?>>Indiana</option>
									<option value="IA" <?PHP if((isset($state)?$state:'')=="IA") echo "selected";?>>Iowa</option>
									<option value="KS" <?PHP if((isset($state)?$state:'')=="KS") echo "selected";?>>Kansas</option>
									<option value="KY" <?PHP if((isset($state)?$state:'')=="KY") echo "selected";?>>Kentucky</option>
									<option value="LA" <?PHP if((isset($state)?$state:'')=="LA") echo "selected";?>>Louisiana</option>
									<option value="ME" <?PHP if((isset($state)?$state:'')=="ME") echo "selected";?>>Maine</option>
									<option value="MD" <?PHP if((isset($state)?$state:'')=="MD") echo "selected";?>>Maryland</option>
									<option value="MA" <?PHP if((isset($state)?$state:'')=="MA") echo "selected";?>>Massachusetts</option>
									<option value="MI" <?PHP if((isset($state)?$state:'')=="MI") echo "selected";?>>Michigan</option>
									<option value="MN" <?PHP if((isset($state)?$state:'')=="MN") echo "selected";?>>Minnesota</option>
									<option value="MS" <?PHP if((isset($state)?$state:'')=="MS") echo "selected";?>>Mississippi</option>
									<option value="MO" <?PHP if((isset($state)?$state:'')=="MO") echo "selected";?>>Missouri</option>
									<option value="MT" <?PHP if((isset($state)?$state:'')=="MT") echo "selected";?>>Montana</option>
									<option value="NE" <?PHP if((isset($state)?$state:'')=="NE") echo "selected";?>>Nebraska</option>
									<option value="NV" <?PHP if((isset($state)?$state:'')=="NV") echo "selected";?>>Nevada</option>
									<option value="NH" <?PHP if((isset($state)?$state:'')=="NH") echo "selected";?>>New Hampshire</option>
									<option value="NJ" <?PHP if((isset($state)?$state:'')=="NJ") echo "selected";?>>New Jersey</option>
									<option value="NM" <?PHP if((isset($state)?$state:'')=="NM") echo "selected";?>>New Mexico</option>
									<option value="NY" <?PHP if((isset($state)?$state:'')=="NY") echo "selected";?>>New York</option>
									<option value="NC" <?PHP if((isset($state)?$state:'')=="NC") echo "selected";?>>North Carolina</option>
									<option value="ND" <?PHP if((isset($state)?$state:'')=="ND") echo "selected";?>>North Dakota</option>
									<option value="OH" <?PHP if((isset($state)?$state:'')=="OH") echo "selected";?>>Ohio</option>
									<option value="OK" <?PHP if((isset($state)?$state:'')=="OK") echo "selected";?>>Oklahoma</option>
									<option value="OR" <?PHP if((isset($state)?$state:'')=="OR") echo "selected";?>>Oregon</option>
									<option value="PA" <?PHP if((isset($state)?$state:'')=="PA") echo "selected";?>>Pennsylvania</option>
									<option value="RI" <?PHP if((isset($state)?$state:'')=="RI") echo "selected";?>>Rhode Island</option>
									<option value="SC" <?PHP if((isset($state)?$state:'')=="SC") echo "selected";?>>South Carolina</option>
									<option value="SD" <?PHP if((isset($state)?$state:'')=="SD") echo "selected";?>>South Dakota</option>
									<option value="TN" <?PHP if((isset($state)?$state:'')=="TN") echo "selected";?>>Tennessee</option>
									<option value="TX" <?PHP if((isset($state)?$state:'')=="TX") echo "selected";?>>Texas</option>
									<option value="UT" <?PHP if((isset($state)?$state:'')=="UT") echo "selected";?>>Utah</option>
									<option value="VT" <?PHP if((isset($state)?$state:'')=="VT") echo "selected";?>>Vermont</option>
									<option value="VA" <?PHP if((isset($state)?$state:'')=="VA") echo "selected";?>>Virginia</option>
									<option value="WA" <?PHP if((isset($state)?$state:'')=="WA") echo "selected";?>>Washington</option>
									<option value="WV" <?PHP if((isset($state)?$state:'')=="WV") echo "selected";?>>West Virginia</option>
									<option value="WI" <?PHP if((isset($state)?$state:'')=="WI") echo "selected";?>>Wisconsin</option>
									<option value="WY" <?PHP if((isset($state)?$state:'')=="WY") echo "selected";?>>Wyoming</option>
								</select>


							<!-- <div class="row">
                                <div class="col-lg-6 col-xs-12">
                                    <label>height</label>

								<input type="hidden" name="heightFilter" value="<?php //echo isset( $_GET['heightFilter'] ) && $_GET['heightFilter'] != '' ? str_replace('lbs','',$_GET['heightFilter']) : '36-36'; ?>"
								 id="amount2" readonly style="border:0; color:#f6931f; font-weight:bold;">


								<p id="result"><span class="min"></span> - <span class="max"></span></p>
								<div id="slider"></div>


                                </div>

                                <div class="col-lg-6 col-xs-12 lbsplace">
                                    <label>Weight</label>
                                    
									<input type="text" value="<?php //echo isset( $_GET['weightFilter'] ) && $_GET['weightFilter'] != '' ? str_replace('lbs','',$_GET['weightFilter']) : '0-0'; ?>" name="weightFilter" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">

									<div id="slider-range"></div>
                                </div>
                            </div> -->
							<div class="clearfix">&nbsp;</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<input type="submit" class='btn btn-info' name="submit" value='Submit'>
								</div>
								<div class="col-lg-6 col-xs-12">
									<a  href='/athletes/' class='btn btn-info orange' >Reset Search</a>
								</div>
							</div>
						</form>
						@if(count($users) > 0 )
						<a @click.prevent='showemail( <?php echo $userIdsArray; ?>)' class="btn <?php if( $userData->user_approved == 'INACTIVE' && $userData->role_id == 4){ echo 'orange addblurrClass'; }else{echo 'orange'; } ?>">	
								Email These Users
						</a>
						@endif
					</div>
				</div>
			</div>
		</section>
	</div>
<?php if(isset($_GET['position'])){

$postion = $_GET['position'];
}else{

$postion = '';

}
$found = \App\UserInformation::where('user_id', '=', $userData->id)->where('meta_key', '=', 'sport')->first();
if($found->meta_value){
$positionsport = $found->meta_value; 
}else{
$positionsport = ''; 
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

		$( window ).on( "load", function() {
				console.log($( "#sportsFilter option:selected" ).val());
				var vald = '<?php echo $postion; ?>';
				$.ajax({
					type: 'GET',
					url: '/spouser/position',
					data: 'id=<?php echo $positionsport; ?>',
					success: function(data) {
						var parsed = JSON.parse(data);
						var arr = [];
						$('#playedpos').html('');
						$('#playedpos').append('<option value="">Select Position</option>');

						for(var x in parsed){
							$('#playedpos').append('<option value="' + x + '">' + parsed[x] + '</option>');
						}

						$('#playedpos').append('<option value="other">other</option>');
						$('#playedpos option[value="'+vald+'"]').attr('selected','selected');
					},
	                error: function (error) {
	                    $('#playedpos').html('');
						$('#playedpos').append('<option value="">Select Position</option>');
	                }
				});
			
		});

		$('#sportsFilter').on('change', function() {
			$.ajax({
				type: 'GET',
				url: '/spouser/position',
				data: 'id=' +this.value,
				success: function(data) {
					var parsed = JSON.parse(data);
					var arr = [];
					$('#playedpos').html('');
					$('#playedpos').append('<option value="">Select Position</option>');
					for(var x in parsed){
						$('#playedpos').append('<option value="' + x + '">' + parsed[x] + '</option>');
					}
					$('#playedpos').append('<option value="other">other</option>');
				},
                error: function (error) {
                   $('#playedpos').html('');
				   $('#playedpos').append('<option value="">Select Position</option>');
                }
			});
		});
});

    </script>
@stop

