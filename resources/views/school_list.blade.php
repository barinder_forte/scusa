@extends('layouts.coachImport-layout')

@section('content')

	<div class="main-container main-inner">
		<section class="grey-bg">
			<div class="container">
				<div class="col-md-12 padding-right">
						
					
					<div class="events-section atheletes-main">
							<div class="">School ID</div>
		                	<div class="">School Name</div>
		                	<div class="">City</div>
		                	<div class="">State</div>
		                	<div class="">Zip</div>
		                	<div class="">School URL</div>
                            <div class="">Actions</div>
						@foreach($all_schools as $school)
						
						<div class="events-content">
							<div class="events-data">
								{{$school->school_id}}
							</div>
							<div class="events-data">
								{{$school->school_name}}
							</div>
							<div class="events-data">
								{{$school->city}}
							</div>
							<div class="events-data">
								{{$school->state}}
							</div>
							<div class="events-data">
								{{$school->zip}}
							</div>
							<div class="events-data">
								<a href='{{"http://".$school->school_url}}' title='School Site'>{{$school->school_url}}
							</div>
							<div class="events-data">
								<p>
									<a href="#">View School</a>
								</p>
							</div>
						</div>
						@endforeach
					</div>
						{!! $all_schools->render() !!}
				</div>
    		</div>
		</section>
	</div>
	
@stop

