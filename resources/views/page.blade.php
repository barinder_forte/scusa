@if( Voyager::page('layout', $page->id) != '' )
    <?php $layout = Voyager::page('layout', $page->id); ?>
@else
    <?php $layout = 'full-width'; ?>
@endif

@extends('layouts.'.$layout )

@section('content')
    <h1 class="main-title">{{ Voyager::pagemeta('page_title', $page->id) }}</h1>
    {!! $page->body !!}
@endsection