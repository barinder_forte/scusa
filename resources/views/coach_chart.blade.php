@extends('layouts.charts-layout-2')
<style type="text/css">
    ol.breadcrumb{
        display: none;
    }
</style>
@section('content')
<div class='clearfix'>&nbsp;</div>
<div class="main-container main-inner">
    <section class="grey-bg">
        <div class="container">
            <div class='row'>
                <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('sports_played_coaches', '8') }}</h3></div>
                     </div>
                <div class="col-md-12 padding-right">
                    <div id="jqChartCoaches"  class='jqChart'  style="width: 100%; height: 450px;">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('profile_complete_coaches', '8') }}</h3></div>
                     </div>
                <div class="col-md-12 padding-right">
                    <div id="jqChartcoach2"  class='jqChart'  style="width: 100%; height: 450px;">
                    </div>
                </div>
            </div>
             <div class='row'>
                 <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{  Voyager::pagemeta('coaches_in_state', '8') }}</h3></div>
                     </div>
                <div class="col-md-12 padding-right">
                    <div id="jqChartCoach4"  class='jqChart'  style="width: 100%; height: 450px;">
                    </div>
                </div>
            </div>
             <div class='row'>
                <div class="col-md-12 padding-right"> 
                    <div   class='title' ><h3>{{ Voyager::pagemeta('active_deactive_coaches', '8')}}</h3></div>
                    </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChartCoach5"  class='jqChart'  style="width: 100%; height: 450px;">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-8 padding-right"> 
                    <div   class='title' ><h3>{{ Voyager::pagemeta('emials_sent_coaches', '8')}}</h3></div>
                </div>
                 <div class="col-md-4 padding-left"> 
                    <select id='user_select' name='user_select' class='user_select' >   
                    </select>
                </div>
                <div class="col-md-12 padding-right">
                    
                    <div id="jqChartCoach6"  class='jqChart'  style="width: 100%; height: 450px;">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-8 padding-right"> 
                    <div   class='title' ><h3>{{ Voyager::pagemeta('how_singing_in_coaches', '8')}}</h3></div>
                </div>
                <div class="col-md-4 padding-left"> 
                    <select id='date_selected' name='date_selected' class='graph_select' >
                    </select>
                </div>
                <div class="col-md-12 padding-right">
                  
                    <div id="jqChartCoach7"  class='jqChart'  style="width: 100%; height: 450px;">
                    </div>
                </div>
            </div>
            <div class='row user_signin_row'>
                <div class="col-md-8 padding-right"> 
                    <div   class='title' ><h3>{{ Voyager::pagemeta('how_singing_in_single_coach', '8')}}</h3></div>
                </div>
                <div class="col-md-2 padding-left"> 
                    <select id='user_select_single' name='user_select_single' class='user_select' >
                    </select>
                </div> 
                <div class="col-md-2 padding-left"> 
                    <select id='date_select_single' name='date_select_single' class='date_select_single' >
                    </select>
                </div>
                <div class="col-md-12 padding-right">
                  
                    <div id="jqChartCoach8"  class='jqChart'  style="width: 100%; height: 450px;">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop