<style type="text/css">
	#step_1 > .row, #step_1 textarea#message{
		width: 100%;
	}
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>

@extends('layouts.signup')
@section('content')
<div id="steps" class="info-forms">
	<form v-show="hide_step2" class="details no-padding" method="POST" action="/contact">
		<section class="grey-bg">
			<div class="container">
				<div class="info-forms">
					<div class="sign-up-form">
						<h1>Contact Us</h3>
						<h2>Simply fill out the form and we will contact you shortly.</h2>
						@if(\Session::has('message'))
							<ul class="alert alert-success">
						        <li>{{ \Session::get('message') }}</li>
							</ul>
						@endif
						@if($errors->any)
							        <ul class="alert alert-danger">
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
						@endif
						<div id="step_1" class="details"> 
						{{csrf_field()}}
	    					<div class="row">
	    						<div class="col-md-6 col-sm-6">
		    						<label>First Name <sup>*</sup></label>
		    						
		            				<input type="text" name="first_name" value="{{ old('first_name') }}" id="first_name" placeholder="First Name" />
		            				<span class='error'></span>
	    						</div>
	    						<div class="col-md-6 col-sm-6">
		    						<label>Last Name <sup>*</sup></label>
		            				<input type="text" name="last_name" value="{{ old('last_name') }}" id="last_name" placeholder="Last Name" />
		            				<span class='error'></span>
	    						</div>
	    						<div class="col-md-12 col-sm-12">
									<label>Email Address <sup>*</sup></label>
		            				<input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="Email Address" />
		            				<span class='error'></span>
	    						</div>
	    						<div class="col-md-12 col-sm-12">
									<label>Your Message <sup>*</sup></label>
		            				<textarea rows="6" id="message" name="message">{{ old('message') }}</textarea>
		            				<span class='error'></span>
	    						</div>
	    						<div class="col-md-12 col-sm-12" style="margin-top:10px">
		    						<div class="col-md-6 col-sm-12" style="float:left;">
	        								<div class="g-recaptcha" data-sitekey="6Lde-EQUAAAAANrgbSuaNY_COOy_YqybjGPGbqW0"></div>
									</div>
		    						<div class="col-md-6 col-sm-12 send-now">

		        						<button class="btn orange" style="float:right;">Send Now</button>
		        					</div>
	        					</div>
	        				</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</form>
</div>
@endsection