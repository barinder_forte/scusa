<!DOCTYPE html>
<html>
<head>
   <title>Athlete PDF</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body style="A_CSS_ATTRIBUTE:all;padding-top: 180px">
<div class="fix-header" style="A_CSS_ATTRIBUTE:all;position: fixed;top: 10px; left: 10px;max-width: 1140px;
    margin: 0 auto;
    width: 85%;
    right: 0;
    z-index: 999;">
   <div class="client-profile" style="A_CSS_ATTRIBUTE:all;text-align: center;
    margin: 0px 0 20px 0;">
        <h2 style="A_CSS_ATTRIBUTE:all;margin: 0 0 0 30px;font-size: 12px;font-weight: bolder;
   /* font-size: 36px;
    font-style: normal;
    font-variant: normal;
    font-weight: 500;
    line-height: 26.4px;*/
    ">Sport Contact USA <br> Player Profile</h2>
      </div>
      <?php  $coaches = json_decode(isset($userInfoArray['coaches'])?($userInfoArray['coaches']):'');  ?>
  <div class="maini-heading" style="A_CSS_ATTRIBUTE:all;background-image: url('http://res.cloudinary.com/webforte/image/upload/h_100,w_670/circlen_gnbvmg.png');background-size: 100% 100%; background-repeat: no-repeat;
    text-align: center;
    color: #fff;
    padding: 0px 30px 20px 30px;
    margin: 0;
    height: 100%;
    ">

        <h1 style="A_CSS_ATTRIBUTE:all;margin: 0;">{{ucwords((isset($userInfoArray['athelete_firstname'])?($userInfoArray['athelete_firstname']):'') ." ". (isset($userInfoArray['athelete_lastname'])?($userInfoArray['athelete_lastname']):''))}}</h1>


                <h2 style="A_CSS_ATTRIBUTE:all;margin: 0;font-size: 14px;">{{(isset($userInfoArray['graduation'])?"Class of ".($userInfoArray['graduation']):'')}}</h2>
        @if(isset($coaches[0]->clubname) || (isset($userInfoArray['position_name']) && $userInfoArray['position_name'] != 'NA' ) || isset($coaches[0]->jersey))

          <h2 style="A_CSS_ATTRIBUTE:all;margin: 0;font-size: 14px;"> 
             {{isset($coaches[0]->clubname)?ucwords($coaches[0]->clubname):''}} 

             {{(isset($userInfoArray['position_name']) && $userInfoArray['position_name'] != 'NA' )?(" - ".$userInfoArray['position_name']):''}}
              {{isset($coaches[0]->jersey)?(" Jersey # ".($coaches[0]->jersey)):''}}
           </h2>
         @endif
        
    </div>
  </div>

  <div class="pdf-wrapper" style="A_CSS_ATTRIBUTE:all;max-width: 1140px;margin: 0;width: 94%;display: block;">
    <div class="inner-pdf" style="A_CSS_ATTRIBUTE:all;position: relative;width: 100%;margin: 0;z-index: 99;">
      
      <div class="left-section" style="A_CSS_ATTRIBUTE:all;width: 25%;float: left;padding-right: 4%;">
            <div class="important-heading" style="A_CSS_ATTRIBUTE:all;color: #fff;width: 100%;margin: 0 0 115px 0;position: relative;height: 0;">
              <h3 style="">Important Information</h3>
            </div>
           <div class="img">
          <img class='img-responsive' style="max-width: 150px;" src="<?php if($userData->avatar == 'users/default.png' || $userData->avatar == ''){ echo 'https://res.cloudinary.com/webforte/image/upload/h_170,w_170/default_qsdhgx.jpg'; } else{ 
            $userimage = explode('/upload/',$userData->avatar)[1];
            $userimage2 = str_replace('png', 'jpg', $userimage);
            echo "https://res.cloudinary.com/webforte/image/upload/h_170,w_170/".$userimage2; } ?>" > 
        </div>
            <h2>HEIGHT: <?php if(isset($userInfoArray['athelete_height'])){
                          if($userInfoArray['athelete_height'] != ''){
                          $centimetres = $userInfoArray['athelete_height'] + 36; 
                         $feet = floor($centimetres/12);
                         $inches = ($centimetres%12);
                         echo $feet."'".$inches.'"';
                          }
                         // else{
                          //   echo "NA";
                          // }
                          }else{
                            echo "NA";
                          }?> </h2>
            <h2>WEIGHT: {{isset($userInfoArray['athelete_weight'])?($userInfoArray['athelete_weight'])." lbs":'NA'}}<span style="text-transform: lowercase;"></span></h2>
          <!--   <h2>40 DASH:  </h2>
            <h2>100 SPRINT: </h2> -->
       <br />
            <h2>GPA: {{isset($academics)?((($academics->gpa) != 0)?"".$academics->gpa:'NA'):'NA'}} </h2>
            <h2>ACT: {{isset($academics)?((($academics->act) != 0)?"".$academics->act:'NA'):'NA'}} </h2>
            <br />
    <!--         <h2>SAT COMBINED: </h2>
     -->        <h2>SAT MATH: {{isset($academics)?((($academics->math) != 200)?"".$academics->math:'NA'):'NA'}}</h2>
            <h2>SAT VERBAL: {{isset($academics)?((($academics->verbal) != 200)?"".$academics->verbal:'NA'):'NA'}}</h2>
            <h2>SAT READING: {{isset($academics)?((($academics->sats) != 200)?"".$academics->sats:'NA'):'NA'}}</h2>
            <br />
        <?php  $galleryVideos = json_decode(isset($userInfoArray['videogallery'])? ($userInfoArray['videogallery']):'');  ?>
                     @if(count($galleryVideos) > 0)
                        <h2 style="font-size:14px;
        text-transform: uppercase;">Video Highlights</h2>
                  @foreach($galleryVideos as $key => $video)
                  
                  <p>
                    <a class="btn video-btn" href="{{$video->url}}" target="_blank">
                      {{$video->url}}
                    </a>
                  </p> 
                  @endforeach
                 
                  
                              
                  @endif
      </div>

      <div class="right-section" style="A_CSS_ATTRIBUTE:all;padding-top: 17px; float: right;width: 58%;">
        <div class="info-section">
             <?php $acdmic = array();
                        $acdmic[0] = isset($academics->school_id)?ucwords($academics->school_id):'';
                        // $acdmic[1] = isset($academics->city)?ucwords($academics->city):'' ;
                        // $acdmic[2] = isset($academics->state)?$academics->state:'' ;
                       
                        $acdmic = array_filter($acdmic);
                        $acdmic1 = implode(', ',$acdmic);
                     ?>

          <h3 class="school-name">{{$acdmic1}}</h3>
          <h3><a class="email" href="mailto:{{isset($userInfoArray['athelete_email'])?($userInfoArray['athelete_email']):''}}">
          {{(isset($userInfoArray['athelete_email'])?($userInfoArray['athelete_email']):'')}}</a></h3>

          <h3 style="font-size: 12px; font-weight: bold; "><a href="#">

<?php 

                                if(isset($userInfoArray['athelete_phone'])){
                                   if(isset($userInfoArray['c_code']) && $userInfoArray['c_code']!= ''){
                                  $code = '+'.$userInfoArray['c_code'];

                                 }else{
                                  $code = '';

                                 }

                                   
                                 $daat =  str_replace(array('(',')','-','_',' '),'',$userInfoArray['athelete_phone']);
                                   echo $formatted =$code. ' (
                                   '.substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);


                                }



                                



                              ?>

          </a>

          </h3>
        </div>
<ul> <?php  $athleticAchievements = json_decode(isset($userInfoArray['athletic_achievements'])?($userInfoArray['athletic_achievements']):''); 
                     ?>
          @if(count($athleticAchievements) > 0)
                  <h2>Athletic Achievements</h2>
             
                     @if(count($athleticAchievements) > 0)
                     @foreach($athleticAchievements as $key3=>$athleticAchievement)
                     @if( ( (isset($athleticAchievement->athleticname) && (isset($athleticAchievement->athleticname) ?$athleticAchievement->athleticname:"") != "null") ||(isset($athleticAchievement->years) && (isset($athleticAchievement->years ) ?$athleticAchievement->years:"") != "null") )  && $key3 < 6 )
                     <?php $ath = array();
                        $ath[0] = (isset($athleticAchievement->athleticname)&& (isset($athleticAchievement->athleticname) ?$athleticAchievement->athleticname:"") != "null")?$athleticAchievement->athleticname:'';
                            if(isset($athleticAchievement->years)  && $athleticAchievement->years != '') {
                              $arraydata = array(); 
                              foreach($athleticAchievement->years as $key => $yearsss){
                                 $arraydata[$key] = isset($yearsss->name)?$yearsss->name:'';
                              }
                              if(count($arraydata) > 0){
                                arsort($arraydata);
                                 $dataat = implode(', ', ($arraydata));
                              }else{
                                 $dataat = "";
                              }
                           }else{
                              $dataat = "";
                          }                 
                        $ath[1] = $dataat ;
                        $ath = array_filter($ath);
                        $ath1 = implode(', ',$ath);
                     ?>
                     <li>{{$ath1}}</li>
                     @endif
                     @endforeach
                    
                     @endif
                @endif     
        </ul>



        <ul>
                  
               <?php  $academicAchievements = json_decode(isset($userInfoArray['athelete_acadmic_achivment'])?($userInfoArray['athelete_acadmic_achivment']):''); 
                  // print_r($academicAchievements);die; ?>
                  @if(count($academicAchievements) > 0)
                  <h2>Academic Achievements</h2>
                  @foreach($academicAchievements as $key=>$achievement)
                  @if((isset($achievement->achivment) || (isset($achievement->year) && (isset($achievement->year ) ?$achievement->year:"") != "null")) && $key < 4 )
                     <?php $acd = array();
                        $acd[0] = isset($achievement->achivment)?$achievement->achivment:'';
                       if(isset($achievement->year) && $achievement->year != '') {
                              $arraydata2 = array(); 
                              foreach($achievement->year as $key => $yearsss){
                                 $arraydata2[$key] = isset($yearsss->name)?$yearsss->name:'';
                              }
                              if(count($arraydata2) > 0){
                                arsort($arraydata2);
                                 $dataat2 = implode(', ', ($arraydata2));
                              }else{
                                 $dataat2 = "";
                              }
                           }else{
                              $dataat2 = "";
                          }                 
                        $acd[1] = $dataat2 ;
                       
                        $acd = array_filter($acd);
                        $acd1 = implode(', ',$acd);
                     ?>
                  <li>{{$acd1}}</li>
                  @endif
                  @endforeach
                  
                  @endif
        </ul>



        <ul>
                  
     <?php  $extra_curriculars = json_decode(isset($userInfoArray['extra_curricular'])?($userInfoArray['extra_curricular']):'');
                        ?>
                     @if(count($extra_curriculars) > 0)
                     <h2>Extra Curricular Activities</h2>
                     @foreach($extra_curriculars as $key2=>$extra_curricular)
                     @if((isset($extra_curricular->activity) || (isset($extra_curricular->year) && (isset($extra_curricular->year ) ?$extra_curricular->year:"") != "null")) && $key2 < 4 )
                      <?php $ext = array();
                        $ext[0] = isset($extra_curricular->activity)?$extra_curricular->activity:'';
                         if(isset($extra_curricular->year)  && $extra_curricular->year != '') {
                              $arraydata3 = array(); 
                              foreach($extra_curricular->year as $key => $yearsss){
                                 $arraydata3[$key] = isset($yearsss->name)?$yearsss->name:'';
                              }
                              if(count($arraydata3) > 0){
                                 arsort($arraydata3);
                                 $dataat3 = implode(', ', ($arraydata3));
                              }else{
                                 $dataat3 = "";
                              }
                           }else{
                              $dataat3 = "";
                          }                 
                        $ext[1] = $dataat3 ;
                        $ext = array_filter($ext);
                        $ext1 = implode(', ',$ext);
                     ?>
                     <li>{{$ext1}}</li>
                     @endif

                     @endforeach
                     
                     @endif
        </ul>
       
        <div class="text-center">
         @if($coaches)
                  @foreach($coaches as $coach) 
                    <h2 style="A_CSS_ATTRIBUTE:all;margin: 0;font-size: 14px;"><?php 
                    if((isset($coach->type)?$coach->type:'') == "School" )
                      { echo 'High School Coach Contact Info';}
                      // else if((isset($coach->type)?$coach->type:'') == "Odp" )
                      //   { echo 'ODP Contact Information' ;}
                        else{ echo 'Club Coach Contact Info'; }
                      ;?> </h2>
                    <h4 style="A_CSS_ATTRIBUTE:all;margin: 0;font-size: 11px;">{{isset($coach->clubname)?$coach->clubname:''}} </h4>
                    <h4 style="A_CSS_ATTRIBUTE:all;margin: 0;font-size: 11px;"> {{isset($coach->coachname)?$coach->coachname:''}} </h4>
                    @if( isset($coach->email) )
                     <h4 style="A_CSS_ATTRIBUTE:all;margin: 0;font-size: 11px;">
                        <a class="email" href="mailto:{{isset($coach->email)?$coach->email:''}}">{{isset($coach->email)?$coach->email:''}}</a>
                     </h4>
                     @endif
                    <h4 style="A_CSS_ATTRIBUTE:all;margin: 0;font-size: 11px;"><a class="tel" href="#">

                     <?php 
                                if(isset($coach->phone)){
                                  if(isset($coach->c_code) && $coach->c_code!= ''){
                                  $code1 = '+'.$coach->c_code;

                                 }else{
                                  $code1 = '';

                                 }

                                  $daat =  str_replace(array('(',')','-','_',' '),'',$coach->phone);
                                   echo   $formatted = $code1." (".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);

                                }
                              ?>


                    </a></h4>
                     <h4><br /></h4>
                   @endforeach
         @endif          
              
        </div>

      </div>
    </div>
  </div>


  <style type="text/css">
 /* @font-face {
      font-family: 'Rockwell-ExtraBold';
      src: url("{{ asset('fonts/Rockwell-ExtraBold.eot') }}");
      src: url("{{ asset('fonts/Rockwell-ExtraBold.eot?#iefix') }}") format('embedded-opentype'),
          url("{{ asset('fonts/Rockwell-ExtraBold.woff2') }}") format('woff2'),
          url("{{ asset('fonts/Rockwell-ExtraBold.woff') }}") format('woff'),
          url("{{ asset('fonts/Rockwell-ExtraBold.ttf') }}") format('truetype');
      font-weight: 800;
      font-style: normal;
  }*/
  .fix-header {
      max-width: 1140px;
      margin: 0 auto;
      width: 94%;
      right: 0;
      z-index: 999;
  }
  .maini-heading h1{
    text-transform: capitalize;
    font-size: 28px;
  }
  .important-heading h3 {
    margin: 0;
    font-size: 11px;
    color: #fff;
    position: relative;
    top: 40px;
    left: 20px;
  }
  .important-heading:before {
      height: 0;
      border-color: transparent transparent transparent #000;
      border-style: solid;
      border-width: 50px 0px 50px 230px;
      width: 0;
      content: "";
      position: absolute;
  }
  body{
    position: relative;
  }
  /*.fix-header:before {
   content: "";
      position: fixed !Important;
      width: 30px;
      height: 1500px;
      position: absolute;
      background: #cc6633;
      left: 25%;
      top: 0;
      z-index: -9;
    }*/
  body:before {
      content: "";
      width: 30px;
      height: 1500px;
      position: absolute;
      background: #cc6633;
      left: 25%;
      top: 0;
      z-index: -9;
  }
  .left-section .img {
      margin: 0 0 40px 0;
  }
  .left-section h2 {
      margin: 0 0 7px 0;
      font-size: 10px;
  }
  .left-section p {
      margin: 0 0 7px 0;
      /*text-transform: capitalize;*/
      font-size: 11px;
  }
  .right-section {
      float: left;
      width: 63%;
      padding-left: 8%;
  }
  .right-section .info-section{
    margin-bottom: 20px;
  }
  .right-section .info-section h3 {
        margin: 0 0 10px 0;
        font-size: 24px;
    }
    .right-section .info-section h3 a {
        color: #000;
        text-decoration: none;
    }
    .right-section .info-section h3 a.email {
        color: #c63;
        font-size: 12px;
        text-decoration: underline;
        font-weight: bold;
    }
    .right-section ul {
        margin: 0 0 20px 0;
        padding: 0;
    }
    .right-section .text-center {
        /* text-align: center; */
        padding: 0px;
    }
    .right-section ul li {
        font-size: 11px;
        margin: 0 0 9px 0;
        text-transform: capitalize;
        padding-left: 15px;
        line-height: 9px;
    }
    .right-section ul li a {
        text-transform: none;
    }
    .right-section h2 {
        text-transform: uppercase;
        margin: 0 0 10px 0;
        font-size: 14px;
    }
    .right-section .text-center h2 {
        text-transform: uppercase;
        margin: 0 0 10px 0;
        font-size: 19px;
    }
    .right-section .text-center h4 {
        margin: 0;
        font-size: 18px;
        font-weight: bold;
        text-transform: capitalize;
    }
    .right-section .text-center h4 a {
        color: #000;
        text-decoration: none;
        text-transform: none;
    }
    .right-section .text-center h4 a.email {
        color: #c63;
    }
    .right-section .profile-link {
        text-align: right;
        margin: 0 0 20px 0;
    }
    .right-section .profile-link a {
        background: #000;
        color: #fff;
        text-decoration: none;
        padding: 10px 20px;
        border-radius: 20px;
        font-size: 13px;
    }
    .right-section .info-section h3.school-name {
        font-size: 12px;
    }
    a.btn.video-btn{
      color: #cc6633;
      text-decoration: underline;
      padding: 0;
      display: inline-block;
      width: 160px;
      word-break: break-all;
      word-wrap: break-word;
    }
</style>

</body>
</html>