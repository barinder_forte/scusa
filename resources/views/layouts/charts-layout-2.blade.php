<!DOCTYPE html>
<html>
<head>
    {{-- <title>{{Voyager::setting('admin_title')}} - {{Voyager::setting('admin_description')}}</title> --}}
    <title>@yield('page_title',Voyager::setting('admin_title') . " - " . Voyager::setting('admin_description'))</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400|Lato:300,400,700,900' rel='stylesheet'
          type='text/css'>

    <!-- CSS Libs -->
    <link rel="stylesheet" type="text/css" href="{{ voyager_asset('lib/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ voyager_asset('lib/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ voyager_asset('lib/css/bootstrap-switch.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ voyager_asset('lib/css/perfect-scrollbar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ voyager_asset('css/bootstrap-toggle.min.css') }}">
    <!-- CSS App -->
    <link rel="stylesheet" type="text/css" href="{{ voyager_asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ voyager_asset('css/themes/flat-blue.css') }}">

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/x-icon">

    <!-- CSS Fonts -->
    <link rel="stylesheet" href="{{ voyager_asset('fonts/voyager/styles.css') }}">
    <script type="text/javascript" src="{{ voyager_asset('lib/js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

    @yield('css')

    <!-- Voyager CSS -->
    <link rel="stylesheet" href="{{ voyager_asset('css/voyager.css') }}">

    <!-- Few Dynamic Styles -->
    <style type="text/css">
        .flat-blue .side-menu .navbar-header, .widget .btn-primary, .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
            background:{{ config('voyager.primary_color','#22A7F0') }};
            border-color:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .breadcrumb a{
            color:{{ config('voyager.primary_color','#22A7F0') }};
        }
    </style>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.jqChart.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.jqRangeSlider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui-1.10.4.css')}}" />
    <!--<script src="https://sapui5.hana.ondemand.com/resources/sap-ui-core.js" type="text/javascript"  data-sap-ui-theme="sap_bluecrystal" data-sap-ui-libs="sap.m"></script>-->
    <script src="{{ asset('js/jquery.mousewheel.js')}}" type="text/javascript"></script>
    <script src=" {{asset('js/jquery.jqRangeSlider.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.jqChart.min.js')}}" type="text/javascript"></script>
    <!--<script src=" {{asset('js/jquery.jqChart.OpenUI5.min.js')}}" type="text/javascript"></script>-->
    
    <script lang="javascript" type="text/javascript">
    
                jQuery(document).ready(function ($) {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: '#d2e6c9' },
                             { offset: 1, color: 'white' }]
            };
        
            // alert();
             var yAxisOptions = {
                   labels:
                   {
                       stringFormat: 'format'
                   }
                };
            
            $('#jqChartCoaches').jqChart({
                 legend: {
                       font: '20px sans-serif' ,
                
                },
                
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                
                series: [
                    {
                        type: 'column',
                        title: 'Coaches',
                        fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382'],
                        data: <?php echo $sportsplayed?$sportsplayed:'[]' ; ?>
                    }
                ]
            });
        });
    </script>
  
     <script lang="javascript" type="text/javascript">
                jQuery(document).ready(function ($) {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: '#d2e6c9' },
                             { offset: 1, color: 'white' }]
            };
        
            // alert();
            var profileComplete2 = <?php  echo ($profileComplete);?>;
        
                   $('#jqChartcoach2').jqChart({
                        legend: {
                       font: '20px sans-serif' ,
                
                },
             
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
               
                series: [
                    {
                        type: 'column',
                        title: 'Coaches',
                        fillStyles: [ '#418CF0'],
                        data: profileComplete2.coaches

                    }
                ]
               
            });
        
        });
    </script>
    
    <script lang="javascript" type="text/javascript">
                jQuery(document).ready(function ($) {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: '#d2e6c9' },
                             { offset: 1, color: 'white' }]
            };
            var coachesForState = <?php echo $coachesForState?$coachesForState:'[]' ; ?>;

            $('#jqChartCoach4').jqChart({
                 legend: {
                       font: '20px sans-serif' ,
                
                },
              
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                axes: [
                    {
                        type: 'category',
                        location: 'bottom',
                        labels: {
                            font: '12px sans-serif',
                            angle: 45
                        }
                    }
                ],
                series: [
                   
                    {
                        type: 'column',
                        title: "Coaches" ,
                        fillStyles: ['#418CF0'],
                        data: coachesForState
                    }
                ]
               
            });
            
             
        });
    </script>

<script lang="javascript" type="text/javascript">
    $(document).ready(function () {

        var background = {
            type: 'linearGradient',
            x0: 0,
            y0: 0,
            x1: 0,
            y1: 1,
            colorStops: [{ offset: 0, color: '#d2e6c9' },
                         { offset: 1, color: 'white' }]
        };

        $('#jqChartCoach5').jqChart({
          
            legend: {
                font: '20px sans-serif' ,
                
            },
            border: { strokeStyle: '#6ba851' },
            background: background,
            animation: { duration: 1 },
            shadows: {
                enabled: true
            },
            series: [
                {
                    type: 'pie',
                    fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382'],
                    labels: {
                        // stringFormat: '%.1f%%',
                        valueType: 'number',
                        font: '15px sans-serif',
                        fillStyle: 'white'
                    },
                    explodedRadius: 10,
                    explodedSlices: [<?php echo $activeVsDeactive?count($activeVsDeactive):'0' ; ?>],
                    data: <?php echo $activeVsDeactive?$activeVsDeactive:'[]' ; ?>
                }
            ]
        });
    
        $('#jqChartCoach5').bind('tooltipFormat', function (e, data) {
            var percentage = data.series.getPercentage(data.value);
            percentage = data.chart.stringFormat(percentage, '%.2f%%');
    
            return '<b>' + data.dataItem[0] + '</b><br />' +
                   data.value ;
        });
    });
</script> 

<script lang="javascript" type="text/javascript">
    $(document).ready(function () {

        var background = {
            type: 'linearGradient',
            x0: 0,
            y0: 0,
            x1: 0,
            y1: 1,
            colorStops: [{ offset: 0, color: '#d2e6c9' },
                         { offset: 1, color: 'white' }]
        };
          var totalemailsent = <?php echo $totalemailsent ; ?>;
            
            if((totalemailsent.select_data_coach)) {
            $.each(totalemailsent.select_data_coach , function(index,value){
                 
                 $('#user_select').append('<option  value="'+index+'">'+value+'</option>');
            })
        var user_select  = $('#user_select').val();
        // console.log(totalemailsent.emails[user_select]);
        $('#jqChartCoach6').jqChart({
          
            legend: { 
                font: '20px sans-serif' ,
                // width: 250 ,
                
          
            },
            border: { strokeStyle: '#6ba851' },
            title: { text: "Total Emails: " + totalemailsent.total_emails },
            background: background,
            animation: { duration: 1 },
            shadows: {
                enabled: true
            },
            series: [
                {
                    type: 'pie',
                    fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382'],
                    labels: {
                        // stringFormat: '%.1f%%',
                        valueType: 'number',
                        font: '15px sans-serif',
                        fillStyle: 'white'
                    },
                    explodedRadius: 10,
                    // explodedSlices: [<?php echo $totalemailsent?count($totalemailsent):'0' ; ?>],
                    data: totalemailsent.emails[user_select]
                }
            ]
        });
    
        $('#jqChartCoach6').bind('tooltipFormat', function (e, data) {
            var percentage = data.series.getPercentage(data.value);
            percentage = data.chart.stringFormat(percentage, '%.2f%%');
    
            return '<b>' + data.dataItem[0] + '</b><br />' +
                   data.value ;
        });
        $("#user_select").change(function (evt) {
        var userselection = ( $("#user_select").val() );
        // alert(timeSelection);
        $('#jqChartCoach6').jqChart({
          
            legend: { 
                font: '20px sans-serif' ,
                // width: 250 ,
              
          
            },
            border: { strokeStyle: '#6ba851' },
            title: { text: "Total Emails: " + totalemailsent.total_emails },
            background: background,
            animation: { duration: 1 },
            shadows: {
                enabled: true
            },
            series: [
                {
                    type: 'pie',
                    fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382'],
                    labels: {
                        // stringFormat: '%.1f%%',
                        valueType: 'number',
                        font: '15px sans-serif',
                        fillStyle: 'white'
                    },
                    explodedRadius: 10,
                    // explodedSlices: [totalemailsent.email.length,
                    data: totalemailsent.emails[userselection]
                }
            ]
        });
    });
     }
    });
</script> 
   
        <script lang="javascript" type="text/javascript">
                jQuery(document).ready(function ($) {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: '#d2e6c9' },
                             { offset: 1, color: 'white' }]
            };
        
        
            var usersigninginweeklyCoach = <?php echo $usersigninginweekly ; 
           ?>;
            var userdata1  = [];
            $.each(usersigninginweeklyCoach.datesarray , function(index,value){
                $('#date_selected').append('<option value="'+index+'">'+value+'</option>');
            })
            // console.log(usersigninginweeklyCoach.coachs);
            if ( ! jQuery.isEmptyObject(usersigninginweeklyCoach) ){
            var selectboxval  = $('#date_selected').val();
                  userdata1  = usersigninginweeklyCoach.coachs[selectboxval];
            }
            // console.log(userdata1);
            $('#jqChartCoach7').jqChart({
                 legend: {
                       font: '20px sans-serif' ,
                },
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                        type: 'column',
                        title: 'Coaches',
                        fillStyles: [ '#418CF0'],
                        data:  userdata1
                    }
                ]
               
            });
         $("#date_selected").change(function (evt) {
        var timeSelection = ( $("#date_selected").val() );
         //alert(timeSelection);
         $('#jqChartCoach7').jqChart({
                 legend: {
                       font: '20px sans-serif' ,
                },
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                 series: [
                    {
                        type: 'column',
                        title: 'Coaches',
                        fillStyles: [ '#418CF0'],
                        data: usersigninginweeklyCoach.coachs[timeSelection]
                    }
                ]
               
            });
    });
        });
            
    </script>
   
        <script lang="javascript" type="text/javascript">
                jQuery(document).ready(function ($) {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: '#d2e6c9' },
                             { offset: 1, color: 'white' }]
            };
        
        
            var usersingninsingleCoach = <?php echo $usersingninsingle ; 
           ?>;
           $.each(usersingninsingleCoach.userDatesArray , function(index,value){
                 
                 $('#date_select_single').append('<option value="'+index+'">'+value+'</option>');
            })
           
           var date_select_single  = $('#date_select_single').val();
             var usertitle2  = [];
            var userdata2  = [];
            // console.log(date_select_single);
            // console.log(jQuery.isEmptyObject(usersingninsingleCoach.coaches));
            // alert('selectboxval');
            $.each(usersingninsingleCoach.usersArray , function(index,value){
                 
                 $('#user_select_single').append('<option value="'+index+'">'+value+'</option>');
            })
            if ( ! jQuery.isEmptyObject(usersingninsingleCoach.coaches) ){
            
            var user_select  = $('#user_select_single').val();
             usertitle2  = usersingninsingleCoach.usersArray[user_select];
                  userdata2  = usersingninsingleCoach.coaches[user_select][date_select_single];
            }
            // console.log(usersingninsingleCoach);
            
              
            $('#jqChartCoach8').jqChart({
                 legend: {
                       font: '20px sans-serif' ,
                },
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                        type: 'column',
                        title: usertitle2,
                        fillStyles: [ '#418CF0'],
                        data:  userdata2
                    }
                ]
               
            });
         $("#user_select_single").change(function (evt) {
        var user_Select1 = ( $("#user_select_single").val() );
        var date_select_Single2 = ( $("#date_select_single").val() );
         //alert(timeSelection);
         $('#jqChartCoach8').jqChart({
                 legend: {
                       font: '20px sans-serif' ,
                },
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                 series: [
                    {
                        type: 'column',
                        title: usersingninsingleCoach.usersArray[user_Select1],
                        fillStyles: [ '#418CF0'],
                        data: usersingninsingleCoach.coaches[user_Select1][date_select_Single2]
                    }
                ]
               
            });
    });
        $("#date_select_single").change(function (evt) {
        var user_select2 = ( $("#user_select_single").val() );
        var date_select_single2 = ( $("#date_select_single").val() );
         //alert(timeSelection);
         $('#jqChartCoach8').jqChart({
                 legend: {
                       font: '20px sans-serif' ,
                },
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                 series: [
                    {
                        type: 'column',
                        title: usersingninsingleCoach.usersArray[user_select2],
                        fillStyles: [ '#418CF0'],
                        data: usersingninsingleCoach.coaches[user_select2][date_select_single2]
                    }
                ]
               
            });
    });
        });
            
    </script>

    @if(!empty(config('voyager.additional_css')))<!-- Additional CSS -->
    @foreach(config('voyager.additional_css') as $css)<link rel="stylesheet" type="text/css" href="{{ asset($css) }}">@endforeach
    @endif

    @yield('head')
</head>

<body class="flat-blue">

<div id="voyager-loader">
    <?php $admin_loader_img = Voyager::setting('admin_loader', ''); ?>
    @if($admin_loader_img == '')
        <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
    @else
        <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
    @endif
</div>

<?php
$user_avatar = Voyager::image(Auth::user()->avatar);
if ((substr(Auth::user()->avatar, 0, 7) == 'http://') || (substr(Auth::user()->avatar, 0, 8) == 'https://')) {
    $user_avatar = Auth::user()->avatar;
}
?>

<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">
        @include('voyager::dashboard.navbar')
        @include('voyager::dashboard.sidebar')
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                @yield('page_header')
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('voyager::partials.app-footer')
<script>
    (function(){
            var appContainer = document.querySelector('.app-container'),
                sidebar = appContainer.querySelector('.side-menu'),
                navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                loader = document.getElementById('voyager-loader'),
                anchor = document.getElementById('sidebar-anchor'),
                hamburgerMenu = document.querySelector('.hamburger'),
                sidebarTransition = sidebar.style.transition,
                navbarTransition = navbar.style.transition,
                containerTransition = appContainer.style.transition;

            sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
            appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = 
            navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';
            
            if (window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                appContainer.className += ' expanded';
                loader.style.left = (sidebar.clientWidth/2)+'px';
                anchor.className += ' active';
                anchor.dataset.sticky = anchor.title;
                anchor.title = anchor.dataset.unstick;
                hamburgerMenu.className += ' is-active';
            }

            navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
            sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
            appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
    })();
</script>
<!-- Javascript Libs -->
<script type="text/javascript" src="{{ voyager_asset('lib/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('lib/js/bootstrap-switch.min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('lib/js/jquery.matchHeight-min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('lib/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('lib/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('js/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('js/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('js/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<!-- Javascript -->
<script type="text/javascript" src="{{ voyager_asset('js/readmore.min.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('js/app.js') }}"></script>
@if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
@foreach(config('voyager.additional_js') as $js)<script type="text/javascript" src="{{ asset($js) }}"></script>@endforeach
@endif

<script>
    @if(Session::has('alerts'))
        let alerts = {!! json_encode(Session::get('alerts')) !!};

        displayAlerts(alerts, toastr);
    @endif

    @if(Session::has('message'))
    
    // TODO: change Controllers to use AlertsMessages trait... then remove this
    var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
    var alertMessage = {!! json_encode(Session::get('message')) !!};
    var alerter = toastr[alertType];

    if (alerter) {
        alerter(alertMessage);
    } else {
        toastr.error("toastr alert-type " + alertType + " is unknown");
    }

    @endif
</script>
@yield('javascript')
</body>
</html>