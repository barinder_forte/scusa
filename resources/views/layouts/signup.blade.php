<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <meta property="fb:app_id" content="1904642086449795" />
    <!-- <meta property="og:url" content="" /> -->
    <meta property="og:type" content="article" />
    <!-- <meta property="og:title"  content="Sport Contact USA" />
    <meta property="og:description" content="Share Event Details" />
    <meta property="og:image"  content="https://res.cloudinary.com/webforte/image/upload/w_200,h_200/v1514534021/scusax200_hjyfsk.png" /> -->  
    <?php if(isset($page->id)){
    $metadisc =  Voyager::page('meta_description', $page->id);
    if($metadisc == ''){
        $metadisc = 'Sport Contact USA';
    }
    $metakey =  Voyager::page('meta_keywords', $page->id);
    if($metakey == ''){
        $metakey = 'Sport Contact USA';
    }
    $seo_title =  Voyager::page('seo_title', $page->id);
    if($seo_title == ''){
        $seo_title = 'Sport Contact USA';
    }
    ?>
    <title><?php echo $seo_title; ?></title>
    <meta name="description" content="<?php echo $metadisc; ?>">
    <meta name="keywords" content="<?php echo $metakey; ?>">

    <?php } else if(isset($blog_single) && $blog_single == 'blog_single'){
        $metadisc =  $post->meta_description;
        if($metadisc == ''){
            $metadisc = $post->title;
        }
        $metakey =  $post->meta_keywords;
        if($metakey == ''){
            $metakey = $post->title;
        }
        $seo_title =  $post->seo_title;
        if($seo_title == ''){
            $seo_title = $post->seo_title;
        }
    ?>
    <title><?php echo $seo_title; ?></title>

    <meta name="description" content="<?php echo $metadisc; ?>">
    <meta name="keywords" content="<?php echo $metakey; ?>">
    <?php  }else if(isset($blog_single) && $blog_single == 'testimonials_single'){ ?>

    <meta name="description" content="{{$testimonial->title}}">
    <title>{{$testimonial->title}}</title>
    <meta name="keywords" content="{{$testimonial->title}}">
    <?php  }else if(isset($blog_single) && $blog_single == 'school'){ ?>
    <title>{{$schooldata->school_name}}</title>
    <meta name="description" content="{{$schooldata->school_name}}">
    <meta name="keywords" content="{{$schooldata->school_name}}">
    <?php  }else if(isset($blog_single) && $blog_single == 'event'){ ?>
    <title>{{$sportData->description}}</title>

    <meta name="description" content="{{$sportData->description}}">
    <meta name="keywords" content="{{$sportData->title}}">
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.meanmenu/2.0.6/meanmenu.min.css" />
    <link rel="stylesheet" href="{{ asset('css/icono.min.css') }}">
    <!-- <script type="text/javascript" src="{{ asset('js/retina.js') }}"></script> -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--<script type="text/javascript" src="//cdn.jsdelivr.net/vue/2.3.2/vue.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>
    <style>
        .col-md-4.col-sm-12.right-content.addblur-class-search-school .right-box {
    opacity:.5;
    pointer-events:none;
}


.main-inner.addblur-class-school ul.pagination li {
    opacity:.5;
    pointer-events: none;
}

.main-inner.addblur-class-school .events-section.atheletes-main .events-content{
    opacity:.3;
    pointer-events: none;
}

.main-inner.addblur-class-school .events-section.atheletes-main .events-content:nth-child(1),
.main-inner.addblur-class-school .events-section.atheletes-main .events-content:nth-child(2),
.main-inner.addblur-class-school .events-section.atheletes-main .events-content:nth-child(3){
    opacity:1;
    pointer-events:all;
}

.filter-section.addblur-event-search {
    opacity: .5;
    pointer-events: none;
}

.main-container.main-inner.addblur-event-table .events-content:nth-of-type(n+7) {
    opacity: .5;
    pointer-events: none;
}

.main-container.main-inner.addblur-event-table ul.pagination li {
        opacity: .5;
    pointer-events: none;
}

.col-md-8.padding-right.addblur-userlist-athlete ul.pagination li {
    opacity:.5;
    pointer-events: none;
}
.col-md-8.padding-right.addblur-userlist-athlete .events-content:nth-of-type(n+4) {
    opacity: .5;
    pointer-events: none;
}

.btn.light.addblurrClass {
    opacity: .5;
    pointer-events: none;
}

.btn.orange.addblurrClass {
    opacity: .5;
    pointer-events: none ;
}

/*.right-content .right-box.addblur-filter-athlete {
    opacity:.5;
    pointer-events: none;
}*/


    </style>
</head>
<body>
    <?php
function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";
    $ub = "";
    $ua = "";
    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

// now try it
$ua=getBrowser();
$yourbrowser= $ua['name']."-".$ua['version'];
// print_r($yourbrowser);die;
?>
<?php
use App\Http\Controllers\MenuController;
?>
<div id="app">
    <header class="main-header">
        <div class="col-md-1">
            <div class="logo">
              <a href="{{ url('/') }}"><img src="{{ asset( 'storage/' . Voyager::setting('logo') ) }}" data-rjs="{{ asset( 'storage/' . Voyager::setting('site_logo_2x') ) }}" /></a>
            </div>
        </div>
        <div class="col-md-9 menu-top">
            <div class="main-menu">
                <ul>
                    @if( Auth::guest() )
                        {!! MenuController::display('not-logged-in', 'header_nav') !!}
                    @else
                        @if( Auth::user()->role_id == 1 )
                            {!! MenuController::display('admin-front', 'header_nav') !!}
                        @elseif(Auth::user()->role_id == 4)
                    
                           {!! MenuController::display('coach-header', 'header_nav') !!}    
                        @else
                            {!! MenuController::display('logged-in', 'header_nav') !!}    
                        @endif
                    @endif
                    @if (Auth::guest())
                        <li><a @click.prevent="login_form" id="show_login" href="{{ route('login') }}">Login</a></li>
                    @endif
                </ul>
            </div>
        </div>
      
        <div class="col-md-2 sign-up pull-right">
            @if(Auth::guest())
                <a class="btn orange" href="#" @click="signup_open" >Sign Up Now</a>
            @else
                <?php
                    $user_avatar = Voyager::image(Auth::user()->avatar);
                    if ((substr(Auth::user()->avatar, 0, 7) == 'http://') || (substr(Auth::user()->avatar, 0, 8) == 'https://')) {
                        $user_avatar = Auth::user()->avatar;
                    }
                ?>
                <?php  
                if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
                    // $user = App\User::find(Auth::User()->id);
                    // var_dump(Auth::user()->subscribed('main') );die;
                    $userCreatedDate1 = (array) Auth::User()->created_at;
                    $current_date = new \DateTime();
                    $current_date = $current_date->format('Y-m-d 24:59:59');
                    $userCreatedDate = new \DateTime($userCreatedDate1['date']);
                    $userCreatedDate = $userCreatedDate->format('Y-m-d 00:00:00');
                    $datediff = strtotime($current_date) - strtotime($userCreatedDate);
                    $diff1 = floor($datediff / (60 * 60 * 24));
                    if (Auth::user()->subscribed('main') || ( $diff1 < 15 || Auth::user()->is_paid == 'yes') ){
                            ?>
                            @if(Auth::User()->role_id != 1)
                            <?php 
                            $getNotiUnRead = \App\Notification::where('user_id',\Auth::User()->id)->where('key','profile_viewed')->where('is_seen','0')->count();  ?>
                            <ul class="nav navbar-nav user_notifications">
                                <li class="dropdown click-noti">
                                    <a href="/notifications">   <span class="notification_icon"><i class="fa fa-bell  fa-border ">
                                        @if($getNotiUnRead > 0)<span class="badge notification-count">{{$getNotiUnRead}}</span>@endif
                                    </i></span></a>
                                </li>
                            </ul>
                            @endif
                       <?php }
                    }else{ ?>
                    @if(Auth::User()->role_id != 1)
                    <?php 
                    $getNotiUnRead = \App\Notification::where('user_id',\Auth::User()->id)->where('key','profile_viewed')->where('is_seen','0')->count();  ?>
                    <ul class="nav navbar-nav user_notifications">
                        <li class="dropdown click-noti">
                            <a href="/notifications">   <span class="notification_icon"><i class="fa fa-bell  fa-border ">
                                @if($getNotiUnRead > 0)<span class="badge notification-count">{{$getNotiUnRead}}</span>@endif
                            </i></span></a>
                        </li>
                    </ul>
                    @endif
                    <?php   }
                ?>
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown profile">
                        <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button"
                           aria-expanded="false"><img src="<?php if($user_avatar == 'users/default.png' || $user_avatar == ''){ echo env('APP_URL').'/storage/EFuG6j0fKnCeYPkABRMIvbzHdUi2rLC4xB24f4lB.png'; } else{ echo $user_avatar ; } ?>" class="profile-img"> <span class="caret">{{ Auth::user()->name }}</span></a>
                        <ul class="dropdown-menu dropdown-menu-animated">
                            @if(\Auth::User()->role_id == 1)
                             <li><a href="{{ url('/admin') }}">Admin Dashboard</a></li>
                             @endif
                            <?php $nav_items = config('voyager.dashboard.navbar_items'); ?>
                            @if (Auth::guest())
                            @else
                            <li><a href="{{ url('/profile') }}">My Profile</a></li>
                            <li><a href="{{ url('/change/password') }}">Settings</a></li>
                           
                                @if((\Auth::User()->role_id == 2 || \Auth::User()->role_id == 3 ) )
                                <?php 
                                $plan = App\Plan::find(1);     ?>
                                    <li><a href="{{ url('/plan/'.$plan->slug) }}">Subscriptions</a></li>
                                    <li><a href="{{ url('/payments/history/athlete') }}">Payment Info</a></li>
                                @endif
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            @endif
                        </ul>
                    </li>
                </ul>
               
            @endif
        </div>
      
    </header>
    @if(!Auth::guest())
    @if(Auth::User()->role_id != 1)
        @if( Auth::User()->role_id == 2 ||  Auth::User()->role_id == 3)
            @if(! \Auth::User()->subscribed('main') && \Auth::user()->is_paid != 'yes' )
                @if( Voyager::setting('profile_upgrade_header_athlete') )
                    <div class="notification-bar">
                        <a class="close-notification-bar"></a>
                        <p class="account-upgrade" style="color: #000;">
                        <?php $header = str_replace( '<p>','', Voyager::setting('profile_upgrade_header_athlete') );
                        $header = str_replace( '</p>','', $header );
                         ?>
                        {!! $header !!}
                        </p>
                    </div>
                @endif
            @endif
        @endif
        @if( Auth::User()->role_id == 4)
            @if(\Auth::User()->user_approved == 'INACTIVE')
                @if( Voyager::setting('profile_upgrade_header_coach') )
                    <div class="notification-bar">
                        <a class="close-notification-bar"></a>
                        <p class="account-upgrade">
                            <?php $header2 = str_replace( '<p>','', Voyager::setting('profile_upgrade_header_coach') );
                            $header2 = str_replace( '</p>','', $header2 );
                             ?>
                            {!! $header2 !!}
                        </p>
                    </div>
                @endif
            @endif
        @endif
    @endif
    @endif
        @if(isset($page_id))
            @if( Voyager::page('image',$page_id) )
            <div class="main-banner inner-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::page('image', $page_id) ) }} )">
                <div class="container">
                    @if( Voyager::page('banner_heading', $page_id) )
                        <h1>{{ Voyager::page('banner_heading', $page_id) }}</h1>
                    @endif  
                    @if( Voyager::page('banner_subheading',$page_id) )
                        <h2>{{ Voyager::page('banner_subheading', $page_id) }}</h2>
                    @endif  
                </div>
            </div>
            @endif
        @endif
        
        <div class="main-container main-inner">
            @yield('content')
        </div>
        
    @if (Auth::guest())
        @if(isset($page_id))
            @if( Voyager::page('cta_show',$page_id) == 'show' )
                @if( Voyager::page('cta_bg_image', $page_id) )
                <div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::page('cta_bg_image', $page_id) ) }} )">
                    <div class="container">
                        {!! Voyager::page('cta_content', $page_id) !!}
                    </div>
                </div>
                @else
                <div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::setting('cta_bg_image') ) }} )">
                    <div class="container">
                        {!! Voyager::setting('cta_content') !!}
                    </div>
                </div>
                @endif
            @endif
        @endif
        @endif
        <?php $valid = '' ;?>
        @if (Auth::user())
        <?php 
        if( Auth::user()->role_id == 2 || Auth::user()->role_id == 3){
            $userInfo = \App\UserInformation::where('user_id',Auth::user()->id)->where('meta_key','email_counter')->first();
            // print_r(Auth::user()->is_paid);
        if(( ! Auth::user()->subscribed('main') && Auth::user()->is_paid != 'yes' ) && ((isset($userInfo->meta_value)?$userInfo->meta_value:'0') >= 5 ) ){
            $valid = 0;
        }else{
            $valid = 1;
        }
        }else{
            $valid = 1;
        }
        ?>
        @endif
        <?php 
        // print_r($valid.'-----');die;
        if( isset($templatedata) ):  
        if( Auth::user()->role_id == 4 ){
            $shortcodes = array( 
                'event' => '{athlete_name} {athlete_school}   {event_title} {event_description} {event_event_start} {event_event_end} {event_place} {event_city} {event_state} {event_zipcode} {sport_title} {event_division} {event_tournaments} {event_campus_clinics} {event_school_attending} {my_name} {my_email} {my_sport} {college_name} {my_phone} {my_web} {ncaa_championship} {national_tournament} {ncaa_final_four} {ncaa_post_season_appearance} {conference_championship} {my_bio} {my_title} {my_videogallery_urls} {school_information} {gender_coached}',
                'without' => '{athlete_name} {athlete_school}   {my_name} {my_email} {my_sport} {college_name} {my_phone} {my_web} {ncaa_championship} {national_tournament} {ncaa_final_four} {ncaa_post_season_appearance} {conference_championship} {my_bio} {my_title} {my_videogallery_urls} {school_information} {gender_coached}' );
        }else{
            $shortcodes = array( 
                'event' => '{coach_name} {college_name}   {event_title} {event_description} {event_event_start} {event_event_end} {event_place} {event_city} {event_state} {event_zipcode} {sport_title} {event_division} {event_tournaments} {event_campus_clinics} {event_school_attending} {my_name} {my_email} {my_position} {athlete_school} {class_year} {my_phone} {club_name} {club_jersey_number} {gpa} {my_facebook} {my_twitter} {my_instagram} {my_snapchat} {my_skype} {my_videogallery_urls} {my_sport} {my_address} {my_height} {my_weight} {my_birth} {my_academics} {my_club_info} {athletic_achievements} {academic_achievement} {extra_curricular_activity}', 
                'without' => '{coach_name}  {college_name}   {my_name} {my_email} {my_position} {athlete_school} {class_year} {club_name} {club_jersey_number} {gpa} {my_phone} {my_facebook} {my_twitter} {my_instagram} {my_snapchat} {my_skype} {my_videogallery_urls} {my_sport} {my_address} {my_height} {my_weight} {my_birth} {my_academics} {my_club_info} {athletic_achievements} {academic_achievement} {extra_curricular_activity}' );
        }

        ?>
        
        <div class="log-in-popup email email-popup" v-cloak v-show="showpop_email">
            <email :validemail= "{{$valid}}" :shortcodes="{{json_encode($shortcodes)}}" :templateda= "{{$templatedata}}" v-bind:userdata="dataform" v-bind:userdatabkd="dataform" v-bind:event_id="email_event_id"></email>
        </div>
        <!-- <div class="log-in-popup signup_attend email-popup" v-cloak v-show="sendemail_attend">
            <sendemail_attendee :templateda= "{{$templatedata}}"></sendemail_attendee>
        </div> -->
        <?php
        endif;
        ?>
        
    <footer id="footer" class="main-footer">
        <div class="top-footer">
            <div class="container">
                <div class="col-md-6">
                    <a href="{{ url('/') }}"><img src="{{ asset( 'storage/' . Voyager::setting('footer_logo') ) }}" ></a>
                    <div class="social">
                        <ul>
                            <li><a href="{{ Voyager::setting('facebook_url') }}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="{{ Voyager::setting('Twitter_url') }}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <!-- <li><a href="{{ Voyager::setting('instagram_url') }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li> -->
                            <!-- <li><a href="{{ Voyager::setting('snapchat_url') }}"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></a></li> -->
                            <li><a href="{{ Voyager::setting('youtube_url') }}"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            <li><a href="{{ Voyager::setting('linkedin_url') }}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <h2>athletes</h2>
                    <ul>
                        {!! MenuController::display('athletes', 'header_nav') !!}
                    </ul>
                </div>
                @if( Auth::user() )
                    @if( \Auth::user()->role_id == 4 )

                        <div class="col-md-2 col-sm-4">
                            <h2>coaches</h2>
                            <ul>
                                {!! MenuController::display('COACHES', 'header_nav') !!}
                            </ul>
                        </div>

                        @else

                        <div class="col-md-2 col-sm-4">
                            <h2>coaches</h2>
                            <ul>
                                {!! MenuController::display('Coach-Login', 'header_nav') !!}
                            </ul>
                        </div>

                    @endif
                @else
                <div class="col-md-2 col-sm-4">
                            <h2>coaches</h2>
                            <ul>
                                {!! MenuController::display('COACHES', 'header_nav') !!}
                            </ul>
                        </div>
                @endif      

                <div class="col-md-2 col-sm-4">
                    <h2>company</h2>
                    <ul>
                        {!! MenuController::display('COMPANY', 'header_nav') !!}
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="container">
                <div class="col-md-8">
                    {!! Voyager::setting('footer_left') !!}
                </div>
                <div class="col-md-4 text-right">
                    {!! Voyager::setting('footer_right') !!}
                </div>
            </div>
        </div>
        @if (Auth::guest())
            <div v-cloak v-show="login_popup" class="log-in-popup">
                <div class="sign-up-content">
                    <a @click="login_close" href="#" class="close"><i class="icono-cross"></i></a>
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <login></login>
                        <!-- <div class="radio-section">
                            <a @click="login_url(2)"  href="#" :class="athelete_class"><input type="radio" checked name="role" value="2">i&#39;m an athlete</a>
                            <a @click="login_url(4)" href="#" :class="coach_class"><input type="radio" name="role" value="4">i&#39;m a coach</a>
                            <a @click="login_url(3)" href="#" :class="parent_class"><input type="radio" name="role" value="3">i&#39;m a parent</a> 
                        </div> -->
                        <div class="social-signup">
                            <social-login  :loginid="{{ json_encode('none') }}" ></social-login>
                        </div>
                    </div>
                </div>
            </div>
            @include('parts.signuppopup')
        @endif
    </footer>
</div>


<link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css" />
@if (Auth::guest())
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
@elseif( $yourbrowser == 'Internet Explorer-10.0' || $yourbrowser == 'Internet Explorer-9.0')
<script type="text/javascript" src="{{asset('js/profile1.js')}}"></script>
@else
<script type="text/javascript" src="{{asset('js/profile.js')}}"></script>
@endif
@yield('braintree')
<style type="text/css">
    form.details.no-padding {
        padding: 0;
    }
</style>
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?5NiKDErc1Vh5FbjXO06MFai3ygPTzMaZ";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.meanmenu/2.0.6/jquery.meanmenu.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<!-- <script src="{{ asset('/js/jquery-mask.js') }}"></script> -->
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.2/select2.min.js"></script>-->
<script type="text/javascript">
window.onload = function() {
@if(Auth::user())
    window.addEventListener("beforeunload", function (e) {
        // console.log( typeof($("#user1").val()) );
        @if(Auth::user()->role_id == 4)
            if(  $("#coach1").val() != 'false' && $("#coach2").val() != 'false' && $("#coach3").val() != 'false' ){
        @else
            if ( $("#user1").val() != 'false' && $("#user2").val() != 'false' && $("#user3").val() != 'false' && $("#user4").val() != 'false' && $("#academic").val() != 'false' && $("#sports").val() != 'false' && $("#athletic_ach").val() != 'false' && $("#academics").val() != 'false' && $("#awards").val() != 'false' && $("#extra_curr").val() != 'false' && $("#testimonials").val() != 'false' ) {
        @endif
            return undefined;
        }
        var confirmationMessage = 'It looks like you have been editing something. '
                                + 'If you leave before saving, your changes will be lost.';
        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    });
@endif
};
$( "#calendar_birth" ).datepicker({
    //dateFormat: 'yy-mm-dd',
    changeMonth: true, 
    changeYear: true, 
    yearRange: "-30:+00",
    // onChangeMonthYear: function(year, month, inst) {
    //   var birth = $('#calendar_birth').val();
    //      if(birth != ''){
    //          console.log(month);
    //          var res = birth.split("/");
    //          if(res[0] != ''){
    //          console.log(month);
    //             $(this).val(res[0]+"/"+month + "/" + year);
    //          }
    //         }
 //        },


});
$('#calendar_birth').change(function(){
    console.log($('#calendar_birth').val());
    var birth = $('#calendar_birth').val();
    $('#calendar_birth1').val(birth);
    $('span.birthnew').empty();
    $('span.birthnew').append($('#calendar_birth').val());
});
// $(document).ready(function(){
//     $('[data-toggle="tooltip"]').tooltip();   
// });
jQuery( '.radio-section a' ).on( 'click', function(e) {
    e.preventDefault();
    $('.radio-section a').removeClass('checked');
    $(this).addClass('checked');
    $('.radio-section a input').prop( "checked", false );
    $(this).children('input').prop( "checked", true )
});

jQuery('header .main-menu').meanmenu({
    meanMenuContainer: '.menu-top',
    meanScreenWidth: "767"
});
// jQuery(document).ready(function($) {
//  $(".phone").mask("(999) 999-9999");
//  $(".fax").mask("(999) 999-9999");
//  $(".zipmask").mask("99999");
// });

</script>
<script>
jQuery( document ).ready(function($) {
    jQuery( '.checkboxrequired p a' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').show();
});
    jQuery( '.close-term' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').hide();});
});
</script>
<script>
jQuery( document ).ready(function($) {
    jQuery( 'form .termpop' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').show();
});
    jQuery( '.close-term' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').hide();});
});
</script>
<script>
jQuery( document ).ready(function($) {
    jQuery( '.userform form a' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').show();
});
    jQuery( '.close-term' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').hide();});
});
</script>
<script >
$( document ).ready(function() {
    jQuery( '.close-notification-bar' ).on( 'click', function(e) {
    e.preventDefault();
    $('.notification-bar').remove();
    $(document.body).css("margin-top",'0');
});
    $(window).resize(function() {
        $(document.body).css("margin-top", $(".notification-bar").height());
    }).resize();
});

</script>
</body>
</html>