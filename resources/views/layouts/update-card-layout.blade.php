<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <meta property="fb:app_id" content="1904642086449795" />
	<!-- <meta property="og:url" content="" /> -->
	<meta property="og:type" content="article" />
	<meta property="og:title"  content="Sport Contact USA" />
	<meta property="og:description" content="Share Event Details" />
	 <meta property="og:image"  content="https://res.cloudinary.com/webforte/image/upload/w_200,h_200/v1514534021/scusax200_hjyfsk.png" />
	  
	<title>{{ Voyager::setting('title') }}</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
	<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.meanmenu/2.0.6/meanmenu.min.css" /> -->
	<link rel="stylesheet" href="{{ asset('css/icono.min.css') }}">
	<!-- <script type="text/javascript" src="{{ asset('js/retina.js') }}"></script> -->
	<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
	<!--<script type="text/javascript" src="//cdn.jsdelivr.net/vue/2.3.2/vue.js"></script>-->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script> -->
	<style>
		.col-md-4.col-sm-12.right-content.addblur-class-search-school .right-box {
	opacity:.5;
	pointer-events:none;
}


.main-inner.addblur-class-school ul.pagination li {
	opacity:.5;
	pointer-events: none;
}

.main-inner.addblur-class-school .events-section.atheletes-main .events-content{
	opacity:.3;
	pointer-events: none;
}

.main-inner.addblur-class-school .events-section.atheletes-main .events-content:nth-child(1),
.main-inner.addblur-class-school .events-section.atheletes-main .events-content:nth-child(2),
.main-inner.addblur-class-school .events-section.atheletes-main .events-content:nth-child(3){
	opacity:1;
	pointer-events:all;
}

.filter-section.addblur-event-search {
    opacity: .5;
    pointer-events: none;
}

.main-container.main-inner.addblur-event-table .events-content:nth-of-type(n+7) {
    opacity: .5;
    pointer-events: none;
}

.main-container.main-inner.addblur-event-table ul.pagination li {
        opacity: .5;
    pointer-events: none;
}

.col-md-8.padding-right.addblur-userlist-athlete ul.pagination li {
	opacity:.5;
	pointer-events: none;
}
.col-md-8.padding-right.addblur-userlist-athlete .events-content:nth-of-type(n+4) {
    opacity: .5;
    pointer-events: none;
}

.btn.light.addblurrClass {
    opacity: .5;
    pointer-events: none;
}

.btn.orange.addblurrClass {
    opacity: .5;
    pointer-events: none ;
}

/*.right-content .right-box.addblur-filter-athlete {
	opacity:.5;
	pointer-events: none;
}*/


	</style>

<style>
.credit-card-box .panel-title {
    display: inline;
    font-weight: bold;
}
.credit-card-box .form-control.error {
    border-color: red;
    outline: 0;
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(255,0,0,0.6);
}
.credit-card-box label.error {
  font-weight: bold;
  color: red;
  padding: 2px 8px;
  margin-top: 2px;
}
.credit-card-box .payment-errors {
  font-weight: bold;
  color: red;
  padding: 2px 8px;
  margin-top: 2px;
}
.credit-card-box label {
    display: block;
}
/* The old "center div vertically" hack */
.credit-card-box .display-table {
    display: table;
}

</style>

</head>
<body>
	<?php
function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

// now try it
$ua=getBrowser();
$yourbrowser= $ua['name']."-".$ua['version'];
// print_r($yourbrowser);die;
?>
<?php
use App\Http\Controllers\MenuController;
?>
<div id="app">
	<header class="main-header">
    	<div class="col-md-1">
            <div class="logo">
              <a href="{{ url('/') }}"><img src="{{ asset( 'storage/' . Voyager::setting('logo') ) }}" data-rjs="{{ asset( 'storage/' . Voyager::setting('site_logo_2x') ) }}" /></a>
            </div>
        </div>
        <div class="col-md-9 menu-top">
            <div class="main-menu">
            	<ul>
            		@if( Auth::guest() )
            			{!! MenuController::display('not-logged-in', 'header_nav') !!}
            		@else
            			@if( Auth::user()->role_id == 1 )
                            {!! MenuController::display('admin-front', 'header_nav') !!}
                        @elseif(Auth::user()->role_id == 4)
                    
                           {!! MenuController::display('coach-header', 'header_nav') !!}    
                        @else
                            {!! MenuController::display('logged-in', 'header_nav') !!}    
                        @endif
            		@endif
	                @if (Auth::guest())
	                	<li><a @click.prevent="login_form" id="show_login" href="{{ route('login') }}">Login</a></li>
	                @endif
	            </ul>
            </div>
        </div>
      
        <div class="col-md-2 sign-up pull-right">
        	@if(Auth::guest())
    	    	<a class="btn orange" href="#" @click="signup_open" >Sign Up Now</a>
        	@else
        		<?php
					$user_avatar = Voyager::image(Auth::user()->avatar);
					if ((substr(Auth::user()->avatar, 0, 7) == 'http://') || (substr(Auth::user()->avatar, 0, 8) == 'https://')) {
						$user_avatar = Auth::user()->avatar;
					}
				?>
				<?php  
				if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
					// $user = App\User::find(Auth::User()->id);
					// var_dump(Auth::user()->subscribed('main') );die;
		            $userCreatedDate1 = (array) Auth::User()->created_at;
		            $current_date = new \DateTime();
		            $current_date = $current_date->format('Y-m-d 24:59:59');
		            $userCreatedDate = new \DateTime($userCreatedDate1['date']);
		            $userCreatedDate = $userCreatedDate->format('Y-m-d 00:00:00');
		            $datediff = strtotime($current_date) - strtotime($userCreatedDate);
		            $diff1 = floor($datediff / (60 * 60 * 24));
		            if (Auth::user()->subscribed('main') || ( $diff1 < 15 || Auth::user()->is_paid == 'yes') ){
		                    ?>
		                    @if(Auth::User()->role_id != 1)
							<?php 
				          	$getNotiUnRead = \App\Notification::where('user_id',\Auth::User()->id)->where('key','profile_viewed')->where('is_seen','0')->count();  ?>
				    	 	<ul class="nav navbar-nav user_notifications">
					            <li class="dropdown click-noti">
					            	<a href="/notifications"> 	<span class="notification_icon"><i class="fa fa-bell  fa-border ">
					            		@if($getNotiUnRead > 0)<span class="badge notification-count">{{$getNotiUnRead}}</span>@endif
					            	</i></span></a>
					            </li>
					        </ul>
					        @endif
		               <?php }
		            }else{ ?>
		            @if(Auth::User()->role_id != 1)
					<?php 
		          	$getNotiUnRead = \App\Notification::where('user_id',\Auth::User()->id)->where('key','profile_viewed')->where('is_seen','0')->count();  ?>
		    	 	<ul class="nav navbar-nav user_notifications">
			            <li class="dropdown click-noti">
			            	<a href="/notifications"> 	<span class="notification_icon"><i class="fa fa-bell  fa-border ">
			            		@if($getNotiUnRead > 0)<span class="badge notification-count">{{$getNotiUnRead}}</span>@endif
			            	</i></span></a>
			            </li>
			        </ul>
			        @endif
		        	<?php   }
		        ?>
				
				<ul class="nav navbar-nav navbar-right">
		            <li class="dropdown profile">
		                <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button"
		                   aria-expanded="false"><img src="<?php if($user_avatar == 'users/default.png' || $user_avatar == ''){ echo env('APP_URL').'/storage/EFuG6j0fKnCeYPkABRMIvbzHdUi2rLC4xB24f4lB.png'; } else{ echo $user_avatar ; } ?>" class="profile-img"> <span class="caret">{{ Auth::user()->name }}</span></a>
		                <ul class="dropdown-menu dropdown-menu-animated">
		                 	@if(\Auth::User()->role_id == 1)
		                     <li><a href="{{ url('/admin') }}">Admin Dashboard</a></li>
		                     @endif
		                    <?php $nav_items = config('voyager.dashboard.navbar_items'); ?>
		                   	@if (Auth::guest())
		                   	@else
		                   	<li><a href="{{ url('/profile') }}">My Profile</a></li>
		                   	<li><a href="{{ url('/change/password') }}">Settings</a></li>
		                   
		                   	    @if((\Auth::User()->role_id == 2 || \Auth::User()->role_id == 3 ) )
                                <?php 
                                $plan = App\Plan::find(1);     ?>
			                    	<li><a href="{{ url('/plan/'.$plan->slug) }}">Subscriptions</a></li>
			                   		<li><a href="{{ url('/payments/history/athlete') }}">Payment Info</a></li>
			                    @endif
		                    <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
		                    @endif
		                </ul>
		            </li>
		        </ul>
		       
        	@endif
        </div>
      
	</header>
		@if(isset($page_id))
    		@if( Voyager::page('image',$page_id) )
    		<div class="main-banner inner-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::page('image', $page_id) ) }} )">
    			<div class="container">
    				@if( Voyager::page('banner_heading', $page_id) )
    					<h1>{{ Voyager::page('banner_heading', $page_id) }}</h1>
    				@endif	
    				@if( Voyager::page('banner_subheading',$page_id) )
    					<h2>{{ Voyager::page('banner_subheading', $page_id) }}</h2>
    				@endif	
    			</div>
    		</div>
    		@endif
    	@endif
    	
    	<div class="main-container main-inner">
    	    @yield('content')
    	</div>
    	
	@if (Auth::guest())
	    @if(isset($page_id))
		    @if( Voyager::page('cta_show',$page_id) == 'show' )
	    		@if( Voyager::page('cta_bg_image', $page_id) )
	    		<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::page('cta_bg_image', $page_id) ) }} )">
	    			<div class="container">
	    				{!! Voyager::page('cta_content', $page_id) !!}
	    			</div>
	    		</div>
	    		@else
				<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::setting('cta_bg_image') ) }} )">
					<div class="container">
						{!! Voyager::setting('cta_content') !!}
					</div>
				</div>
				@endif
	    	@endif
    	@endif
    	@endif
    	
	<footer id="footer" class="main-footer">
		<div class="top-footer">
			<div class="container">
				<div class="col-md-6">
					<a href="{{ url('/') }}"><img src="{{ asset( 'storage/' . Voyager::setting('footer_logo') ) }}" ></a>
					<div class="social">
						<ul>
							<li><a href="{{ Voyager::setting('facebook_url') }}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="{{ Voyager::setting('Twitter_url') }}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<!-- <li><a href="{{ Voyager::setting('instagram_url') }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li> -->
							<!-- <li><a href="{{ Voyager::setting('snapchat_url') }}"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></a></li> -->
							<li><a href="{{ Voyager::setting('youtube_url') }}"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							<li><a href="{{ Voyager::setting('linkedin_url') }}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<h2>athletes</h2>
					<ul>
						{!! MenuController::display('athletes', 'header_nav') !!}
					</ul>
				</div>
				@if( Auth::user() )
					@if( \Auth::user()->role_id == 4 )

						<div class="col-md-2 col-sm-4">
							<h2>coaches</h2>
							<ul>
								{!! MenuController::display('COACHES', 'header_nav') !!}
							</ul>
						</div>

						@else

						<div class="col-md-2 col-sm-4">
							<h2>coaches</h2>
							<ul>
								{!! MenuController::display('Coach-Login', 'header_nav') !!}
							</ul>
						</div>

					@endif
				@else
				<div class="col-md-2 col-sm-4">
							<h2>coaches</h2>
							<ul>
								{!! MenuController::display('COACHES', 'header_nav') !!}
							</ul>
						</div>
				@endif		

				<div class="col-md-2 col-sm-4">
					<h2>company</h2>
					<ul>
						{!! MenuController::display('COMPANY', 'header_nav') !!}
					</ul>
				</div>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="container">
				<div class="col-md-8">
					{!! Voyager::setting('footer_left') !!}
				</div>
				<div class="col-md-4 text-right">
					{!! Voyager::setting('footer_right') !!}
				</div>
			</div>
		</div>
	</footer>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script>
var $form = $('#payment-form');
$form.find('.subscribe').on('click', payWithStripe);
function payWithStripe(e) {
    e.preventDefault();
    if (!validator.form()) {
        return;
    }

    /* Visual feedback */
    $form.find('.subscribe').html('Validating <i class="fa fa-spinner fa-pulse"></i>').prop('disabled', true);

    var PublishableKey = 'pk_test_6pRNASCoBOKtIshFeQd4XMUh'; // Replace with your API publishable key
    Stripe.setPublishableKey(PublishableKey);
    
    /* Create token */
    var expiry = $form.find('[name=cardExpiry]').payment('cardExpiryVal');
    var creditcardtoken = $('.credit-card-token').val();
    var ccData = {
        number: $form.find('[name=cardNumber]').val().replace(/\s/g,''),
        // cvc: $form.find('[name=cardCVC]').val(),
        exp_month: expiry.month, 
        exp_year: expiry.year,
       
    };
    
    Stripe.card.createToken(ccData, function stripeResponseHandler(status, response) {
        if (response.error) {
            /* Visual feedback */
            $form.find('.subscribe').html('Try again').prop('disabled', false);
            /* Show Stripe errors on the form */
            $form.find('.payment-errors').text(response.error.message);
            $form.find('.payment-errors').closest('.row').show();
        } else {
            /* Visual feedback */
            $form.find('.subscribe').html('Processing <i class="fa fa-spinner fa-pulse"></i>');
            /* Hide Stripe errors on the form */
            $form.find('.payment-errors').closest('.row').hide();
            $form.find('.payment-errors').text("");
            
            $.ajax({
                    type: 'GET',
                    url: '/updateCardDetailsSave',
                    data: {
                        token:ccData,
                        cardtoken:creditcardtoken
                    },
                    success: function(data) {
                        // console.log(data);
                        if(data == 'yes'){
                        $form.find('.subscribe').html('Card Updated successful <i class="fa fa-check"></i>');
                window.location.href = '/payments/history/athlete';
                        }else{
                            // alert(error);
                    alert('Please enter correct card details!');
                    location.reload();
                        }
                    },
                    errors: function (errors) {

                    }
                });
            // $.get('https://sportcontactusa.com/updateCardDetailsSave/', {
            //         token: ccData,
            //         cardtoken: creditcardtoken
            //     })
            //     // Assign handlers immediately after making the request,
            //     .done(function(data, textStatus, jqXHR) {
            //         $form.find('.subscribe').html('Card Updated successful <i class="fa fa-check"></i>');
            //     window.location.href = 'https://sportcontactusa.com/payments/history/athlete';
            //     })
            //     .fail(function(jqXHR, textStatus, errorThrown) {
                
            //     });
        }
    });
}

$('input[name=cardNumber]').payment('formatCardNumber');
$('input[name=cardExpiry').payment('formatCardExpiry');

jQuery.validator.addMethod("cardNumber", function(value, element) {
    return this.optional(element) || Stripe.card.validateCardNumber(value);
}, "Please specify a valid credit card number.");

jQuery.validator.addMethod("cardExpiry", function(value, element) {    
    value = $.payment.cardExpiryVal(value);
    return this.optional(element) || Stripe.card.validateExpiry(value.month, value.year);
}, "Invalid expiration date.");


validator = $form.validate({
    rules: {
        cardNumber: {
            required: true,
            cardNumber: true            
        },
        cardExpiry: {
            required: true,
            cardExpiry: true
        },
        // cardCVC: {
        //     required: true,
        //     cardCVC: true
        // }
    },
    highlight: function(element) {
        $(element).closest('.form-control').removeClass('success').addClass('error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-control').removeClass('error').addClass('success');
    },
    errorPlacement: function(error, element) {
        $(element).closest('.form-group').append(error);
    }
});

paymentFormReady = function() {
    if ($form.find('[name=cardNumber]').hasClass("success") &&
        $form.find('[name=cardExpiry]').hasClass("success")) {
        return true;
    } else {
        return false;
    }
}

$form.find('.subscribe').prop('disabled', true);
var readyInterval = setInterval(function() {
    if (paymentFormReady()) {
        $form.find('.subscribe').prop('disabled', false);
        clearInterval(readyInterval);
    }
}, 250);
</script>
</div>
</body>
</html>