@if (session('success'))
    <div class="alert alert-success homepage sign-up-message">
    	<span class="close-sign-up-message"><a href="/"></a></span>
        {{ session('success') }}
    </div>
     <!-- <div class="sign-up-message"> {{ session('success') }}<br /></div> -->
@endif

@if (session('error'))
    <div class="alert alert-danger homepage sign-up-message">
    	<span class="close-sign-up-message"><a href="/"></a></span>
        {{ session('error') }}
        @if( session('error') == "Account doesn't exist, Please sign up" )
            <br />
            <h2 style="text-align: center;"><a class="btn orange" href="/signup">SIGN UP NOW</a></h2>
        @endif
    </div>
     <!-- <div class="sign-up-message"> {{ session('error') }}<br /></div> -->
@endif