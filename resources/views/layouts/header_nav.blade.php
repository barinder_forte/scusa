<?php //echo "<pre>"; var_dump((array) $items); ?>
@foreach( $items as $item )
    <?php $count = 0; ?>
    @foreach( $items as $repeat )
        @if( $item->id == $repeat->parent_id )
            <?php $count++; ?>
        @endif
    @endforeach
    @if( $item->parent_id == '' )
        <li class="@if( $count > 0 ){{'has-children'}}@endif">
            <a href="{{ $item->url }}" target="{{ $item->target }}">{{ $item->title }}</a>
            @if( $count > 0 )
            <ul class="childrens">
                @foreach( $items as $child )
                    @if( $item->id == $child->parent_id )
                        <li><a href="{{ $child->url }}" target="{{ $child->target }}">{{ $child->title }}</a></li>
                    @endif
                @endforeach
            </ul>
            @endif
        </li>
    @endif
@endforeach
