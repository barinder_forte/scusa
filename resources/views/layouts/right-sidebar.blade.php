</html>
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<?php if(isset($page->id)){
    $metadisc =  Voyager::page('meta_description', $page->id);
    if($metadisc == ''){
        $metadisc = 'Sport Contact USA';
    }
    $metakey =  Voyager::page('meta_keywords', $page->id);
    if($metakey == ''){
        $metakey = 'Sport Contact USA';
    }
    $seo_title =  Voyager::page('seo_title', $page->id);
    if($seo_title == ''){
        $seo_title = 'Sport Contact USA';
    }
    ?>
    <title><?php echo $seo_title; ?></title>
   
    <meta name="description" content="<?php echo $metadisc; ?>">
    <meta name="keywords" content="<?php echo $metakey; ?>">

    <?php }else{ ?>

    	<title>{{ Voyager::setting('title') }}</title>


   <?php } ?>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.meanmenu/2.0.6/meanmenu.min.css" />
	<link rel="stylesheet" href="{{ asset('css/icono.min.css') }}">
	<!-- <script type="text/javascript" src="{{ asset('js/retina.js') }}"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>
</head>
<body>
<?php
use App\Http\Controllers\MenuController;
?>
<div id="app">
	<header class="main-header">
    	<div class="col-md-1">
            <div class="logo">
              <a href="{{ url('/') }}"><img src="{{ asset( 'storage/' . Voyager::setting('logo') ) }}" data-rjs="{{ asset( 'storage/' . Voyager::setting('site_logo_2x') ) }}" /></a>
            </div>
        </div>
        <div class="col-md-9 menu-top">
            <div class="main-menu">
            	<ul>
            		@if( Auth::guest() )
            			<li class="mobile" style="display: none"><a  href="/signup">SignUp</a></li>

            			{!! MenuController::display('not-logged-in', 'header_nav') !!}
            		@else
            			@if( Auth::user()->role_id == 1 )
                            {!! MenuController::display('admin-front', 'header_nav') !!}
                        @elseif(Auth::user()->role_id == 4)
                    
                           {!! MenuController::display('coach-header', 'header_nav') !!}    
                        @else
                            {!! MenuController::display('logged-in', 'header_nav') !!}    
                        @endif
            		@endif
            		
	                @if (Auth::guest())
	                	<li><a @click.prevent="login_form" id="show_login" href="{{ route('login') }}">Login</a></li>
	                @endif
	            </ul>
            </div>
        </div>
        <div class="col-md-2 sign-up pull-right">
        	@if(Auth::guest())
    	    	<a class="btn orange" href="#" @click="signup_open" >Sign Up Now</a>
        	@else
        		<?php
					$user_avatar = Voyager::image(Auth::user()->avatar);
					if ((substr(Auth::user()->avatar, 0, 7) == 'http://') || (substr(Auth::user()->avatar, 0, 8) == 'https://')) {
						$user_avatar = Auth::user()->avatar;
					}
				?>
				<?php  
				if(Auth::User()->role_id == 2 || Auth::User()->role_id == 3){
					// $user = App\User::find(Auth::User()->id);
					// var_dump(Auth::user()->subscribed('main') );die;
		            $userCreatedDate1 = (array) Auth::User()->created_at;
		            $current_date = new \DateTime();
		            $current_date = $current_date->format('Y-m-d 24:59:59');
		            $userCreatedDate = new \DateTime($userCreatedDate1['date']);
		            $userCreatedDate = $userCreatedDate->format('Y-m-d 00:00:00');
		            $datediff = strtotime($current_date) - strtotime($userCreatedDate);
		            $diff1 = floor($datediff / (60 * 60 * 24));
		            if (Auth::user()->subscribed('main') || ( $diff1 < 15 || Auth::user()->is_paid == 'yes')){
		                    ?>
		                    @if(Auth::User()->role_id != 1)
							<?php 
				          	$getNotiUnRead = \App\Notification::where('user_id',\Auth::User()->id)->where('key','profile_viewed')->where('is_seen','0')->count();  ?>
				    	 	<ul class="nav navbar-nav user_notifications">
					            <li class="dropdown click-noti">
					            	<a href="/notifications"> 	<span class="notification_icon"><i class="fa fa-bell  fa-border ">
					            		@if($getNotiUnRead > 0)<span class="badge notification-count">{{$getNotiUnRead}}</span>@endif
					            	</i></span></a>
					            </li>
					        </ul>
					        @endif
		               <?php }
		            }else{ ?>
		            @if(Auth::User()->role_id != 1)
					<?php 
		          	$getNotiUnRead = \App\Notification::where('user_id',\Auth::User()->id)->where('key','profile_viewed')->where('is_seen','0')->count();  ?>
		    	 	<ul class="nav navbar-nav user_notifications">
			            <li class="dropdown click-noti">
			            	<a href="/notifications"> 	<span class="notification_icon"><i class="fa fa-bell  fa-border ">
			            		@if($getNotiUnRead > 0)<span class="badge notification-count">{{$getNotiUnRead}}</span>@endif
			            	</i></span></a>
			            </li>
			        </ul>
			        @endif
		        	<?php   }
		        ?>
				<ul class="nav navbar-nav navbar-right">
		            <li class="dropdown profile">
		                <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button"
		                   aria-expanded="false"><img src="<?php if($user_avatar == 'users/default.png' || $user_avatar == ''){ echo env('APP_URL').'/storage/EFuG6j0fKnCeYPkABRMIvbzHdUi2rLC4xB24f4lB.png'; } else{ echo $user_avatar ; } ?>" class="profile-img"> <span
		                            class="caret">{{ Auth::user()->name }}</span></a>
		                <ul class="dropdown-menu dropdown-menu-animated">
		                   @if(\Auth::User()->role_id == 1)
		                     <li><a href="{{ url('/admin') }}">Admin Dashboard</a></li>
		                     @endif
		                    <?php $nav_items = config('voyager.dashboard.navbar_items'); ?>
		                   	@if (Auth::guest())
		                   	@else
		                   	<li><a href="{{ url('/profile') }}">My Profile</a></li>
		                   	<li><a href="{{ url('/change/password') }}">Settings</a></li>
		                   	@if((\Auth::User()->role_id == 2 || \Auth::User()->role_id == 3 ) )
			                    <?php 
                                $plan = App\Plan::find(1);     ?>
			                    	<li><a href="{{ url('/plan/'.$plan->slug) }}">Subscriptions</a></li>
			                    	<li><a href="{{ url('/payments/history/athlete') }}">Payment Info</a></li>
			                    @endif
		                    <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
		                    @endif
		                </ul>
		            </li>
		        </ul>
        	@endif
        </div>
       
	</header>
	@if(!Auth::guest())
	@if(Auth::User()->role_id != 1)
	    @if( Auth::User()->role_id == 2 ||  Auth::User()->role_id == 3)
	        @if(! \Auth::User()->subscribed('main') && \Auth::user()->is_paid != 'yes' )
	        	@if( Voyager::setting('profile_upgrade_header_athlete') )
			        <div class="notification-bar">
			        	<a class="close-notification-bar"></a>
			        	<p class="account-upgrade" style="color: #000;">
			        	<?php $header = str_replace( '<p>','', Voyager::setting('profile_upgrade_header_athlete') );
		        		$header = str_replace( '</p>','', $header );
		        		 ?>
					    {!! $header !!}
			        	</p>
			        </div>
				@endif
			@endif
        @endif
        @if( Auth::User()->role_id == 4)
        	@if(\Auth::User()->user_approved == 'INACTIVE')
	        	@if( Voyager::setting('profile_upgrade_header_coach') )
			        <div class="notification-bar">
			        	<a class="close-notification-bar"></a>
			        	<p class="account-upgrade">
				        	<?php $header2 = str_replace( '<p>','', Voyager::setting('profile_upgrade_header_coach') );
			        		$header2 = str_replace( '</p>','', $header2 );
			        		 ?>
						    {!! $header2 !!}
				        </p>
			        </div>
			    @endif
	        @endif
        @endif
    @endif
    @endif
	@if( Voyager::page('image', $page->id) )
	<div class="main-banner inner-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::page('image', $page->id) ) }} )">
		<div class="container">
			<h1>{{ Voyager::page('banner_heading', $page->id) }}</h1>
			<h2>{{ Voyager::page('banner_subheading', $page->id) }}</h2>
		</div>
	</div>
	@endif
	
	<div class="main-container main-inner">
		<section class="white-bg">
			<div class="container">
				<div class="col-md-8 col-sm-12 left-content">
                    @yield('content')
				</div>
				{!! Voyager::setting('sidebar_contant') !!}
			</div>
		</section>
	</div>
	
	@if (Auth::guest())
	 @if( Voyager::page('cta_show',$page->id) == 'show' )
		@if( Voyager::page('cta_bg_image', $page->id) )
		<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::page('cta_bg_image', $page->id) ) }} )">
			<div class="container">
				{!! Voyager::page('cta_content', $page->id) !!}
			</div>
		</div>
		@else
		<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::setting('cta_bg_image') ) }} )">
			<div class="container">
				{!! Voyager::setting('cta_content') !!}
			</div>
		</div>
		@endif
	@endif
	@endif
	
	<footer id="footer" class="main-footer">
		<div class="top-footer">
			<div class="container">
				<div class="col-md-6">
					<a href="{{ url('/') }}"><img src="{{ asset( 'storage/' . Voyager::setting('footer_logo') ) }}" ></a>
					<div class="social">
						<ul>
							<li><a href="{{ Voyager::setting('facebook_url') }}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="{{ Voyager::setting('Twitter_url') }}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<!-- <li><a href="{{ Voyager::setting('instagram_url') }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li> -->
							<!-- <li><a href="{{ Voyager::setting('snapchat_url') }}"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></a></li> -->
							<li><a href="{{ Voyager::setting('youtube_url') }}"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							<li><a href="{{ Voyager::setting('linkedin_url') }}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<h2>athletes</h2>
					<ul>
						{!! MenuController::display('athletes', 'header_nav') !!}
					</ul>
				</div>
				@if( Auth::user() )
					@if( \Auth::user()->role_id == 4 )

						<div class="col-md-2 col-sm-4">
							<h2>coaches</h2>
							<ul>
								{!! MenuController::display('COACHES', 'header_nav') !!}
							</ul>
						</div>

						@else

						<div class="col-md-2 col-sm-4">
							<h2>coaches</h2>
							<ul>
								{!! MenuController::display('Coach-Login', 'header_nav') !!}
							</ul>
						</div>

					@endif
				@else
				<div class="col-md-2 col-sm-4">
							<h2>coaches</h2>
							<ul>
								{!! MenuController::display('COACHES', 'header_nav') !!}
							</ul>
						</div>
				@endif		

				<div class="col-md-2 col-sm-4">
					<h2>company</h2>
					<ul>
						{!! MenuController::display('COMPANY', 'header_nav') !!}
					</ul>
				</div>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="container">
				<div class="col-md-8">
					{!! Voyager::setting('footer_left') !!}
				</div>
				<div class="col-md-4 text-right">
					{!! Voyager::setting('footer_right') !!}
				</div>
			</div>
		</div>
		
		<div v-cloak v-show="signup_popup" class="sign-up-popup">
			<div class="sign-up-content">
				<a @click="signup_close" href="#" class="close"><i class="icono-cross"></i></a>
				<h1>Create an account</h1>
				<div class="radio-section">
					<a class="checked" href="#"><input type="radio" checked name="role" value="2">i&#39;m an athlete</a>
					<a href="#"><input type="radio" name="role" value="4">i&#39;m a coach</a>
					<a href="#"><input type="radio" name="role" value="3">i&#39;m an parent</a>
				</div>
				<form class="create-acc" id="signup-user" action=" {{ url( '/ajax_create' ) }} " method="post">
					<?php
						$data = \Session::get('social_info');
					?>
					<div class="half">
						<label>First Name <sup>*</sup></label>
						<input type="text" name="first_name" id="first_name" value="@if(isset($data['first_name'])) {{ $data['first_name'] }} @endif ">
					</div>
					<div class="half">
						<label>Last Name <sup>*</sup></label>
						<input type="text" name="last_name" id="last_name"value="@if(isset($data['last_name'])) {{ $data['last_name'] }} @endif">
					</div>
					<div class="small">
						<label>Country Code</label>
						<select name="countrycode" id="countrycode">
							<option>+1</option>
							<option>+11</option>
							<option>+12</option>
							<option>+12</option>
							<option>+13</option>
						</select>
					</div>
					<div class="medium">
						<label>Phone Number</label>
						<input type="number" name="phone" id="phone">
					</div>
					<div class="full">
						<label>Email Address <sup>*</sup></label>
						<input type="email" name="email" id="email" value="@if(isset($data['email'])) {{ $data['email'] }} @endif ">
					</div>
					<div class="full">
						<label>Password <sup>*</sup></label>
						<input type="password" name="password" id="password">
					</div>
					<button class="btn orange">Sign Up</button>
					<div style="display: none;">{{ csrf_field() }}</div>
				</form>
				<h2>or, sign up using:</h2>
				<div class="social-signup">
					<div class="col-md-6 facebook">
						<a href="{{ url('/auth/facebook') }}">Facebook<i class="fa fa-facebook" aria-hidden="true"></i></a>
					</div>
					<div class="col-md-6 twitter">
						<a href="{{ url('/auth/twitter') }}">Twitter<i class="fa fa-twitter" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	
		@if (Auth::guest())
		   	<div v-cloak v-show="login_popup" class="log-in-popup">
				<div class="sign-up-content">
					<a @click="login_close" href="#" class="close"><i class="icono-cross"></i></a>
					<div class="panel-heading">Login</div>
	                <div class="panel-body">
	   					<login></login>
	   					<!-- <div class="radio-section">
				    		<a @click="login_url(2)"  href="#" :class="athelete_class"><input type="radio" checked name="role" value="2">i&#39;m an athlete</a>
					    	<a @click="login_url(4)" href="#" :class="coach_class"><input type="radio" name="role" value="4">i&#39;m a coach</a>
					    	<a @click="login_url(3)" href="#" :class="parent_class"><input type="radio" name="role" value="3">i&#39;m a parent</a> 
				    	</div> -->
	   					<div class="social-signup">
							<social-login :loginid="{{ json_encode('none') }}"></social-login>
						</div>
	                </div>
				</div>
			</div>
			@include('parts.signuppopup')
		@endif
	
	</footer>
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?5NiKDErc1Vh5FbjXO06MFai3ygPTzMaZ";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.meanmenu/2.0.6/jquery.meanmenu.min.js"></script>
<script >
$( document ).ready(function() {
    jQuery( '.close-notification-bar' ).on( 'click', function(e) {
    e.preventDefault();
    $('.notification-bar').remove();
    $(document.body).css("margin-top",'0');
});
    $(window).resize(function() {
        $(document.body).css("margin-top", $(".notification-bar").height());
    }).resize();
});

</script>
<script>
jQuery( document ).ready(function($) {
    jQuery( '.checkboxrequired p a' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').show();
});
    jQuery( '.close-term' ).on( 'click', function(e) {
    e.preventDefault();
    $('#show-term').hide();});
});
</script>
<script>
jQuery( document ).ready(function($) {
	$('header .main-menu').meanmenu({
		meanMenuContainer: '.menu-top',
		meanScreenWidth: "767"
	});
});
</script>
<script type="text/javascript" src="/js/main.js"></script>
<script>
jQuery( '.radio-section a' ).on( 'click', function(e) {
	e.preventDefault();
	$('.radio-section a').removeClass('checked');
	$(this).addClass('checked');
	$('.radio-section a input').prop( "checked", false );
	$(this).children('input').prop( "checked", true )
});

</script>
<?php if( isset($data) ):  ?>
	<script>
			jQuery('html').addClass('popup');
	</script>
<?php endif; ?>
@if( $errors->has('email') || $errors->has('password') )
	<script>
		// console.log('sdasd');
		// app.login_popup = true;
		jQuery('.log-in-popup').trigger('click');
		jQuery('html').addClass('popup');
	</script>
@endif
</div>
</body>
</html>