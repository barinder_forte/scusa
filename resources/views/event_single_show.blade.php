<?php if($sportData->banner_image != ''){?>
<style>
.main-banner.inner-banner{
  
    background: url({{ asset( 'storage/'. $sportData->cta_bg_image ) }}) !important;
    
}
</style>
<?php } ?>

<?php 
  function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }
?>
@extends('layouts.signup')

@section('content')

@if(Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
   <div class="main-banner inner-banner">
		<div class="container">
			<h1>{{$sportData->title}}</h1>
			<input type="hidden" name="eventID" id='eventID' value='{{$sportData->id}}'>
			<input type="hidden" name="userRole" id='userRole' value='{{$role}}'>
			<h2>
				{{date('F',strtotime($sportData->event_start))}} 
				{{addOrdinalNumberSuffix(date('d',strtotime($sportData->event_start))).","}} 
				{{date('Y',strtotime($sportData->event_start))}}
				{{ ' to ' }}
				{{date('F',strtotime($sportData->event_end))}} 
				{{addOrdinalNumberSuffix(date('d',strtotime($sportData->event_end))).","}} 
				{{date('Y',strtotime($sportData->event_end))}}
			</h2>
			<?php  $address[0] = $sportData->place; $address[1] = $sportData->city; $address[2] = $sportData->state; $address[3] = $sportData->zipcode;
			 $address = array_filter($address);?>
			<h2>{{ implode(', ',$address) }}</h2>
			<a href="/eventregister/{{$sportData->event_slug}}" class="btn orange">@if($UserRegisteredStatus == 'no'){{ "Show" }}@if(\Auth::User()->role_id == 4){{" Athletes "}}@else{{" Coaches "}}@endif{{ "I’m Attending This Event" }} @else {{ "Click to Cancel Your Attendance" }}@endif</a>
		</div>
	</div>

	<div class="share-event">
		<div class="container">
			<div class="col-md-4">
				<a href="/events"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back to Event List</a>
			</div>
			<div class="col-md-8">
				<div class="share-section">
					<p>Share this event:</p>
					<?php  $url = url()->current(); 
						$contant = "I’m Attending {$sportData->title} on  {$sportData->event_start} to {$sportData->event_end} at {{$sportData->place} {', '.$sportData->city} {', '.$sportData->state} {', '.$sportData->zip}. #SCUSA"; ?> 
					<ul>
						<li>
							<a href="https://www.facebook.com/sharer/sharer.php?u={{$url}}&t={{$sportData->title}}" onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"><i class="fa fa-facebook" aria-hidden="true" ></i>
							</a>
						</li>
						<!-- <li><a href="https://www.facebook.com/sharer/sharer.php?u={{$url}}&t={{$sportData->title}}&picture=http://sportcontactusa.com/storage/settings/December2017/3fcsbuYSySHc0i9QkkZB.png" onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"><i class="fa fa-facebook" aria-hidden="true" ></i></a></li> -->
						<li>
							<a href="http://twitter.com/share?text={{$sportData->title}}&url={{$url}}" onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"><i aria-hidden="true" class="fa fa-twitter"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="main-container main-inner">
		@if($sportData->description || $sportData->event_url)
			<section class="white-bg">
				<div class="container">
				    @if(isset($sportsMeta))
					<?php  $galleryImages = 0; 
				    ?>
					@if($galleryImages) > 0)
						<div class="col-md-8 col-sm-12 left-content padding-right">
					@else
						<div class="col-md-12 col-sm-12 left-content padding-right">
					@endif
					@else
						<div class="col-md-12 col-sm-12 left-content padding-right">
					@endif
						@if($sportData->event_url)
							<a target="_blank" href="{{$sportData->event_url}}" class="btn orange pull-right event-url">Event Url</a>
						@endif
						<h2 class="main-title">about this event</h2>
						<p>{{$sportData->description}}</p>
					</div>
					@if(isset($sportsMeta))
					<?php  $galleryImages = 0; 

						if($galleryImages > 0){

					?>
					<div class="col-md-4 col-sm-12 right-content">
						<div class="right-box">
							<div class="uploaded-images">
								<!--<div class="col-md-6 padding-10">-->
								<!--	<a class="upload" href="#"><i class="fa fa-plus" aria-hidden="true"></i>Upload New</a>-->
								</div>
								@if(isset($sportsMeta))
								<?php  $galleryImages = json_decode($sportsMeta->details); ?>
								@foreach($galleryImages as $image)
								<div class="col-md-6 padding-10 ">
									<a target='_blank' href="{{$image}}"><img src="{{$image}}" title='' class="img-responsive" ></a>
								</div>
								@endforeach
								@endif
							</div>
						</div>
					<?php } ?>
						@endif
					</div>
			</section>
		@endif
		<section class="grey-bg">
			<div class="container">
								<?php if($role == 'athlete_register'){
									$roleSlug = 'athlete';
									$text = 'Athletes';	
								}else if($role == 'coach_register'){
									$text = 'Coaches';
									$roleSlug = 'coach';
								}
								else{
										$text = '';
										$roleSlug = 'athlete';
								} 	?>
			@if(count($attendeesArray) > 0 || isset($_GET['_token']))
				<div class="col-md-8 padding-right">
					@if(count($attendeesArray) > 0)


					<div class="atheletes-heading">
						<div class="row">
							<div class="col-md-8">
								<?php 
								
									if(count($attendeesArrayFull) > 0 ){
										$userIdsArray1 = array();
							        		foreach($attendeesArrayFull as $key=>$attendee){
											$userInfo = \Voyager::UserInformation('all', $attendee->id );
											if($attendee->role_id != 4){
												$userName = (isset($userInfo['athelete_firstname'])?$userInfo['athelete_firstname']:'').' '.str_replace("'", '',isset($userInfo['athelete_lastname'])?$userInfo['athelete_lastname']:'');
												$useremail = str_replace("'", '',isset($userInfo['athelete_email'])?$userInfo['athelete_email']:'');
											}else{
												$userName = (isset($userInfo['coach_firstname'])?$userInfo['coach_firstname']:'').' '.str_replace("'", '',isset($userInfo['coach_lastname'])?$userInfo['coach_lastname']:'');
												$useremail = str_replace("'", '',isset($userInfo['coach_email'])?$userInfo['coach_email']:'');
											}
											$userIdsArray1[$key]['userid'] = $useremail ; 
											$userIdsArray1[$key]['username'] = $userName;
										}
										$userIdsArray = json_encode($userIdsArray1);
										//$userIdsArray = implode(',',$userIdsArray1);
									
									}else{
										$userIdsArray = '' ;	
									}
										?>
								<h2>{{$text}} attending ({{count($attendeesArrayFull)}})</h2>
							</div>
						@if(1==1)
						<div class="col-md-4 text-right">
						<a @click.prevent='showemail(<?php echo $userIdsArray; ?>, <?php echo $sportData->id; ?> )' href="#" id='show_login' class="btn <?php if( ( (Auth::user()->role_id == 2 || Auth::user()->role_id == 3) && ((!Auth::user()->subscribed('main')) && Auth::user()->is_paid != 'yes') ) || ( Auth::user()->user_approved == 'INACTIVE' && Auth::user()->role_id == 4) ){ echo 'light addblurrClass'; }else{ echo 'orange'; } ?> emailAllEvent" name=''>email all
						</a>			
						</div>
						@endif
						</div>
					</div>
					@else

						<?php $userIdsArray = '' ;	?>

					@endif
			
					<div class="events-section atheletes-main">
						@if(count($attendeesArray) > 0)
						@foreach($attendeesArray as $attendee)
							<?php $userSchool = '' ; $userSchool = App\UserInformation::where('user_id',$attendee->id);
							if($attendee->role_id == 4){
								$userSchool = $userSchool->where('meta_key','school')->first();
								$schoolA = App\ApiData::where('id',isset($userSchool->meta_value)?$userSchool->meta_value:'')->first();
								$school = isset($schoolA->school_name)?$schoolA->school_name:'';
							}else{
								$userSchool = $userSchool->where('meta_key','school')->first();
								$school = isset($userSchool->meta_value)?$userSchool->meta_value:'';
							}
							// $userPosition = App\UserInformation::where('user_id',$attendee->id)->where('meta_key','position')->first();
								// $arrayform = array();
							 //$arrayform["userid"] =  $attendee->id ;
		      //               $arrayform["username"] =  $attendee->name ;
		      //               $arrayformjson = json_encode($arrayform);
		    				$arrayformjson = $attendee->id;
								$userInfo2 = \Voyager::UserInformation('all', $attendee->id );
											if($attendee->role_id != 4){
												$userName = (isset($userInfo2['athelete_firstname'])?$userInfo2['athelete_firstname']:'').' '.str_replace("'", '',isset($userInfo2['athelete_lastname'])?$userInfo2['athelete_lastname']:'');
												$useremail = str_replace("'", '',isset($userInfo2['athelete_email'])?$userInfo2['athelete_email']:'');
											}else{
												$userName = (isset($userInfo2['coach_firstname'])?$userInfo2['coach_firstname']:'').' '.str_replace("'", '',isset($userInfo2['coach_lastname'])?$userInfo2['coach_lastname']:'');
												$useremail = str_replace("'", '',isset($userInfo2['coach_email'])?$userInfo2['coach_email']:'');
											}?>
						<div class="events-content">
							<div class="events-data athlete-img">
								<img height='40px' width='40px' src="{{($attendee->avatar == 'users/default.png' || $attendee->avatar == '' )? asset('storage/users/default.png') : $attendee->avatar}}">
							</div>
							<div class="events-data">
								<h3>{{$userName}}
									<span>{{isset($school)?$school:''}}  
									</span>
								</h3>
							</div>
							<div class="events-data">
								<p>
									<p>
									<?php  	
									$userIdsArf = '';
									$userIdsAr = array(); 
											$userIdsAr[0]['userid'] = $useremail ;
											$userIdsAr[0]['username'] = $userName ;
											$userIdsArf = json_encode($userIdsAr);
											if($attendee->role_id == 4){
												$roleSlug1 = 'coach';
											}else{
												$roleSlug1 = 'athlete';
											}
										?>	

									<a href="/profile/{{$roleSlug1}}/{{$attendee->user_slug}}">View Profile</a>
									<a @click.prevent='showemail( <?php echo $userIdsArf; ?>, <?php echo $sportData->id; ?> )' href="#" id='show_login' class="send_email_event" >Send Email</a>
								</p>
								</p>
							</div>
						</div>
						@endforeach
						@else
					
						<div class="events-content no-event-found">
						<h3>No Attendees Available</h3>	
					    </div>
						@endif
					</div>
					@if(count($attendeesArray) > 0 )
						{!! $attendeesArray->appends(Request::except('page'))->render() !!}
					@endif
				</div>
				<div class="col-md-4 col-sm-12 right-content">
					<div class="right-box">
						<h1>filter by</h1>
						<form class="filter-form" action='/event/{{$sportData->event_slug}}' method="GET" >
						{{csrf_field()}}
							<?php if($role == 'coach_register'){  ?>
							<label>State</label>
							<select  <?php if(isset($data['state'])){ echo 'autofocus'; } ?>   id='state'  class="js-example-basic-single state" name="state" >
								<option value=''>Select State</option>
								<option value="AL" <?PHP if((isset($data['state'])?$data['state']:'')=="AL") echo "selected";?>>Alabama</option>
								<option value="AK" <?PHP if((isset($data['state'])?$data['state']:'')=="AK") echo "selected";?>>Alaska</option>
								<option value="AZ" <?PHP if((isset($data['state'])?$data['state']:'')=="AZ") echo "selected";?>>Arizona</option>
								<option value="AR" <?PHP if((isset($data['state'])?$data['state']:'')=="AR") echo "selected";?>>Arkansas</option>
								<option value="CA" <?PHP if((isset($data['state'])?$data['state']:'')=="CA") echo "selected";?>>California</option>
								<option value="CO" <?PHP if((isset($data['state'])?$data['state']:'')=="CO") echo "selected";?>>Colorado</option>
								<option value="CT" <?PHP if((isset($data['state'])?$data['state']:'')=="CT") echo "selected";?>>Connecticut</option>
								<option value="DE" <?PHP if((isset($data['state'])?$data['state']:'')=="DE") echo "selected";?>>Delaware</option>
								<option value="DC" <?PHP if((isset($data['state'])?$data['state']:'')=="DC") echo "selected";?>>District of Columbia</option>
								<option value="FL" <?PHP if((isset($data['state'])?$data['state']:'')=="FL") echo "selected";?>>Florida</option>
								<option value="GA" <?PHP if((isset($data['state'])?$data['state']:'')=="GA") echo "selected";?>>Georgia</option>
								<option value="HI" <?PHP if((isset($data['state'])?$data['state']:'')=="HI") echo "selected";?>>Hawaii</option>
								<option value="ID" <?PHP if((isset($data['state'])?$data['state']:'')=="ID") echo "selected";?>>Idaho</option>
								<option value="IL" <?PHP if((isset($data['state'])?$data['state']:'')=="IL") echo "selected";?>>Illinois</option>
								<option value="IN" <?PHP if((isset($data['state'])?$data['state']:'')=="IN") echo "selected";?>>Indiana</option>
								<option value="IA" <?PHP if((isset($data['state'])?$data['state']:'')=="IA") echo "selected";?>>Iowa</option>
								<option value="KS" <?PHP if((isset($data['state'])?$data['state']:'')=="KS") echo "selected";?>>Kansas</option>
								<option value="KY" <?PHP if((isset($data['state'])?$data['state']:'')=="KY") echo "selected";?>>Kentucky</option>
								<option value="LA" <?PHP if((isset($data['state'])?$data['state']:'')=="LA") echo "selected";?>>Louisiana</option>
								<option value="ME" <?PHP if((isset($data['state'])?$data['state']:'')=="ME") echo "selected";?>>Maine</option>
								<option value="MD" <?PHP if((isset($data['state'])?$data['state']:'')=="MD") echo "selected";?>>Maryland</option>
								<option value="MA" <?PHP if((isset($data['state'])?$data['state']:'')=="MA") echo "selected";?>>Massachusetts</option>
								<option value="MI" <?PHP if((isset($data['state'])?$data['state']:'')=="MI") echo "selected";?>>Michigan</option>
								<option value="MN" <?PHP if((isset($data['state'])?$data['state']:'')=="MN") echo "selected";?>>Minnesota</option>
								<option value="MS" <?PHP if((isset($data['state'])?$data['state']:'')=="MS") echo "selected";?>>Mississippi</option>
								<option value="MO" <?PHP if((isset($data['state'])?$data['state']:'')=="MO") echo "selected";?>>Missouri</option>
								<option value="MT" <?PHP if((isset($data['state'])?$data['state']:'')=="MT") echo "selected";?>>Montana</option>
								<option value="NE" <?PHP if((isset($data['state'])?$data['state']:'')=="NE") echo "selected";?>>Nebraska</option>
								<option value="NV" <?PHP if((isset($data['state'])?$data['state']:'')=="NV") echo "selected";?>>Nevada</option>
								<option value="NH" <?PHP if((isset($data['state'])?$data['state']:'')=="NH") echo "selected";?>>New Hampshire</option>
								<option value="NJ" <?PHP if((isset($data['state'])?$data['state']:'')=="NJ") echo "selected";?>>New Jersey</option>
								<option value="NM" <?PHP if((isset($data['state'])?$data['state']:'')=="NM") echo "selected";?>>New Mexico</option>
								<option value="NY" <?PHP if((isset($data['state'])?$data['state']:'')=="NY") echo "selected";?>>New York</option>
								<option value="NC" <?PHP if((isset($data['state'])?$data['state']:'')=="NC") echo "selected";?>>North Carolina</option>
								<option value="ND" <?PHP if((isset($data['state'])?$data['state']:'')=="ND") echo "selected";?>>North Dakota</option>
								<option value="OH" <?PHP if((isset($data['state'])?$data['state']:'')=="OH") echo "selected";?>>Ohio</option>
								<option value="OK" <?PHP if((isset($data['state'])?$data['state']:'')=="OK") echo "selected";?>>Oklahoma</option>
								<option value="OR" <?PHP if((isset($data['state'])?$data['state']:'')=="OR") echo "selected";?>>Oregon</option>
								<option value="PA" <?PHP if((isset($data['state'])?$data['state']:'')=="PA") echo "selected";?>>Pennsylvania</option>
								<option value="RI" <?PHP if((isset($data['state'])?$data['state']:'')=="RI") echo "selected";?>>Rhode Island</option>
								<option value="SC" <?PHP if((isset($data['state'])?$data['state']:'')=="SC") echo "selected";?>>South Carolina</option>
								<option value="SD" <?PHP if((isset($data['state'])?$data['state']:'')=="SD") echo "selected";?>>South Dakota</option>
								<option value="TN" <?PHP if((isset($data['state'])?$data['state']:'')=="TN") echo "selected";?>>Tennessee</option>
								<option value="TX" <?PHP if((isset($data['state'])?$data['state']:'')=="TX") echo "selected";?>>Texas</option>
								<option value="UT" <?PHP if((isset($data['state'])?$data['state']:'')=="UT") echo "selected";?>>Utah</option>
								<option value="VT" <?PHP if((isset($data['state'])?$data['state']:'')=="VT") echo "selected";?>>Vermont</option>
								<option value="VA" <?PHP if((isset($data['state'])?$data['state']:'')=="VA") echo "selected";?>>Virginia</option>
								<option value="WA" <?PHP if((isset($data['state'])?$data['state']:'')=="WA") echo "selected";?>>Washington</option>
								<option value="WV" <?PHP if((isset($data['state'])?$data['state']:'')=="WV") echo "selected";?>>West Virginia</option>
								<option value="WI" <?PHP if((isset($data['state'])?$data['state']:'')=="WI") echo "selected";?>>Wisconsin</option>
								<option value="WY" <?PHP if((isset($data['state'])?$data['state']:'')=="WY") echo "selected";?>>Wyoming</option>
							</select>

							<label>Division</label>
                            <?php $divisionss = \Voyager::pagemetadetail('Division', '16');
				    		$divisionss = json_decode($divisionss);
						   	$divisionss = $divisionss->options;
						  		if(isset($_GET['division'])){
						   		$division = $_GET['division'];
						   	}?>
							<select class="js-example-basic-single" name="division" >
								 <option  value="">Select Division</option>
								 <?php foreach($divisionss as $key => $div){ ?>
								 	<option <?php if((isset($division)?$division:'') == $div){ echo 'selected';} ?> value="<?php echo $div; ?>"><?php echo $key; ?></option>
								 <?php } ?>
							</select>
							<?php }else{   ?>	
							<label>Year Graduating</label>
							<select name='year_graduating' class="js-example-basic-single">
								<option value='' >Select Year</option>
								<?php   $date = date('Y');?>
								@for($i = $date; $i <= $date + 8 ; $i++)
									<option <?php if((isset($data['year_graduating'])?$data['year_graduating']:'') == $i) echo "selected";?> value='{{$i}}'>{{$i}}</option>
								@endfor
							</select>

							

							<label>Position Played</label>
							<select name="position" value="{{isset($position)?$position:''}}" class="js-example-basic-single">
								<option value="">Select Position</option>
								<?php $previousSetting = App\SportsMeta::where('sport_id', '=', $sportData->sport_id)->where('key','=','position')->first();
								$data = (array)json_decode(isset($previousSetting->details)?$previousSetting->details:'');
								if(isset($data['options'])){
									foreach ($data['options'] as $key => $value) { ?>

										<option <?PHP if((isset($_GET['position'])?$_GET['position']:'') == $key) echo "selected";?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 

									<?php    
										}
								}
								?>
							</select>
							
							<label>Gpa</label>
							<select name='gpa' id='gpa'  class="js-example-basic-single">
								<option  value=''>Select GPA</option>
								<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="1") echo "selected";?> value='1'> >= 1</option>
								<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="2") echo "selected";?> value='2'> >= 2</option>
								<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="3") echo "selected";?> value='3'> >= 3</option>
								<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="4") echo "selected";?> value='4'> >= 4</option>
								<option <?PHP if((isset($_GET['gpa'])?$_GET['gpa']:'')=="5") echo "selected";?> value='5'>5</option>
							</select>
							<?php } ?>
							<div class="row">
							<div class="col-md-6">	
								<input type="submit" class='btn btn-info' name="submit" value='Submit'>
							</div>
							<div class="col-md-6">	
								<a class='btn orange' href="/event/{{$sportData->event_slug}}" >Clear </a>
							</div>
							</div>
						</form>
						<?php 
						if($role == 'athlete_register'){
							$text2 = 'Email These Athletes';	
						}else if($role == 'coach_register'){
							$text2 = 'Email These Coaches';
						}else{
							$text2 = 'Email To All';
						}	?>
						
					@if(count($attendeesArray) > 0)
					@if(1==1)
					<a  @click.prevent='showemail( <?php echo $userIdsArray; ?>, <?php echo $sportData->id; ?> )' class="emailAllEvent btn <?php if( ( (Auth::user()->role_id == 2 || Auth::user()->role_id == 3) && ((!Auth::user()->subscribed('main')) && Auth::user()->is_paid != 'yes') ) || ( Auth::user()->user_approved == 'INACTIVE' && Auth::user()->role_id == 4) ){ echo 'orange addblurrClass'; }else{ echo 'orange'; } ?>" >
						{{$text2}} 


					</a>				
					@endif
					@endif
					</div>
				</div>
				@else
				<div class="related-events">
					<div class="col-lg-12">
						<div class="events-content no-event-found">
							<h3>No Attendees Available</h3>	
						</div>
					</div>
				</div>
				@endif
				@if(count($sportsRelatedEvents) > 0)
				<div class="related-events">
					<div class="col-lg-12">
						<h1>related or similar events</h1>
						<div class="row">
							<div class="events-main">
							    @foreach($sportsRelatedEvents as $key=>$Related)
							    @if($key < 4)
								<div class="col-md-3">
									<div class="events-box">
										
										<h2><a href="/event/{{$Related->event_slug}}">{{$Related->title}}</a></h2>
										<p class="date">{{ date_format(date_create( $Related->event_start) , 'F d, Y')  . ' to ' . date_format(date_create( $Related->event_end) , 'F d, Y') . '  ' .$Related->location}}</p>
									</div>
								</div>
								@endif
								@endforeach
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="no-event-found related">
					<!-- <h3>No Related Events Available</h3>	 -->
	    		</div>
				@endif
			
			</div>
		</section>
		@if(isset($sportData->id))
			@if($sportData->cta_show == 'show')
				@if(  $sportData->cta_bg_image)
				<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. $sportData->cta_bg_image ) }} )">
					<div class="container">
						{!! $sportData->cta_content !!}
					</div>
				</div>
				@else
				<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::setting('cta_bg_image') ) }} )">
					<div class="container">
						{!! Voyager::setting('cta_content') !!}
					</div>
				</div>
			
				@endif
			@endif
		@endif
	</div>
  <script>
 	
 </script>
@stop

