<style>
	.step-forms .right-section .main-info .tab-content .tab-pane .tab-inner .inner-content p.content-achi {
	    text-transform: inherit;
	}
</style>
@extends('layouts.signup')
@section('content')
<link href="{{ asset('css/simplelightbox.min.css') }}" rel='stylesheet' type='text/css'>
<link href="{{ asset('css/demo.css') }}" rel='stylesheet' type='text/css'>
	<div class="main-container main-inner step-forms front-form">
		<!-- @if($userData["id"] == \Auth::User()->id )
		<section class="grey-bg heading-top">
			<div class="container">
				<div class="info-forms">
					<h1>{{Voyager::pagemeta('page_heading', '13')}}</h1>
					<h2>{{Voyager::pagemeta('page_subheading', '13')}}</h2>
				</div>
			</div>
		</section>
		@endif -->
			<section class="grey-bg">
				<div class="container">
					<div class="info-forms">
						<div class="col-lg-4 col-md-12 left-section">
							<div class="upload-photo">
								<img class='img-responsive' src="<?php if($userData->avatar == 'users/default.png' || $userData->avatar == ''){ echo 'https://res.cloudinary.com/webforte/image/upload/h_400,w_400/default_qsdhgx.jpg'; } else{ 
									$userimage = explode('/upload/',$userData->avatar)[1];
									echo "https://res.cloudinary.com/webforte/image/upload/h_400/".$userimage; 
								} ?>" >
							</div>
							<div class="left-box video-profile">
								<h1>video highlights </h1>
								<div class="uploaded-images">
									<?php  $galleryVideos = json_decode(isset($userInfoArray['videogallery'])? ($userInfoArray['videogallery']):'');  ?>
							        @if($galleryVideos)
							        <?php $vide = 0; ?>
									@foreach($galleryVideos as $video)
									<div id="popupvideo{{$vide}}" class="popupbutton"><a class="click enchor-video"></a>
									<div class="col-md-12 padding-10 ">
                                    	<iframe src='{{$video->url}}' width="100%" height="200"></iframe>
									</div>
									</div>
									<?php $vide++; ?>
									@endforeach
									@else
									<div class="noinfo">
										<h3>No Information Available</h3>	
									</div>
									@endif
								</div>
							</div>
							<div class="left-box gallery-profile">
								<h1>photos </h1>
								<div class="uploaded-images">
									<ul class='gallery list-group'>
									<?php  $galleryImages = json_decode(isset($userInfoArray['gallery'])?($userInfoArray['gallery']):'');?>
							        @if($galleryImages)
									@foreach($galleryImages as $image)
									<li class="list-group-items">
									<a href='https://res.cloudinary.com/webforte/image/upload/{{$image->public}}.png'><img class="img-responsive" src='https://res.cloudinary.com/webforte/image/upload/{{$image->public}}.jpg'></a>
									</li>
									@endforeach
									@else
									<div class="noinfo">
										<h3>No Information Available</h3>	
									</div>
									@endif
									</ul>
								</div>
							</div>
							
						</div>
						<div class="col-lg-8 col-md-12 right-section">
							<div class="row">
								<div class="top-heading">
									<div class="col-md-8">
										<div>
											<h1>{{(isset($userInfoArray['athelete_firstname'])?($userInfoArray['athelete_firstname']):'') ." ". (isset($userInfoArray['athelete_lastname'])?($userInfoArray['athelete_lastname']):'')}}</h1>
											@if(isset($userInfoArray['school']))
											<span><h2>{{($userInfoArray['school'])}}</h2></span>@endif
										</div>
									</div>
									<div class="col-md-4 text-right">
									@if(Auth::User())
									@if(Auth::user()->id != $userData["id"])	
										<?php 
                                    $userIdsArray = array();
                                    $userIdsArray1[0]['userid'] = str_replace("'", '',(isset($userInfoArray['athelete_email'])?($userInfoArray['athelete_email']):'')) ;
                                    $userIdsArray1[0]['username'] = str_replace("'", '',(isset($userInfoArray['athelete_firstname'])?($userInfoArray['athelete_firstname']):'') ." ". (isset($userInfoArray['athelete_lastname'])?($userInfoArray['athelete_lastname']):'')) ;
                                    $userIdsArray = json_encode($userIdsArray1);
                                    ?>
                                    <a @click.prevent='showemail( <?php echo $userIdsArray; ?>)'  class="btn orange contact_me">    
                                    Contact Me</a>

                                    <!--     <a @click.prevent="showemail_profile" href="#" id='show_login' name='{{$userData["id"]}}' class="btn orange contact_me">Contact Me <i class="fa fa-angle-right" aria-hidden="true"></i></a>
 -->

                                    @endif
									@if(Auth::user()->role_id == 4 || Auth::user()->id == $userData["id"])			
										<a href="/exportToPdf/{{$userData['user_slug']}}"  class="btn purple dfgd">Export to PDF<i aria-hidden="true" class="fa fa-angle-right"></i></a> 
									@endif
									@if(Auth::user()->id == $userData["id"])			
										<a href="/profile/"  class="btn orange backtoprofile">Back to Profile<i aria-hidden="true" class="fa fa-angle-right"></i></a> 
									@endif
									@endif
									</div>
									<div class="col-md-12">
										<div>
											<ul class="top-listings">
												@if(isset($userInfoArray['graduation']))
												<li>
												<a href="#" class="btn orange">Class of {{$userInfoArray['graduation']}}</a>
												</li>
												@endif
												<?php if(isset($userInfoArray['sport'])){ $sportname = \App\Sport::where('id',$userInfoArray['sport'])->pluck('title')->first(); } ?>
												@if(isset($sportname))	
												<li>
													<p>Sport: <span>{{$sportname}}</span></p>
												</li>
												@endif
										        @if(isset($userInfoArray['position_name']))
												<li>
													<p>Position: <span>{{$userInfoArray['position_name']}}</span></p>
												</li>
												@endif
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="main-info">
								<!-- Centered Tabs -->
								<ul class="nav nav-tabs">
								  <li class="active"><a data-toggle="tab" href="#contact">Contact Info</a></li>
								  <li><a data-toggle="tab" href="#sports">Sports Stats</a></li>
								  <li><a data-toggle="tab" href="#achievements">Achievements</a></li>
								  <li><a data-toggle="tab" href="#acadmics">Academics</a></li>
								</ul>
								

							<div class="tab-content"><!-- Tab Conent -->

								<div id="contact" class="tab-pane fade in active">
									<div class="tab-inner">
								    	<div class="information">
										    <h3>athlete information</h3>
										    <div class="inner-content">
										    	<div class="row">
											    	<div class="col-md-5">
											    		<h4>Body Stats</h4>
											    			@if(isset($userInfoArray['athelete_height'])) <?php $body  = 0; ?> <p><label>Height:</label> 
                                                            <span>
                                                            <?php if(isset($userInfoArray['athelete_height'])){
                                                                    if($userInfoArray['athelete_height'] != ''){
                                                                        $centimetres = $userInfoArray['athelete_height'] + 36; 
                                                                        $feet = floor($centimetres/12);
                                                                        $inches = ($centimetres%12);
                                                                        echo $feet."'".$inches.'"';
                                                                    }
                                                                }
                                                            ?>
                                                            </span></p>@endif
											    			@if(isset($userInfoArray['athelete_weight']))<?php $body  = 0; ?><p><label>Weight:</label> <span>{{($userInfoArray['athelete_weight']).' lbs'}}</span></p>@endif
											    			@if(isset($userInfoArray['athelete_gender']))<?php $body  = 0; ?><p><label>Gender:</label> <span>@if($userInfoArray['athelete_gender'] == 'Men') {{'Male'}} @elseif($userInfoArray['athelete_gender'] == 'Women') {{'Female'}} @endif</span></p>@endif
											    			@if(isset($userInfoArray['athelete_birth']))<?php $body  = 0; ?><p><label>Birthdate:</label> 
											    			<span>
											    				{{date("m/d/Y", strtotime($userInfoArray['athelete_birth']))}}
											    			</span>
											    			</p>@endif
											    			
											    		  
											    		   
											    		    @if( !isset($body))
											    		    	<div class="noinfo">
																	<h3>No Information Available</h3>	
											    				</div>
											    		    @endif
											    		    
											    	</div>
											    <div class="col-md-7">
											    		<h4>Contact Information</h4>
											    			@if(isset($userInfoArray['athelete_email']))<?php $bodyc  = 0; ?><p><span><a href="mailto:{{$userInfoArray['athelete_email']}}"><img src="/images/icon/Send.png" /></a>{{isset($userInfoArray['athelete_email'])?($userInfoArray['athelete_email']):''}}</span></p>@endif
											    		@if(isset($userInfoArray['athelete_phone']))

											    		<?php $bodyc  = 0; ?>

											    		<p><span><a href="tel:{{$userInfoArray['athelete_phone']}}"><i class="fa fa-mobile" aria-hidden="true" ></i></a>


											    		<?php 

												if(isset($userInfoArray['athelete_phone'])){


												$daat =  str_replace(array('(',')','-','_',' '),'',$userInfoArray['athelete_phone']);


												if($daat){

													 if(isset($userInfoArray['c_code']) && $userInfoArray['c_code']!= ''){
											    			 	$code = '+'.$userInfoArray['c_code'];

											    			 }else{
											    			 	$code = '';

											    			 }
													echo   $formatted = $code. " (".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);


												}

											    			}



											    			



											    		?>



											    	</span></p>

											    		@endif
											    		

											    		<?php $arrayaddress = array(); ?>

											    		@if(isset($userInfoArray['athelete_address']))

											    		<?php $bodyc  = 0; ?>

											    		<?php 
											    		$arrayaddress[] = $userInfoArray['athelete_address']; 

											    		?>


											    		@endif


											    		@if(isset($userInfoArray['city']))<?php $body  = 0; ?>

											    		<?php 
											    		$arrayaddress[] = $userInfoArray['city']; 

											    		?>

											    		@endif
											    		    
											    		

											    		@if(isset($userInfoArray['state']))<?php $bodyc  = 0; ?>
											    		<?php 
											    		$arrayaddress[] = $userInfoArray['state']; 

											    		?>
											    		@endif


											    		 @if(isset($userInfoArray['zip']))<?php $body  = 0; ?>

											    		<?php 
											    		$arrayaddress[] = $userInfoArray['zip']; 

											    		?>
											    		 @endif

											    		 <?php if(!empty($arrayaddress)){ ?>
											    		 	<p><label>Address:</label> 
											    		 	<?php 
											    		 	echo implode(', ',$arrayaddress);

											    		 	?></p>
											    		 	<?php
											    		 }
											    		 ?>

											    		
											    			@if(!isset($bodyc) )
											    		    	<div class="noinfo">
																	<h3>No Information Available</h3>	
											    				</div>
											    		    @endif
											    	</div>
										    	</div>
										    	<hr class="light" />
									    		<div class="social-profile">
									    			<h4>Social Profiles</h4>
													@if(isset($userInfoArray['athelete_facebook']) || isset($userInfoArray['athelete_twitter']) || isset($userInfoArray['athelete_instagram']) 
											    	|| isset($userInfoArray['athelete_snapchat']) || isset($userInfoArray['athelete_skype']) || isset($userInfoArray['athelete_whatsapp']))
										    		<div class="row">
										    			@if(isset($userInfoArray['athelete_facebook']))<div class="col-md-6"><a class="facebook active" target='_blank' href="{{($userInfoArray['athelete_facebook'])}}"><i class="fa fa-facebook" aria-hidden="true"></i><?php $face = explode('/', $userInfoArray['athelete_facebook']);
										    			$faceb = count($face); 
										    			// print_r($face);die;
										    			$faceID = $face[$faceb-1] ?>{{$faceID}}</a>	</div>@endif
										    			@if(isset($userInfoArray['athelete_snapchat']))<div class="col-md-6"><a class="snapchat active" target='_blank'  href="{{($userInfoArray['athelete_snapchat'])}}"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i><?php $snap = explode('/', $userInfoArray['athelete_snapchat']);
										    			$snapc = count($snap); 
										    			// print_r($face);die;
										    			$snapcID = $snap[$snapc-1] ?>{{$snapcID}}</a></div>@endif
											    		@if(isset($userInfoArray['athelete_twitter']))<div class="col-md-6"><a class="twitter active" target='_blank'  href="{{($userInfoArray['athelete_twitter'])}}"><i class="fa fa-twitter" aria-hidden="true"></i><?php $twit = explode('/', $userInfoArray['athelete_twitter']);
										    			$twitc = count($twit); 
										    			// print_r($face);die;
										    			$twitID = $twit[$twitc-1] ?>{{$twitID}}</a></div>@endif
											    		@if(isset($userInfoArray['athelete_skype']))<div class="col-md-6"><a class="skype active" target='_blank'  href="{{($userInfoArray['athelete_skype'])}}"><i class="fa fa-skype" aria-hidden="true"></i><?php $sky = explode('/', $userInfoArray['athelete_skype']);
										    			$skyc = count($sky); 
										    			// print_r($face);die;
										    			$skyID = $sky[$skyc-1] ?>{{$skyID}}</a></div>@endif
											    		@if(isset($userInfoArray['athelete_instagram']))<div class="col-md-6"><a class="instagram active" target='_blank'  href="{{($userInfoArray['athelete_instagram'])}}"><i class="fa fa-instagram" aria-hidden="true"></i><?php $inst = explode('/', $userInfoArray['athelete_instagram']);
										    			$instc = count($inst); 
										    			// print_r($face);die;
										    			$instID = $inst[$instc-1] ?>{{$instID}}</a></div>@endif
										    			@if(isset($userInfoArray['athelete_whatsapp']))<div class="col-md-6"><a class="whatsapp active" target='_blank'  href="{{($userInfoArray['athelete_whatsapp'])}}"><i class="fa fa-whatsapp" aria-hidden="true"></i><?php $what = explode('/', $userInfoArray['athelete_whatsapp']);
										    			$whatc = count($what); 
										    			// print_r($face);die;
										    			$whatID = $what[$whatc-1] ?>{{$whatID}}</a></div>@endif
													</div>
									            	@else
											    		<div class="noinfo">
															<h3>No Information Available</h3>	
											    		</div>
									    			@endif
											    		
										    	</div>
										    </div>
									    </div>
									</div>
								<?php  $parents = json_decode(isset($userInfoArray['parents'])?($userInfoArray['parents']):'');  ?>
								
									<div class="tab-inner">
										<div>
										    <h3>Parent information</h3>
										    <div class=" inner-content">
											@if($parents)	    	
											@foreach($parents as $parent)
									    	@if(isset($parent->email) || isset($parent->firstname) || isset($parent->lastname) || isset($parent->phone))

												<div class="row">
											    	<div class="col-md-12">
											    	@if(isset($parent->gender)  || isset($parent->firstname) || isset($parent->lastname))
											    		<p>
											    		<label>{{(isset($parent->gender)?$parent->gender:'NAME')}}:</label> 
											    		 <span>{{(isset($parent->firstname)?$parent->firstname:'') . ' ' . (isset($parent->lastname)?$parent->lastname:'')}} </span>
											    		</p>
											    		@endif
											    			@if(isset($parent->email)  || isset($parent->phone))
									
											    		<!-- <h4>Contact Information</h4> -->
											    		
											    		@endif
											    		@if(isset($parent->email))
														<div class="col-md-6">
											    			<p><span class='lc' ><a href="mailto:{{$parent->email}}"><img src="/images/icon/Send.png" /></a>{{isset($parent->email)?$parent->email:'' }}</span></p>
											    		</div>
											    		@endif
											    		@if(isset($parent->phone))
											    		<div class="col-md-6">
											    			<p><span  ><a href="tel:{{$parent->phone}}"><i class="fa fa-mobile"  aria-hidden="true"></i></a>
											    				
											    					<?php 
$code1 = '';
												$daat1 =  str_replace(array('(',')','-','_',' '),'',$parent->phone);
												if($daat1){

													 if(isset($parent->c_code) && $parent->c_code!= ''){
											    			 	$code1 = '+'.$parent->c_code;

											    			 }else{
											    			 	$code1 = '';

											    			 }
													// echo   $formatted = $code."(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);
													echo $formatted = $code1. " (".substr($daat1,0,3).") ".substr($daat1,3,3)."-".substr($daat1,6);
												}

											    			
											    			?>
											    			</span></p>
											    		</div>
											    		@endif
											    	</div>
										    	</div>
										    	<hr class="light">
										    	@else
										    	
								    			<div class="noinfo">
													<h3>No Information Available</h3>
														<hr class="light">
									    		</div>
										    @endif
											@endforeach
											@else
										<div class="noinfo">
												<h3>No Information Available</h3>	
									    		</div>
									@endif
										    </div>
									    </div>
									</div>
									
									<div class="tab-inner">
										<div>
											<h3>Club/High School Coach Information</h3>
											<div class="inner-content clubs-info">
												
												<?php  $coaches = json_decode(isset($userInfoArray['coaches'])?($userInfoArray['coaches']):'');  


// print_r($coaches);
// die();

												?>
										        @if($coaches)
												@foreach($coaches as $coach)
												@if((isset($coach->type) && ( isset($coach->type)?$coach->type:'') != 'null' ) || isset($coach->clubname) || isset($coach->jersey) || isset($coach->coachname) || isset($coach->email) || isset($coach->phone) )
													<div class="inner-content">
											  				<div class="col-md-2 col-sm-2 orange-bg">
											  					<label class="club_type">{{(isset($coach->type) && (isset($coach->type)?$coach->type:'') != 'null')?$coach->type:''}}</label>
											  				</div>
											  				@if( isset($coach->clubname) || isset($coach->jersey))
											  				<div class="col-md-5 col-sm-5">
											  				@if(isset($coach->clubname))
											  				<p></p>
											  					<h4 class="coachclub_name">
											  						{{isset($coach->clubname)?$coach->clubname:''}}
											  					</h4>
															@endif
															@if(isset($coach->jersey))
															<p></p>
															<p>
																<label>Jersey number:</label>
																<span class="club_jersey" >
																 {{$coach->jersey}}
																</span>
															</p>
															@endif

											  				</div>
											  				@endif
											  			@if( isset($coach->coachname) || isset($coach->email) || isset($coach->phone) )
											  				<div class="col-md-5 col-sm-5">
											  				@if( isset($coach->coachname) )
											  				<p></p>
															<h4 class="club_coachname" >
																{{isset($coach->coachname)?$coach->coachname:''}}
															</h4>
															@endif
												    		@if(isset($coach->email))
												    		<p></p>
												    		<p class="club_email" >
												    			<span class='lc' ><a href="mailto:{{$coach->email}}">
												    			<img src="/images/icon/Send.png"   /></a>{{$coach->email}}
												    			</span>
												    		</p>
				@endif
												    		@if( (isset($coach->phone)?($coach->phone):'') != "")
												    		<p class="club_phone" >
												    			<span >
												    				<a href="tel:{{$coach->phone}}"><i class="fa fa-mobile"  aria-hidden="true"></i></a>
											    					<?php
															$daat2 =  str_replace(array('(',')','-','_',' '),'',$coach->phone);
															// echo $daat;
															if($daat2){
																if(isset($coach->c_code) && $coach->c_code!= ''){
											    			 	$code2 = '+'.$coach->c_code;

											    			 }else{
											    			 	$code2 = '';

											    			 }
													// echo   $formatted = $code."(".substr($daat,0,3).") ".substr($daat,3,3)."-".substr($daat,6);
													// echo $formatted = $code2. " (".substr($daat1,0,3).") ".substr($daat1,3,3)."-".substr($daat1,6);
																echo  $formatted = $code2. " (".substr($daat2,0,3).") ".substr($daat2,3,3)."-".substr($daat2,6);
															}	
											    			
											    				?>
												    			</span>
																</p>
															@endif
															</div>
											  			@endif
										  				</div>
													@else
													<!-- <div class="noinfo">
												<h3>No Information Available</h3>	
													<hr class="light">
									    		</div> -->
												@endif
												@endforeach
												@else
												<div class="noinfo">
													<h3>No Information Available</h3>	
									    		</div>
												@endif
											</div>
										</div>
									</div>
								</div>
								<div id="sports" class="tab-pane fade">
									<div>
										<div class='sports_fields'>
											<?php  $sportsStats =  json_decode(isset($userInfoArray['sportsdata'])?($userInfoArray['sportsdata']):'');   ?>
										   	 <?php $exist = 0;
										     	$statFields = array() ;
														 ?>

									    	
									    	@if(count($sportsStats) > 0  && (isset($sportsStats[0]->sport)?$sportsStats[0]->sport:'')  != null)
									    	<?php $i = 0; $j = 0; ?>
											@foreach($sportsStats as $sportsStat)

											<?php $sportsData = App\Sport::where('id',$sportsStat->sport)->first(); 
											$sportsMetaData = $sportsData?$sportsData->sportsMetaData:'';	?>
										
											@if($sportsMetaData)
										
											<div class="tab-inner">
										    <h3><span> {{isset($sportsData)?$sportsData->title:''}}</span></h3>
										    
										    <div class="inner-content">
										    	<div class="row">
											    	<div class="col-md-12">
											    		<form>
											    <?php
										     	$statDataField = array() ;
														 ?>
											    @if($sportsMetaData)
													@foreach($sportsMetaData as $statkey => $sportsMeta)
													@if($sportsMeta->field_type == 'field')
													@if(isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key}:'')

														<?php $statDataField[$statkey] = $sportsStat->{$sportsMeta->key} ;
														 ?>
													@endif
													@endif
													@endforeach
												@endif 
											    			 <?php $i = 0; ?>
											    			@if($sportsMetaData && count($statDataField) > 0 )
															
											    	        @foreach($sportsMetaData as $sportsMeta)
											    	       
											    	        <?php $i = 1; ?>
											    	       
											    	      @if($sportsMeta->field_type == 'field' && isset($sportsStat->{$sportsMeta->key}))
											    	        
											    	        <div class="front_sport_div @if($sportsMeta->type == 'checkbox') {{'position_front'}} @endif">
												    	        <div class="row @if(isset(json_decode($sportsMeta->details)->class)) {{json_decode($sportsMeta->details)->class}}@endif ">
										    						<div class="col-md-12 ">
										    							@if($sportsMeta->type == 'slider')
										    							<div class="slider">
																			
										    							<label class="@if($sportsMeta->type == 'instruction'){{'instructionclass'}} @endif" > {{$sportsMeta->display_name}}
										    							@if( isset(json_decode($sportsMeta->details)->units)  && (isset(json_decode($sportsMeta->details)->units)?json_decode($sportsMeta->details)->units:'') != '') <small>({{json_decode($sportsMeta->details)->units}})</small>@endif
</label>
										    							
										    							@else
										    							<label class="@if($sportsMeta->type == 'instruction'){{'instructionclass'}} @endif" >{{$sportsMeta->display_name}}
										    							@if( isset(json_decode($sportsMeta->details)->units)  && (isset(json_decode($sportsMeta->details)->units)?json_decode($sportsMeta->details)->units:'') != '')  <small>({{json_decode($sportsMeta->details)->units}})</small>@endif</label>

										    							@endif
														    		
														    		    @if($sportsMeta->type == 'checkbox')
														    			<div class="checbox ">
														    				<?php $checks = isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'' ; 
														    			// echo $sportsMeta->display_name	?>
														    				@if($checks)
																			@if(isset($userInfoArray['position']) && ($sportsMeta->display_name == 'Position'))
														    				<?php 
														    				$key = $userInfoArray['position'];
														    				if(in_array($key, $checks)){
																			$arr = $checks;
																			array_unshift($arr,$key);
																			$checks = array_unique($arr);
																			}
																			?>
																			@endif
														    				@foreach($checks as $check)
														    				@if($check)
														    				<?php $checked2 = isset(json_decode($sportsMeta->details)->options->$check)?json_decode($sportsMeta->details)->options->$check:'' ?>
														    				<p>
														    					<input checked='checked' type="checkbox" name="" value="" disabled >
														    					<label>{{$checked2}}</label>
														    				</p>
														    				@endif
														    				@endforeach
														    				@endif
														    			</div>
														    			@elseif($sportsMeta->type == 'slider')
														    			<?php 	//	print_r(json_decode($sportsMeta->details)->input->max) ; ?> 
															    			<div class="progress-bar">
															    			<!-- <span>@if(isset(json_decode($sportsMeta->details)->input->min)) {{json_decode($sportsMeta->details)->input->min}}@endif</span>
</span> -->
																    			<input class='sports-input' type="text" name="" value="{{isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'0'}}" min="{{json_decode($sportsMeta->details)->input->min}}" max="{{json_decode($sportsMeta->details)->input->max}}" disabled="disabled">
																    		<!-- 	<span>@if(isset(json_decode($sportsMeta->details)->input->max)) {{json_decode($sportsMeta->details)->input->max}}@endif</span>
																    		 {{  isset($sportsStat->{$sportsMeta->key})?"".$sportsStat->{$sportsMeta->key} :'0' }} -->
																    		</div>
															    		</div>@elseif($sportsMeta->type == 'select_dropdown')
														    			<?php 	$value = isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'' ; ;	?> 
															    		<input class='sports-input' type="text"  name="" value='{{isset(json_decode($sportsMeta->details)->options->$value)?json_decode($sportsMeta->details)->options->$value :'Not Filled in'}}' disabled>@elseif($sportsMeta->type == 'radio_btn')
														    			<?php 	$valuerad = isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'' ; ;	?> 
															    		<input class='sports-input' type="text"  name="" value='{{isset(json_decode($sportsMeta->details)->options->$valuerad)?json_decode($sportsMeta->details)->options->$valuerad :'Not Filled in'}}' disabled>
															    			
															    		@elseif($sportsMeta->type == 'instruction')
														    			
														    			@else
														    				<input class='sports-input' type="text"  name="" value='{{isset($sportsStat->{$sportsMeta->key})?($sportsStat->{$sportsMeta->key} ) :"Not Filled in"}}' disabled>
														    		
														    			@endif
														    		</div>
														 		</div>
														 	</div>
											    		  	@else
												 			<!-- 	<div class="noinfo hide">
																	<h3>No Information Available</h3>	
																</div> -->
															@endif
											 				@endforeach
											 				@else
												 				<div class="noinfo hide">
																	<h3>No Information Available</h3>	
																</div>
											 				@endif
													 		
											 			</form>
											    	</div>
										    	</div>
										    </div>
										    </div>
										  
										    @endif
										    <?php $exist = 0;
										     	$statData = array() ;
														 ?>
										    @if($sportsMetaData)
												@foreach($sportsMetaData as $statkey => $sportsMeta)
												@if($sportsMeta->field_type == 'stats')
												<?php $exist = 1;?>
												@if(isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key}:'')

													<?php $statData[$statkey] = $sportsStat->{$sportsMeta->key} ;
													 ?>
												@endif
												@endif
												@endforeach
											@endif
										    @if($exist > 0)
										    @if(count($sportsMetaData) > 0)
										    <div class="tab-inner">
												<h3>Stats</h3>
												<div class="inner-content">
													<form>
														<?php $j = 0 ; $statData = array_filter($statData);  ?>
														@if($sportsMetaData && count($statData) > 0)
														@foreach($sportsMetaData as $sportsMeta)
														@if($sportsMeta->field_type == 'stats' && isset($sportsStat->{$sportsMeta->key}) )
														<?php $j = 1; ?>
														<div class="front_sport_div @if($sportsMeta->type == 'checkbox') {{'position_front'}} @endif">
															<div class="row @if(isset(json_decode($sportsMeta->details)->class)) {{json_decode($sportsMeta->details)->class}}@endif">
												    			<div class="col-md-12 ">
																	<label class="@if($sportsMeta->type == 'instruction'){{'instructionclass'}} @endif" >{{$sportsMeta->display_name}}
										    							@if( isset(json_decode($sportsMeta->details)->units)  && (isset(json_decode($sportsMeta->details)->units)?json_decode($sportsMeta->details)->units:'') != '')  <small>({{json_decode($sportsMeta->details)->units}})</small>@endif</label>
																	@if($sportsMeta->type == 'checkbox')
																	<div class="checbox ">
																	<?php $checks = isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'' ; ?>
																	@if($checks)
																	@foreach($checks as $check)
																	@if($check)
																	<?php $checked1 = isset(json_decode($sportsMeta->details)->options->$check)?json_decode($sportsMeta->details)->options->$check:''  ?><p>
																			<input checked='checked' type="checkbox" name="" value="" disabled >
																			<label>{{$checked1}}</label>
																		</p>
																	@endif
																	@endforeach
																	@endif
																	</div>
																	@elseif($sportsMeta->type == 'slider')
																	<?php 	//	print_r(json_decode($sportsMeta->details)->input->max) ; ?> 
																	<div class="progress-bar">
																	<span>@if(isset(json_decode($sportsMeta->details)->input->min)) {{json_decode($sportsMeta->details)->input->min}}@endif</span>
																		<input class='sports-input' type="range" name="" value="{{isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'0'}}" min="{{json_decode($sportsMeta->details)->input->min}}" max="{{json_decode($sportsMeta->details)->input->max}}" disabled="disabled">

																		<span>@if(isset(json_decode($sportsMeta->details)->input->max)) {{json_decode($sportsMeta->details)->input->max}}@endif</span>
																		
																		{{  isset($sportsStat->{$sportsMeta->key})?'='.$sportsStat->{$sportsMeta->key} :'"= ".0' }}
																	</div>
																	@elseif($sportsMeta->type == 'select_dropdown')
														    			<?php 		
														    				$valuestat = isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'' ;?> 
															    		<input class='sports-input' type="text"  name="" value='{{isset(json_decode($sportsMeta->details)->options->$valuestat)?json_decode($sportsMeta->details)->options->$valuestat :'Not Filled in'}}' disabled>
															    	@elseif($sportsMeta->type == 'radio_btn')
														    			<?php 	$valuerad2 = isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key} :'' ;;	?> 
															    		<input class='sports-input' type="text"  name="" value='{{isset(json_decode($sportsMeta->details)->options->$valuerad2)?json_decode($sportsMeta->details)->options->$valuerad2 :'Not Filled in'}}' disabled>
																	@elseif($sportsMeta->type == 'instruction')
																	
																	@else
																		<input class='sports-input' type="text"  name="" value="{{isset($sportsStat->{$sportsMeta->key})?$sportsStat->{$sportsMeta->key}  :'Not Filled in'}}" disabled>
																	@endif
																</div>
															</div>
														</div>
													
														@endif
														
														@endforeach
															@else
															<div class="noinfo hide">
																		<h3>Not Filled In</h3>	
																</div>
														@endif
													</form>	
												</div>
											</div>
											@endif
											@endif
										    @endforeach
								 				@else
													<div class="tab-inner">
													    <h3>Sport: <span> {{isset($sportname)?$sportname:''}}
													   </span></h3>
													    <div class="inner-content">
													    	<div class="row">
														    	<div class="col-md-12">
														    		<form>
														    		<div class="noinfo hide">
																		<h3>No Information Available</h3>	
																</div>
														    		</form>
														    	</div>
														    </div>
														</div>
													</div>
											@endif
											
										</div>
									</div>
								</div>
								<div id="achievements" class="tab-pane fade">
									<div class="tab-inner">
										<div>
										    <h3>athletic achievements</h3>
										    <div class="inner-content">
										    	<?php  $athleticAchievements = json_decode(isset($userInfoArray['athletic_achievements'])?($userInfoArray['athletic_achievements']):'');  ?>
												@if($athleticAchievements)
												@foreach($athleticAchievements as $athleticAchievement)
													@if(isset($athleticAchievement->athleticname) && ($athleticAchievement->athleticname !='null') || (isset($athleticAchievement->years)&& ($athleticAchievement->years !='null') ) || 
													 isset($athleticAchievement->tournamentname) || isset($athleticAchievement->champ_run) )
												
											    	<div class="row">
											    		<div class="relative achive">
												    		<form>
														    	<div class="col-md-8">
														    		<label>Achievement</label>
														    		<!-- <input disabled value='{{(isset($athleticAchievement->athleticname)&& (isset($athleticAchievement->athleticname) ?$athleticAchievement->athleticname:"") != "null")?$athleticAchievement->athleticname:"Not Filled in"}}' type="text" name="" placeholder=""> -->
														    		<p class="content-achi" >{{(isset($athleticAchievement->athleticname)&& (isset($athleticAchievement->athleticname) ?$athleticAchievement->athleticname:"") != "null")?$athleticAchievement->athleticname:"Not Filled in"}}</p>
														    	</div>
														    	<div class="col-md-4">
														    		
														    		<label>Years</label>
														    		<?php if(isset($athleticAchievement->years) && $athleticAchievement->years != '') {
														    			$arraydata = array(); 
														    			foreach($athleticAchievement->years as $key => $yearsss){

														    				$arraydata[$key] = $yearsss->name;

														    			}
														    			if(count($arraydata) > 0){
														    				arsort($arraydata);
														    				$dataat = implode(', ', $arraydata);
														    			}else{
														    				$dataat = "Not Filled in";
														    			}
														    		}else{
														    			$dataat = "Not Filled in";
														    		}
														    		?>
														    		<input  disabled value='<?php echo $dataat; ?>'  type="text" name="" placeholder="">
														    	
														    	</div>
														    <!-- 	<div class="clear"></div>
														    	<div class="col-md-4">
														    		<label>School Name</label>
														    		<input disabled value='{{isset($athleticAchievement->school)?$athleticAchievement->school:"Not Filled in"}}'  type="text" name="" placeholder="">
														    	</div> -->
														    <!-- 	<div class="col-md-6">
														    		<label>Tournament Name</label>
														    		<input disabled value='{{isset($athleticAchievement->tournamentname)?$athleticAchievement->tournamentname:"Not Filled in"}}'  type="text" name="" placeholder="">
														    	</div> -->
														    	<!-- <div class="col-md-4">
														    		<label>Years</label>
														    		<input disabled  value='{{(isset($athleticAchievement->years1) && (isset($athleticAchievement->year1) ?$athleticAchievement->year1:"") != "null")?$athleticAchievement->year1:"Not Filled in"}}' type="text" name="" placeholder="">
														    	</div>-->
														    	<!-- <div class="col-md-6"> -->
														    		<!-- <label>Camp</label> -->
														    		<!-- <p class="champion-class" >{{isset($athleticAchievement->champ_run)?$athleticAchievement->champ_run:""}}</p> -->
														    	<!-- </div> -->
														    	<div class="clear"></div>
															</form>
															<div class="clear"></div>
															<hr class='light'>

												    		</div>
												    	</div>
												    	@else
												    	<!-- <div class="noinfo">
														<h3>No Information Available</h3>	
															<hr class='light'>
															
											    		</div> -->
												    	@endif
											    	@endforeach
											    	@else
												    <div class="noinfo">
														<h3>No Information Available</h3>	
															<hr class='light'>
											    	</div>
											    	@endif
											    </div>
											</div>
										</div>
										<div class="tab-inner">
											<div>
											    <h3>Academic Achievements
											
											    </h3>
											    <div class="inner-content">
											    
													    		<?php  $academicAchievements = json_decode(isset($userInfoArray['athelete_acadmic_achivment'])?($userInfoArray['athelete_acadmic_achivment']):'');  ?>
														    	@if($academicAchievements)
														    	@foreach($academicAchievements as $achievement)
														    	@if(isset($achievement->achivment) || (isset($achievement->year) && ($achievement->year !='null')))
													<div class="row">
											    		<div class="relative achive">
													    	<form>
														    	<div class="col-md-8">
														    		<label>Achievements</label>
														    		<p  class="content-achi" >{{isset($achievement->achivment)?$achievement->achivment:"Not Filled in"}}</p>
														    	</div>
														    	<div class="col-md-4">
														    		<label>Years</label>
														    		<?php if(isset($achievement->year) && $achievement->year != '') {
														    			$arraydata2 = array(); 
														    			foreach($achievement->year as $key => $yearsss){

														    				$arraydata2[$key] = $yearsss->name;

														    			}
														    			if(count($arraydata2) > 0){
														    				arsort($arraydata2);
														    				$dataat2 = implode(', ', $arraydata2);
														    			}else{
														    				$dataat2 = "Not Filled in";
														    			}
														    		}else{
														    			$dataat2 = "Not Filled in";
														    		}
														    		?>
														    		<input  disabled value='<?php echo $dataat2; ?>'  type="text" name="" placeholder="">
														    	</div>
															    <div class="clear"></div>
																<hr class='light'>
																 </form>
														</div>
											    	</div>
																@else
																<!-- <div class="noinfo">
																	<h3>No Information Available</h3>
																	<hr class='light'>
														    	</div> -->
																@endif
														    	@endforeach
														   
											    	@else
												    <div class="noinfo">
														<h3>No Information Available</h3>	
														<hr class='light'>
											    	</div>
											    	@endif
											    	<div class="clear"></div>
											    	<!--<hr class="light" />-->
						    
											    </div>
										    </div>
										</div>
										
									<div class="tab-inner">
										<div>
										    <h3>extra-curricular activities</h3>
										    <div class="inner-content">
										    
												         	<?php  $extra_curriculars = json_decode(isset($userInfoArray['extra_curricular'])?($userInfoArray['extra_curricular']):'');  ?>
													    	@if($extra_curriculars)
													   		@foreach($extra_curriculars as $extra_curricular)
													   		@if(isset($extra_curricular->activity) || (isset($extra_curricular->year) && ($extra_curricular->year != 'null' )))
												<div class="row">
											    		<div class="relative achive">
												    	<form>
													    	<div class="col-md-8">
													    		<label>Activity</label>
													    		<p  class="content-achi"   >{{isset($extra_curricular->activity)?$extra_curricular->activity:'Not Filled in'}}</p>
													    	</div>
													    	<div class="col-md-4">
													    		<label>Years</label>
														    		<?php if(isset($extra_curricular->year) && $extra_curricular->year != '') {
														    			$arraydata3 = array(); 
														    			foreach($extra_curricular->year as $key => $yearsss){

														    				$arraydata3[$key] = $yearsss->name;

														    			}
														    			if(count($arraydata3) > 0){
														    				arsort($arraydata3);
														    				$dataat3 = implode(', ', $arraydata3);
														    			}else{
														    				$dataat3 = "Not Filled in";
														    			}
														    		}else{
														    			$dataat3 = "Not Filled in";
														    		}
														    		?>
														    		<input  disabled value='<?php echo $dataat3; ?>'  type="text" name="" placeholder="">
													    	</div>
													    	<div class="clear"></div>
															<hr class='light'>
															 </form>
										    		</div>
										    	</div>
															@else
															<!-- <div class="noinfo">
																<h3>No Information Available</h3>	
																<hr class='light'>
													    	</div> -->
															@endif
													    	@endforeach
													    	@else
														    <div class="noinfo">
																<h3>No Information Available</h3>	
													    	</div>
													    	@endif
													    	<div class="clear"></div>
													    	<!--<hr class="light" />-->
													   	
										    </div>
										</div>
									</div>
									<div class="tab-inner">
								    	<div >
									    	<h3>Reference Letter</h3>
							 				
									    		<div class="inner-content">
											    
													    		<?php  $testimonails = json_decode(isset($userInfoArray['testimonails'])?($userInfoArray['testimonails']):'');  ?>
														        @if($testimonails)
														    	@foreach($testimonails as $testimonail)
														    
																@if( isset($testimonail->testimonail) )
														<form>
												    		<div class="row">
										    					<div class="relative testmonials">
															    	<div class="col-md-12">
															    		<label style="display: none">Testimonial</label>
															    		<p    disabled name="" >{!! isset($testimonail->testimonail)?$testimonail->testimonail:"Not Filled in" !!}</p>
															    	</div>
															    	
															    	<div class="clear"></div>
																	<hr class='light'>
																	</div>
										   					</div>
													     </form>
																@else
																 <!--  <div class="noinfo">
																	<h3>No Information Available</h3>	
																	<hr class='light'>
														    	</div> -->
																@endif
																@endforeach
																@else
															    <div class="noinfo ">
																	<h3>No Information Available</h3>	
																	<hr class='light'>
														    	</div>
																@endif
														    	<div class="clear"></div>
														    	<!--<hr class="light" />-->
												    		
									    		</div>
									   		
										</div>
									</div>
								</div>
							<div id="acadmics" class="tab-pane fade">
									<div class="tab-inner">
									    <h3>Academic information</h3>
									    <div class="inner-content">
											<?php  
												$academics = \App\AcademicDetailAthelete::where('user_id',$userData["id"])->get();?>
									        @if(count($academics) > 0 )
									    	@foreach($academics as $academic)
									    	
									    	<div class="row relative">
								    			<div class="relative academics">
								    				@if(($academic->qualification) != '' || ($academic->school_id) != '' || ($academic->location) != '' || ($academic->city) != ''||
								    				($academic->state) != '' ||
								    				($academic->zip) != '' || 
								    				    ($academic->gpa) != 0 || ($academic->sats) != 0 || ($academic->math) != 0 || ($academic->verbal) != 0 || ($academic->act) != 0)
									    			<div class="col-md-12">
											    		<!-- <h4>
											    			HIGH SCHOOL NAME
											    		</h4> -->
											    		@if(($academic->school_id) != '')
											    		<h2>
											    			{{ $academic->school_id }}
											    		</h2>
											    		@endif
											    		@if(($academic->location) != '')
												    		<p>
												    		<label>Location: </label>
												    		{{$academic->location}}
												    		</p>
											    		@endif
											    		@if(($academic->city) != '')
												    		<p>
												    		<label>City: </label>
												    		{{$academic->city}}
												    		</p>
											    		@endif
											    		@if(($academic->state) != '')
												    		<p>
												    		<label>State: </label>
												    		{{$academic->state}}
												    		</p>
											    		@endif
											    		@if(($academic->zip) != '')
												    		<p>
												    		<label>Zip Code: </label>
												    		{{$academic->zip}}
												    		</p>
											    		@endif
											    			
											    		<br />
											    		
											    		<h4>Academic Status</h4>
										
											    		<div class="progress-bar">
															<h2>GPA</h2>
															<!-- <input type="range" name="GPA"   step="0.1" min="0" max="5" disabled="disabled"  value="{{isset($academic->gpa)?$academic->gpa:''}}"> -->
															
															 {{(($academic->gpa) != 0)?"".$academic->gpa:'NA'}}
														</div>
														<p></p>
											    		<h4>Standardized Test Scores (SATs)</h4>
											    		<div class="progress-bar">
															<h2 class='light'>Reading</h2>
															<!-- <input type="range" name="Reading" step="1" min="200" max="800" disabled="disabled"  value="{{isset($academic->sats)?$academic->sats:''}}" > -->
													       
													        {{(($academic->sats) != 200)?"".$academic->sats:'NA'}}
													    </div>
														<div class="progress-bar">
															<h2 class='light'>Math</h2>
															<!-- <input type="range" name="Math" step="1"   min="200" max="800" disabled="disabled" value="{{isset($academic->math)?$academic->math:''}}"> -->
													  		
													  		{{(($academic->math) != 200)?"".$academic->math:'NA'}}
														</div>
														<div class="progress-bar">
															<h2 class='light'>Verbal</h2>
															<!-- <input type="range" name="Verbal" step="1"  min="200" max="800" disabled="disabled"  value="{{isset($academic->verbal)?$academic->verbal:''}}"> -->
															{{(($academic->verbal) != 200)?"".$academic->verbal:'NA'}}
														</div>
											    		<br />
											    		<div class="progress-bar">
															<h2 class='light'>ACT</h2>
															<!-- <input type="range" name="ACT" step="1"   min="0" max="36" disabled="disabled" value="{{isset($academic->act)?$academic->act:''}}"> -->
															{{ (($academic->act) != 0) ?"".$academic->act:'NA'}}
														</div>
										    		</div>
										    		@else
										    		<!-- <div class="noinfo">
											    		<h3>No Information Available</h3>
														<hr class='light'>
													</div> -->
										    		@endif
								    			</div>
							    			</div>
									    	@endforeach
									    	@else
										    <div class="noinfo">
												<h3>No Information Available</h3>	
									    	</div>
									    	@endif
									    </div>
									</div>
								</div>
								</div><!-- Tab Conent -->
							</div>
						</div>
					</div>
				</div>
		</section>

<div class="modal fade videowatch in" id="myModal" role="dialog" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close cloSEPOP" data-dismiss="modal">×</button>
				</div>
				<div class="modal-body">
				<div class="uploaded-images">
									<?php  $galleryVideos = json_decode(isset($userInfoArray['videogallery'])? ($userInfoArray['videogallery']):'');  ?>
							        @if($galleryVideos)
							        <?php $vide3 = 0; ?>
									@foreach($galleryVideos as $video)

									<?php if(isset($video->url)){ ?>
										<p style="display: none" id="videohide" class="popupvideo{{$vide3}} videohide"><iframe src="{{$video->url}}" frameborder="0" width="100%" height="500"></iframe></p>
									<?php } ?>
									<!-- <a href="#" id="video{{$vide}}" class="popupbutton">click</a>
									<div class="col-md-12 padding-10 ">
                                    	<iframe src='{{$video->url}}' width="100%" height="200"></iframe>
									</div> -->


									<?php $vide3++; ?>
									@endforeach
									@else
									<div class="noinfo">
										<h3>No Information Available</h3>	
									</div>
									@endif
								</div>
				
				</div>
			</div>
		</div>
	</div>
	</div>
<script type="text/javascript" src="{{  asset('js/simple-lightbox.js') }}"></script>
<script>
	jQuery(document).ready(function($){
		 $gallery = $('.gallery .list-group-items a').simpleLightbox();
	});
	
</script>
<script type="text/javascript">
		jQuery( document ).ready(function($) {
			$( ".popupbutton" ).on('click',function(e) {
					e.preventDefault();

					console.log($(this).attr('id'));

					show = $(this).attr('id');

					$('.'+show).show();

					$('.videowatch').show();

					$('body').addClass("bodyadd");

				});
			$( ".cloSEPOP" ).on('click',function() {

					console.log(show);

					$('.videohide').hide();
					$('.videowatch').hide();
					$('uploaded-images iframe').hide();

					$('.'+show+' iframe').attr('src', $('.'+show+' iframe').attr('src'));

					$('body').removeClass("bodyadd");

				});
			jQuery('#et_mobile_nav_menu .mobile_nav').meanmenu({
				meanMenuContainer: '#main-header',
				meanScreenWidth: "980"
			});

		});

	</script>
@stop

