@extends('layouts.signup')
@section('content')
    @if( \Auth::User()->role_id == 2 ||  \Auth::User()->role_id == 3)
        @if(! \Auth::User()->subscribed('main') &&  \Auth::user()->is_paid != 'yes' )
            @if( Voyager::setting('schools_upgrade_header') )
                <div class="notification-bar">
                    <a class="close-notification-bar"></a>
                    <p class="account-upgrade">
                        <?php $header = str_replace( '<p>','', Voyager::setting('schools_upgrade_header') );
                        $header = str_replace( '</p>','', $header );
                         ?>
                        {!! $header !!}
                    </p>
                </div>
            @endif
        @endif
    @endif
<?php $divisions = array();
    $conferences = array();
    foreach($user as $u){
        $CoachSchool = \App\CoachsSchool::where('school_id',$schooldata->id)->where('user_id',$u->id)->first();
        $divisionss = \Voyager::pagemetadetail('Division', '16');
        $divisionss = json_decode($divisionss);
        $divisionss = $divisionss->options;
        foreach($divisionss as $key => $div){ 
            if($div == $CoachSchool->division){ 
                $divisions[] = $key;
            }
        }
        $conferences[] = $CoachSchool->conference;
     }
     // print_r(array_unique($divisions));
      // print_r(count(array_filter(array_unique($conferences))));die; 
    if( Auth::user()->role_id == 2 || Auth::user()->role_id == 3){
            $userInfo = \App\UserInformation::where('user_id',Auth::user()->id)->where('meta_key','email_counter')->first();
            // print_r(Auth::user()->is_paid);
        if(( ! Auth::user()->subscribed('main') && Auth::user()->is_paid != 'yes' ) && ((isset($userInfo->meta_value)?$userInfo->meta_value:'0') >= 5 ) ){
            $valid = 0;
        }else{
            $valid = 1;
        }
        }else{
            $valid = 1;
        }
        if( isset($templatedata) ):  
        
    if( Auth::user()->role_id == 4 ){
            $shortcodes = array( 
                'event' => '{athlete_name} {athlete_school}   {event_title} {event_description} {event_event_start} {event_event_end} {event_place} {event_city} {event_state} {event_zipcode} {sport_title} {event_division} {event_tournaments} {event_campus_clinics} {event_school_attending} {my_name} {my_email} {my_sport} {college_name}  {my_phone} {my_web} {ncaa_championship} {national_tournament} {ncaa_final_four} {ncaa_post_season_appearance} {conference_championship} {my_bio} {my_title} {my_videogallery_urls} {school_information} {gender_coached}',
                'without' => '{athlete_name} {athlete_school}   {my_name} {my_email} {my_sport} {college_name} {my_phone} {my_web} {ncaa_championship} {national_tournament} {ncaa_final_four} {ncaa_post_season_appearance} {conference_championship} {my_bio} {my_title} {my_videogallery_urls} {school_information} {gender_coached}' );
        }else{
            $shortcodes = array( 
                'event' => '{coach_name} {college_name}   {event_title} {event_description} {event_event_start} {event_event_end} {event_place} {event_city} {event_state} {event_zipcode} {sport_title} {event_division} {event_tournaments} {event_campus_clinics} {event_school_attending} {my_name} {my_email} {my_position} {class_year} {my_phone} {club_name} {club_jersey_number} {gpa} {my_facebook} {my_twitter} {my_instagram} {my_snapchat} {my_skype} {my_videogallery_urls} {my_sport} {athlete_school} {my_address} {my_height} {my_weight} {my_birth} {my_academics} {my_club_info} {athletic_achievements} {academic_achievement} {extra_curricular_activity}', 
                'without' => '{coach_name} {college_name}   {my_name} {my_email} {athlete_school}  {club_jersey_number} {gpa} {my_position} {class_year} {my_phone} {my_facebook} {my_twitter} {my_instagram} {my_snapchat} {my_skype} {my_videogallery_urls} {my_sport} {my_address} {my_height} {my_weight} {my_birth} {my_academics} {my_club_info} {athletic_achievements} {academic_achievement} {extra_curricular_activity}' );
        }
        ?>
        <div class="log-in-popup email email-popup" v-cloak v-show="showpop_email">
            <email :validemail= "{{$valid}}"  :shortcodes="{{json_encode($shortcodes)}}" :templateda= "{{$templatedata}}" v-bind:userdata="dataform" v-bind:userdatabkd="dataform" v-bind:event_id="email_event_id"></email>
        </div>




        <?php
        endif;
        ?>
<div class="main-banner inner-banner">
        <div class="container">
            <h1>{{$schooldata->school_name}}</h1>
        </div>
    </div>
    <?php  $userIdsArray = '';
        if(count($all) > 0 ){
            $userIdsArray1 = array();
            foreach($all as $key=>$attendee){
            $userIdsArray1[$key]['userid'] = str_replace("'", '',$attendee->email);
            $userIdsArray1[$key]['username'] = str_replace("'", '',$attendee->name) ;
            } 
            $userIdsArray = json_encode($userIdsArray1);
            ?>
            <?php } ?>
    <div class="main-container main-inner">
            <section class="grey-bg">
            <div class="container single_school">
                <div class="col-md-12 ">
                    <div class="schoolinfo">
                        <div class="School"> 
                            <h2>School Information</h2>
                            <div class="row ">
                                <div class="col-md-8 "> 
                                     <p class="school-contact-information"> <span >SCHOOL CONTACT INFORMATION</span>  </p>
                                    <p><label>Address:</label> {{$schooldata->address?$schooldata->address:'No Information Available'}}</p>
                                    <p><label>City:</label> {{$schooldata->city?$schooldata->city:'No Information Available'}}</p>
                                    <p><label>State:</label> {{$schooldata->state?$schooldata->state:'No Information Available'}}</p>
                                    <?php $zip = $schooldata->zip;
                                            if(strlen($zip) == 4 && strlen($zip) > 0){
                                               $zip = '0'.$zip ;
                                            }
                                            if(strlen($zip) < 4 && strlen($zip) > 0){
                                               $zip = '00'.$zip ;
                                            }    ?>
                                    <p><label>Zip Code:</label> {{$zip?$zip:'No Information Available'}}</p>
                                    <?php 
                                    $url = str_replace('Https://', '', ($schooldata->school_url) ); 
                                    $url2 = str_replace('https://', '', $url ); 
                                    $url3 = str_replace('Http://', '', $url2) ;
                                    $url4 = str_replace('http://', '', $url3) ;?>
                                    <p><label>School Url:</label>  
                                    <?php 
                                    if($url4 != ''){  ?><a title="School Website" target="_blank" href="{{'http://'.$url4 }}">{{$url4}} </a> <?php }
                                    else{  ?>{{'No Information Available'}} <?php } ?>
                                    </p>
                                   

                                    <p><label>Phone:</label> {{$schooldata->telephone?$schooldata->telephone:'No Information Available'}}</p>
                                     <p><label>Institution Size:</label> {{$schooldata->school_size_total?$schooldata->school_size_total:'No Information Available'}}</p>
                                    <p><label>Percent Admitted - Total:</label> {{$schooldata->percent_admission?$schooldata->percent_admission."%":'No Information Available'}}</p>
                                    <p><label>Public or Private:</label> {{$schooldata->public?'$'.$schooldata->public:'No Information Available'}}</p> 
                                    <p></p>
                                     <p class="school-athletic-information"> <span >ATHLETIC INFORMATION</span>  </p>
                                       <p><label >Member of National Athletic Association:</label> {{$schooldata->member_naa?$schooldata->member_naa:'No Information Available'}}</p>
                                     <p><label>Conference:</label> {{implode(', ',array_filter(array_unique($conferences) ) )?implode(', ',array_filter(array_unique($conferences) ) ):'No Information Available'}}</p>
                                    <p><label>Division:</label> {{implode(', ',array_filter(array_unique($divisions) ) )?implode(', ',array_filter(array_unique($divisions) ) ):'No Information Available'}}</p>
                                    
                                </div>
                                <div class="col-md-4 ">
                                    <p class="school-tuition"> <span >TUITION</span>  </p>
                                    <p><label>In State:</label> {{$schooldata->tuition_in_state?'$'.number_format($schooldata->tuition_in_state,0, '.', ','):'No Information Available'}}</p>
                               
                                    <p><label>Out Of State:</label> {{$schooldata->tuition_out_of_state?'$'.number_format($schooldata->tuition_out_of_state,0, '.', ','):'No Information Available'}}</p>
                                
                                    <p ><label>Room & Board:</label> {{$schooldata->tuition_revenue_per_fte?'$'.number_format($schooldata->tuition_revenue_per_fte,0, '.', ','):'No Information Available'}}</p>  
                                    <p></p>
                                    <p class="school-academic-information"> <span >ACADEMIC INFORMATION</span>  </p>
                                    <p><label>SAT Critical Reading 25th Percentile Score:</label> {{$schooldata->sat_cri_25?$schooldata->sat_cri_25:'No Information Available'}}</p>
                                    <p><label >SAT Critical Reading 75th Percentile Score:</label> {{$schooldata->sat_cri_75?$schooldata->sat_cri_75:'No Information Available'}}</p>
                                    <p><label>SAT Math 25th Percentile Score:</label> {{$schooldata->sat_math_25?$schooldata->sat_math_25:'No Information Available'}}</p>
                                    <p><label>SAT Math 75th Percentile Score :</label> {{$schooldata->sat_math_75?$schooldata->sat_math_75:'No Information Available'}}</p>
                                    <p><label >ACT Composite 25th Percentile Score:</label> {{$schooldata->act_compose_25?$schooldata->act_compose_25:'No Information Available'}}</p>
                                    <p><label >ACT Composite 75th Percentile Score:</label> {{$schooldata->act_compose_75?$schooldata->act_compose_75:'No Information Available'}}</p>

                                   
                                </div>
                            </div>
                           <!--  @if(($schooldata->alias) != '')
                                <p><label>alias</label> {{$schooldata->alias}}</p>
                            @endif -->
                            <!-- @if(($schooldata->accreditor) != '')
                                <p><label>Accreditor</label> {{$schooldata->accreditor}}</p>
                            @endif -->
                        </div>
                    </div>
                </div>
                @if(count($user) > 0 || isset($_GET['_token']))
                <div class="col-md-7 ">
                @if(count($all) != 0)
                <div class="atheletes-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>Coaches ({{count($all)}})</h2>
                        </div>
                        <div class="col-md-4 text-right">
                            <a @click.prevent='showemail( <?php echo $userIdsArray; ?>)'  class="btn <?php if((Auth::user()->role_id == 2 || Auth::user()->role_id == 3) && ((!Auth::user()->subscribed('main')) && Auth::user()->is_paid != 'yes')){ echo 'light addblurrClass'; }else{ echo 'orange'; } ?> emailAllEvent">email all</a>                 
                        </div>
                    </div>
                </div>
                <div class="events-section atheletes-main">
                @foreach($user as $users)
                    <div class="events-content">
                        <div class="events-data">
                            <h3>
                            {{$users->name}}
                            </h3>
                        </div>
                        <div class="events-data">
                            <p>
                                <?php   
                                $userIdsArf = '';
                                $userIdsAr = array(); 
                                        $userIdsAr[0]['userid'] =   str_replace("'", '',$users->email);

                                        $userIdsAr[0]['username'] = str_replace("'", '',$users->name) ;
                                        $userIdsArf = json_encode($userIdsAr);
                                    ?>  
                             <a href="/profile/coach/{{$users->user_slug}}">See Profile</a>
                                <a  @click.prevent='showemail( <?php echo $userIdsArf; ?>)' href="#" id='show_login' class="send_email_event" >Send Email</a>   
                            </p>
                        </div>
                    </div>
                @endforeach
                </div>
              {!! $user->render() !!}
                @else
                <div class="atheletes-heading noresultfound no-school-found"><div class="row"><div class="col-md-8"><h2>No Information Available</h2></div> <div class="col-md-4 text-right"></div></div></div>
                @endif
                </div>
                @endif
                @if(count($user) > 0 || isset($_GET['_token']))
                    <div class="col-md-5 col-sm-12 ">
                @else
                    <div class="col-md-12 col-sm-12 ">
                @endif
                    <?php //if($schooldata->location_lat != '' && $schooldata->location_lon != ''){ ?>
                        <div class="schoolinfo map-school">
                            <div class="School">
                                <h2>Map</h2>
                                <?php $school_name = str_replace(' & ',' ',$schooldata->school_name) ;
                                $school_name = str_replace('&',' ',$school_name) ;
                                $arrayaddress = array(); 
                                if(isset($school_name)){
                                    $arrayaddress[] = $school_name;
                                }
                                // if(isset($schooldata->address)){
                                //     $arrayaddress[] = $schooldata->address; 
                                // }
                                if(isset($schooldata->city)){
                                    $arrayaddress[] = $schooldata->city; 
                                }
                                if(isset($schooldata->state)){
                                    $arrayaddress[] = $schooldata->state; 
                                }
                                // if(isset($schooldata->zip)){
                                //     $arrayaddress[] = explode('-',$schooldata->zip)[0]; 
                                // }
                                if(!empty($arrayaddress)){                         ?>
                                <iframe  width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"  src="https://maps.google.com/?ie=UTF8&amp;q=(<?php echo implode(', ',$arrayaddress); ?>)&amp;z=14&amp;iwloc=&amp;output=embed"></iframe>
                                <!-- <iframe width="100%" height="350" src="https://www.google.com/maps?q=({{$school_name}},{{$schooldata->city}},{{$schooldata->state}})&output=embed"></iframe> -->
                                <?php } ?>
                
                            </div>
                        </div>
                    <?php// } ?> 
                </div>
                </div>
                </div>
            </section>
        </div>  

    </div>
    
@stop

