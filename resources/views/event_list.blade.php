@extends('layouts.signup')

@section('content')
<?php
  function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }
?>
    @if( \Auth::User()->role_id == 2 ||  \Auth::User()->role_id == 3)
	    @if(! \Auth::User()->subscribed('main') &&  \Auth::user()->is_paid != 'yes' )
	        @if( Voyager::setting('events_upgrade_header') )
		        <div class="notification-bar">
		        	<a class="close-notification-bar"></a>
		        	<p class="account-upgrade">
		        		<?php $header = str_replace( '<p>','', Voyager::setting('events_upgrade_header') );
		        		$header = str_replace( '</p>','', $header );
		        		 ?>
					    {!! $header !!}
		        	</p>
		        </div>
			@endif
		@endif
	@endif
<?php $userData = \Auth::User();  ?>
<div class="filter-section <?php if( ( ( ! $userData->subscribed('main') &&  $userData->is_paid != 'yes')  && ( $userData->role_id == 2 || $userData->role_id == 3 ) ) || ( $userData->user_approved == 'INACTIVE' && $userData->role_id == 4)){ echo 'addblur-event-search'; } ?>">
	<div class="container">
		<h3>Filter Events</h3>
		<form name='eventSearchForm' id='eventSearchForm' action='/events/search' method='GET' >
			{{ csrf_field() }}
			<div>

				<div class="row">
					<div style="display: none" class="col-md-3 col-lg-3	 padd-right-remove">
						<label>Sport</label>
						@if($allSports && count($allSports) == 1)
								<select class="newsportsingle" name='sport_id' id='sport_id'>
								@if($allSports)
								@foreach($allSports as $sport)
								<option <?php if((isset($sport_id)?$sport_id:'') == $sport->id){ echo 'selected';} ?> value='{{$sport->id}}'>{{$sport->title}}</option>
								@endforeach
								@endif
							</select>
						@else
							<select class="" name='sport_id' id='sport_id'>
								<option value=''>Select Sport</option>
								@if($allSports)
								@foreach($allSports as $sport)
								<option <?php if((isset($sport_id)?$sport_id:'') == $sport->id){ echo 'selected';} ?> value='{{$sport->id}}'>{{$sport->title}}</option>
								@endforeach
								@endif
							</select>
						@endif
					</div>
					<div  style="display: none" class="col-md-4 col-lg-4 padd-right-remove">
						<label>My Location</label>
						<input type="text" name="location" minlength="5"  maxlength="5" id="location" value='{{isset($myLocation)?$myLocation:""}}' onkeypress="return isNumberKey(event)" placeholder="Zip Code">
					</div>
					<div style="display: none" class="col-md-3 col-lg-3 padd-right-remove">
						<label>Radius</label>
						<select class="" name='radius' id='state'>
						     <option <?php if((isset($radius)?$radius:'') == 'only_zip'){ echo 'selected';} ?> value="Only Zip Code">Zip Code</option>
						     <option <?php if((isset($radius)?$radius:'') == '10'){ echo 'selected';} ?> value="10">10 Miles</option>
						     <option <?php if((isset($radius)?$radius:'') == '50'){ echo 'selected';} ?> value="50">50 Miles</option>
						     <option <?php if((isset($radius)?$radius:'') == '100'){ echo 'selected';} ?> value="100">100 Miles</option>
						     <option <?php if((isset($radius)?$radius:'') == '200'){ echo 'selected';} ?> value="200">200 Miles</option>
						     <option <?php if((isset($radius)?$radius:'') == '500'){ echo 'selected';} ?> value="500">500 Miles</option>
						     <option <?php if((isset($radius)?$radius:'') == '1000000000'){ echo 'selected';} ?> value="1000000000"> > 500 Miles</option>
	   					</select>
					</div>
					<div class="col-md-3 col-lg-3 padd-right-remove">
						<label>State</label>
						<select name="state" id="state">
							<option value=''>Select State</option>
							<option value="AL" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AL") echo "selected";?>>Alabama</option>
							<option value="AK" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AK") echo "selected";?>>Alaska</option>
							<option value="AZ" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AZ") echo "selected";?>>Arizona</option>
							<option value="AR" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="AR") echo "selected";?>>Arkansas</option>
							<option value="CA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="CA") echo "selected";?>>California</option>
							<option value="CO" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="CO") echo "selected";?>>Colorado</option>
							<option value="CT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="CT") echo "selected";?>>Connecticut</option>
							<option value="DE" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="DE") echo "selected";?>>Delaware</option>
							<option value="DC" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="DC") echo "selected";?>>District of Columbia</option>
							<option value="FL" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="FL") echo "selected";?>>Florida</option>
							<option value="GA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="GA") echo "selected";?>>Georgia</option>
							<option value="HI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="HI") echo "selected";?>>Hawaii</option>
							<option value="ID" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="ID") echo "selected";?>>Idaho</option>
							<option value="IL" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="IL") echo "selected";?>>Illinois</option>
							<option value="IN" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="IN") echo "selected";?>>Indiana</option>
							<option value="IA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="IA") echo "selected";?>>Iowa</option>
							<option value="KS" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="KS") echo "selected";?>>Kansas</option>
							<option value="KY" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="KY") echo "selected";?>>Kentucky</option>
							<option value="LA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="LA") echo "selected";?>>Louisiana</option>
							<option value="ME" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="ME") echo "selected";?>>Maine</option>
							<option value="MD" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MD") echo "selected";?>>Maryland</option>
							<option value="MA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MA") echo "selected";?>>Massachusetts</option>
							<option value="MI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MI") echo "selected";?>>Michigan</option>
							<option value="MN" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MN") echo "selected";?>>Minnesota</option>
							<option value="MS" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MS") echo "selected";?>>Mississippi</option>
							<option value="MO" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MO") echo "selected";?>>Missouri</option>
							<option value="MT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="MT") echo "selected";?>>Montana</option>
							<option value="NE" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NE") echo "selected";?>>Nebraska</option>
							<option value="NV" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NV") echo "selected";?>>Nevada</option>
							<option value="NH" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NH") echo "selected";?>>New Hampshire</option>
							<option value="NJ" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NJ") echo "selected";?>>New Jersey</option>
							<option value="NM" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NM") echo "selected";?>>New Mexico</option>
							<option value="NY" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NY") echo "selected";?>>New York</option>
							<option value="NC" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="NC") echo "selected";?>>North Carolina</option>
							<option value="ND" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="ND") echo "selected";?>>North Dakota</option>
							<option value="OH" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="OH") echo "selected";?>>Ohio</option>
							<option value="OK" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="OK") echo "selected";?>>Oklahoma</option>
							<option value="OR" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="OR") echo "selected";?>>Oregon</option>
							<option value="PA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="PA") echo "selected";?>>Pennsylvania</option>
							<option value="RI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="RI") echo "selected";?>>Rhode Island</option>
							<option value="SC" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="SC") echo "selected";?>>South Carolina</option>
							<option value="SD" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="SD") echo "selected";?>>South Dakota</option>
							<option value="TN" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="TN") echo "selected";?>>Tennessee</option>
							<option value="TX" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="TX") echo "selected";?>>Texas</option>
							<option value="UT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="UT") echo "selected";?>>Utah</option>
							<option value="VT" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="VT") echo "selected";?>>Vermont</option>
							<option value="VA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="VA") echo "selected";?>>Virginia</option>
							<option value="WA" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WA") echo "selected";?>>Washington</option>
							<option value="WV" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WV") echo "selected";?>>West Virginia</option>
							<option value="WI" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WI") echo "selected";?>>Wisconsin</option>
							<option value="WY" <?PHP if((isset($_GET['state'])?$_GET['state']:'')=="WY") echo "selected";?>>Wyoming</option>
						</select>
					</div>
					<?php 
					$scholllist = \App\ApiData::select('id','school_name')->orderBy('school_name','ASC')->get();
					?>
					<div style="display: none;" class="col-md-3 col-lg-3 padd-right-remove">
						<label>School Attending <p class="school-loader" style="display: none">
                        <img src="/images/spin.gif" />
                        </p></label>
						<?PHP if(! isset($_GET['school'])){ ?>

							<input type="text" placeholder="school search" class="school-search-event" name="school">

						<?php }else{ ?>
						<input type="text" placeholder="school search" value="<?php echo $_GET['school']; ?>" class="school-search-event" name="school">

						<?php } ?>
						<ul class="school-result" style="display:none">
							
						</ul>
						<?PHP if(isset($_GET['school_attending']) &&  $_GET['school_attending'] != ''){
							$DATA = $_GET['school_attending'];
						}
						else{
							$DATA  = '';

						}
						?>
						<input type="hidden" class="school_attending" name="school_attending" value="{{$DATA}}" >
					</div>

						<?php $divisionss = Voyager::pagemetadetail('Division', '16');
						   $divisionss = json_decode($divisionss);
						   $divisionss = $divisionss->options;
						    ?>
					<div style="display: none" class="col-md-3 col-lg-3 padd-right-remove">
						<label>Division</label>
						<select name="division" >
							 <option  value="">Select Division</option>
							 <?php foreach($divisionss as $key => $div){ ?>
							 	<option <?php if((isset($division)?$division:'') == $div){ echo 'selected';} ?> value="<?php echo $div; ?>"><?php echo $key; ?></option>
							 <?php } ?>
						</select>
					</div>
					<div class="col-md-3 col-lg-3 padd-right-remove">
						<label>Date From</label>
						<input type="text" name="date_from" id="date_from" value='{{isset($_GET["date_from"])?$_GET["date_from"]:""}}' placeholder="Enter Start Date">
					</div>
					<div class="col-md-3 col-lg-3 padd-right-remove">
						<label>Date To</label>
						<input type="text" name="date_to" id="date_to" value='{{isset($_GET["date_to"])?$_GET["date_to"]:""}}' placeholder="Enter End Date">
					</div>
					<div class="col-md-3 col-lg-3 padd-right-remove">
						<label>Type Of Event</label>
						<select name="event_type"  class="">
							 <option  value="">Select Type Of Event</option>
	    				     <option <?php if((isset($_GET['event_type'])?$_GET['event_type']:'') == 'tournament'){ echo 'selected';} ?> value="{{'tournament'}}">{{'Tournament'}}</option> 
	    				     <option <?php if((isset($_GET['event_type'])?$_GET['event_type']:'') == 'camp'){ echo 'selected';} ?> value="{{'camp'}}">{{'Camp'}}</option>
	    				      <option <?php if((isset($_GET['event_type'])?$_GET['event_type']:'') == 'clinic'){ echo 'selected';} ?> value="{{'clinic'}}">{{'Clinic'}}</option>
						</select>
					</div>
					<!-- <div class="col-md-6 col-lg-3 padd-right-remove">
						<label>Tournaments</label>
						<select name="tournaments"  class="">
							 <option  value="">Select Tournaments</option>
	    				      <?php $tournamentsss = Voyager::pagemetadetail('tournaments', '16');
									   $tournamentsss = json_decode($tournamentsss);
									   $tournamentsss = $tournamentsss->options; ?>
							 <?php foreach($tournamentsss as $key => $divs){ ?>
							 	<option <?php if((isset($tournaments)?$tournaments:'') == $divs){ echo 'selected';} ?> value="<?php echo $divs; ?>"><?php echo $key; ?></option>
							 <?php } ?>
						</select>
					</div> -->
					<!-- <div class="col-md-6 col-lg-3">
						<label>Campus/Clinics</label>
						<input type="text" name="cmapus_clinics" value='{{isset($campus_clinics)?$campus_clinics:""}}'  placeholder="Enter School">
					</div> -->
				</div>
				<div class="row">
					<div class="col-md-3">	
						<input type="submit" class='btn btn-info' name="submit" value='SUBMIT'>
					</div>
					<div class="col-md-3">	
						<a class='btn orange' href="/events" >Clear Search</a>
					</div>
				</div>	
			</div>
		</form>
	</div>
</div>
<div id='focus_here' class="main-container main-inner <?php if( ( ( ! $userData->subscribed('main') &&  \Auth::user()->is_paid != 'yes' ) && ( $userData->role_id == 2 || $userData->role_id == 3 ) ) || ( $userData->user_approved == 'INACTIVE' && $userData->role_id == 4)){ echo 'addblur-event-table'; } ?>">
	<section class="grey-bg">
		<div class="container">
			<div class="events-section">
				<div class="events-heading">
					<div class="head">
						<h2>Event</h2>
					</div>
					<!-- <div class="head">
						<h2>Sport</h2>
					</div> -->
					<div class="head">
						<h2>State</h2>
					</div>
					<div class="head">
						<h2>Dates</h2>
					</div>
					<!-- <div class="head">
						<h2>Division</h2>
					</div> -->
					<div class="head"></div>
				</div>
			@if(count($allEvents) > 0)
				@foreach($allEvents as $event)
					<?php
					if(\Auth::User()->role_id ==4){
						$eventMeta = App\SportEventMeta::where('event_id',$event->id)->where('key','athlete_register')->first();
						$genderVal = \App\UserInformation::where('user_id',Auth::User()->id)->where('meta_key','coach_gender')->first();
                  		$genderKey = 'athelete_gender';
					}else{
						$eventMeta = App\SportEventMeta::where('event_id',$event->id)->where('key','coach_register')->first();
						$genderVal = \App\UserInformation::where('user_id',Auth::User()->id)->where('meta_key','athelete_gender')->first();
                  		$genderKey = 'coach_gender';
					}
					$attendees = array();
					if($eventMeta){
						$attendees = explode(',',$eventMeta->details);
					}
					
					$userIdsArray1 = array();
					if($attendees){
						foreach($attendees as $key3=>$u){
							$attendeesget = array();
							$attendeesget[] = $u;
							if(\Auth::User()->role_id != 1){
		                    if((isset($genderVal->meta_value)?$genderVal->meta_value:'') == 'Both' || (\Auth::User()->role_id == 2 || \Auth::User()->role_id == 3) ){                            
		                        $usersInfo7 = \App\UserInformation::select('user_id');
		                        if(\Auth::User()->role_id == 2 || \Auth::User()->role_id == 3){
		                            $usersInfo7->where('meta_key',$genderKey)->where('meta_value',isset($genderVal->meta_value)?$genderVal->meta_value:'')->orWhere('meta_value','Both')->where('meta_key',$genderKey);
		                        }else{
		                            $usersInfo7->where('meta_key',$genderKey);
		                        }
		                        $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
		                        $attendeesget = array_intersect($usersInfoArray7,$attendeesget); 
		                     // print_r($attendeesget);  die('here');
		                    }else{
		                         $usersInfo7 = \App\UserInformation::select('user_id');
		                        $usersInfo7->where('meta_key',$genderKey)->where('meta_value' ,isset($genderVal->meta_value)?$genderVal->meta_value:'');
		                        $usersInfoArray7 = $usersInfo7->pluck('user_id')->toArray();
		                        $attendeesget = array_intersect($usersInfoArray7,$attendeesget);
		                        // print_r($usersInfoArray7);  die('here');
		                    }
		                	}
							$user = App\User::where('user_approved','ACTIVE')->whereIn('role_id',[2,3,4])->whereIn('id',$attendeesget)->first();
							if($user){
								$userInfo = \Voyager::UserInformation('all', $user->id );
										if($user->role_id != 4){
											$userName = (isset($userInfo['athelete_firstname'])?$userInfo['athelete_firstname']:'').' '.str_replace("'", '',isset($userInfo['athelete_lastname'])?$userInfo['athelete_lastname']:'');
											$useremail = str_replace("'", '',isset($userInfo['athelete_email'])?$userInfo['athelete_email']:'');
										}else{
											$userName = (isset($userInfo['coach_firstname'])?$userInfo['coach_firstname']:'').' '.str_replace("'", '',isset($userInfo['coach_lastname'])?$userInfo['coach_lastname']:'');
											$useremail = str_replace("'", '',isset($userInfo['coach_email'])?$userInfo['coach_email']:'');
										}
							
							 	$userIdsArray1[$key3]['userid'] = $useremail ; 
								$userIdsArray1[$key3]['username'] = $userName; 
							}
						}
					}
					$userIdsArray = json_encode($userIdsArray1);
					//print_r($userIdsArray);
					?>
				<div class="events-content">
					<div class="events-data">
						<h2><a href="/event/{{$event->event_slug}}">{{$event->title}}</a></h2>
					</div>
					<!-- <div class="events-data">
					<?php //	$sport = Voyager::Sportevent($event->sport_id);   ?>
                  	<p>{{$sport->title}}</p>
					</div> -->
					<div class="events-data">
						<p>{{$event->state}}</p><!-- 
						<p>{{$event->event_gender}}</p>
						<p>{{$event->id}}</p> -->
					</div>
					<div class="events-data">
						<p>
							{{date('F',strtotime($event->event_start))}} 
							{{addOrdinalNumberSuffix(date('d',strtotime($event->event_start))).","}} 
							{{date('Y',strtotime($event->event_start))}}
							{{ ' to ' }}
							{{date('F',strtotime($event->event_end))}} 
							{{addOrdinalNumberSuffix(date('d',strtotime($event->event_end))).","}} 
							{{date('Y',strtotime($event->event_end))}}</p>
					</div>
					
					<div class="events-data event-break">
						<p>
						@if( count($userIdsArray1) == 0 )
						<a class="contact_coaches"><?php if(\Auth::User()->role_id == 4 ) { echo 'No Athletes Available' ;} else{ echo 'No Coaches Available' ; } ?></a>
						@else
							<a @click.prevent='showemail(<?php echo $userIdsArray; ?>, <?php echo $event->id; ?> )' href="#" id='contact_coaches-{{$event->id}}' class='contact_coaches' dataitem='<?php echo $userIdsArray; ?>' name='{{$event->id}}-{{\Auth::User()->role_id}}'>@if(\Auth::User()->role_id == 4 ) Contact Athletes @else Contact Coaches  @endif</a>
							<a href="/event/<?php if(\Auth::User()->role_id == 4 ) { echo 'athlete' ;} else if(\Auth::User()->role_id == 1 ){ echo 'admin' ; }else { echo 'coach' ; } ?>/{{$event->event_slug}}/">WHO's ATTENDING</a>
						@endif
						</p>
					</div>
				</div>
				@endforeach
				@else
				<script>
					jQuery('.events-section').addClass('not-found');
				</script>
				<div class="events-data">
					<h3>Sorry, no events found in your search. Please modify your search and try again.</h3>
				</div>
				@endif
				</div>
				
				{!! $allEvents->appends(Request::except('page'))->render() !!}
		</div>
	</section>
</div>
@if(isset($_GET['_token']))
    <script>
	    $('html, body').animate({
	        'scrollTop' : $("#focus_here").position().top
	    });
	</script>
@endif
<script>
jQuery( document ).ready(function($) {
	$('.school-loader').hide();
	$('.school-search-event').on('keyup', function() {
			console.log(this.value.length);
			
			if(this.value.length > 1){
				$('.school-loader').show();

				$.ajax({
					type: 'GET',
					url: '/api/schoolnameevent',
					data: 'schoolname=' +this.value,
					success: function(data) {
						$('.school-result').show();
					    $('.school-loader').hide();

						var parsed = JSON.parse(data);
						
						$('.school-result').empty();
						console.log(parsed);
						for(var i = 0; i < parsed.length; i++){

						 $('.school-result').append('<li class="selectschool" name="'+parsed[i].school_name+'" value="' + parsed[i].id + '">' + parsed[i].school_name+ '</li>');	    

						}
						
					},
	                error: function (error) {
	     			   	('.school-loader').hide();

	                }
				});
			}else{
				 $(".school_attending").val('');
				 $('.school-result').empty();
				  $('.school-result').hide();
				 $('.school-loader').hide();

			}

		});
		$("body").delegate(".selectschool", "click", function(){

			 var schoolname = $(this).attr('name');
			 var school_id = $(this).attr('value');
			 $(".school_attending").val(school_id);
			 $(".school-search-event").val(schoolname);
			 $('.school-result').hide();

		});	

    jQuery("#date_from").datepicker({
	   	changeMonth: true, 
	    changeYear: true, 
	    yearRange: "-05:+05"
    });
    jQuery("#date_to").datepicker({
	 	changeMonth: true, 
	    changeYear: true, 
	    yearRange: "-05:+05"
    });

});
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
@stop

