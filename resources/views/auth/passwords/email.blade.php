@extends('layouts.main')

@section('content')
<div class="log-in-popup site-login">
<div class="sign-up-content">
    <div class="panel-heading">Reset Password</div>
    <div class="panel-body">
        <div class="container">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }} 
                          <div class="half">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">Email</label>
                                    <input  id="email" type="email" class="form-control" name="email" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                       <div class="form-group">
                    <button type="submit" class="btn btn-primary orange">
                    Send Password Reset Link
                    </button>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
