@extends('layouts.main')
@section('content')

<div class="log-in-popup site-login">
    @if(\Session::has('message'))
    <ul class="alert alert-success update_password">
        <li>{{ \Session::get('message') }}</li>
    </ul>
@endif
<div class="sign-up-content">

    <div class="panel-heading">Change Password</div>
    <div class="panel-body">
        <div class="container">
           
            <form id="form-change-password" role="form" method="POST" action="/change/password" novalidate class="form-horizontal"> 
                {{ csrf_field() }}  
                    <!-- <div class="full">
                            <label for="current-password" class="control-label">User Email</label>
                            <input type="email" value='{{$user->email}}' class="form-control" id="email" name="email" placeholder="Email" >
                    </div> -->
                    <!-- <div class="half">
                        <div class="form-group">
                            <label for="current-password" class="control-label">Current Password</label>
                           <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Password"  >
                            
                        </div>
                    </div> -->
                    <div class="half">
                        <div class="form-group">
                            <label for="password" class="control-label">New Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password"  >
                        </div>
                    </div>
                    
                    <div class="half">
                        <div class="form-group">
                            <label for="password_confirmation" class="control-label">Re-enter Password</label>
                           <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password" >
                            
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="form-group">
                        <div class="ol-sm-12">
                          <button type="submit" class="btn btn-primary orange">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
         <div class="">&nbsp;</div>
        @if(\Auth::User()->role_id != 1 && \Auth::User()->role_id != 4)
    <div class="delete-acc-box">
         <p >If you no longer wish to have a player profile, search colleges, email coaches and view sporting events near you, you can delete your account <a  style="cursor:pointer;"  class="delete-acc-btn-here delete-acc-btn" >here</a>. Please note, once you delete, all information WILL be lost.</p>
         <a  id="delete-acc-btn" style="cursor:pointer;"  class="btn btn-primary orange delete-acc-btn" >Delete Your Account</a>
     </div>
      
    @endif
       
    </div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#form-change-password').validate({ // initialize the plugin
        rules: {
            // email: {
            //     required: true,
            //     email: true
            // },
            password: {
                 required: true
            },
             password_confirmation: {
                // required: true,
                equalTo: "#password"
            },
        }
    });
    jQuery('.delete-acc-btn').on( 'click', function(e) {
        // alert('here');
        e.preventDefault();
        var link = "<?php echo url('/delete/user_account'); ?>";
        var txt;
        var r = confirm("Are you sure, You want to delete your account.");
        if (r == true) {
            window.location.replace(link);
        } else {
        }
    });
});

</script>
@endsection
