@extends('layouts.main')

@section('content')

<div class="log-in-popup site-login">
<div class="sign-up-content">
    <div class="panel-heading">Login</div>
    <div class="panel-body">
        <div class="container">
            <form role="form" method="POST" action="{{ route('login') }}" class="form-horizontal">
             {{ csrf_field() }}
                <div class="half">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">E-Mail Address</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus class="form-control">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                       
                    </div>
                </div>
                <div class="half">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">Password</label>
                        <input  id="password" type="password" class="form-control" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                            <input  type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                        </a>
                    </div>
                </div> 
                <div class="form-group">
                    <button type="submit" class="btn btn-primary orange">
                    Login
                    </button>
                </div>
                <!-- <div class="radio-section">
                    <a @click="login_url(2)"  href="#" :class="athelete_class"><input type="radio" checked name="role" value="2">i&#39;m an athlete</a>
                    <a @click="login_url(4)" href="#" :class="coach_class"><input type="radio" name="role" value="4">i&#39;m a coach</a>
                    <a @click="login_url(3)" href="#" :class="parent_class"><input type="radio" name="role" value="3">i&#39;m a parent</a> 
                </div> -->
                <div class="social-signup">
                    <social-login  :loginid="{{ json_encode('none') }}"></social-login>
                </div>
            </form>
        </div>
    </div>
</div>
        </div>
        
@endsection
