@extends('layouts.signup')

@section('content')
	@if( \Auth::User()->role_id == 2 ||  \Auth::User()->role_id == 3)
	    @if(! \Auth::User()->subscribed('main') &&  \Auth::user()->is_paid != 'yes' )
	        @if( Voyager::setting('schools_upgrade_header') )
		        <div class="notification-bar">
		        	<a class="close-notification-bar"></a>
		        	<p class="account-upgrade">
		     			<?php $header = str_replace( '<p>','', Voyager::setting('schools_upgrade_header') );
		        		$header = str_replace( '</p>','', $header );
		        		 ?>
					    {!! $header !!}
		        	</p>
		        </div>
			@endif
		@endif
	@endif
<?php $userData = \Auth::User();
// print_r(count($totalUserIds));?>

	<div class="main-container main-inner <?php if(( ! $userData->subscribed('main') &&  \Auth::user()->is_paid != 'yes' ) && ( $userData->role_id == 2 || $userData->role_id == 3 ) ){ echo 'addblur-class-school'; } ?>">
		<section class="grey-bg">
			<div class="container">
				
				<div class="col-md-8 padding-right">
				
				@if($total && count($total) != 0)
				<div class="atheletes-heading">
					<div class="row">
						<div class="col-md-8">
							<h2> <!-- Total Number of --> Colleges ({{$total}})</h2>
						</div>
						@if( ( (isset($_GET['school_name'])?$_GET['school_name']:'' != '') || (isset($_GET['school_city'])?$_GET['school_city']:'' != '') || (isset($_GET['stateFilter'])?$_GET['stateFilter']:'' != '') || (isset($_GET['division'])?$_GET['division']:'' != '') || (isset($_GET['conference'])?$_GET['conference']:'' != '') || (isset($_GET['school_size'])?$_GET['school_size']:'' != '') ) && (Auth::user()->role_id != 1 ) )
						
						 <?php 	if(count($totalUserIds) > 0 ){
						  	// echo count( $totalUserIds );
								$userIdsArray1 = array();
				        		foreach($totalUserIds as $key=>$user){
								$userInfo = \Voyager::UserInformation('all', $user['id'] );
									 
									$userName = str_replace("'", '',(isset($userInfo['coach_firstname'])?$userInfo['coach_firstname']:'')).' '.str_replace("'", '',isset($userInfo['coach_lastname'])?$userInfo['coach_lastname']:'');
									$useremail = str_replace("'", '',isset($userInfo['coach_email'])?$userInfo['coach_email']:'');

								$userIdsArray1[$key]['userid'] = $useremail ; 
								$userIdsArray1[$key]['username'] = $userName;
							}
							$userIdsArray = json_encode($userIdsArray1);
							//$userIdsArray = implode(',',$userIdsArray1);
						
						}else{
							$userIdsArray = '' ;	
						} 
						//print_r(($userIdsArray)) ;die;	
					?>
					
						<div class="col-md-4 text-right">
							<a class="btn <?php if((Auth::user()->role_id == 2 || Auth::user()->role_id == 3) && ((!Auth::user()->subscribed('main')) && Auth::user()->is_paid != 'yes')){ echo 'light addblurrClass'; }else{ echo 'orange'; } ?> " @click.prevent='showemail( <?php echo $userIdsArray; ?>)' href="#" id='show_login' class="send_email_event" >Email All</a>			
						</div>
					@endif	
					</div>
				</div>
				@else
				<div class="atheletes-heading noresultfound noschool">
					<div class="row">
						<div class="col-md-8">
							<h2>No Result Found</h2>
						</div> 
						<div class="col-md-4 text-right">
							
						</div>
					</div>
				</div>
				@endif	
					<div class="events-section atheletes-main">
						@foreach($ApiData as $key=>$ApiDa)
						<?php $cityState = array();
						 $cityState[0] = $ApiDa->city;
						 $cityState[1] = $ApiDa->state;
						 ?>
						<div class="events-content">
							<div class="events-data">
								<h3>
									<a href="/college/{{$ApiDa->school_slug}}">{{$ApiDa->school_name}}
									<br />
									<h5>
										<!-- <b> -->
										{{implode(', ',array_filter($cityState))}}
										<!-- </b> -->
									</h5>
									</a>
								</h3>
							</div>
							<div class="events-data">
								<p>
								 <a href="/college/{{$ApiDa->school_slug}}">View School</a>
								</p>
							</div>
						</div>
						@endforeach
						</div>
						{!! $ApiData->appends(Request::except('page'))->render() !!}
				</div>
				<div  class="col-md-4 col-sm-12 right-content ">
					<div class="right-box">
						<h1>filter by</h1>
						<form  class="filter-form" action='/colleges' method='GET'>
							
							{{csrf_field() }}
						
					<div class="progress-bar">
 
						<label>School Name<p class="school-loader" style="display: none">
                        <img src="/images/spin2.gif" />
                        </p></label>
						<input autocomplete="off" placeholder="School Name" name="school_name" value="{{isset($_GET['school_name'])?$_GET['school_name']:''}}" class="school-search-event" id='VerJumpFilter' >
						
						<ul class="school-result" style="display:none">
							
						</ul>
						
					</div>
		    				<label>City</label>
							<div class="progress-bar">
		    					<input type="text" placeholder="City" name="school_city" id='school_city'  value='{{isset($school_city)?$school_city:""}}' >
		    				</div>

							<label>State</label>
								<select  id='stateFilter' placeholder="School State" name='stateFilter'  class="js-example-basic-single stateFilter" >
									<option value=''>Select State</option>
									<option value="AL" <?PHP if((isset($state)?$state:'')=="AL") echo "selected";?>>Alabama</option>
									<option value="AK" <?PHP if((isset($state)?$state:'')=="AK") echo "selected";?>>Alaska</option>
									<option value="AZ" <?PHP if((isset($state)?$state:'')=="AZ") echo "selected";?>>Arizona</option>
									<option value="AR" <?PHP if((isset($state)?$state:'')=="AR") echo "selected";?>>Arkansas</option>
									<option value="CA" <?PHP if((isset($state)?$state:'')=="CA") echo "selected";?>>California</option>
									<option value="CO" <?PHP if((isset($state)?$state:'')=="CO") echo "selected";?>>Colorado</option>
									<option value="CT" <?PHP if((isset($state)?$state:'')=="CT") echo "selected";?>>Connecticut</option>
									<option value="DE" <?PHP if((isset($state)?$state:'')=="DE") echo "selected";?>>Delaware</option>
									<option value="DC" <?PHP if((isset($state)?$state:'')=="DC") echo "selected";?>>District of Columbia</option>
									<option value="FL" <?PHP if((isset($state)?$state:'')=="FL") echo "selected";?>>Florida</option>
									<option value="GA" <?PHP if((isset($state)?$state:'')=="GA") echo "selected";?>>Georgia</option>
									<option value="HI" <?PHP if((isset($state)?$state:'')=="HI") echo "selected";?>>Hawaii</option>
									<option value="ID" <?PHP if((isset($state)?$state:'')=="ID") echo "selected";?>>Idaho</option>
									<option value="IL" <?PHP if((isset($state)?$state:'')=="IL") echo "selected";?>>Illinois</option>
									<option value="IN" <?PHP if((isset($state)?$state:'')=="IN") echo "selected";?>>Indiana</option>
									<option value="IA" <?PHP if((isset($state)?$state:'')=="IA") echo "selected";?>>Iowa</option>
									<option value="KS" <?PHP if((isset($state)?$state:'')=="KS") echo "selected";?>>Kansas</option>
									<option value="KY" <?PHP if((isset($state)?$state:'')=="KY") echo "selected";?>>Kentucky</option>
									<option value="LA" <?PHP if((isset($state)?$state:'')=="LA") echo "selected";?>>Louisiana</option>
									<option value="ME" <?PHP if((isset($state)?$state:'')=="ME") echo "selected";?>>Maine</option>
									<option value="MD" <?PHP if((isset($state)?$state:'')=="MD") echo "selected";?>>Maryland</option>
									<option value="MA" <?PHP if((isset($state)?$state:'')=="MA") echo "selected";?>>Massachusetts</option>
									<option value="MI" <?PHP if((isset($state)?$state:'')=="MI") echo "selected";?>>Michigan</option>
									<option value="MN" <?PHP if((isset($state)?$state:'')=="MN") echo "selected";?>>Minnesota</option>
									<option value="MS" <?PHP if((isset($state)?$state:'')=="MS") echo "selected";?>>Mississippi</option>
									<option value="MO" <?PHP if((isset($state)?$state:'')=="MO") echo "selected";?>>Missouri</option>
									<option value="MT" <?PHP if((isset($state)?$state:'')=="MT") echo "selected";?>>Montana</option>
									<option value="NE" <?PHP if((isset($state)?$state:'')=="NE") echo "selected";?>>Nebraska</option>
									<option value="NV" <?PHP if((isset($state)?$state:'')=="NV") echo "selected";?>>Nevada</option>
									<option value="NH" <?PHP if((isset($state)?$state:'')=="NH") echo "selected";?>>New Hampshire</option>
									<option value="NJ" <?PHP if((isset($state)?$state:'')=="NJ") echo "selected";?>>New Jersey</option>
									<option value="NM" <?PHP if((isset($state)?$state:'')=="NM") echo "selected";?>>New Mexico</option>
									<option value="NY" <?PHP if((isset($state)?$state:'')=="NY") echo "selected";?>>New York</option>
									<option value="NC" <?PHP if((isset($state)?$state:'')=="NC") echo "selected";?>>North Carolina</option>
									<option value="ND" <?PHP if((isset($state)?$state:'')=="ND") echo "selected";?>>North Dakota</option>
									<option value="OH" <?PHP if((isset($state)?$state:'')=="OH") echo "selected";?>>Ohio</option>
									<option value="OK" <?PHP if((isset($state)?$state:'')=="OK") echo "selected";?>>Oklahoma</option>
									<option value="OR" <?PHP if((isset($state)?$state:'')=="OR") echo "selected";?>>Oregon</option>
									<option value="PA" <?PHP if((isset($state)?$state:'')=="PA") echo "selected";?>>Pennsylvania</option>
									<option value="RI" <?PHP if((isset($state)?$state:'')=="RI") echo "selected";?>>Rhode Island</option>
									<option value="SC" <?PHP if((isset($state)?$state:'')=="SC") echo "selected";?>>South Carolina</option>
									<option value="SD" <?PHP if((isset($state)?$state:'')=="SD") echo "selected";?>>South Dakota</option>
									<option value="TN" <?PHP if((isset($state)?$state:'')=="TN") echo "selected";?>>Tennessee</option>
									<option value="TX" <?PHP if((isset($state)?$state:'')=="TX") echo "selected";?>>Texas</option>
									<option value="UT" <?PHP if((isset($state)?$state:'')=="UT") echo "selected";?>>Utah</option>
									<option value="VT" <?PHP if((isset($state)?$state:'')=="VT") echo "selected";?>>Vermont</option>
									<option value="VA" <?PHP if((isset($state)?$state:'')=="VA") echo "selected";?>>Virginia</option>
									<option value="WA" <?PHP if((isset($state)?$state:'')=="WA") echo "selected";?>>Washington</option>
									<option value="WV" <?PHP if((isset($state)?$state:'')=="WV") echo "selected";?>>West Virginia</option>
									<option value="WI" <?PHP if((isset($state)?$state:'')=="WI") echo "selected";?>>Wisconsin</option>
									<option value="WY" <?PHP if((isset($state)?$state:'')=="WY") echo "selected";?>>Wyoming</option>
								</select>
							<!-- <label>Zip Code</label>
							<div class="progress-bar">
		    					<input type="text" placeholder="Zip Code" name="zipcode" id='zipcode'  value='{{isset($zipcode)?$zipcode:""}}' >
		    				</div> -->
		    				<label>Division</label>
							<?php $divisionss = Voyager::pagemetadetail('Division', '16');
							   	$divisionss = json_decode($divisionss);
							   	$divisionss = $divisionss->options;
						    ?>
							<select name="division" >
								 <option  value="">Select Division</option>
								 <?php foreach($divisionss as $key => $div){ ?>
								 	<option <?php if((isset($_GET['division'])?$_GET['division']:'') == $div){ echo 'selected';} ?> value="<?php echo $div; ?>"><?php echo $key; ?></option>
								 <?php } ?>
							</select>
							<label>Conference</label>
							<select name="conference" >
								 <option  value="">Select Conference</option>
								 <?php //print_r($conferences); ?>
								 <?php foreach($conferences as $conferencess){ ?>
								 	<option <?php if((isset($_GET['conference'])?$_GET['conference']:'') == $conferencess){ echo 'selected';} ?> value="<?php echo $conferencess; ?>"><?php echo $conferencess; ?></option>
								 <?php }  ?>
							</select>	
							<label>School Size</label>
							<select name="school_size" >
								 <option  value="">Select School Size</option>
								 <?php foreach($schoolSizes as $schoolSize){ ?>
								 	<option <?php if((isset($_GET['school_size'])?$_GET['school_size']:'') == $schoolSize){ echo 'selected';} ?> value="<?php echo $schoolSize; ?>"><?php echo $schoolSize; ?></option>
								 <?php }  ?>
							</select>

							<div class="col-lg-6 col-xs-12">
								<input type="submit" class='btn btn-info' name="submit" value='Submit'>
							</div>
							<div class="col-lg-6 col-xs-12">
								<a href='/colleges' class=' btn orange'>Reset</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
 <script>
 	jQuery( document ).ready(function($) {
	$('.school-loader').hide();
	$('.school-search-event').on('keyup', function() {
			console.log(this.value.length);
			
			if(this.value.length > 1){
				$('.school-loader').show();

				$.ajax({
					type: 'GET',
					url: '/api/schoolnameeventschool',
					data: 'schoolname=' +this.value,
					success: function(data) {
						$('.school-result').show();
					    $('.school-loader').hide();

						var parsed = JSON.parse(data);
						
						$('.school-result').empty();
						console.log(parsed);
						for(var i = 0; i < parsed.length; i++){

							 $('.school-result').append('<li class="selectschool" schoolcity="'+parsed[i].city+'" schoolstate="'+parsed[i].state+'" name="'+parsed[i].school_name1+'" value="' + parsed[i].id + '">' + parsed[i].school_name+ '</li>');	    

						}
						
					},
	                error: function (error) {
	     			   	('.school-loader').hide();

	                }
				});
			}else{
				 $(".school_attending").val('');
				 $('.school-result').empty();
				  $('.school-result').hide();
				 $('.school-loader').hide();

			}

		});
		$("body").delegate(".selectschool", "click", function(){

			 var schoolname = $(this).attr('name');
			 var school_id = $(this).attr('value');
			 var schoolcity = $(this).attr('schoolcity');
			 var schoolstate = $(this).attr('schoolstate');
			 $(".school_attending").val(school_id);
			 $("#school_city").val(schoolcity);
			 $("#stateFilter").val(schoolstate);
			 $(".school-search-event").val(schoolname);
			 $('.school-result').hide();
			 
		});
});
 	jQuery( document ).ready(function($) {
		jQuery('.send_email_event').click( function ($){
		  	var userID = jQuery(this).attr('name');
		  
			jQuery('#user_id').val(userID);
		}); 	 
	});
	jQuery( document ).ready(function($) {
		jQuery('.emailAllEvent').click( function ($){
		var getName = jQuery(this).attr('name');
		if(getName != ''){
		jQuery('#user_ids').val(getName);
		}
  	 	
		}); 	 
	});
 </script>
@stop

