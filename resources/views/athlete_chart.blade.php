@extends('layouts.charts-layout')
<style type="text/css">
    ol.breadcrumb{
        display: none;
    }
</style>
@section('content')
    <div class='clearfix'>&nbsp;</div>
    <div class="main-container main-inner">
        <section class="grey-bg">
            <div class="container">
                <div class='row'>
                    <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('sports_played_athletes', '8') }}</h3></div>
                     </div>
                    <div class="col-md-12 padding-right"> 
                        <div id="jqChart"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('user_age_athletes', '8')  }}</h3></div>
                     </div>
                    <div class="col-md-12 padding-right">
                      
                        <div id="jqChart1"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('profile_complete_athletes', '8')}}</h3></div>
                     </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChart2"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('parent_vs_athlete', '8') }}</h3></div>
                     </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChart3"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('athletes_in_state', '8') }}</h3></div>
                     </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChart4"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('active_deactive_athletes', '8') }}</h3></div>
                     </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChart5"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-12 padding-right"> 
                     <div   class='title' ><h3>{{ Voyager::pagemeta('paid_nonpaid_users', '8') }}</h3></div>
                     </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChart6"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-8 padding-right"> 
                        <div   class='title' >
                            <h3>
                            {{ Voyager::pagemeta('emails_sent_athletes', '8') }}
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-4 padding-left"> 
                        <select id='user_select' name='user_select' class='user_select' >   
                        </select>
                    </div>
                    <div class="col-md-12 padding-right">
                   
                        <div id="jqChart7"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="col-md-8 padding-right"> 
                        <div   class='title' >
                            <h3>
                            {{ Voyager::pagemeta('how_singing_in_athletes', '8') }}
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-4 padding-left"> 
                        <select id='date_selected' name='date_selected' class='graph_select' >
                        </select>
                    </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChart8"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
                <div class='row user_signin_row'>
                    <div class="col-md-8 padding-right"> 
                        <div   class='title' >
                            <h3>
                            {{ Voyager::pagemeta('how_singing_in_single_athlete', '8') }}
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-2 padding-left"> 
                        <select id='user_select_single' name='user_select_single' class='user_select' >
                        </select>
                    </div>
                    <div class="col-md-2 padding-left"> 
                        <select id='date_select_single' name='date_select_single' class='date_select_single' >
                        </select>
                    </div>
                    <div class="col-md-12 padding-right">
                        <div id="jqChart9"  class='jqChart'  style="width: 100%; height: 450px;">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>    
@stop