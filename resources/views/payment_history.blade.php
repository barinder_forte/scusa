@extends('voyager::master')

<style type="text/css">
    ol.breadcrumb{
        display: none;
    }
</style>
@section('content')

<div class='clearfix'>&nbsp;</div>
<div class='clearfix'>&nbsp;</div>
<div class="container">
    <div class="page-content container-fluids">
        <div class='row'>
            <div class='col-md-12'>
                <h3 >
                   Payments History
                </h3>
            </div>
        </div> 
            <form name='paymentsSearch' id='paymentsSearch' action='/admin/payments/history' method='GET' >
            {{ csrf_field() }}
            
            <div>
                <div class="row">
                    <div class="col-md-4 ">
                    <label>Status</label>
                    <select name="status"  class="">
                         <option  value="">Select Status</option>
                            <option <?php if((isset($statusSelected)?$statusSelected:'') == "Active"){ echo 'selected';} ?> value="Active">Active</option>
                            <option <?php if((isset($statusSelected)?$statusSelected:'') == "Canceled"){ echo 'selected';} ?> value="Canceled">Canceled</option>
                            <option <?php if((isset($statusSelected)?$statusSelected:'') == "Expired"){ echo 'selected';} ?> value="Expired">Expired</option>
                            <option <?php if((isset($statusSelected)?$statusSelected:'') == "Past Due"){ echo 'selected';} ?> value="Past Due">Past Due</option>
                            <option <?php if((isset($statusSelected)?$statusSelected:'') == "Pending"){ echo 'selected';} ?> value="Pending">Pending</option>
                    </select>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-3">  
                        <input type="submit" class='btn btn-info' name="submit" value='Submit'>
                        
                    </div>
                    <div class="col-md-3">  
                        <a  class='btn btn-danger' href='/admin/payments/history' >Clear Search</a>
                    </div>
                </div>  
            </div>
        </form>
       
        <table class="payment-history new">
            <tr>
                <th>User Name</th>
                <th>Braintree ID</th>
                <th>Athlete Plan</th>
                <th>Card Brand</th>
                <th>Card Last Four Digits</th>
                <th>Created At</th>
                <th>Ends At</th>
                <th>Status</th>
                <th>Cost</th>
                <th>Actions</th>
            </tr>
            <?php 
           // echo "<pre>"; 
                       $getin = 0;

           // print_r(($collections));die('`here'); ?>
            @if( count($collections) > 0 )
            @foreach ($collections as $subscription)
            <?php 
            $getin = 1;
           // echo "<pre>"; // echo $subscription->id;
            $transaction = \Braintree_Subscription::find($subscription->id);
           // print_r($subscription);die('`here');
            ?>
            <tr>
            <?php $user = \App\User::where('email',$transaction->transactions[0]->customerDetails->email)->first(); 
                    // echo '<pre>';
                    // print_r($userInvoiceS);die;
                    $invoiceDownId = $transaction->transactions[0]->id;
                    
                     // print_r($user);die;
                    // foreach ($userInvoiceS as $key => $value) {
                    //     if($invoice->braintree_id == $value->subscriptionId){
                    //         $invoiceDownId = $value->id;
                    //     }
                    // } 
                    // if(($invoiceDownId) == ''){
                    //     $userinfo = \App\UserInformation::where('user_id',$invoice->user_id)->where('meta_key','subscription_id')->first();
                    //     if(($userinfo)){
                    //         $invoiceDownId = $userinfo->meta_value;
                    //     }
                    // }?>
                    
                    <td>{{ $transaction->transactions[0]->customerDetails->firstName .' '. $transaction->transactions[0]->customerDetails->lastName}}</td>
                    <td>{{ $transaction->transactions[0]->subscriptionId }}</td>
                    <td>{{ $transaction->transactions[0]->planId }}</td>
                    <td>{{ $transaction->transactions[0]->creditCard['cardType'] }}</td>
                    <td>{{ $transaction->transactions[0]->creditCard['last4'] }}</td>
                    <td>{{date_format($transaction->transactions[0]->createdAt , 'd M Y')  }}</td>
                    <td>@if(isset($transaction->transactions[0]->subscription['billingPeriodEndDate'])){{date_format($transaction->transactions[0]->subscription['billingPeriodEndDate'] , 'd M Y')  }}@endif</td>
                     <td>{{ ( ($transaction->status == 'Active') && ($transaction->neverExpires != 1) )?"Cancellation Processing":$transaction->status }}</td>
                    <td>{{'$'. $transaction->transactions[0]->amount }}</td>
                    <td>
                    @if($user && ( $user->braintree_id != '' ) )
                        <a href="/user/invoice/{{isset($invoiceDownId)? $invoiceDownId:''}}/{{$user->id}}">Download</a>
                    @endif
                    </td>
                <tr>
            @endforeach
            @endif
            @if($getin == 0)
            <tr>
                <td>
                    <div class="no_result_found_payment">
                    <h4> No Result Found</h4>
                    </div>
                </td>
            <tr>
            @endif
        </table> 
    </div>
</div>

@stop

