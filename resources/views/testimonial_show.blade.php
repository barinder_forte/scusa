@extends('layouts.signup')

@section('content')
@if(Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
 	<div class="main-container main-inner">
		<section class="white-bg">
			<div class="container">
				<!-- <div class='row'> -->
					@if($testimonial->image == '')
						<div class="col-md-12 col-sm-12 left-content padding-right">
					@else
						<div class="col-md-8 col-sm-12 left-content padding-right">
					@endif
						<h2 class="main-title">{{$testimonial->title}}</h2>
						<p>{!!$testimonial->body!!}</p>
					</div>
					@if($testimonial->image != '')
					<div class="col-md-4 col-sm-12 right-content">
						<div class="right-box">
							
							<div class="uploaded-images">
								<div class="col-md-12 padding-10 ">
									<img height='200px' width='200px' src="{{asset('storage/'.$testimonial->image)}}" title='' class="img-responsive" >
								</div>
							</div>
						</div>
					</div>
					@endif
				<!-- </div>	 -->
				<!-- <div class='row'> -->
					<div class="col-md-4">
						<a class="back_home" href="/">
						<i aria-hidden="true" class="fa fa-arrow-left"></i>
						Back to Home</a>
			    	</div>
			    <!-- </div> -->
			</div>
		</section>
		@if (\Auth::guest())
		<div class="main-banner inner-banner cta-banner" style="background-image: url( {{ asset( 'storage/'. Voyager::setting('cta_bg_image') ) }} )">
			<div class="container">
				{!! Voyager::setting('cta_content') !!}
			</div>
		</div>
		@endif
	</div>		
 @stop

