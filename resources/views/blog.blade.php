@extends('layouts.main')
@section('content')
	<div class="main-container main-inner">
		<section class="white-bg">
			<div class="container">
				<!-- <div class='row'> -->
					<div class="col-md-12 col-sm-12 not-content">
						@if(isset($post))
							@foreach($post as $posts)								
								<article id="post-2726" class="post-2726 post type-post status-publish format-standard hentry category-pet">

								@if($posts->image != '')
									<img src="/storage/{{$posts->image}}" class="imagepost">
								@endif

								<header class="entry-header">
									<h2 class="entry-title"><a href="/blog/{{ $posts->slug }}" rel="bookmark">{{ $posts->title}}</a></h2>		
									<div class="entry-meta">
									<span class="posted-on">Posted on 
									<?php $originalDate = $posts->created_at;
										echo $newDate = date("M d, Y", strtotime($originalDate));?> by {{isset($posts->getauthor)?$posts->getauthor->name :''}}</p>
									</span>
								</header>

								<div class="entry-content">
								@if($posts->body != '')
                            	<p>{{substr(strip_tags($posts->body), 0, 300) . ' ...'}} </p>
								@endif
								<a class="read-more" href="/blog/{{ $posts->slug }}">Read More</a> 
								</div>
								</article>
							@endforeach
						@endif	
					</div>
					{!! $post->appends(Request::except('page'))->render() !!}
				<!-- </div> -->
			</div>
		<section>
	</div>			
	
@stop

