@extends('layouts.update-card-layout')
@section('content')
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<div class="log-in-popup card-update-popup">
<div class="sign-up-content">
    <a  href="/payments/history/athlete" class="close"><i class="icono-cross"></i></a>
    <div class="panel-heading">
        <h3 class="panel-title display-td" >
           Update Payment Details
        </h3>
        <div class="display-td" >
            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                    <div class="row display-tr" >
                        
                    </div>                    
                </div>
                <?php //print_r($creditCardToken);die; ?>
                <div class="panel-body">
                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);" class="card-update-popup-form">
                    <input type="hidden" class='credit-card-token' value="{{isset($creditCardToken)?$creditCardToken:''}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="cardNumber">CARD NUMBER</label>
                                    <div class="input-group">
                                        <input style="width: 95%;"
                                            type="tel"
                                            class="form-control"
                                            name="cardNumber"
                                            placeholder="Valid Card Number"
                                            autocomplete="cc-number"
                                            required autofocus 
                                        />
                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                         <br />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="cardExpiry">EXPIRATION DATE</label>
                                    <input style="width: 95%;" 
                                        type="tel" 
                                        class="form-control" 
                                        name="cardExpiry"
                                        placeholder="MM / YY"
                                        autocomplete="cc-exp"
                                        required 
                                    />
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <button class="subscribe btn btn-success btn-lg btn-block orange" type="button">Update Card</button>
                            </div>
                        </div>
                        
                        <div class="row" style="display:none;">
                            <div class="col-md-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>           
    </div>
    </div>
    </div>
    </div>
@stop
