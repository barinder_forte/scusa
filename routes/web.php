<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'forceSsl'], function () {

Route::get('/exportToPdf/{slug}', 'UserController@exportToPdf');

Route::get('/verify/{token}', 'Auth\AuthController@verify');

Route::get('/signup', 'Auth\RegisterController@signup');

Route::get('/subscription', 'SubscriptionsController@index');
Route::get('/', 'HomeController@index');
Route::get('/email-templates', 'HomeController@index');
Route::post('order-post', ['as'=>'order-post','uses'=>'HomeController@orderPost']);
Route::post('/ajax_create','Auth\RegisterController@create_user');
Route::get('/blog/{slug}', 'PostsController@postShow');
Route::get('/testimonialshow/{id}', 'TestimonialController@show');
Route::get('/logout', 'Auth\AuthController@getSignOut');
Route::get('/contact', 'PageController@contact_show');
Route::post('/contact', 'PageController@contact_submit');
Route::get('/blog/', 'PostsController@blog');
Route::get('/spouser/position', 'sportsController@sportposition');
Route::post('/api/schoolname', 'ApiController@schoolresultsearch');
Route::get('/api/schoolnameevent', 'ApiController@schoolresultsearchevent');
Route::get('/api/schoolnameeventschool', 'ApiController@schoolresultsearcheventschool');


Route::get('/profile', 'UserController@profile');
Route::post('/profile', 'UserController@update_profile');
Route::get('/complete-registration', 'UserController@complete_registration');
Route::post('/sport/position', 'sportsController@sportposition');
Route::post('/profile/acadmic', 'UserController@profileacadmic');
Route::post('/profile/deleteacadmic', 'UserController@deleteacadmic');
Route::post('/complete-registration', 'UserController@save_profile');
    Route::post('/save-template', 'UserController@savetemplate');
 Route::get('/photo', 'PhotoController@index');
    Route::post('/photo/store', 'PhotoController@store');
    Route::post('/photo/delete/image', 'PhotoController@destroyimage');
    Route::post('/photo/gallerydelete', 'PhotoController@gallerydelete');
    Route::post('/photo/swap', 'PhotoController@swap');
    Route::post('/photo/storegallery', 'PhotoController@storegallery');
    Route::post('/photo/videostore', 'PhotoController@videostore');
    Route::post('/photo/videostore1', 'PhotoController@videostore1');
    Route::post('/photo/videodelete', 'PhotoController@videodelete');
    Route::post('/photo/imagedrag', 'PhotoController@dragdrop');
    Route::post('/sportfields', 'sportsController@get_fields');
    Route::post('/profile_reg', 'UserController@update_profile_register');
    Route::resource('/sportsmeta', 'sportsController' );
    Route::post('/contact-coaches', 'SportsEventController@contactcoaches');
    Route::post('/contact-profile', 'SportsEventController@sendEmail');
    Route::post('/sendemail-attendee', 'SportsEventController@sendEmailToAttendee');
    Route::post('/sendemail-allattendees', 'SportsEventController@sendEmailToAllAttendees');
    // Route::post('/sport/position', 'sportsController@sportposition');
    Route::post('resultsearch', 'ApiController@schollposition');
    Route::post('/subscription_status', 'SubscriptionsController@cancelled_hook');
    Route::get('/progress/data', 'UserController@progress');
    Auth::routes();
    Route::middleware(['UserProfileExpire'])->group(function () {

   

    // Route::post('/save-template', 'UserController@savetemplate');

    Route::get('/notifications', 'UserController@notifications');
    Route::get('/notification', 'UserController@notification');
    Route::get('/notification/seen', 'UserController@notificationSeen');
    Route::get('/notification/read/{slug}/{notiID}', 'UserController@notificationProfileShow');
    Route::get('/notification/delete/{notiID}', 'UserController@notificationDelete');
    Route::get('/payments/history/athlete', 'PlansController@paymentListathlete');
    
    Route::get('/college/{slug}/', 'ApiController@schoolresult');
    Route::get('/colleges', 'UserController@school_search');
    Route::get('/athletes', 'UserController@usersList');
    Route::get('/events', 'SportsEventController@eventlist');
    Route::get('/events/search', 'SportsEventController@eventSearch');
    Route::middleware(['AtheleteMiddleware'])->group(function () {
        Route::get('/school_search', 'UserController@school_search');
    });
    
    Route::middleware(['CoachMiddleware'])->group(function () {
       // Route::get('/users-list', 'UserController@usersList');
    });
  
});

Route::post(
    'braintree/webhook',
    '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook'
);


Route::group(['middleware' => 'auth'], function () {
    Route::post('/coacheshool', 'UserController@coachschool');
    Route::post('/profile/deleteacadmic', 'UserController@deleteacadmic');
    Route::post('/athletic_achievements', 'UserController@athletic_achievements');

    Route::post('/profile/deleteacoach', 'UserController@deleteacoach');
   
    Route::post('/subscription/cancel', 'SubscriptionsController@cancel');
    Route::get('/athletes/search', 'UserController@usersListSearch');
    Route::get('/event-import', 'SportsEventController@eventimport');
    Route::post('/event_import/import', 'SportsEventController@importstart');
    Route::post('/subscription/resume', 'SubscriptionsController@resume');

    Route::get('/profile/coach/{slug}', 'UserController@profileShow');
    Route::get('/profile/athlete/{slug}', 'UserController@profileShow');

    Route::get('/event/add', 'SportsEventController@addevent');
    Route::get('/event/{slug}', 'SportsEventController@show');
    Route::get('/add/event1', 'SportsEventController@addevent1');
    Route::get('/eventregister/{slug}', 'SportsEventController@eventRegister');
    Route::get('/event/{userType}/{slug}/', 'SportsEventController@eventShow');
    Route::get('/email/test', 'EmailController@index');
    Route::post('/add/event', 'SportsEventController@store');
    Route::post('/event/gallery/{id}', 'SportsEventController@galleryStore');
    Route::post('/event/search', 'SportsEventController@eventSearch');
    Route::post('/eventdelete', 'PhotoController@eventdelete');
    // Route::get('/plans', 'PlansController@index');
    Route::get('/plan/{plan}', 'PlansController@show');
    Route::get('/thankyou', 'SubscriptionsController@thanks');
    Route::get('/braintree/token', 'BraintreeTokenController@token');
    Route::post('/subscribe', 'SubscriptionsController@store');
    Route::get('user/invoice/{invoice}/{user_id}', 'SubscriptionsController@generate_invoice');
    
    Route::get('/change/password', 'Auth\ChangePasswordController@index');
    Route::post('/change/password', 'Auth\ChangePasswordController@change_password_save');

    Route::get('/delete/user_account', 'Auth\AuthController@deleteUserAccount');

    Route::get('/payment/updateCardDetails/{token}', 'PlansController@updateCardDetails');
    Route::get('updateCardDetailsSave', 'PlansController@updateCardDetailsSave');
});

Route::group(['prefix' => 'admin'], function () {
    \Voyager::routes();
});

Route::group(['prefix' => 'admin','middleware' => 'admin.user'], function () {
    Route::get('/approve/{user_id}', 'Auth\AuthController@approve');
    Route::resource('/pagemeta', 'PageMetaController' );
    Route::get('/sportsmeta/{id}/move_up', 'sportsController@move_up' );
    Route::get('/sportsmeta/{id}/move_down', 'sportsController@move_down' );
    Route::post('/field_store', 'sportsController@store' );
    Route::post('/page_field_store', 'PageMetaController@page_field_store' );
    Route::get('/sports/{sport}/edit/{field}', 'Voyager\VoyagerBreadController@edit_field');
    Route::get('/pages/{sport}/edit/{field}', 'Voyager\VoyagerBreadController@edit_field_pages');
    Route::get('/userProfile/Complete', 'ChartsController@userProfileComplete');
    Route::get('/userProfile/sportPlay', 'ChartsController@userSportPlayed');
    Route::get('/userProfile/userPerYearAge', 'ChartsController@userPerYearAge');
    Route::get('/userProfile/parentsVsAthlete', 'ChartsController@parentsVsAthlete');
    Route::get('/userProfile/athletesForState', 'ChartsController@athletesForState');
    Route::get('/userProfile/activeVsInactive', 'ChartsController@activeVsInactive');
    Route::get('/userProfile/paidVsNonpaid', 'ChartsController@paidVsNonpaid');
    Route::get('/userProfile/totalEmailsSentAthlete', 'ChartsController@totalEmailsSentAthlete');
    Route::get('/userProfile/userSingnInAthletes', 'ChartsController@userSingnInAthletes');
    Route::get('/userProfile/userSingnInSingleAth', 'ChartsController@userSingnInSingleAth');
    Route::get('/coachProfile/Complete', 'ChartsController@coachProfileComplete');
    Route::get('/coachProfile/sportPlay', 'ChartsController@coachSportPlayed');
    Route::get('/coachProfile/coachesForState', 'ChartsController@coachesForState');
    Route::get('/coachProfile/totalEmailsSentCoach', 'ChartsController@totalEmailsSentCoach');
    Route::get('/coachProfile/userSingnInCoaches', 'ChartsController@userSingnInCoaches');
    Route::get('/coachProfile/userSingnInSingleCoach', 'ChartsController@userSingnInSingleCoach');
    Route::get('/charts/athlete', 'ChartsController@athleteCharts');
    Route::get('/charts/coach', 'ChartsController@coachCharts');
    Route::get('/coach/import', 'UserController@coachimport');
    Route::post('/coach/import/save', 'UserController@coachimportsave');
    Route::get('/school/import', 'UserController@schoolimport');
    Route::post('/school/import/save', 'UserController@schoolimportsave');
    Route::get('/payments/history', 'PlansController@paymentList');
    Route::get('/userapprove', 'ApiController@userapprove');

    // Route::get('fetch/ApiData', 'ApiController@fetchApiData');
    // Route::get('fetch/ApiDataCopy/{page}/{apikey}', 'ApiController@fetchApiDataCopy');
    Route::get('/schoolslists', 'ApiController@school_search');
    Route::get('/user', 'ApiController@usersearch');
    Route::get('/sportevent', 'ApiController@sportevent');
    // Route::post('/save/apikey', 'ApiController@saveApiKay');


    Route::get('/export/athletes', 'UserController@exportAthletes');
    Route::get('/export/parents', 'UserController@exportParent');
    
    Route::group([
            'as'     => 'pagemeta.',
            'prefix' => 'pagemeta',
        ], function () {
        Route::get('{id}/delete_value', ['uses' => 'PageMetaController@delete_value', 'as' => 'delete_value']);
    });
    Route::group([
            'as'     => 'testimonial.',
            'prefix' => 'testimonial',
        ], function () {
        Route::get('{id}/delete_column/{column?}', ['uses' => 'TestimonialController@delete_column', 'as' => 'delete_column']);
    });
    Route::group([
            'as'     => 'page.',
            'prefix' => 'page',
        ], function () {
        Route::get('{id}/delete_column/{column?}', ['uses' => 'PageController@delete_column', 'as' => 'delete_column']);
    });
    Route::group([
            'as'     => 'sports.',
            'prefix' => 'sports',
        ], function () {
        Route::get('{id}/delete_column/{column?}', ['uses' => 'sportsController@delete_column', 'as' => 'delete_column']);
    });

    Route::get('/delete/banner/{page_id}', 'PageController@deletebanner');
    Route::get('/delete/eventbanner/{page_id}', 'SportsEventController@deletebanner');
  
});


Route::get('sendemail/', 'EmailController@sendemail');
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::any('{any}', 'PageController@index')->where('any', '.*');

});