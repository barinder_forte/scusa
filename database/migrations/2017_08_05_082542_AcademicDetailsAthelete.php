<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AcademicDetailsAthelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academicdetailathelete', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id')->nullable();
            $table->string('user_id');
            $table->string('location')->nullable();
            $table->string('city')->nullable();            
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('qualification')->nullable();
            $table->decimal('gpa',8, 1)->nullable();
            $table->integer('sats')->nullable();
            $table->integer('math')->nullable();
            $table->integer('act')->nullable();
            $table->integer('verbal')->nullable();
            $table->timestamps();
        }); 
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academicdetailathelete');
    }
}