<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SportEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('sport_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('author_id');
            $table->text('description');
            $table->string('event_start');
            $table->string('event_end');
            $table->string('place');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('division');
            $table->string('tournaments');
            $table->string('campus_clinics');
            $table->string('school_attending');
            $table->integer('sport_id');
            $table->text('image');
            $table->text('gallery_images')->nullable();
            $table->text('cta_bg_image')->nullable();
            $table->string('cta_content')->nullable();
            $table->text('banner_image')->nullable();
            $table->text('cta_show')->nullable();
            $table->string('event_slug')->nullable();
            $table->string('event_type')->nullable();
       
 
            
         //   $table->enum('event_status', ['PUBLISH', 'UNPUBLISH'])->default('PUBLISH');    
                        $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING'])->default('DRAFT');
//            $table->enum('status', ['UPCOMING', 'EXPIRED', 'CURRENT'])->default('UPCOMING');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('sport_events');
    }
}
