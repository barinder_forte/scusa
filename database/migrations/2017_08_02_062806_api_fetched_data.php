<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiFetchedData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_fetched_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->nullable();
            $table->string('school_name')->nullable();
            $table->string('location_lon')->nullable();
            $table->string('location_lat')->nullable();
            $table->integer('ope8_id')->nullable();
            $table->integer('ope6_id')->nullable();
            $table->string('main_campus')->nullable();
            $table->text('accreditor')->nullable();
            $table->string('city')->nullable();
            $table->string('branches')->nullable();
            $table->string('zip')->nullable();
            $table->string('school_url')->nullable();
            $table->string('state')->nullable();
            $table->string('operating')->nullable();
            $table->string('instructional_expenditure_per_fte')->nullable();
            $table->string('faculty_salary')->nullable();
            $table->string('tuition_revenue_per_fte')->nullable();
            $table->text('alias')->nullable();
            $table->integer('author_id')->nullable();
            $table->string('price_calculator_url')->nullable();
            $table->string('ft_faculty_rate')->nullable();
            $table->string('locale')->nullable();
            $table->string('state_fips')->nullable();
            $table->string('carnegie_undergrad')->nullable();
            $table->string('women_only')->nullable();
            $table->string('religious_affiliation')->nullable();
            $table->string('under_investigation')->nullable();
            $table->string('region_id')->nullable();
            $table->string('carnegie_basic')->nullable();
            $table->string('men_only')->nullable();
            $table->string('online_only')->nullable();
            $table->string('ownership')->nullable();
            $table->string('carnegie_size_setting')->nullable();
            $table->string('school_slug')->nullable();
            $table->string('tuition_in_state')->nullable();
            $table->string('tuition_out_of_state')->nullable();
  
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_fetched_data');
    }
}
