<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Coachsschool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('coachschool', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('school_id')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('div_code')->nullable();
            $table->string('division')->nullable();
            $table->string('sport_id')->nullable();
            $table->string('gender')->nullable();
            $table->string('head_coach')->nullable();
            $table->string('rank')->nullable();
            $table->string('conference')->nullable();
            $table->string('year')->nullable();
            $table->string('students_coached')->nullable();
            $table->string('school_tuition')->nullable();
            $table->string('tuition_room')->nullable();
            $table->string('school_size')->nullable();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('coachschool');
    
    }
}
